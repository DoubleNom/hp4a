package a.humanitas.hp4a;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

public class SocketRx extends Thread {

    private final int BUFFER_MAX_SIZE = 4096;

    private final int port;
    private Callback callback;

    private DatagramSocket socket = null;
    private byte[] buffer = new byte[BUFFER_MAX_SIZE];

    public SocketRx(int port, Callback callback) {
        super("UdpRx:" + Integer.toString(port));
        this.port = port;
        this.callback = callback;
        start();
    }

    @Override
    public void interrupt() {
        super.interrupt();
        if(socket != null && !socket.isClosed()) socket.close();
    }

    @Override
    public void run() {
        super.run();
        try {
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
            socket = new DatagramSocket(null);
            socket.setReuseAddress(true);
            socket.bind(new InetSocketAddress(port));
            while(!socket.isClosed()) {
                socket.receive(packet);
                String message = new String(buffer, 0, packet.getLength());
                callback.messageReceived(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(socket != null && !socket.isClosed()) {
                socket.close();
            }
        }
    }

    public interface Callback {
        void messageReceived(String message);
    }
}
