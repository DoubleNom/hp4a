package a.humanitas.hp4a;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelUuid;
import android.util.Log;
import android.webkit.PermissionRequest;

import java.io.File;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.UUID;

import static a.humanitas.hp4a.Config.TAG;

public class Tools {
    private final static String UUID_BASE = "00000000-0000-1000-8000-00805f9b34fb";

    public static ParcelUuid makeParcelUUID(String uuid) {
        return ParcelUuid.fromString(
                UUID.nameUUIDFromBytes(uuid.getBytes()).toString()
        );
    }

    private static UUID makeUUID(String uuid) {
        return UUID.nameUUIDFromBytes(uuid.getBytes());
    }

    public static ParcelUuid makeParcel16UUID(String uuid) {
        String UUID_16 = makeUUID(uuid).toString().substring(4,8);
        return ParcelUuid.fromString(UUID_BASE.replace("00000000", "0000" + UUID_16));
    }

    // TODO : remove
    public static String fromBytes(byte[] data) {
        return new String(data, Charset.forName("UTF-8"));
    }

    private static String uuidToHeavenAddress(String uuid) {
        char[] c = new char[4];
        for(int i = 0; i < 4; i++) {
            BigInteger value = new BigInteger(uuid.substring(i * 8, i * 8 + 8), 16);
            value = value.mod(BigInteger.valueOf(26));
            c[i] = (char) (value.intValue() + 'a');
        }

        return String.format("%c%c%c%c", c[0], c[1], c[2], c[3]);
    }

    @SuppressLint("ApplySharedPref")
    static String getHeavenAddress(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Config.KEY_HEAVEN_CONFIG, Context.MODE_PRIVATE);

        String heavenAddress = sharedPreferences.getString(Config.KEY_HEAVEN_ADDRESS, "");
        if(heavenAddress.isEmpty()) {
            heavenAddress = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
            heavenAddress = uuidToHeavenAddress(heavenAddress);
            sharedPreferences.edit().putString(Config.KEY_HEAVEN_ADDRESS, heavenAddress).commit();
        }
        return heavenAddress;
    }

    /**
     * @param context
     * @param heavenAddress
     * @return true if heaven address changed, false otherwise
     */
    static boolean setHeavenAddress(Context context, String heavenAddress) {
        SharedPreferences sharedPreferences =
                context.getSharedPreferences(Config.KEY_HEAVEN_CONFIG, Context.MODE_PRIVATE);

        String currentAddress = sharedPreferences.getString(Config.KEY_HEAVEN_ADDRESS, "");

        if (!currentAddress.equals(heavenAddress)) {
            Log.d(TAG, "setHeavenAddress: " + heavenAddress);
            sharedPreferences.edit().putString(Config.KEY_HEAVEN_ADDRESS, heavenAddress).apply();
            return true;
        } else {
            Log.d(TAG, "setHeavenAddress: " + heavenAddress + " == " + currentAddress);
            return false;
        }
    }

    public static String getRandomString(int length) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < length; i++) {
            stringBuilder.append((char)(new Random().nextInt(26) + 'a'));
        }
        return stringBuilder.toString();
    }

    public static String getRandomSizedString(int length, int min) {
        length = new Random().nextInt(length - min) + min;
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < length; i++) {
            char c = (char)(new Random().nextInt(27) + 'a');
            if(c == '{') c = ' ';
            stringBuilder.append(c);
        }
        return stringBuilder.toString();
    }

    public static String getSaveFolder(Context applicationContext) {
        File folder = new File(Environment.getExternalStorageDirectory(), Config.SAVE_DIRECTORY);
        if (!folder.exists()) folder.mkdirs();
        return folder.getAbsolutePath();
    }
}
