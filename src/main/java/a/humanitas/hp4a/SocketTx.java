package a.humanitas.hp4a;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class SocketTx extends HandlerThread {
    private final String TAG;
    private final int port;
    private Handler handler;

    public SocketTx(String TAG, int port) {
        super("UdpTx");
        this.TAG = TAG;
        this.port = port;
        this.start();
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        handler = new Handler(Looper.myLooper());
    }

    public void send(final String message) {
        if (handler == null) {
            Log.d(TAG, "send: Handler is null, abort");
            return;
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                DatagramSocket socket = null;
                try {
                    socket = new DatagramSocket();
                    socket.setReuseAddress(true);
                    socket.setBroadcast(false);

                    InetAddress address = InetAddress.getByName("127.0.0.1");

                    byte[] buffer = message.getBytes();
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, port);
                    socket.send(packet);
//                    Log.d(TAG, "sent request: " + message + " to: " + port);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.d(TAG, "send: exception when sending");
                } finally {
                    if(socket != null && !socket.isClosed()) {
                        socket.close();
                    }
                }
            }
        });
    }
}
