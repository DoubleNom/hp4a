package a.humanitas.hp4a;

import android.util.Log;

import java.util.ArrayList;

import static a.humanitas.hp4a.Config.TAG;

public class TcpClientsSocket {

    private StoredMessages sendMessages;

    public interface Callback {
        String send(String target, int port, String message);
        String sendFile(String target, int port, String filePath);
        void sent(String address, int port, String message);
        void sentFile(String address, int port, String filePath);
    }
    private TcpClientsSocket.Callback callback;

    TcpClientsSocket(TcpClientsSocket.Callback callback) {
        this.callback = callback;
        sendMessages = new StoredMessages();
    }

    void send(String target, int port, String message) {
            StoredMessage msg = new StoredMessage(target, message);
            msg.port = port;
            msg.messageKind = StoredMessage.MESSAGE;

            sendMessages.add(msg);
            msg.requestId = callback.send(target, port, message);
    }

    void sendFile(String target, int port, String filePath) {
        StoredMessage msg = new StoredMessage(target, filePath);
        msg.messageKind = StoredMessage.FILE;
        sendMessages.add(msg);
        msg.requestId = callback.sendFile(target, port, filePath);
    }

    void replySendTcpSocket(String requestId, String sessionId, boolean accepted) {
        StoredMessage message = sendMessages.getByRequestId(requestId);
        if (message == null) {
            Log.d(TAG, "replaySendTcpClientSocket: message unknown");
            return;
        }

        if (!accepted) {
            Log.d(TAG, "replaySendTcpClientSocket: " + sessionId + " failure");
            sendMessages.remove(message);
        } else {
            message.clientId = sessionId;
        }
    }

    void replyTcpSocketMessageSent(String requestId, String sessionId, boolean sent,
                                   int fileSize, long elapsedTime, int bps) {
        StoredMessage message = sendMessages.getByRequestId(requestId);

        if (message == null) {
            Log.d(TAG, "replySendUdp: message unknown");
            return;
        }

        if (!sent) {
            Log.d(TAG, "replySendUdp: " + sessionId + " failure");
        } else {
            if (StoredMessage.MESSAGE == message.messageKind) {
                callback.sent(message.address, message.port, message.message);
            } else {
                callback.sentFile(message.address, message.port, message.message);
            }
        }

        sendMessages.remove(message);
    }

    void replyTcpSocketError(String requestId, String serverAddress, int serverPort,
                             String sessionId, int error) {
        StoredMessage message = sendMessages.getByRequestId(requestId);

        if (message == null) {
            Log.d(TAG, "replySendUdp: message unknown");
            return;
        }

        Log.d(TAG, "replySendUdp: " + sessionId + " failure");
        sendMessages.remove(message);
    }
}
