package a.humanitas.hp4a;

import android.content.Context;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Base64OutputStream;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

import static a.humanitas.hp4a.Config.TAG;

public class FileExchanger {

    private static final int BUFFER_SIZE = 1024;

    /**
     * Encode a file before sending it over heaven
     * @param context
     * @param path file we want to encode
     * @return path of the encoded file, it should be removed after sending
     */
    static public String EncodeFile(Context context, String path) {
        try {
            final File inTmp = new File(path);
            int bufferLength;
            byte[] buffer = new byte[BUFFER_SIZE];
            final InputStream inStream = new FileInputStream(path);

            final String outName = inTmp.getName() + ".tmp";
            final File outFile = new File(Tools.getSaveFolder(context), outName);
            final OutputStream outStream = new FileOutputStream(outFile, false);
            final Base64OutputStream outB64 = new Base64OutputStream(outStream, Base64.DEFAULT);

            outB64.write(inTmp.getName().getBytes());

            outB64.write(0);

            while ((bufferLength = inStream.read(buffer)) != -1) {
                outB64.write(buffer, 0, bufferLength);
            }

            inStream.close();
            outB64.close();

            return outFile.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Decode a file after receiving it from heaven
     * @param context
     * @param path path of the file we want to decode, it will be deleted after process
     * @return path of the decoded file
     */
    static public String DecodeFile(Context context, String path) {
        try {
            final File inFile = new File(path);
            int bufferLength;
            byte[] buffer = new byte[BUFFER_SIZE];
            final InputStream inStream = new FileInputStream(path);
            final Base64InputStream inB64 = new Base64InputStream(inStream, Base64.DEFAULT);

            // Extract filename by reading till we reach null char
            StringBuilder filename = new StringBuilder();
            int offset = 0;
            while((bufferLength = inB64.read(buffer)) != -1) {
                boolean over = false;
                offset = 0; // if we open a new buffer, we need to reset offset
                for(Byte b: buffer) {
                    ++offset; // Increase buffer all the time because we want to skip null char
                    if (b != 0) {
                        filename.append((char)b.intValue());
                    } else {
                        over = true;
                        break;
                    }
                }
                if (over) break;
            }

            // Open file now that we know its name
            final File outFile = new File(Tools.getSaveFolder(context), filename.toString());
            final OutputStream outStream = new FileOutputStream(outFile);
            final BufferedOutputStream outBStream = new BufferedOutputStream(outStream);

            // Write remaining data read from previous buffer
            outBStream.write(buffer, offset, bufferLength - offset);
            while ((bufferLength = inB64.read(buffer)) != -1) {
                outBStream.write(buffer, 0, bufferLength);
            }

            inB64.close();
            outBStream.close();
            if (!inFile.delete()) {
                Log.d(TAG, "DecodeFile: temporary file was not deleted: " + inFile.getName());
            }

            return outFile.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
