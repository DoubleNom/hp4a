package a.humanitas.hp4a;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ConfigFileEditor {
    private final List<String> heavenConfig;
    private final String filepath;

    public ConfigFileEditor(String path) {
        heavenConfig = new ArrayList<>();
        filepath = path;
    }

    public void addParameter(String parameter, String value) {
        if(parameter == null) return;
        if(heavenConfig.contains(parameter)) return;
        heavenConfig.add(parameter);
        if(value == null) {
            heavenConfig.add("");
        } else {
            heavenConfig.add(value);
        }
    }

    private static void editHeavenConfig(String filepath, String[] params) {
        List<String> lines = new ArrayList<>();
        String line;
        int i = 0;

        try {
            File file = new File(filepath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                if(i < params.length && line.equals(params[i])) {
                    i++;
                    line += params[i++];
                }
                lines.add(line);
            }
            br.close();
            fr.close();

            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            for (String s : lines) {
                bw.write(s + '\n');
            }
            bw.flush();
            bw.close();
            fw.close();

        } catch (IOException error) {
            Log.d(TAG, "failed to edit heaven configuration. Reason: " + error);
        }
    }

    public void applyConfig() {
        String[] configuration = new String[heavenConfig.size()];
        for(int i = 0; i < heavenConfig.size(); i += 2) {
            configuration[i] = heavenConfig.get(i) + "=";
            configuration[i+1] = heavenConfig.get(i+1);
        }
        editHeavenConfig(filepath, configuration);
    }
}
