package a.humanitas.hp4a;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;

import static a.humanitas.hp4a.PythonBridge.TAG;

public class PythonManager extends HandlerThread {
    private final Context context;
    private String rootFolder = Config.PYTHON_ROOT_FOLDER;
    private String pythonFolder = Config.PYTHON_FOLDER;
    private String scriptname;
    private boolean running = false;
    private Handler handler;

    PythonManager(String name, String scriptname, Context context) {
        super(name);
        this.context = context;
        rootFolder = context.getFilesDir() + "/" + rootFolder;
        pythonFolder = rootFolder + "/" + pythonFolder;
        this.scriptname = pythonFolder + "/" + scriptname;

        start();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    protected void onLooperPrepared() {
        super.onLooperPrepared();
        handler = new Handler(Looper.myLooper());
    }

    void startPython(){
        while(handler == null) {
            Log.d(TAG, "startPython: Handler is still null");
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                init();
            }
        });
        handler.post(new Runnable() {
            @Override
            public void run() {
                running = true;
                PythonWrapper.run(pythonFolder, scriptname, TAG);
                running = false;
                if (callback != null) {
                    callback.scriptEnded();
                }
            }
        });
    }

    public boolean isRunning() {
        return running;
    }

    public void init() {
        new AssetExtractor(context, rootFolder).run();
    }

    private Callback callback;


    public interface Callback{
        void scriptEnded();
    }
}
