package a.humanitas.hp4a;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import static a.humanitas.hp4a.Config.TAG;

public class PythonService extends Service {

    public static final String CHANNEL_ID = "a.humanitas.hp4a";
    public static final String ACTION_START_HEAVEN = "a.humanitas.hp4a.action.start_heaven";
    public static final String ACTION_STOP_HEAVEN = "a.humanitas.hp4a.action.stop_heaven";
    public static final String ACTION_KILL_PROCESS = "a.humanitas.hp4a.action.kill_process";

    private PythonManager pythonManager;

    private WifiManager wifiManager;
    private WifiManager.WifiLock wifiLock;
    private static final int WIFI_LOCK_PERIOD = 30000;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Python service created");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(pythonManager != null && pythonManager.isRunning()) pythonManager.interrupt();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (intent == null) return START_NOT_STICKY;

        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "onStartCommand: no action");
            return START_NOT_STICKY;
        }

        if (ACTION_START_HEAVEN.equals(action)) {
            Log.d(TAG, "onStartCommand: start python");
            if(startHeaven()) return START_STICKY;
            else return START_NOT_STICKY;
        }

        else if (ACTION_STOP_HEAVEN.equals(action)) {
            Log.d(TAG, "onStartCommand: stop python");
            stopHeaven();
        }

        else if (ACTION_KILL_PROCESS.equals(action)) {
            Log.d(TAG, "onStartCommand: kill process");
            killProcess();
        }

        return START_NOT_STICKY;

    }

    private boolean startHeaven() {
        if(pythonManager != null && pythonManager.isRunning()) return false;

        startForeground(10, buildForegroundNotification());

        wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);

        if (wifiManager != null) wifiLock = wifiManager.createWifiLock("heaven_wifi_lock");

        pythonManager = new PythonManager("Heaven Messenger Python",
                Config.PYTHON_SCRIPT, getApplicationContext());

        pythonManager.setCallback(new PythonManager.Callback() {
            @Override
            public void scriptEnded() {
                if(wifiLock.isHeld()) {
                    runWifiLock = null;
                    Log.d(TAG, "Releasing Wifi");
                    wifiLock.release();
                }
                stopForeground(true);
                stopSelf();
            }
        });

        pythonManager.init();

        ConfigFileEditor configFileEditor = new ConfigFileEditor(
                getFilesDir().getAbsolutePath() + "/" +
                        Config.PYTHON_ROOT_FOLDER + "/" +
                        Config.PYTHON_FOLDER +
                        "/heaven_config.ini");

        configFileEditor.addParameter("heaven_address",
                Tools.getHeavenAddress(getApplicationContext()));

        configFileEditor.addParameter("files_path",
                Tools.getSaveFolder(getApplicationContext()));

        configFileEditor.applyConfig();

        pythonManager.startPython();

        new Handler(getMainLooper()).postDelayed(runWifiLock, WIFI_LOCK_PERIOD);

        return true;
    }

    private void stopHeaven() {
        Client.stopHeaven();
    }

    private void killProcess() {
        if(wifiLock.isHeld()) {
            runWifiLock = null;
            Log.d(TAG, "Releasing Wifi");
            wifiLock.release();
        }
        stopForeground(true);
        stopSelf();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private Notification buildForegroundNotification() {
        Intent stopHeavenIntent = new Intent(PythonService.ACTION_STOP_HEAVEN, null,
                getApplicationContext(), PythonService.class);
        PendingIntent stopHeavenPendingIntent =
                PendingIntent.getService(this, 0, stopHeavenIntent, 0);

        Intent killProcessIntent = new Intent(PythonService.ACTION_KILL_PROCESS, null,
                getApplicationContext(), PythonService.class);
        PendingIntent killProcessPendingIntent =
                PendingIntent.getService(this, 0, killProcessIntent, 0);

        Notification.Builder builder =
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
                ? new Notification.Builder(this, CHANNEL_ID)
                : new Notification.Builder(this);

        builder = builder
                .setContentTitle(getString(R.string.service_name))
                .setContentText(getString(R.string.service_content))
                .setSmallIcon(R.drawable.ic_cloud_black_24dp)
                .setOngoing(true);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            builder.addAction(new Notification.Action(
                        R.drawable.ic_pan_tool_black_24dp,
                        getString(R.string.stop_heaven),
                        stopHeavenPendingIntent))
                    .addAction(new Notification.Action(
                            R.drawable.ic_sentiment_very_dissatisfied_black_24dp,
                            "Kill Process",
                            killProcessPendingIntent));
        } else {
            builder.addAction(
                    R.drawable.ic_pan_tool_black_24dp,
                    getString(R.string.stop_heaven),
                    stopHeavenPendingIntent)
                    .addAction(
                    R.drawable.ic_sentiment_very_dissatisfied_black_24dp,
                    "Kill Process",
                    killProcessPendingIntent);
        }

        // Create notification Channel for android 26+
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            builder.setChannelId(CHANNEL_ID);
        }

        return builder.build();
    }

    private Runnable runWifiLock = new Runnable() {
        @Override
        public void run() {
            if(pythonManager != null
                    && pythonManager.isRunning()
                    && pythonManager.isAlive()
                    && !wifiLock.isHeld()) {
                Log.d(TAG, "Locking Wifi");
                wifiLock.acquire();

            }
            new Handler(getMainLooper()).postDelayed(this, WIFI_LOCK_PERIOD);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
