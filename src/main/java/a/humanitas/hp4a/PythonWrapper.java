package a.humanitas.hp4a;

class PythonWrapper {

    static {
        System.loadLibrary("python2.7");
        System.loadLibrary("pyjni");
    }

    public static native void run(String pythonpath, String scriptpath, String TAG);
}
