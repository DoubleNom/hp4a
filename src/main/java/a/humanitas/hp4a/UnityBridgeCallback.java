package a.humanitas.hp4a;

public interface UnityBridgeCallback {

    //* HEAVEN
    void replyGetHeavenAddress(String requestId, String address, String errorDescription);


    //* NETWORK
    void replyGetPeers(String requestId, String peers, String errorDescription);
    void replyGetPeersUpdate(String requestId, boolean subscription, String subscriptionId,
                             String errorDescription);
    void replyStopPeersUpdate(String requestId, boolean unsubscription, String subscriptionId,
                              String errorDescription);
    void replyGetNeighbors(String requestId, String neighbors, String errorDescription);
    void replyGetLsp(String requestId, String lsp, String errorDescription);
    void replyGetRoutingTable(String requestId, String routingTable, String errorDescription);
    void replyGetRoutingTrees(String requestId, String routingTrees, String errorDescription);
    void replyGetFullNetwork(String requestId, String fullNetwork, String errorDescription);
    void replyBlockNeighbor(String requestId, String address, boolean blocked,
                            String errorDescription);
    void replyUnblockNeighbor(String requestId, String address, boolean unblocked,
                              String errorDescription);
    void replyGetPeerIp(String requestId, String address, String ip, String errorDescription);
    void replyGetMyIpForPeer(String requestId, String address, String ip, String errorDescription);


    //* TCP SERVER
    void replyCreateTcpServer(String requestId, int port, boolean created, String errorDescription);
    void replyTcpServerReceivedMessage(String requestId, int port, String source, String sessionId,
                                       String message, int messageLength, long elapsedTime, int bps,
                                       String errorDescription);
    void replyTcpServerReceivedFile(String requestId, int port, String source, String sessionId,
                                    String filePath, int fileSize, long elapsedTime, int bps,
                                    String errorDescription);
    void replyStopTcpServer(String requestId, int port, boolean stopped, String errorDescription);
    void replyGetTcpServers(String requestId, String servers, String errorDescription);


    //* TCP SERVER SOCKET
    void replyCreateTcpServerSocket(String requestId, int port, boolean created,
                                    String errorDescription);
    void replyTcpServerSocketReceivedMessage(String requestId, int port, String source,
                                             String sessionId,String message, int messageLength,
                                             long elapsedTime, int bps, String errorDescription);
    void replyTcpServerSocketReceivedFile(String requestId, int port, String source,
                                          String sessionId, String filePath, int fileSize,
                                          long elapsedTime, int bps, String errorDescription);
    void replyStopTcpServerSocket(String requestId, int port, boolean stopped,
                                  String errorDescription);
    void replyGetTcpServersSocket(String requestId, String servers, String errorDescription);


    //* TCP CLIENT
    void replyCreateTcpClient(String requestId, String serverAddress, int port, String sessionId,
                              boolean accepted, String errorDescription);
    void replyTcpSessionEstablished(String requestId, String serverAddress, int port,
                                    String sessionId, boolean accepted, String errorDescription);
    void replySendTcp(String requestId, String sessionId, String messageId, boolean accepted,
                      String errorDescription);
    void replySendTcpFile(String requestId, String sessionId, String messageId, boolean accepted,
                          String errorDescription);
    void replyTcpSessionAbort(String requestId, String serverAddress, int serverPort,
                              String sessionId, int error, String errorDescription);

    void replyTcpMessageSent(String requestId, String sessionId, String messageId, boolean sent,
                             int fileSize, long elapsedTime, int bps, String errorDescription);

    void replyStopTcpClient(String requestId, String sessionId, boolean stopped,
                            String errorDescription);

    void replyGetTcpClients(String requestId, String clients, String errorDescription);


    //* TCP CLIENT SOCKET
    void replySendTcpSocket(String requestId, String sessionId, boolean accepted,
                            String errorDescription);

    void replyTcpSocketMessageSent(String requestId, String sessionId, boolean sent, int fileSize,
                                   long elapsedTime, int bps, String errorDescription);

    void replyTcpSocketError(String requestId, String serverAddress, int serverPort,
                             String sessionId, int error, String errorDescription);


    //* UDP SERVER
    void replyCreateUdpServer(String requestId, int port, boolean created, String errorDescription);

    void replyUdpServerReceivedMessage(String requestId, int port, String source, int sourcePort,
                                       String message, String errorDescription);

    void replyStopUdpServer(String requestId, int port, boolean stopped, String errorDescription);

    void replyGetUdpServers(String requestId, String servers, String errorDescription);


    //* UDP SERVER SOCKET
    void replyCreateUdpServerSocket(String requestId, int port, boolean created,
                                    String errorDescription);

    void replyUdpServerSocketReceivedMessage(String requestId, int port, String source,
                                             int sourcePort, String message,
                                             String errorDescription);

    void replyStopUdpServerSocket(String requestId, int port, boolean stopped,
                                  String errorDescription);

    void replyGetUdpServersSocket(String requestId, String servers, String errorDescription);


    //* UDP CLIENT
    void replyCreateUdpClient(String requestId, String serverAddress, int serverPort,
                              String sourcePort, boolean created, String errorDescription);

    void replySendUdp(String requestId, String sourcePort, boolean sent, String errorDescription);

    void replyStopUdpClient(String requestId, String sourcePort, boolean stopped,
                            String errorDescription);

    void replyGetUdpClients(String requestId, String clients, String errorDescription);

    //* UDP CLIENT SOCKET
    void replyCreateUdpClientSocket(String requestId, String serverAddress, int serverPort,
                                    String sourcePort, boolean created, String errorDescription);

    void replySendUdpSocket(String requestId, String sourcePort, boolean sent,
                            String errorDescription);

    void replyStopUdpClientSocket(String requestId, String sourcePort, boolean stopped,
                                  String errorDescription);

    void replyGetUdpClientsSocket(String requestId, String clients, String errorDescription);


    //* EASY
    void messageReceived(String sender, int port, String message);
    void fileReceived(String sender, int port, String filepath);
    void messageSent(String target, int port, String message);
    void fileSent(String target, int port, String filepath);
}
