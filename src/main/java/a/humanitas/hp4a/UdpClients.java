package a.humanitas.hp4a;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static a.humanitas.hp4a.Config.TAG;

class UdpClients {

    private ClientsData clients;
    private StoredMessages storedMessages;
    private StoredMessages sendMessages;

    interface Callback {
        void createClient(String target, int port);
        String send(String sourcePort, String message);
        void sent(String address, int port, String message);
    }
    private Callback callback;

    UdpClients(Callback callback) {
        this.callback = callback;
        clients = new ClientsData();
        storedMessages = new StoredMessages();
        sendMessages = new StoredMessages();
    }

    void send(String target, int port, String message) {
        if (target == null) target = Config.HEAVEN_BROADCAST_ADDRESS;
        String sourcePort = clients.getId(target, port);

        StoredMessage storedMessage = new StoredMessage(target, message);
        storedMessage.port = port;
        storedMessage.clientId = sourcePort;


        if (sourcePort == null) {
            requestClientCreation(target, port);
            storedMessages.add(storedMessage);
        }
        else if (sourcePort.equals("")) {
            storedMessages.add(storedMessage);
        }
        else {
            storedMessage.requestId = callback.send(sourcePort, message);
            sendMessages.add(storedMessage);
        }
    }

    private void requestClientCreation(String target, int port) {
        if (!clients.add(target, port)) return;
        callback.createClient(target, port);
        removePreviousMessages(target, port);
    }


    private void removePreviousMessages(String address, int port) {
        storedMessages.removeFrom(address, port);
        sendMessages.removeFrom(address, port);
    }

    void replyCreateUdpClient(String requestId, String serverAddress, int serverPort, String sourcePort, boolean created) {
        if (!created) {
            Log.d(TAG, "replyCreateUdpClient: " + serverAddress + " failure");
            clients.remove(serverAddress, serverPort);
        } else {
            clients.setId(serverAddress, serverPort, sourcePort);
            for (Iterator<StoredMessage> i = storedMessages.iterator(); i.hasNext();) {
                StoredMessage message = i.next();
                if (serverAddress.equals(message.address)) {
                    send(message.address, serverPort, message.message);
                    i.remove();
                }
            }
        }
    }

    void replySendUdp(String requestId, String sourcePort, boolean sent) {
        StoredMessage message = sendMessages.getByRequestId(requestId);

        if (message == null) {
            Log.d(TAG, "replySendUdp: message unknown");
            return;
        }

        if (!sent) {
            sendMessages.remove(message);
        } else {
            callback.sent(message.address, message.port, message.message);
        }

        sendMessages.remove(message);
    }

    void replyStopUdpClient(String requestId, String sourcePort, boolean stopped) {
        if (stopped) {
            clients.removeFromId(sourcePort);
        }
    }
}
