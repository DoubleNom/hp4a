package a.humanitas.hp4a;

public interface EasyHeavenReplies extends HeavenReplies {
    void messageReception(String sender, int port, String message);
    void fileReception(String sender, int port, String filepath);
    void messageSent(String target, int port, String message);
    void fileSent(String target, int port, String message);
}
