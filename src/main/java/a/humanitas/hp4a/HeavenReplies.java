package a.humanitas.hp4a;

import java.util.ArrayList;

public interface HeavenReplies {

   String KEY_HEAVEN_ADDRESS = "heaven_address";
   String KEY_PEERS = "peers";
   String KEY_PEERS_SUBSCRIPTION = "subscription";
   String KEY_PEERS_UNSUBSCRIPTION = "unsubscription";
   String KEY_PEERS_SUBSCRIPTION_ID = "subscription_id";
   String KEY_NEIGHBORS = "neighbors";
   String KEY_IP_ADDRESS = "ip";
   String KEY_LSP = "lsp";
   String KEY_ROUTING_TABLE = "routing_table";
   String KEY_PORT = "port";
   String KEY_CREATED = "created";
   String KEY_ERROR = "error";
   String KEY_STOPPED = "stopped";
   String KEY_SERVER_ADDRESS = "server_address";
   String KEY_SESSION_ID = "session_id";
   String KEY_SOURCE_PORT = "source_port";
   String KEY_REQUEST_ACCEPTED = "request_accepted";
   String KEY_MESSAGE_ID = "message_id";
   String KEY_MESSAGE_SENT = "sent";
   String KEY_SOURCE = "source";
   String KEY_MESSAGE = "message";
   String KEY_FILE_PATH = "file_path";
   String KEY_TCP_SERVERS = "tcp_servers";
   String KEY_TCP_CLIENTS = "tcp_clients";
   String KEY_UDP_SERVERS = "udp_servers";
   String KEY_UDP_CLIENTS = "udp_clients";
   String KEY_FILE_SIZE = "file_size";
   String KEY_FULL_NETWORK = "full_network";
   String KEY_ROUTING_TREES = "routing_trees";
   String KEY_ELAPSED_TIME = "elapsed_time";
   String KEY_BPS = "bps";
   String KEY_ERROR_DESCRIPTION = "error_description";

   String KEY_BLOCKED_NEIGHBOR = "blocked_neighbor";
   String KEY_UNBLOCKED_NEIGHBOR = "unblocked_neighbor";

   String KEY_REQUEST_ID = "request_id";
   String KEY_COMMAND = "command";
   String KEY_DELETE_REQUEST = "delete_request";


    //* HEAVEN
    void replyGetHeavenAddress(
            String requestId,
            String address,
            String errorDescription);


    //* NETWORK
    void replyGetPeers(
            String requestId,
            ArrayList<String> peers,
            String errorDescription);

    void replyGetPeersUpdate(
            String requestId,
            boolean subscription,
            String subscriptionId,
            String errorDescription);

    void replyStopPeersUpdate(
            String requestId,
            boolean unsubscription,
            String subscriptionId,
            String errorDescription);

    void replyGetNeighbors(
            String requestId,
            ArrayList<String> neighbors,
            String errorDescription);

    void replyGetLsp(
            String requestId,
            String lsp,
            String errorDescription);

    void replyGetRoutingTable(
            String requestId,
            String routingTable,
            String errorDescription);

    void replyGetRoutingTrees(
            String requestId,
            String routingTrees,
            String errorDescription);

    void replyGetFullNetwork(
            String requestId,
            String fullNetwork,
            String errorDescription);

    void replyBlockNeighbor(
            String requestId,
            String neighborAddress,
            boolean blocked,
            String errorDescription);

    void replyUnblockNeighbor(
            String requestId,
            String neighborAddress,
            boolean unblocked,
            String errorDescription);

    void replyGetPeerIp(
            String requestId,
            String peerAddress,
            String peerIp,
            String errorDescription);

    void replyGetMyIpForPeer(
            String requestId,
            String peerAddress,
            String myIp,
            String errorDescription);


    //* TCP SERVER
    void replyCreateTcpServer(
            String requestId,
            int port,
            boolean created,
            String errorDescription);

    void replyTcpServerReceivedMessage(
            String requestId,
            int port,
            String source,
            String sessionId,
            String message,
            int messageLength,
            long elapsedTime,
            int bps,
            String errorDescription);

    void replyTcpServerReceivedFile(
            String requestId,
            int port,
            String source,
            String sessionId,
            String filePath,
            int fileSize,
            long elapsedTime,
            int bps,
            String errorDescription);

    void replyStopTcpServer(
            String requestId,
            int port,
            boolean stopped,
            String errorDescription);

    void replyGetTcpServers(
            String requestId,
            ArrayList<String> servers,
            String errorDescription);


    //* TCP SERVER SOCKET
    void replyCreateTcpServerSocket(
            String requestId,
            int port,
            boolean created,
            String errorDescription);

    void replyTcpServerSocketReceivedMessage(
            String requestId,
            int port,
            String source,
            String sessionId,
            String message,
            int messageLength,
            long elapsedTime,
            int bps,
            String errorDescription);

    void replyTcpServerSocketReceivedFile(
            String requestId,
            int port,
            String source,
            String sessionId,
            String filePath,
            int fileSize,
            long elapsedTime,
            int bps,
            String errorDescription);

    void replyStopTcpServerSocket(
            String requestId,
            int port,
            boolean stopped,
            String errorDescription);

    void replyGetTcpServersSocket(
            String requestId,
            ArrayList<String> servers,
            String errorDescription);


    //* TCP CLIENT
    void replyCreateTcpClient(
            String requestId,
            String serverAddress,
            int port,
            String sessionId,
            boolean accepted,
            String errorDescription);

    void replyTcpSessionEstablished(
            String requestId,
            String serverAddress,
            int port,
            String sessionId,
            boolean created,
            String errorDescription);

    void replySendTcp(
            String requestId,
            String sessionId,
            String messageId,
            boolean accepted,
            String errorDescription);

    void replySendTcpFile(
            String requestId,
            String sessionId,
            String messageId,
            boolean accepted,
            String errorDescription);

    void replyTcpSessionAbort(
            String requestId,
            String serverAddress,
            int serverPort,
            String sessionId,
            int error,
            String errorDescription);

    void replyTcpMessageSent(
            String requestId,
            String sessionId,
            String messageId,
            boolean sent,
            int fileSize,
            long elapsedTime,
            int bps,
            String errorDescription);

    void replyStopTcpClient(
            String requestId,
            String sessionId,
            boolean stopped,
            String errorDescription);

    void replyGetTcpClients(
            String requestId,
            ArrayList<String> clients,
            String errorDescription);


    //* TCP CLIENT SOCKET
    void replySendTcpSocket(
            String requestId,
            String sessionId,
            boolean accepted,
            String errorDescription);

    void replyTcpSocketMessageSent(
            String requestId,
            String sessionId,
            boolean sent,
            int fileSize,
            long elapsedTime,
            int bps,
            String errorDescription);

    void replyTcpSocketError(
            String requestId,
            String serverAddress,
            int serverPort,
            String sessionId,
            int error,
            String errorDescription);


    //* UDP SERVER
    void replyCreateUdpServer(
            String requestId,
            int port,
            boolean created,
            String errorDescription);

    void replyUdpServerReceivedMessage(
            String requestId,
            int port,
            String source,
            int sourcePort,
            String message,
            String errorDescription);

    void replyStopUdpServer(
            String requestId,
            int port,
            boolean stopped,
            String errorDescription);

    void replyGetUdpServers(
            String requestId,
            ArrayList<String> servers,
            String errorDescription);


    //* UDP SERVER SOCKET
    void replyCreateUdpServerSocket(
            String requestId,
            int port,
            boolean created,
            String errorDescription);

    void replyUdpServerSocketReceivedMessage(
            String requestId,
            int port,
            String source,
            int sourcePort,
            String message,
            String errorDescription);

    void replyStopUdpServerSocket(
            String requestId,
            int port,
            boolean stopped,
            String errorDescription);

    void replyGetUdpServersSocket(
            String requestId,
            ArrayList<String> servers,
            String errorDescription);


    //* UDP CLIENT
    void replyCreateUdpClient(
            String requestId,
            String serverAddress,
            int serverPort,
            String sourcePort,
            boolean created,
            String errorDescription);

    void replySendUdp(
            String requestId,
            String sourcePort,
            boolean sent,
            String errorDescription);

    void replyStopUdpClient(
            String requestId,
            String sourcePort,
            boolean stopped,
            String errorDescription);

    void replyGetUdpClients(
            String requestId,
            ArrayList<String> clients,
            String errorDescription);

    //* UDP CLIENT SOCKET
    void replyCreateUdpClientSocket(
            String requestId,
            String serverAddress,
            int serverPort,
            String sourcePort,
            boolean created,
            String errorDescription);

    void replySendUdpSocket(
            String requestId,
            String sourcePort,
            boolean sent,
            String errorDescription);

    void replyStopUdpClientSocket(
            String requestId,
            String sourcePort,
            boolean stopped,
            String errorDescription);

    void replyGetUdpClientsSocket(
            String requestId,
            ArrayList<String> clients,
            String errorDescription);
}
