package a.humanitas.hp4a;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

@SuppressWarnings("unused")
public class UnityBridge {

    public static final String TAG = "Python";

    private Context context;
    private UnityBridge unityBridge = this;
    private UnityBridgeCallback unityCallback;

    private EasyClient easyClient;
    private EasyHeavenReplies easyHeavenReplies = new EasyHeavenReplies() {
        @Override
        public void replyGetHeavenAddress(String requestId, String address, String errorDescription) {
            unityCallback.replyGetHeavenAddress(requestId, address, errorDescription);
        }

        @Override
        public void replyGetPeers(String requestId, ArrayList<String> peers, String errorDescription) {
            if(peers == null) unityCallback.replyGetPeers(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String peer: peers) {
                    sb.append(peer).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetPeers(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyGetPeersUpdate(String requestId, boolean subscription, String subscriptionId, String errorDescription) {
            unityCallback.replyGetPeersUpdate(requestId, subscription, subscriptionId, errorDescription);
        }

        @Override
        public void replyStopPeersUpdate(String requestId, boolean unsubscription, String subscriptionId, String errorDescription) {
            unityCallback.replyStopPeersUpdate(requestId, unsubscription, subscriptionId, errorDescription);
        }

        @Override
        public void replyGetNeighbors(String requestId, ArrayList<String> neighbors, String errorDescription) {
            if(neighbors == null) unityCallback.replyGetNeighbors(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String peer: neighbors) {
                    sb.append(peer).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetNeighbors(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyGetLsp(String requestId, String lsp, String errorDescription) {
            unityCallback.replyGetLsp(requestId, lsp, errorDescription);
        }

        @Override
        public void replyGetRoutingTable(String requestId, String routingTable, String errorDescription) {
            unityCallback.replyGetRoutingTable(requestId, routingTable, errorDescription);
        }

        @Override
        public void replyGetRoutingTrees(String requestId, String routingTrees, String errorDescription) {
            unityCallback.replyGetRoutingTrees(requestId, routingTrees, errorDescription);
        }

        @Override
        public void replyGetFullNetwork(String requestId, String fullNetwork, String errorDescription) {
            unityCallback.replyGetFullNetwork(requestId, fullNetwork, errorDescription);
        }

        @Override
        public void replyBlockNeighbor(String requestId, String neighborAddress, boolean blocked, String errorDescription) {
            unityCallback.replyBlockNeighbor(requestId, neighborAddress, blocked, errorDescription);
        }

        @Override
        public void replyUnblockNeighbor(String requestId, String neighborAddress, boolean unblocked, String errorDescription) {
            unityCallback.replyUnblockNeighbor(requestId, neighborAddress, unblocked, errorDescription);
        }

        @Override
        public void replyGetPeerIp(String requestId, String peerAddress, String peerIp, String errorDescription) {
            unityCallback.replyGetPeerIp(requestId, peerAddress, peerIp, errorDescription);
        }

        @Override
        public void replyGetMyIpForPeer(String requestId, String peerAddress, String myIp, String errorDescription) {
            unityCallback.replyGetMyIpForPeer(requestId, peerAddress, myIp, errorDescription);
        }

        @Override
        public void replyCreateTcpServer(String requestId, int port, boolean created, String errorDescription) {
            unityCallback.replyCreateTcpServer(requestId, port, created, errorDescription);
        }

        @Override
        public void replyTcpServerReceivedMessage(String requestId, int port, String source, String sessionId, String message, int messageLength, long elapsedTime, int bps, String errorDescription) {
            unityCallback.replyTcpServerReceivedMessage(requestId, port, source, sessionId, message, messageLength, elapsedTime, bps, errorDescription);
        }

        @Override
        public void replyTcpServerReceivedFile(String requestId, int port, String source, String sessionId, String filePath, int fileSize, long elapsedTime, int bps, String errorDescription) {
            unityCallback.replyTcpServerReceivedFile(requestId, port, source, sessionId, filePath, fileSize, elapsedTime, bps, errorDescription);
        }

        @Override
        public void replyStopTcpServer(String requestId, int port, boolean stopped, String errorDescription) {
            unityCallback.replyStopTcpServer(requestId, port, stopped, errorDescription);
        }

        @Override
        public void replyGetTcpServers(String requestId, ArrayList<String> servers, String errorDescription) {
            if(servers == null) unityCallback.replyGetTcpServers(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String server: servers) {
                    sb.append(server).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetTcpServers(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyCreateTcpServerSocket(String requestId, int port, boolean created, String errorDescription) {
            unityCallback.replyCreateTcpServerSocket(requestId, port, created, errorDescription);
        }

        @Override
        public void replyTcpServerSocketReceivedMessage(String requestId, int port, String source, String sessionId, String message, int messageLength, long elapsedTime, int bps, String errorDescription) {
            unityCallback.replyTcpServerSocketReceivedMessage(requestId, port, source, sessionId, message, messageLength, elapsedTime, bps, errorDescription);
        }

        @Override
        public void replyTcpServerSocketReceivedFile(String requestId, int port, String source, String sessionId, String filePath, int fileSize, long elapsedTime, int bps, String errorDescription) {
            unityCallback.replyTcpServerSocketReceivedFile(requestId, port, source, sessionId, filePath, fileSize, elapsedTime, bps, errorDescription);
        }

        @Override
        public void replyStopTcpServerSocket(String requestId, int port, boolean stopped, String errorDescription) {
            unityCallback.replyStopTcpServerSocket(requestId, port, stopped, errorDescription);
        }

        @Override
        public void replyGetTcpServersSocket(String requestId, ArrayList<String> servers, String errorDescription) {
            if(servers == null) unityCallback.replyGetTcpServersSocket(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String server: servers) {
                    sb.append(server).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetTcpServersSocket(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyCreateTcpClient(String requestId, String serverAddress, int port, String sessionId, boolean accepted, String errorDescription) {
            unityCallback.replyCreateTcpClient(requestId, serverAddress, port, sessionId, accepted, errorDescription);
        }

        @Override
        public void replyTcpSessionEstablished(String requestId, String serverAddress, int port, String sessionId, boolean created, String errorDescription) {
            unityCallback.replyTcpSessionEstablished(requestId, serverAddress, port, sessionId, created, errorDescription);
        }

        @Override
        public void replySendTcp(String requestId, String sessionId, String messageId, boolean accepted, String errorDescription) {
            unityCallback.replySendTcp(requestId, sessionId, messageId, accepted, errorDescription);
        }

        @Override
        public void replySendTcpFile(String requestId, String sessionId, String messageId, boolean accepted, String errorDescription) {
            unityCallback.replySendTcpFile(requestId, sessionId, messageId, accepted, errorDescription);
        }

        @Override
        public void replyTcpSessionAbort(String requestId, String serverAddress, int serverPort, String sessionId, int error, String errorDescription) {
            unityCallback.replyTcpSessionAbort(requestId, serverAddress, serverPort, sessionId, error, errorDescription);
        }

        @Override
        public void replyTcpMessageSent(String requestId, String sessionId, String messageId, boolean sent, int fileSize, long elapsedTime, int bps, String errorDescription) {
            unityCallback.replyTcpMessageSent(requestId, sessionId, messageId, sent, fileSize, elapsedTime, bps, errorDescription);
        }

        @Override
        public void replyStopTcpClient(String requestId, String sessionId, boolean stopped, String errorDescription) {
            unityCallback.replyStopTcpClient(requestId, sessionId, stopped, errorDescription);
        }

        @Override
        public void replyGetTcpClients(String requestId, ArrayList<String> clients, String errorDescription) {
            if(clients == null) unityCallback.replyGetTcpClients(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String client: clients) {
                    sb.append(client).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetTcpClients(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replySendTcpSocket(String requestId, String sessionId, boolean accepted, String errorDescription) {
            unityCallback.replySendTcpSocket(requestId, sessionId, accepted, errorDescription);
        }

        @Override
        public void replyTcpSocketMessageSent(String requestId, String sessionId, boolean sent, int fileSize, long elapsedTime, int bps, String errorDescription) {
            unityCallback.replyTcpSocketMessageSent(requestId, sessionId, sent, fileSize, elapsedTime, bps, errorDescription);
        }

        @Override
        public void replyTcpSocketError(String requestId, String serverAddress, int serverPort, String sessionId, int error, String errorDescription) {
            unityCallback.replyTcpSocketError(requestId, serverAddress, serverPort, sessionId, error, errorDescription);
        }

        @Override
        public void replyCreateUdpServer(String requestId, int port, boolean created, String errorDescription) {
            unityCallback.replyCreateUdpServer(requestId, port, created, errorDescription);
        }

        @Override
        public void replyUdpServerReceivedMessage(String requestId, int port, String source, int sourcePort, String message, String errorDescription) {
            unityCallback.replyUdpServerReceivedMessage(requestId, port, source, sourcePort, message, errorDescription);
        }

        @Override
        public void replyStopUdpServer(String requestId, int port, boolean stopped, String errorDescription) {
            unityCallback.replyStopUdpServer(requestId, port, stopped, errorDescription);
        }

        @Override
        public void replyGetUdpServers(String requestId, ArrayList<String> servers, String errorDescription) {
            if(servers == null) unityCallback.replyGetUdpServers(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String server: servers) {
                    sb.append(server).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetUdpServers(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyCreateUdpServerSocket(String requestId, int port, boolean created, String errorDescription) {
            unityCallback.replyCreateUdpServerSocket(requestId, port, created, errorDescription);
        }

        @Override
        public void replyUdpServerSocketReceivedMessage(String requestId, int port, String source, int sourcePort, String message, String errorDescription) {
            unityCallback.replyUdpServerSocketReceivedMessage(requestId, port, source, sourcePort, message, errorDescription);
        }

        @Override
        public void replyStopUdpServerSocket(String requestId, int port, boolean stopped, String errorDescription) {
            unityCallback.replyStopUdpServerSocket(requestId, port, stopped, errorDescription);
        }

        @Override
        public void replyGetUdpServersSocket(String requestId, ArrayList<String> servers, String errorDescription) {
            if(servers == null) unityCallback.replyGetUdpServersSocket(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String server: servers) {
                    sb.append(server).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetUdpServersSocket(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyCreateUdpClient(String requestId, String serverAddress, int serverPort, String sourcePort, boolean created, String errorDescription) {
            unityCallback.replyCreateUdpClient(requestId, serverAddress, serverPort, sourcePort, created, errorDescription);
        }

        @Override
        public void replySendUdp(String requestId, String sourcePort, boolean sent, String errorDescription) {
            unityCallback.replySendUdp(requestId, sourcePort, sent, errorDescription);
        }

        @Override
        public void replyStopUdpClient(String requestId, String sourcePort, boolean stopped, String errorDescription) {
            unityCallback.replyStopUdpClient(requestId, sourcePort, stopped, errorDescription);
        }

        @Override
        public void replyGetUdpClients(String requestId, ArrayList<String> clients, String errorDescription) {
            if(clients == null) unityCallback.replyGetUdpClients(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String server: clients) {
                    sb.append(server).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetUdpClients(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void replyCreateUdpClientSocket(String requestId, String serverAddress, int serverPort, String sourcePort, boolean created, String errorDescription) {
            unityCallback.replyCreateUdpClientSocket(requestId, serverAddress, serverPort, sourcePort, created, errorDescription);
        }

        @Override
        public void replySendUdpSocket(String requestId, String sourcePort, boolean sent, String errorDescription) {
            unityCallback.replySendUdpSocket(requestId, sourcePort, sent, errorDescription);
        }

        @Override
        public void replyStopUdpClientSocket(String requestId, String sourcePort, boolean stopped, String errorDescription) {
            unityCallback.replyStopUdpClientSocket(requestId, sourcePort, stopped, errorDescription);
        }

        @Override
        public void replyGetUdpClientsSocket(String requestId, ArrayList<String> clients, String errorDescription) {
            if(clients == null) unityCallback.replyGetUdpClientsSocket(requestId,"", errorDescription);
            else {
                StringBuilder sb = new StringBuilder();
                for(String server: clients) {
                    sb.append(server).append(";");
                }
                if (sb.length() > 0) sb.deleteCharAt(sb.length() - 1);
                unityCallback.replyGetUdpClientsSocket(requestId, sb.toString(), errorDescription);
            }
        }

        @Override
        public void messageReception(String sender, int port, String message) {
            unityCallback.messageReceived(sender, port, message);
        }

        @Override
        public void fileReception(String sender, int port, String filepath) {
            unityCallback.fileReceived(sender, port, filepath);
        }

        @Override
        public void messageSent(String target, int port, String message) {
            unityCallback.messageSent(target, port, message);
        }

        @Override
        public void fileSent(String target, int port, String message) {
            unityCallback.fileSent(target, port, message);
        }
    };

    public UnityBridge(final Context context, final UnityBridgeCallback callback) {
        this.context = context;
        unityCallback = callback;
    }

    //* HEAVEN

    public void startHeaven() {
        if (easyClient != null) {
            Log.d(TAG, "startHeaven: service is probably already on");
            return;
        }

        easyClient = new EasyClient(context, easyHeavenReplies);
        easyClient.startHeaven();
    }

    public void stopHeaven() {
        if (easyClient == null) {
            Log.d(TAG, "stopHeaven: Client is already stopped\n" +
                    "If service still here, kill it by killing the whole app\n" +
                    "Do force stop in app settings, or restart device");
            return;
        }
        Client.stopHeaven();
        easyClient.stopClient();
        easyClient = null;
    }

    public void getHeavenAddress() {
        easyClient.getHeavenAddress();
    }

    public void setHeavenAddress(String address) {
        if (Tools.setHeavenAddress(context, address)) {
            File file = new File(context.getFilesDir(),
                    Config.PYTHON_ROOT_FOLDER + AssetExtractor.mAssetModifiedPath);

            if (file.delete()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,
                                "Heaven Address will be updated on next Heaven Start up",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context,
                                "Failed to update heaven address",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }


    //* NETWORK

    public void getPeers() {
        easyClient.getPeers();
    }

    public void getPeersUpdate() {
        easyClient.getPeersUpdate();
    }

    public void stopPeersUpdate(String subscriptionId) {
        easyClient.stopPeersUpdate(subscriptionId);
    }

    public void getNeighbors() {
        easyClient.getNeighbors();
    }

    public void getLsp() {
        easyClient.getLsp();
    }

    public void getRoutingTable() {
        easyClient.getRoutingTable();
    }

    public void getRoutingTrees() {
        easyClient.getRoutingTrees();
    }

    public void getFullNetwork() {
        easyClient.getFullNetwork();
    }

    public void blockNeighbor(String neighbor) {
        easyClient.blockNeighbor(neighbor);
    }

    public void unblockNeighbor(String neighbor) {
        easyClient.unblockNeighbor(neighbor);
    }

    public void getMyIpForPeer(String address) {
        easyClient.getMyIpForPeer(address);
    }

    public void getPeerIp(String address) {
        easyClient.getPeerIp(address);
    }


    //* TCP SERVER
    public void createTcpServer(int port) {
        easyClient.createTcpServer(port);
    }

    public void stopTcpServer(int port) {
        easyClient.stopTcpServer(port);
    }

    public void getTcpServers() {
        easyClient.getTcpServers();
    }


    //* TCP SERVER SOCKET
    public void createTcpServerSocket(int port) {
        easyClient.createTcpServerSocket(port);
    }

    public void stopTcpServerSocket(int port) {
        easyClient.stopTcpServerSocket(port);
    }

    public void getTcpServersSocket() {
        easyClient.getTcpServersSocket();
    }


    //* TCP CLIENT
    public void createTcpClient(String address, int port, int routing) {
        easyClient.createTcpClient(address, port, routing);
    }

    public void sendTcp(String sessionId, String message) {
        easyClient.sendTcp(sessionId, message);
    }

    public void sendTcpFile(String sessionId, boolean delete, String path) {
        easyClient.sendTcpFile(sessionId, delete, path);
    }

    public void stopTcpClient(String sessionId) {
        easyClient.stopTcpClient(sessionId);
    }

    public void getTcpClients() {
        easyClient.getTcpClients();
    }

    public void unicastTcp(String target, String message) {
        easyClient.unicastTcp(target, message);
    }

    public void unicastTcp(String target, int port, String message) {
        easyClient.unicastTcp(target, port, message);
    }

    public void unicastTcpFile(String target, String filepath) {
        easyClient.unicastTcpFile(target, filepath);
    }

    public void unicastTcpFile(String target, int port, String filepath) {
        easyClient.unicastTcpFile(target, port, filepath);
    }


    //* TCP CLIENT SOCKET
    public void sendTcpSocket(String address, int port, String message) {
        easyClient.sendTcpSocket(address, port, message);
    }

    public void sendTcpFileSocket(String address, int port, boolean delete, String filepath) {
        easyClient.sendTcpFileSocket(address, port, delete, filepath);
    }

    public void unicastTcpSocket(String target, String message) {
        easyClient.unicastTcpSocket(target, message);
    }

    public void unicastTcpSocket(String target, int port, String message) {
        easyClient.unicastTcpSocket(target, port, message);
    }

    public void unicastTcpSocketFile(String target, String filePath) {
        easyClient.unicastTcpSocketFile(target, filePath);
    }

    public void unicastTcpSocketFile(String target, int port, String filePath) {
        easyClient.unicastTcpSocketFile(target, port, filePath);
    }


    //* UDP SERVER
    public void createUdpServer(int port) {
        easyClient.createUdpServer(port);
    }

    public void stopUdpServer(int port) {
        easyClient.stopUdpServer(port);
    }

    public void getUdpServers() {
        easyClient.getUdpServers();
    }


    //* UDP SERVER SOCKET
    public void createUdpServerSocket(int port) {
        easyClient.createUdpServerSocket(port);
    }

    public void stopUdpServerSocket(int port) {
        easyClient.stopUdpServerSocket(port);
    }

    public void getUdpServersSocket() {
        easyClient.getUdpServersSocket();
    }


    //* UDP CLIENT
    public void createUdpClient(String address, int port, int mtu, int maxHop, int packetRate,
                                int routingMethod) {
        easyClient.createUdpClient(address, port, mtu, maxHop, packetRate, routingMethod);
    }

    public void sendUdp(int port, String message) {
        easyClient.sendUdp(port, message);
    }

    public void stopUdpClient(int port) {
        easyClient.stopUdpClient(port);
    }

    public void getUdpClients() {
        easyClient.getUdpClients();
    }

    public void setDefaultUdpClientMaxHop(int maxHop) {
        easyClient.setDefaultUdpClientMaxHop(maxHop);
    }

    public void setDefaultUdpClientMtu(int mtu) {
        easyClient.setDefaultUdpClientMtu(mtu);
    }

    public void setDefaultUdpClientPacketRate(int packetRate) {
        easyClient.setDefaultUdpClientPacketRate(packetRate);
    }

    public void setDefaultUdpClientRoutingMethod(int method) {
        easyClient.setDefaultUdpClientRoutingMethod(method);
    }

    public void unicastUdp(String target, int port, String message) {
        easyClient.unicastUdp(target, port, message);
    }

    public void broadcastUdp(String message) {
        easyClient.broadcastUdp(message);
    }

    public void broadcastUdp(int port, String message) {
        easyClient.broadcastUdp(port, message);
    }


    //* UDP SOCKET

    public void unicastUdpSocket(String target, String message) {
        easyClient.unicastUdpSocket(target, message);
    }

    public void unicastUdpSocket(String target, int port, String message) {
        easyClient.unicastUdpSocket(target, port, message);
    }

}
