package a.humanitas.hp4a;

public class Config {
    public static final String TAG = "p4a";

    static final String PYTHON_ROOT_FOLDER = "from_assets";
    static final String PYTHON_FOLDER = "python";
    static final String PYTHON_SCRIPT = "heaven_android.py";
    static final String SAVE_DIRECTORY = "heaven";

    public static final String ANDROID_TO_INTERFACE = "a2i"; //!\\ Will mutate to: getFiles() + /<current value>
    public static final String INTERFACE_TO_ANDROID = "i2a"; //!\\ Will mutate to: getFiles() + /<current value>
    public static final String ANDROID_TO_NEIGHBOURING = "a2n"; //!\\ Will mutate to: getFiles() + /<current value>

    static final long INIT_RESTART_DELAY = 5000L;

    static final String KEY_HEAVEN_CONFIG = "heaven_config";
    static final String KEY_HEAVEN_ADDRESS = "heaven_address";
    public static final String HEAVEN_BROADCAST_ADDRESS = "&&&&";
    static final boolean DELETE_AFTER_SEND = false;


    static final int HEAVEN_REQUEST_PORT = 30020;
    static final int HEAVEN_REPLY_PORT = 9111;

    static final int DEFAULT_TCP_PORT = 2500;
    static final int DEFAULT_TCP_SOCKET_PORT = 30200;
    static final int DEFAULT_UDP_PORT = 2600;
    static final int DEFAULT_UDP_SOCKET_PORT = 30300;

    static int DEFAULT_ROUTING_METHOD = 0; // Gossip
    static int DEFAULT_UDP_MTU = 0;
    static int DEFAULT_UDP_MAX_HOP = 0;
    static int DEFAULT_UDP_PACKET_RATE = 0;
    static int DEFAULT_UDP_SOCKET_BUFFER_SIZE = 4096;
    static int DEFAULT_UDP_SOCKET_QUEUE_LEN = 1;
    static int DEFAULT_UDP_SOCKET_TIMEOUT = 10;
}
