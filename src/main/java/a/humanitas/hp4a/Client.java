package a.humanitas.hp4a;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static a.humanitas.hp4a.Config.HEAVEN_REPLY_PORT;
import static a.humanitas.hp4a.Config.HEAVEN_REQUEST_PORT;
import static a.humanitas.hp4a.HeavenCommands.*;
import static a.humanitas.hp4a.HeavenReplies.*;
import static a.humanitas.hp4a.PythonBridge.TAG;

public class Client {

    public static final String TAG_HEAVEN_RESPONSE = "Heaven_Reply";
    public static final String TAG_HEAVEN_REQUEST = "Heaven_Request";
    private SocketTx heavenTx;
    private SocketRx heavenRx;
    protected Handler handler;

    private HeavenReplies heavenReplies;

    private int requestId = 0;

    public Client() {
        startSockets();
        handler = new Handler();
    }

    public String[] getCommands() {
        return HeavenCommands.commands;
    }

    public void stopClient() {
        handler.removeCallbacksAndMessages(null);
        heavenTx.quitSafely();
        heavenRx.interrupt();
    }

    public void setHeavenReplies(HeavenReplies heavenReplies) {
        this.heavenReplies = heavenReplies;
    }

    private String randomRequestId() {
//        StringBuilder stringBuilder = new StringBuilder();
//        for (int i = 0; i < 5; i++) {
//            stringBuilder.append((char) (new Random().nextInt(26) + 'a'));
//        }
//        return stringBuilder.toString();

        return String.format(Locale.US, "%d", requestId++);
    }

    private String createRequest(String heavenCommand) {
        return createRequest(null, HEAVEN_REPLY_PORT, heavenCommand, null);
    }

    private String createRequest(String heavenCommand, String[] parameters) {
        return createRequest(null, HEAVEN_REPLY_PORT, heavenCommand, parameters);
    }

    private String createRequest(int replyPort, String heavenCommand, String[] parameters) {
        return createRequest(null, replyPort, heavenCommand, parameters);
    }

    private String createRequest(String requestId, int replyPort,
                                 String heavenCommand, String[] parameters) {
        if (requestId == null) {
            requestId = randomRequestId();
        }
        String request = getRequest(requestId, replyPort, heavenCommand, parameters);
        Log.d(TAG_HEAVEN_REQUEST, request);
        heavenTx.send(request);
        return requestId;
    }

    public static String getRequest(String requestId, int replyPort,
                                    String heavenCommand, String[] parameters) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s;%s;%s", requestId, replyPort, heavenCommand));
        if (parameters != null) {
            for (String parameter : parameters) {
                stringBuilder.append(";").append(parameter);
            }
        }
        return stringBuilder.toString();
    }

    private void invalidMessage(String message) {
        Log.d(TAG, "invalidMessage: " + message);
    }


//**************************************************************************************************
//  Heaven
//**************************************************************************************************
    public String getHeavenAddress() {
        return createRequest(GET_HEAVEN_ADDRESS);
    }
    private void getHeavenAddressReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if ( jsonObject.has(KEY_HEAVEN_ADDRESS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyGetHeavenAddress(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_HEAVEN_ADDRESS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public static void stopHeaven() {
        final Handler handler = new Handler(Looper.getMainLooper());
        final SocketTx tx = new SocketTx(TAG, Config.HEAVEN_REQUEST_PORT);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                tx.send(getRequest("stop", Config.HEAVEN_REPLY_PORT, STOP_HEAVEN, null));
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tx.quitSafely();
                    }
                }, 2000);
            }
        }, 1000);
    }
    // No reply for this little one

    //* NETWORK
    public String getPeers() {
        return createRequest(GET_PEERS);
    }
    private void getPeersReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        // Peers reception
        if (jsonObject.has(KEY_PEERS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)) {

            ArrayList<String> peers = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_PEERS);
            for (int i = 0; i < jsonArray.length(); i++) {
                peers.add(jsonArray.getString(i));
            }
            heavenReplies.replyGetPeers(
                    jsonObject.getString(KEY_REQUEST_ID),
                    peers,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getPeersUpdate() {
        return createRequest(GET_PEERS_UPDATES);
    }
    private void getPeersUpdateReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        // Subscription response
        if (jsonObject.has(KEY_PEERS_SUBSCRIPTION)
                && jsonObject.has(KEY_PEERS_SUBSCRIPTION_ID)
                && jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyGetPeersUpdate(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PEERS_SUBSCRIPTION) != 0,
                    jsonObject.getString(KEY_PEERS_SUBSCRIPTION_ID),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Peers update
        else if (
                jsonObject.has(KEY_PEERS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            getPeersReception(message);
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String stopPeersUpdate(String subscriptionId) {
        return createRequest(STOP_PEERS_UPDATE, new String[] {subscriptionId});
    }
    private void stopPeersUpdateReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_PEERS_UNSUBSCRIPTION)
            && jsonObject.has(KEY_PEERS_SUBSCRIPTION_ID)
            && jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopPeersUpdate(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PEERS_UNSUBSCRIPTION) != 0,
                    jsonObject.getString(KEY_PEERS_SUBSCRIPTION_ID),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );

        }
    }

    public String getNeighbors() {
        return createRequest(GET_NEIGHBORS);
    }
    private void getNeighborsReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_NEIGHBORS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> neighbors = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_NEIGHBORS);
            for (int i = 0; i < jsonArray.length(); i++) {
                neighbors.add(jsonArray.getString(i));
            }
            heavenReplies.replyGetNeighbors(
                    jsonObject.getString(KEY_REQUEST_ID),
                    neighbors,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getLsp() {
        return createRequest(GET_LSP);
    }
    private void getLspReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_LSP) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            // TODO: find how to parse LSP
            heavenReplies.replyGetLsp(
                    jsonObject.getString(KEY_REQUEST_ID),
                    message,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getRoutingTable() {
        return createRequest(GET_ROUTING_TABLE);
    }
    private void getRoutingTableReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_ROUTING_TABLE) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            // TODO: find how to parse Routing Table
            heavenReplies.replyGetRoutingTable(
                    jsonObject.getString(KEY_REQUEST_ID),
                    message,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getRoutingTrees() {
        return createRequest(GET_ROUTING_TREES);
    }
    private void getRoutingTreesReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if ( jsonObject.has(KEY_ROUTING_TREES) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            // TODO: find how to parse Routing Table
            heavenReplies.replyGetRoutingTrees(
                    jsonObject.getString(KEY_REQUEST_ID),
                    message,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getFullNetwork() {
        return createRequest(GET_FULL_NETWORK);
    }
    private void getFullNetworkReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if ( jsonObject.has(KEY_FULL_NETWORK) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            // TODO: find how to parse Routing Table
            heavenReplies.replyGetFullNetwork(
                    jsonObject.getString(KEY_REQUEST_ID),
                    message,
                    jsonObject.getString(KEY_FULL_NETWORK)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String blockNeighbor(String peerAddress) {
        return createRequest(BLOCK_NEIGHBOR, new String[] {peerAddress});
    }
    private void blockNeighborReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_HEAVEN_ADDRESS)
        && jsonObject.has(KEY_BLOCKED_NEIGHBOR)
        && jsonObject.has(KEY_ERROR_DESCRIPTION)) {
            heavenReplies.replyBlockNeighbor(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_HEAVEN_ADDRESS),
                    jsonObject.getBoolean(KEY_BLOCKED_NEIGHBOR),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }
    }

    public String unblockNeighbor(String peerAddress) {
        return createRequest(UNBLOCK_NEIGHBOR, new String[] {peerAddress});
    }
    private void unblockNeighborReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_HEAVEN_ADDRESS)
        && jsonObject.has(KEY_UNBLOCKED_NEIGHBOR)
        && jsonObject.has(KEY_ERROR_DESCRIPTION)) {
            heavenReplies.replyUnblockNeighbor(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_HEAVEN_ADDRESS),
                    jsonObject.getBoolean(KEY_UNBLOCKED_NEIGHBOR),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }
    }

    public String getMyIpForPeer(String peerAddress) {
        return createRequest(GET_MY_IP_FOR_PEER, new String[] {peerAddress});
    }
    private void getMyIpForPeerReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_HEAVEN_ADDRESS)
        && jsonObject.has(KEY_IP_ADDRESS)
        && jsonObject.has(KEY_ERROR_DESCRIPTION)) {
            heavenReplies.replyGetMyIpForPeer(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_HEAVEN_ADDRESS),
                    jsonObject.getString(KEY_IP_ADDRESS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }
    }

    public String getPeerIp(String peerAddress) {
        return createRequest(GET_PEER_IP, new String[] {peerAddress});
    }
    private void getPeerIpReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if(jsonObject.has(KEY_HEAVEN_ADDRESS)
        && jsonObject.has(KEY_IP_ADDRESS)
        && jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyGetPeerIp(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_HEAVEN_ADDRESS),
                    jsonObject.getString(KEY_IP_ADDRESS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }
    }


    //* TCP SERVER
    public String createTcpServer(int port) {
        return createRequest(CREATE_TCP_SERVER, new String[]{Integer.toString(port)});
    }
    private void createTcpServerReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        // CREATE_TCP_REPLY
        if ( jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_CREATED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateTcpServer(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // TCP_RECEIVED_MESSAGE REPLY
        else if ( jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SOURCE) &&
                        jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_MESSAGE) &&
                        jsonObject.has(KEY_FILE_SIZE) &&
                        jsonObject.has(KEY_ELAPSED_TIME) &&
                        jsonObject.has(KEY_BPS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpServerReceivedMessage(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_MESSAGE),
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // TCP_RECEIVED_FILE REPLY
        else if ( jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SOURCE) &&
                        jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_FILE_PATH) &&
                        jsonObject.has(KEY_FILE_SIZE) &&
                        jsonObject.has(KEY_ELAPSED_TIME) &&
                        jsonObject.has(KEY_BPS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)

        ) {
            heavenReplies.replyTcpServerReceivedFile(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_FILE_PATH),
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String stopTcpServer(int port) {
        return createRequest(STOP_TCP_SERVER, new String[]{Integer.toString(port)});
    }
    private void stopTcpServerReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_STOPPED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopTcpServer(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getTcpServers() {
        return createRequest(GET_TCP_SERVERS);
    }
    private void getTcpServersReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_TCP_SERVERS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> servers = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_TCP_SERVERS);

            for (int i = 0; i < jsonArray.length(); i++) {
                servers.add(jsonArray.getString(i));
            }

            heavenReplies.replyGetTcpServers(
                    jsonObject.getString(KEY_REQUEST_ID),
                    servers,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    //* TCP SERVER SOCKET
    public String createTcpServerSocket(int port) {
        return createRequest(CREATE_TCP_SERVER_SOCKET, new String[]{Integer.toString(port)});
    }
    private void createTcpServerSocketReception(String message) throws JSONException {

        JSONObject jsonObject = new JSONObject(message);
        // CREATE_TCP_REPLY
        if ( jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_CREATED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateTcpServerSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // TCP_RECEIVED_MESSAGE REPLY
        else if ( jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_SOURCE) &&
                jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_MESSAGE) &&
                jsonObject.has(KEY_FILE_SIZE) &&
                jsonObject.has(KEY_ELAPSED_TIME) &&
                jsonObject.has(KEY_BPS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpServerSocketReceivedMessage(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_MESSAGE),
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // TCP_RECEIVED_FILE REPLY
        else if ( jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_SOURCE) &&
                jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_FILE_PATH) &&
                jsonObject.has(KEY_FILE_SIZE) &&
                jsonObject.has(KEY_ELAPSED_TIME) &&
                jsonObject.has(KEY_BPS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)

        ) {
            heavenReplies.replyTcpServerSocketReceivedFile(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_FILE_PATH),
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String stopTcpServerSocket(int port) {
        return createRequest(STOP_TCP_SERVER_SOCKET, new String[]{Integer.toString(port)});
    }
    private void stopTcpServerSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_STOPPED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopTcpServerSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getTcpServersSocket() {
        return createRequest(GET_TCP_SERVERS_SOCKET);
    }
    private void getTcpServersSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_TCP_SERVERS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> servers = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_TCP_SERVERS);

            for (int i = 0; i < jsonArray.length(); i++) {
                servers.add(jsonArray.getString(i));
            }

            heavenReplies.replyGetTcpServersSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    servers,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }


    //* TCP CLIENT
    public String createTcpClient(String serverHeavenAddress, int serverPort, int routingMethod) {
        return createRequest(CREATE_TCP_CLIENT,
                new String[]{
                        serverHeavenAddress,
                        Integer.toString(serverPort),
                        Integer.toString(routingMethod)
                });
    }
    private void createTcpClientReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        // CREATE TCP CLIENT
        if (
                jsonObject.has(KEY_SERVER_ADDRESS) &&
                        jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_REQUEST_ACCEPTED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateTcpClient(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SERVER_ADDRESS),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_REQUEST_ACCEPTED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // TCP SESSION ESTABLISHED
        else if (
                jsonObject.has(KEY_SERVER_ADDRESS) &&
                        jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_CREATED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpSessionEstablished(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SERVER_ADDRESS),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // TCP SESSION ABORTED
        else if (
                jsonObject.has(KEY_SERVER_ADDRESS) &&
                        jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_ERROR) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpSessionAbort(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SERVER_ADDRESS),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_ERROR),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }


    public String sendTcp(String session_id, String message) {
        return createRequest(SEND_TCP, new String[]{session_id, message});
    }
    private void sendTcpReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        // SEND_TCP REPLY
        if (
                jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_MESSAGE_ID) &&
                        jsonObject.has(KEY_REQUEST_ACCEPTED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replySendTcp(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_MESSAGE_ID),
                    jsonObject.getInt(KEY_REQUEST_ACCEPTED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // SENT_TCP REPLY
        else if ( jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_MESSAGE_ID) &&
                jsonObject.has(KEY_MESSAGE_SENT) &&
                jsonObject.has(KEY_FILE_SIZE) &&
                jsonObject.has(KEY_ELAPSED_TIME) &&
                jsonObject.has(KEY_BPS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION))
        {
            heavenReplies.replyTcpMessageSent(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_MESSAGE_ID),
                    jsonObject.getInt(KEY_MESSAGE_SENT) != 0,
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String sendTcpFile(String session_id, boolean delete_after_send, String filepath) {
        return createRequest(SEND_TCP_FILE, new String[]{
                session_id,
                delete_after_send ? "1" : "0",
                filepath
        });
    }
    private void sendTcpFileReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        // SEND_TCP_FILE REPLY
        if (
                jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_MESSAGE_ID) &&
                        jsonObject.has(KEY_REQUEST_ACCEPTED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replySendTcpFile(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_MESSAGE_ID),
                    jsonObject.getInt(KEY_REQUEST_ACCEPTED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // SENT_TCP_FILE REPLY
        // TODO: Check if this happen
         else if ( jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_MESSAGE_ID) &&
                        jsonObject.has(KEY_MESSAGE_SENT) &&
                        jsonObject.has(KEY_FILE_SIZE) &&
                        jsonObject.has(KEY_ELAPSED_TIME) &&
                        jsonObject.has(KEY_BPS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)) {
            Log.d(TAG, "sendTcpFileReception: yep, it happend");
            heavenReplies.replyTcpMessageSent(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getString(KEY_MESSAGE_ID),
                    jsonObject.getInt(KEY_MESSAGE_SENT) != 0,
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }


    public String stopTcpClient(String sessionId) {
        return createRequest(STOP_TCP_CLIENT, new String[]{sessionId});
    }
    private void stopTcpClientReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_REQUEST_ID) &&
                        jsonObject.has(KEY_SESSION_ID) &&
                        jsonObject.has(KEY_STOPPED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopTcpClient(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getTcpClients() {
        return createRequest(GET_TCP_CLIENTS);
    }
    private void getTcpClientsReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_TCP_CLIENTS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> clients = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_TCP_CLIENTS);
            for (int i = 0; i < jsonArray.length(); i++) {
                clients.add(jsonArray.getString(i));
            }

            heavenReplies.replyGetTcpClients(
                    jsonObject.getString(KEY_REQUEST_ID),
                    clients,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }


    //* TCP CLIENT SOCKET
    public String sendTcpSocket(String serverHeavenAddress, int serverPort, String message) {
        return createRequest(SEND_TCP_SOCKET, new String[]{
                serverHeavenAddress,
                Integer.toString(serverPort),
                message
        });
    }
    private void sendTcpSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        // SEND TCP SOCKET
        if (jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_REQUEST_ACCEPTED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replySendTcpSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_REQUEST_ACCEPTED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // SENT TCP SOCKET
        else if ( jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_MESSAGE_SENT) &&
                jsonObject.has(KEY_FILE_SIZE) &&
                jsonObject.has(KEY_ELAPSED_TIME) &&
                jsonObject.has(KEY_BPS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpSocketMessageSent(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_MESSAGE_SENT) != 0,
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // SEND TCP SOCKET ERROR
        else if (jsonObject.has(KEY_SERVER_ADDRESS) &&
                jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_ERROR) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpSocketError(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SERVER_ADDRESS),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_ERROR),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String sendTcpFileSocket(String serverHeavenAddress, int serverPort, boolean deleteAfterSend, String filepath) {
        return createRequest(SEND_TCP_FILE_SOCKET, new String[]{
                serverHeavenAddress,
                Integer.toString(serverPort),
                !deleteAfterSend ? "0" : "1",
                filepath
        });
    }
    private void sendTcpFileSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        // CREATE TCP CLIENT - socket
        if (jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_REQUEST_ACCEPTED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            Log.d(TAG, String.format(
                    Locale.CANADA,
                    "send_tcp_socket: create_tcp_client_socket\n" +
                            "Session ID: %s\n" +
                            "Accepted: %s\n" +
                            "Error Description: %s",
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_REQUEST_ACCEPTED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)));
        }

        //
        else if ( jsonObject.has(KEY_SESSION_ID) &&
                jsonObject.has(KEY_MESSAGE_SENT) &&
                jsonObject.has(KEY_FILE_SIZE) &&
                jsonObject.has(KEY_ELAPSED_TIME) &&
                jsonObject.has(KEY_BPS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyTcpSocketMessageSent(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SESSION_ID),
                    jsonObject.getInt(KEY_MESSAGE_SENT) != 0,
                    jsonObject.getInt(KEY_FILE_SIZE),
                    jsonObject.getLong(KEY_ELAPSED_TIME),
                    jsonObject.getInt(KEY_BPS),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }


//**************************************************************************************************
//  UDP SERVER
// *************************************************************************************************
    public String createUdpServer(int port) {
        return createRequest(CREATE_UDP_SERVER, new String[]{Integer.toString(port)});
    }
    private void createUdpServerReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        // Create Udp Server
        if (
                jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_CREATED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateUdpServer(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // UDP SERVER RECEIVED MESSAGE
        else if (
                jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SOURCE) &&
                        jsonObject.has(KEY_SOURCE_PORT) &&
                        jsonObject.has(KEY_MESSAGE) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyUdpServerReceivedMessage(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE),
                    jsonObject.getInt(KEY_SOURCE_PORT),
                    jsonObject.getString(KEY_MESSAGE),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String stopUdpServer(int port) {
        return createRequest(STOP_UDP_SERVER, new String[]{Integer.toString(port)});
    }
    private void stopUdpServerReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_STOPPED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopUdpServer(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    public String getUdpServers() {
        return createRequest(GET_UDP_SERVERS);
    }
    private void getUdpServersReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        if (
                jsonObject.has(KEY_UDP_SERVERS) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> servers = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_UDP_SERVERS);

            for (int i = 0; i < jsonArray.length(); i++) {
                servers.add(jsonArray.getString(i));
            }

            heavenReplies.replyGetUdpServers(
                    jsonObject.getString(KEY_REQUEST_ID),
                    servers,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // Message does not match any pattern for this command
        else {
            invalidMessage(message);
        }
    }

    //* UDP SERVER SOCKET
    public String createUdpServerSocket(int port, int rxBufferSize, int rxQueueLen) {
        return createRequest(CREATE_UDP_SERVER_SOCKET, new String[]{
                Integer.toString(port),
                Integer.toString(rxBufferSize),
                Integer.toString(rxQueueLen)
        });
    }
    private void createUdpServerSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        // CREATE UDP SERVER SOCKET
        if (jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_CREATED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateUdpServerSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        // UDP SERVER SOCKET RECEIVED
        else if (jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_SOURCE) &&
                jsonObject.has(KEY_SOURCE_PORT) &&
                jsonObject.has(KEY_MESSAGE) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyUdpServerSocketReceivedMessage(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE),
                    jsonObject.getInt(KEY_SOURCE_PORT),
                    jsonObject.getString(KEY_MESSAGE),
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String stopUdpServerSocket(int port) {
        return createRequest(STOP_UDP_SERVER_SOCKET,
                new String[]{
                        Integer.toString(port)
                });
    }
    private void stopUdpServerSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_STOPPED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopUdpServerSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String getUdpServersSocket() {
        return createRequest(GET_UDP_SERVERS_SOCKET);
    }
    private void getUdpServersSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_UDP_SERVERS) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> servers = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_UDP_SERVERS);
            for (int i = 0; i < jsonArray.length(); i++) {
                servers.add(jsonArray.getString(i));
            }

            heavenReplies.replyGetUdpServersSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    servers,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    //* UDP CLIENT
    public String createUdpClient(String serverHeavenAddress, int serverPort,
                           int mtu, int maxHop, int packetRate, int routingMethod) {
        return createRequest(CREATE_UDP_CLIENT, new String[]{
                serverHeavenAddress, Integer.toString(serverPort),
                Integer.toString(mtu), Integer.toString(maxHop),
                Integer.toString(packetRate), Integer.toString(routingMethod)
        });
    }
    private void createUdpClientReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);

        if (
                jsonObject.has(KEY_SERVER_ADDRESS) &&
                        jsonObject.has(KEY_PORT) &&
                        jsonObject.has(KEY_SOURCE_PORT) &&
                        jsonObject.has(KEY_CREATED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateUdpClient(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SERVER_ADDRESS),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE_PORT),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String sendUdp(int source_port, String message) {
        return createRequest(SEND_UDP, new String[]{Integer.toString(source_port), message});
    }
    private void sendUdpReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_SOURCE_PORT) &&
                        jsonObject.has(KEY_MESSAGE_SENT) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replySendUdp(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SOURCE_PORT),
                    jsonObject.getInt(KEY_MESSAGE_SENT) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }


    public String stopUdpClient(int port) {
        return createRequest(STOP_UDP_CLIENT, new String[]{Integer.toString(port)});
    }
    private void stopUdpClientReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (
                jsonObject.has(KEY_SOURCE_PORT) &&
                        jsonObject.has(KEY_STOPPED) &&
                        jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopUdpClient(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SOURCE_PORT),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String getUdpClients() {
        return createRequest(GET_UDP_CLIENTS);
    }
    private void getUdpClientsReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if ( jsonObject.has(KEY_UDP_CLIENTS) && jsonObject.has(KEY_ERROR_DESCRIPTION)) {
            ArrayList<String> clients = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_UDP_CLIENTS);
            for( int i = 0; i < jsonArray.length(); i++) {
                clients.add(jsonArray.getString(i));
            }
            heavenReplies.replyGetUdpClients(
                    jsonObject.getString(KEY_REQUEST_ID),
                    clients,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    // UDP CLIENT SOCKET
    public String createUdpClientSocket(
            String serverAddress,
            int serverPort,
            long txTimeout
    ) {
        return createRequest(
                CREATE_UDP_CLIENT_SOCKET, new String[] {
                        serverAddress,
                        Integer.toString(serverPort),
                        Long.toString(txTimeout)
                }
        );
    }
    private void createUdpClientSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_SERVER_ADDRESS) &&
                jsonObject.has(KEY_PORT) &&
                jsonObject.has(KEY_SOURCE_PORT) &&
                jsonObject.has(KEY_CREATED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyCreateUdpClientSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SERVER_ADDRESS),
                    jsonObject.getInt(KEY_PORT),
                    jsonObject.getString(KEY_SOURCE_PORT),
                    jsonObject.getInt(KEY_CREATED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }
        else {
            invalidMessage(message);
        }
    }

    public String sendUdpSocket(String clientId, String message) {
        return createRequest(
                SEND_UDP_SOCKET, new String[] {
                        clientId, message
                }
        );
    }
    private void sendUdpSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_SOURCE_PORT) &&
                jsonObject.has(KEY_MESSAGE_SENT) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replySendUdpSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SOURCE_PORT),
                    jsonObject.getInt(KEY_MESSAGE_SENT) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String stopUdpClientSocket(String clientId) {
        return createRequest(
                STOP_UDP_CLIENT_SOCKET, new String[] { clientId }
        );
    }
    private void stopUdpClientSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_SOURCE_PORT) &&
                jsonObject.has(KEY_STOPPED) &&
                jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            heavenReplies.replyStopUdpClientSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    jsonObject.getString(KEY_SOURCE_PORT),
                    jsonObject.getInt(KEY_STOPPED) != 0,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }

    public String getUdpClientsSocket() {
        return createRequest(GET_UDP_CLIENTS_SOCKET);
    }
    private void getUdpClientsSocketReception(String message) throws JSONException {
        JSONObject jsonObject = new JSONObject(message);
        if (jsonObject.has(KEY_UDP_CLIENTS) &&
            jsonObject.has(KEY_ERROR_DESCRIPTION)
        ) {
            ArrayList<String> clients = new ArrayList<>();
            JSONArray jsonArray = jsonObject.getJSONArray(KEY_UDP_CLIENTS);
            for( int i = 0; i < jsonArray.length(); i++) {
                clients.add(jsonArray.getString(i));
            }
            heavenReplies.replyGetUdpClientsSocket(
                    jsonObject.getString(KEY_REQUEST_ID),
                    clients,
                    jsonObject.getString(KEY_ERROR_DESCRIPTION)
            );
        }

        else {
            invalidMessage(message);
        }
    }


    //**************************************************************************************************
//  Heaven
// *************************************************************************************************
    void startSockets() {
        heavenTx = new SocketTx(TAG, HEAVEN_REQUEST_PORT);
        heavenRx = new SocketRx(HEAVEN_REPLY_PORT, heavenCallback);
    }

    void stopSockets() {
        heavenRx.interrupt();
        heavenTx.quit();
    }

    private SocketRx.Callback heavenCallback;

    {
        heavenCallback = new SocketRx.Callback() {
            @Override
            public void messageReceived(final String message) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObject = new JSONObject(message);

                            if (jsonObject.has(KEY_DELETE_REQUEST)) return;

                            Log.d(TAG_HEAVEN_RESPONSE, message);

                            switch (jsonObject.getString(KEY_COMMAND)) {
                                // HEAVEN
                                case GET_HEAVEN_ADDRESS:
                                    getHeavenAddressReception(message);
                                    break;


                                // NETWORK
                                case GET_PEERS:
                                    getPeersReception(message);
                                    break;

                                case GET_PEERS_UPDATES:
                                    getPeersUpdateReception(message);
                                    break;

                                case STOP_PEERS_UPDATE:
                                    stopPeersUpdateReception(message);
                                    break;

                                case GET_NEIGHBORS:
                                    getNeighborsReception(message);
                                    break;

                                case GET_LSP:
                                    getLspReception(message);
                                    break;

                                case GET_ROUTING_TABLE:
                                    getRoutingTableReception(message);
                                    break;

                                case GET_ROUTING_TREES:
                                    getRoutingTreesReception(message);
                                    break;

                                case GET_FULL_NETWORK:
                                    getFullNetworkReception(message);
                                    break;

                                case BLOCK_NEIGHBOR:
                                    blockNeighborReception(message);
                                    break;

                                case UNBLOCK_NEIGHBOR:
                                    unblockNeighborReception(message);
                                    break;

                                case GET_MY_IP_FOR_PEER:
                                    getMyIpForPeerReception(message);
                                    break;

                                case GET_PEER_IP:
                                    getPeerIpReception(message);
                                    break;


                                // TCP SERVER
                                case CREATE_TCP_SERVER:
                                    createTcpServerReception(message);
                                    break;

                                case STOP_TCP_SERVER:
                                    stopTcpServerReception(message);
                                    break;

                                case GET_TCP_SERVERS:
                                    getTcpServersReception(message);
                                    break;


                                // TCP SERVER SOCKET
                                case CREATE_TCP_SERVER_SOCKET:
                                    createTcpServerSocketReception(message);
                                    break;

                                case STOP_TCP_SERVER_SOCKET:
                                    stopTcpServerSocketReception(message);
                                    break;

                                case GET_TCP_SERVERS_SOCKET:
                                    getTcpServersSocketReception(message);
                                    break;


                                // TCP CLIENT
                                case CREATE_TCP_CLIENT:
                                    createTcpClientReception(message);
                                    break;

                                case SEND_TCP:
                                    sendTcpReception(message);
                                    break;

                                case SEND_TCP_FILE:
                                    sendTcpFileReception(message);
                                    break;

                                case STOP_TCP_CLIENT:
                                    stopTcpClientReception(message);
                                    break;

                                case GET_TCP_CLIENTS:
                                    getTcpClientsReception(message);
                                    break;


                                // TCP CLIENT SOCKET
                                case SEND_TCP_SOCKET:
                                    sendTcpSocketReception(message);
                                    break;

                                case SEND_TCP_FILE_SOCKET:
                                    sendTcpFileSocketReception(message);
                                    break;


                                // UDP SERVER
                                case CREATE_UDP_SERVER:
                                    createUdpServerReception(message);
                                    break;

                                case STOP_UDP_SERVER:
                                    stopUdpServerReception(message);
                                    break;

                                case GET_UDP_SERVERS:
                                    getUdpServersReception(message);
                                    break;


                                // UDP SERVER SOCKET
                                case CREATE_UDP_SERVER_SOCKET:
                                    createUdpServerSocketReception(message);
                                    break;

                                case STOP_UDP_SERVER_SOCKET:
                                    stopUdpServerSocketReception(message);
                                    break;

                                case GET_UDP_SERVERS_SOCKET:
                                    getUdpServersSocketReception(message);
                                    break;


                                // UDP CLIENT
                                case CREATE_UDP_CLIENT:
                                    createUdpClientReception(message);
                                    break;

                                case SEND_UDP:
                                    sendUdpReception(message);
                                    break;

                                case STOP_UDP_CLIENT:
                                    stopUdpClientReception(message);
                                    break;

                                case GET_UDP_CLIENTS:
                                    getUdpClientsReception(message);
                                    break;


                                // UDP CLIENT SOCKET
                                case CREATE_UDP_CLIENT_SOCKET:
                                    createUdpClientSocketReception(message);
                                    break;

                                case SEND_UDP_SOCKET:
                                    sendUdpSocketReception(message);
                                    break;

                                case STOP_UDP_CLIENT_SOCKET:
                                    stopUdpClientSocketReception(message);
                                    break;

                                case GET_UDP_CLIENTS_SOCKET:
                                    getUdpClientsSocketReception(message);
                                    break;

                                default:
                                    invalidMessage(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            invalidMessage(message);
                        }
                    }
                });
            }
        };
    }
}
