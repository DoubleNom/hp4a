package a.humanitas.hp4a;

import java.util.ArrayList;
import java.util.Iterator;

public class StoredMessages {

    private ArrayList<StoredMessage> messages;

    StoredMessages() {
        messages = new ArrayList<>();
    }

    StoredMessage getByRequestId(String requestId) {
        StoredMessage message = null;
        for (StoredMessage msg: messages) {
            if(requestId.equals(msg.requestId)) {
                message = msg;
                break;
            }
        }
        return message;
    }

    void add(StoredMessage message) {
        messages.add(message);
    }

    void remove(StoredMessage message) {
        messages.remove(message);
    }

    void removeFrom(String address, int port) {
        for (Iterator<StoredMessage> i = messages.iterator(); i.hasNext();) {
            StoredMessage message = i.next();
            if(address.equals(message.address) && port == message.port) {
                i.remove();
            }
        }
    }

    Iterator<StoredMessage> iterator() {
        return messages.iterator();
    }
}
