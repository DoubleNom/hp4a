package a.humanitas.hp4a;

import android.util.Log;

import java.util.Iterator;

import static a.humanitas.hp4a.Config.TAG;

class TcpClients {

    private ClientsData clients;
    private StoredMessages storedMessages;
    private StoredMessages sendMessages;

    interface Callback {
        void createClient(String address, int port, int routingMethod);
        void clientLost(String address, int port);
        String send(String sessionId, String message);
        String sendFile(String sessionId, String filepath);
        void sent(String address, int port, String message);
        void sentFile(String address, int port, String filepath);
    }
    private Callback callback;

    TcpClients(Callback callback) {
        clients = new ClientsData();
        storedMessages = new StoredMessages();
        sendMessages = new StoredMessages();
        this.callback = callback;
    }

    void send(String target, int port, String message) {
        String sessionId = clients.getId(target, port);

        StoredMessage storedMessage = new StoredMessage(target, message);
        storedMessage.port = port;
        storedMessage.clientId = sessionId;
        storedMessage.messageKind = StoredMessage.MESSAGE;

        if (sessionId == null) {
            requestClientCreation(target, port);
            storedMessages.add(storedMessage);
        }
        else if (sessionId.equals("")) {
            storedMessages.add(storedMessage);
        }
        else {
            storedMessage.requestId = callback.send(sessionId, message);
            sendMessages.add(storedMessage);
        }
    }

    void sendFile(String target, int port, String filepath) {
        String sessionId = clients.getId(target, port);

        StoredMessage storedMessage = new StoredMessage(target, filepath);
        storedMessage.port = port;
        storedMessage.clientId = sessionId;
        storedMessage.messageKind = StoredMessage.FILE;

        if (sessionId == null) {
            requestClientCreation(target, port);
            storedMessages.add(storedMessage);
        }
        else if (sessionId.equals("")) {
            storedMessages.add(storedMessage);
        }
        else {
            storedMessage.requestId = callback.sendFile(sessionId, filepath);
            sendMessages.add(storedMessage);
        }
    }

    private void requestClientCreation(String target, int port) {
        if (!clients.add(target, port)) return;
        callback.createClient(target, port, 0);
        removePreviousMessages(target, port);
    }

    private void removePreviousMessages(String address, int port) {
        storedMessages.removeFrom(address, port);
        sendMessages.removeFrom(address, port);
    }

    void replyCreateTcpClient(String requestId, String serverAddress, int port, String sessionId, boolean accepted) {
        if(!accepted) {
            Log.d(TAG, "replyCreateTcpClient: " + serverAddress + " failure");
            clients.remove(serverAddress, port);
            callback.clientLost(serverAddress, port);
        } else {
            clients.setId(serverAddress, port, sessionId);
        }
    }

    void replyTcpSessionEstablished(String requestId, String serverAddress, int port, String sessionId, boolean created) {
        if (!created) {
            Log.d(TAG, "replyTcpSessionEstablished: " + serverAddress + " failure");
            clients.remove(serverAddress, port);
            callback.clientLost(serverAddress, port);
        } else {
            clients.setId(serverAddress, port, sessionId);

            // send stored message
            for (Iterator<StoredMessage> i = storedMessages.iterator(); i.hasNext();) {
                StoredMessage message = i.next();
                if (serverAddress.equals(message.address) && port == message.port) {
                    if (StoredMessage.MESSAGE == message.messageKind)
                        send(message.address, message.port, message.message);
                    else if (StoredMessage.FILE == message.messageKind)
                        sendFile(message.address, message.port, message.message);
                    i.remove();
                }
            }
        }
    }

    void replySendTcp(String requestId, String sessionId, String messageId, boolean accepted) {

        StoredMessage message = sendMessages.getByRequestId(requestId);
        if (message == null) {
            Log.d(TAG, "replySendUdp: message unknown");
            return;
        }
        if (!accepted) {
            sendMessages.remove(message);
        } else {
            message.requestId = requestId;
            message.messageId = messageId;
        }

    }

    void replySendTcpFile(String requestId, String sessionId, String messageId, boolean accepted) {

        StoredMessage message = sendMessages.getByRequestId(requestId);
        if (message == null) {
            Log.d(TAG, "replySendUdp: message unknown");
            return;
        }
        if (!accepted) {
            sendMessages.remove(message);
        } else {
            message.requestId = requestId;
            message.messageId = messageId;
        }

    }

    void replyTcpMessageSent(String requestId, String sessionId, String messageId, boolean sent) {

        StoredMessage message = sendMessages.getByRequestId(requestId);
        if (message == null) {
            Log.d(TAG, "replySendUdp: message unknown");
            return;
        }
        if (!sent) {
            Log.d(TAG, "replyTcpMessageSent: " + sessionId + " : " + messageId + " failed");
        }
        else if (message.messageKind == StoredMessage.MESSAGE) {
            callback.sent(message.address, message.port, message.message);
        } else {
            callback.sentFile(message.address, message.port, message.message);
        }

        sendMessages.remove(message);
    }

    void replyTcpSessionAbort(String requestId, String serverAddress, int serverPort, String sessionId, int error) {
        clients.remove(serverAddress, serverPort);
        callback.clientLost(serverAddress, serverPort);
    }

    void replyStopTcpClient(String requestId, String sessionId, boolean stopped) {
        clients.removeFromId(sessionId);
    }
}
