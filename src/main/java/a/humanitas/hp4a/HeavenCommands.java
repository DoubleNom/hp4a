package a.humanitas.hp4a;

public class HeavenCommands {

//* HEAVEN *****************************************************************************************
    public static final String GET_HEAVEN_ADDRESS = "get_heaven_address";
    public static final String STOP_HEAVEN = "stop_heaven";

//* NETWORK **************************
    public static final String GET_PEERS = "get_peers";
    public static final String GET_PEERS_UPDATES = "get_peers_updates";
    public static final String STOP_PEERS_UPDATE = "stop_peers_updates";
    public static final String GET_NEIGHBORS = "get_neighbors";
    public static final String GET_LSP = "get_lsp";
    public static final String GET_ROUTING_TABLE = "get_routing_table";
    public static final String GET_ROUTING_TREES = "get_routing_trees";
    public static final String GET_FULL_NETWORK = "get_full_network";
    public static final String BLOCK_NEIGHBOR = "block_neighbor";
    public static final String UNBLOCK_NEIGHBOR = "unblock_neighbor";
    public static final String GET_PEER_IP = "get_peer_ip";
    public static final String GET_MY_IP_FOR_PEER = "get_my_ip_for_peer";

//* TCP SERVER
    public static final String CREATE_TCP_SERVER = "create_tcp_server";
    public static final String STOP_TCP_SERVER = "stop_tcp_server";
    public static final String GET_TCP_SERVERS = "get_tcp_servers";

//* TCP SERVER SOCKET
    public static final String CREATE_TCP_SERVER_SOCKET = "create_tcp_server_socket";
    public static final String STOP_TCP_SERVER_SOCKET = "stop_tcp_server_socket";
    public static final String GET_TCP_SERVERS_SOCKET = "get_tcp_servers_socket";

//* TCP CLIENT
    public static final String CREATE_TCP_CLIENT = "create_tcp_client";
    public static final String SEND_TCP = "send_tcp";
    public static final String SEND_TCP_FILE = "send_tcp_file";
    public static final String STOP_TCP_CLIENT = "stop_tcp_client";
    public static final String GET_TCP_CLIENTS = "get_tcp_clients";

//* TCP CLIENT SOCKET
    public static final String SEND_TCP_SOCKET = "send_tcp_socket";
    public static final String SEND_TCP_FILE_SOCKET = "send_tcp_file_socket";

//* UDP SERVER
    public static final String CREATE_UDP_SERVER = "create_udp_server";
    public static final String STOP_UDP_SERVER = "stop_udp_server";
    public static final String GET_UDP_SERVERS = "get_udp_servers";

//* UDP SERVER SOCKET
    public static final String CREATE_UDP_SERVER_SOCKET = "create_udp_server_socket";
    public static final String STOP_UDP_SERVER_SOCKET = "stop_udp_server_socket";
    public static final String GET_UDP_SERVERS_SOCKET = "get_udp_servers_socket";

//* UDP CLIENT
    public static final String CREATE_UDP_CLIENT = "create_udp_client";
    public static final String SEND_UDP = "send_udp";
    public static final String STOP_UDP_CLIENT = "stop_udp_client";
    public static final String GET_UDP_CLIENTS = "get_udp_clients";

//* UDP CLIENT SOCKET
    public static final String CREATE_UDP_CLIENT_SOCKET = "create_udp_client_socket";
    public static final String SEND_UDP_SOCKET = "send_udp_socket";
    public static final String STOP_UDP_CLIENT_SOCKET = "stop_udp_client_socket";
    public static final String GET_UDP_CLIENTS_SOCKET = "get_udp_clients_socket";


    public static final String[] commands = {
            // HEAVEN
            GET_HEAVEN_ADDRESS,
            STOP_HEAVEN,

            // NETWORK
            GET_PEERS,
            GET_PEERS_UPDATES,
            STOP_PEERS_UPDATE,
            GET_NEIGHBORS,
            GET_LSP,
            GET_ROUTING_TABLE,
            GET_ROUTING_TREES,
            GET_FULL_NETWORK,
            BLOCK_NEIGHBOR,
            UNBLOCK_NEIGHBOR,
            GET_MY_IP_FOR_PEER,
            GET_PEER_IP,

            // TCP SERVER
            CREATE_TCP_SERVER,
            STOP_TCP_SERVER,
            GET_TCP_SERVERS,

            // TCP SERVER SOCKET
            CREATE_TCP_SERVER_SOCKET,
            STOP_TCP_SERVER_SOCKET,
            GET_TCP_SERVERS_SOCKET,

            // TCP CLIENT
            CREATE_TCP_CLIENT,
            SEND_TCP,
            SEND_TCP_FILE,
            STOP_TCP_CLIENT,
            GET_TCP_CLIENTS,

            // TCP CLIENT SOCKET
            SEND_TCP_SOCKET,
            SEND_TCP_FILE_SOCKET,

            // UDP SERVER
            CREATE_UDP_SERVER,
            STOP_UDP_SERVER,
            GET_UDP_SERVERS,

            // UDP SERVER SOCKET
            CREATE_UDP_SERVER_SOCKET,
            STOP_UDP_SERVER_SOCKET,
            GET_UDP_SERVERS_SOCKET,

            // UDP CLIENT
            CREATE_UDP_CLIENT,
            SEND_UDP,
            STOP_UDP_CLIENT,
            GET_UDP_CLIENTS,

            // UDP CLIENT SOCKET
            CREATE_UDP_CLIENT_SOCKET,
            SEND_UDP_SOCKET,
            STOP_UDP_CLIENT_SOCKET,
            GET_UDP_CLIENTS_SOCKET
    };
}
