package a.humanitas.hp4a;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class PythonBridge {
    public static final String TAG = "Python";
    public static String testBridge() {
        return "Hello from Android";
    }

    public static String startService(Context context) {
        Log.d(TAG, "startService: starting");
        context.startService(new Intent(context, PythonService.class));
        Log.d(TAG, "startService: started");
        return "Heaven started?";
    }

}
