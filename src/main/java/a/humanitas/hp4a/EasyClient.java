package a.humanitas.hp4a;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import static a.humanitas.hp4a.Config.TAG;
import static a.humanitas.hp4a.EasyClient.Commands.*;

public class EasyClient extends Client {

    public class Commands {
        public static final String UNICAST_TCP = "unicast_tcp";
        public static final String UNICAST_TCP_SOCKET = "unicast_tcp_socket";
        public static final String UNICAST_UDP = "unicast_udp";
        public static final String UNICAST_UDP_SOCKET = "unicast_udp_socket";
        public static final String BROADCAST_UDP = "broadcast_udp";
        public static final String BROADCAST_UDP_SOCKET = "broadcast_udp_socket";
    }

    private EasyClient thisClient = this;
    private Context context;
    private EasyClient easyClient;
    private EasyHeavenReplies easyHeavenReplies;
    private HeavenReplies heavenReplies; {
        heavenReplies = new HeavenReplies() {
            @Override
            public void replyGetHeavenAddress(String requestId, String address, String errorDescription) {
                easyHeavenReplies.replyGetHeavenAddress(requestId, address, errorDescription);
                hasHeavenAddress = true;
            }

            @Override
            public void replyGetPeers(String requestId, ArrayList<String> peers, String errorDescription) {
                easyHeavenReplies.replyGetPeers(requestId, peers, errorDescription);
            }

            @Override
            public void replyGetPeersUpdate(String requestId, boolean subscription, String subscriptionId, String errorDescription) {
                if(subscription) {
                    subscribedToPeersUpdate = true;
                    thisClient.subscriptionId = subscriptionId;
                }
                easyHeavenReplies.replyGetPeersUpdate(requestId, subscription, subscriptionId, errorDescription);
            }


            @Override
            public void replyStopPeersUpdate(String requestId, boolean unsubscription, String subscriptionId, String errorDescription) {
                thisClient.subscriptionId = null;
                subscribedToPeersUpdate = false;
                easyHeavenReplies.replyStopPeersUpdate(requestId, unsubscription, subscriptionId, errorDescription);
            }

            @Override
            public void replyGetNeighbors(String requestId, ArrayList<String> neighbors, String errorDescription) {
                easyHeavenReplies.replyGetNeighbors(requestId, neighbors, errorDescription);
            }

            @Override
            public void replyGetLsp(String requestId, String lsp, String errorDescription) {
                easyHeavenReplies.replyGetLsp(requestId, lsp, errorDescription);
            }

            @Override
            public void replyGetRoutingTable(String requestId, String routingTable, String errorDescription) {
                easyHeavenReplies.replyGetRoutingTable(requestId, routingTable, errorDescription);
            }

            @Override
            public void replyGetRoutingTrees(String requestId, String routingTrees, String errorDescription) {
                easyHeavenReplies.replyGetRoutingTrees(requestId, routingTrees, errorDescription);
            }

            @Override
            public void replyGetFullNetwork(String requestId, String fullNetwork, String errorDescription) {
                easyHeavenReplies.replyGetFullNetwork(requestId, fullNetwork, errorDescription);
            }

            @Override
            public void replyBlockNeighbor(String requestId, String neighborAddress, boolean blocked, String errorDescription) {
                easyHeavenReplies.replyBlockNeighbor(requestId, neighborAddress, blocked, errorDescription);
            }

            @Override
            public void replyUnblockNeighbor(String requestId, String neighborAddress, boolean unblocked, String errorDescription) {
                easyHeavenReplies.replyUnblockNeighbor(requestId, neighborAddress, unblocked, errorDescription);
            }

            @Override
            public void replyGetPeerIp(String requestId, String peerAddress, String peerIp, String errorDescription) {
                easyHeavenReplies.replyGetPeerIp(requestId, peerAddress, peerIp, errorDescription);
            }

            @Override
            public void replyGetMyIpForPeer(String requestId, String peerAddress, String myIp, String errorDescription) {
                easyHeavenReplies.replyGetMyIpForPeer(requestId, peerAddress, myIp, errorDescription);
            }

            @Override
            public void replyCreateTcpServer(String requestId, int port, boolean created, String errorDescription) {
                if (port == Config.DEFAULT_TCP_PORT && created) {
                    tcpServerCreated = true;
                }
                easyHeavenReplies.replyCreateTcpServer(requestId, port, created, errorDescription);
            }

            @Override
            public void replyTcpServerReceivedMessage(
                    String requestId,
                    int port,
                    String source,
                    String sessionId,
                    String message,
                    int messageLength,
                    long elapsedTime,
                    int bps,
                    String errorDescription) {
                easyHeavenReplies.messageReception(source, port, message);
            }

            @Override
            public void replyTcpServerReceivedFile(
                    String requestId,
                    int port,
                    String source,
                    String sessionId,
                    String filePath,
                    int fileSize,
                    long elapsedTime,
                    int bps,
                    String errorDescription) {
                easyHeavenReplies.fileReception(source, port, filePath);
            }

            @Override
            public void replyStopTcpServer(
                    String requestId,
                    int port,
                    boolean stopped,
                    String errorDescription) {
                easyHeavenReplies.replyStopTcpServer(requestId, port, stopped, errorDescription);
            }

            @Override
            public void replyGetTcpServers(String requestId, ArrayList<String> servers, String errorDescription) {
                easyHeavenReplies.replyGetTcpServers(requestId, servers, errorDescription);
            }

            @Override
            public void replyCreateTcpServerSocket(String requestId, int port, boolean created, String errorDescription) {
                Log.d(TAG, "replyCreateTcpServerSocket: ");
                if (port == Config.DEFAULT_TCP_SOCKET_PORT && created) {
                    tcpServerSocketCreated = true;
                }
                easyHeavenReplies.replyCreateTcpServerSocket(requestId, port, created, errorDescription);
            }

            @Override
            public void replyTcpServerSocketReceivedMessage(String requestId, int port, String source, String sessionId, String message, int messageLength, long elapsedTime, int bps, String errorDescription) {
                Log.d(TAG, "replyTcpServerSocketReceivedMessage: ");
                easyHeavenReplies.messageReception(source, port, message);
            }

            @Override
            public void replyTcpServerSocketReceivedFile(String requestId, int port, String source, String sessionId, String filePath, int fileSize, long elapsedTime, int bps, String errorDescription) {
                Log.d(TAG, "replyTcpServerSocketReceivedFile: ");
                easyHeavenReplies.fileReception(source, port, filePath);
            }

            @Override
            public void replyStopTcpServerSocket(String requestId, int port, boolean stopped, String errorDescription) {
                Log.d(TAG, "replyStopTcpServerSocket: ");
                easyHeavenReplies.replyStopTcpServerSocket(requestId, port, stopped, errorDescription);
            }

            @Override
            public void replyGetTcpServersSocket(String requestId, ArrayList<String> servers, String errorDescription) {
                Log.d(TAG, "replyGetTcpServersSocket: ");
                easyHeavenReplies.replyGetTcpServersSocket(requestId, servers, errorDescription);
            }

            @Override
            public void replyCreateTcpClient(String requestId, String serverAddress, int port, String sessionId, boolean accepted, String errorDescription) {
                Log.d(TAG, "replyCreateTcpClient: ");
                tcpClients.replyCreateTcpClient(requestId, serverAddress, port, sessionId, accepted);
                easyHeavenReplies.replyCreateTcpClient(requestId, serverAddress, port, sessionId, accepted, errorDescription);
            }

            @Override
            public void replyTcpSessionEstablished(String requestId, String serverAddress, int port, String sessionId, boolean created, String errorDescription) {
                tcpClients.replyTcpSessionEstablished(requestId, serverAddress, port, sessionId, created);
                easyHeavenReplies.replyTcpSessionEstablished(requestId, serverAddress, port, sessionId, created, errorDescription);
            }

            @Override
            public void replySendTcp(String requestId, String sessionId, String messageId, boolean accepted, String errorDescription) {
                tcpClients.replySendTcp(requestId, sessionId, messageId, accepted);
                easyHeavenReplies.replySendTcp(requestId, sessionId, messageId, accepted, errorDescription);
            }

            @Override
            public void replySendTcpFile(String requestId, String sessionId, String messageId, boolean accepted, String errorDescription) {
                tcpClients.replySendTcpFile(requestId, sessionId, messageId, accepted);
                easyHeavenReplies.replySendTcpFile(requestId, sessionId, messageId, accepted, errorDescription);
            }

            @Override
            public void replyTcpSessionAbort(String requestId, String serverAddress, int serverPort, String sessionId, int error, String errorDescription) {
                tcpClients.replyTcpSessionAbort(requestId, serverAddress, serverPort, sessionId, error);
                easyHeavenReplies.replyTcpSessionAbort(requestId, serverAddress, serverPort, sessionId, error, errorDescription);
            }

            @Override
            public void replyTcpMessageSent(String requestId, String sessionId, String messageId, boolean sent, int fileSize, long elapsedTime, int bps, String errorDescription) {
                tcpClients.replyTcpMessageSent(requestId, sessionId, messageId, sent);
                easyHeavenReplies.replyTcpMessageSent(requestId, sessionId, messageId, sent, fileSize, elapsedTime, bps, errorDescription);
            }

            @Override
            public void replyStopTcpClient(String requestId, String sessionId, boolean stopped, String errorDescription) {
                tcpClients.replyStopTcpClient(requestId, sessionId, stopped);
                easyHeavenReplies.replyStopTcpClient(requestId, sessionId, stopped, errorDescription);
            }

            @Override
            public void replyGetTcpClients(String requestId, ArrayList<String> clients, String errorDescription) {
                Log.d(TAG, "replyGetTcpClients: " + Arrays.toString(clients.toArray()));
                easyHeavenReplies.replyGetTcpClients(requestId, clients, errorDescription);
            }

            @Override
            public void replySendTcpSocket(String requestId, String sessionId, boolean accepted, String errorDescription) {
                Log.d(TAG, "replySendTcpSocket: ");
                tcpClientsSocket.replySendTcpSocket(requestId, sessionId, accepted);
                easyHeavenReplies.replySendTcpSocket(requestId, sessionId, accepted, errorDescription);
            }

            @Override
            public void replyTcpSocketMessageSent(String requestId, String sessionId, boolean sent, int fileSize, long elapsedTime, int bps, String errorDescription) {
                Log.d(TAG, "replyTcpSocketMessageSent: ");
                tcpClientsSocket.replyTcpSocketMessageSent(requestId, sessionId, sent, fileSize, elapsedTime, bps);
                easyHeavenReplies.replyTcpSocketMessageSent(requestId, sessionId, sent, fileSize, elapsedTime, bps, errorDescription);
            }

            @Override
            public void replyTcpSocketError(String requestId, String serverAddress, int serverPort, String sessionId, int error, String errorDescription) {
                Log.d(TAG, "replyTcpSocketError: ");
                tcpClientsSocket.replyTcpSocketError(requestId, serverAddress, serverPort, sessionId, error);
                easyHeavenReplies.replyTcpSocketError(requestId, serverAddress, serverPort, sessionId, error, errorDescription);
            }

            @Override
            public void replyCreateUdpServer(String requestId, int port, boolean created, String errorDescription) {
                if (port == Config.DEFAULT_UDP_PORT) {
                    udpServerCreated = true;
                }
                easyHeavenReplies.replyCreateUdpServer(requestId, port, created, errorDescription);
            }

            @Override
            public void replyUdpServerReceivedMessage(String requestId, int port, String source, int sourcePort, String message, String errorDescription) {
                easyHeavenReplies.messageReception(source, port, message);
            }

            @Override
            public void replyStopUdpServer(String requestId, int port, boolean stopped, String errorDescription) {
                Log.d(TAG, "replyStopUdpServer: ");
                easyHeavenReplies.replyStopUdpServer(requestId, port, stopped, errorDescription);
            }

            @Override
            public void replyGetUdpServers(String requestId, ArrayList<String> servers, String errorDescription) {
                Log.d(TAG, "replyGetUdpServers: " + Arrays.toString(servers.toArray()));
                easyHeavenReplies.replyGetUdpServers(requestId, servers, errorDescription);
            }

            @Override
            public void replyCreateUdpServerSocket(String requestId, int port, boolean created, String errorDescription) {
                Log.d(TAG, "replyCreateUdpServerSocket: ");
                if (port == Config.DEFAULT_UDP_SOCKET_PORT && created) {
                    udpServerSocketCreated = true;
                }
                easyHeavenReplies.replyCreateUdpServerSocket(requestId, port, created, errorDescription);
            }

            @Override
            public void replyUdpServerSocketReceivedMessage(String requestId, int port, String source, int sourcePort, String message, String errorDescription) {
                Log.d(TAG, "replyUdpServerSocketReceivedMessage: ");
                easyHeavenReplies.messageReception(source, port, message);
            }

            @Override
            public void replyStopUdpServerSocket(String requestId, int port, boolean stopped, String errorDescription) {
                Log.d(TAG, "replyStopUdpServerSocket: ");
                easyHeavenReplies.replyStopUdpServerSocket(requestId, port, stopped, errorDescription);
            }

            @Override
            public void replyGetUdpServersSocket(String requestId, ArrayList<String> servers, String errorDescription) {
                Log.d(TAG, "replyGetUdpServersSocket: ");
                easyHeavenReplies.replyGetUdpServersSocket(requestId, servers, errorDescription);

            }

            @Override
            public void replyCreateUdpClient(String requestId, String serverAddress, int serverPort, String sourcePort, boolean created, String errorDescription) {
                udpClients.replyCreateUdpClient(requestId, serverAddress, serverPort, sourcePort, created);
                easyHeavenReplies.replyCreateUdpClient(requestId, serverAddress, serverPort, sourcePort, created, errorDescription);
            }

            @Override
            public void replySendUdp(String requestId, String sourcePort, boolean sent, String errorDescription) {
                udpClients.replySendUdp(requestId, sourcePort, sent);
                easyHeavenReplies.replySendUdp(requestId, sourcePort, sent, errorDescription);
            }

            @Override
            public void replyStopUdpClient(String requestId, String sourcePort, boolean stopped, String errorDescription) {
                udpClients.replyStopUdpClient(requestId, sourcePort, stopped);
                easyHeavenReplies.replyStopUdpClient(requestId, sourcePort, stopped, errorDescription);
            }

            @Override
            public void replyGetUdpClients(String requestId, ArrayList<String> clients, String errorDescription) {
                Log.d(TAG, "replyGetUdpClients: " + Arrays.toString(clients.toArray()));
                easyHeavenReplies.replyGetUdpClients(requestId, clients, errorDescription);
            }

            @Override
            public void replyCreateUdpClientSocket(String requestId, String serverAddress, int serverPort, String sourcePort, boolean created, String errorDescription) {
                Log.d(TAG, "replyCreateUdpClientSocket: ");
                udpClientsSocket.replyCreateUdpClientSocket(requestId, serverAddress, serverPort, sourcePort, created);
                easyHeavenReplies.replyCreateUdpClientSocket(requestId, serverAddress, serverPort, sourcePort, created, errorDescription);
            }

            @Override
            public void replySendUdpSocket(String requestId, String sourcePort, boolean sent, String errorDescription) {
                Log.d(TAG, "replySendUdpSocket: ");
                udpClientsSocket.replySendUdpSocket(requestId, sourcePort, sent);
                easyHeavenReplies.replySendUdpSocket(requestId, sourcePort, sent, errorDescription);
            }

            @Override
            public void replyStopUdpClientSocket(String requestId, String sourcePort, boolean stopped, String errorDescription) {
                Log.d(TAG, "replyStopUdpClientSocket: ");
                easyHeavenReplies.replyStopUdpClientSocket(requestId, sourcePort, stopped, errorDescription);
            }

            @Override
            public void replyGetUdpClientsSocket(String requestId, ArrayList<String> clients, String errorDescription) {
                Log.d(TAG, "replyGetUdpClientsSocket: ");

            }
        };
    }


    private boolean hasHeavenAddress;
    private boolean subscribedToPeersUpdate;
    private String subscriptionId;

    private boolean tcpServerCreated;
    private TcpClients tcpClients;
    private TcpClients.Callback tcpClientsCallback = new TcpClients.Callback() {
        @Override
        public void createClient(String target, int port, int routingMethod) {
            easyClient.createTcpClient(target, port, routingMethod);
        }

        @Override
        public void clientLost(String address, int port) {

        }

        @Override
        public String send(String sessionId, String message) {
            return easyClient.sendTcp(sessionId, message);
        }

        @Override
        public String sendFile(String sessionId, String filepath) {
            return easyClient.sendTcpFile(sessionId, Config.DELETE_AFTER_SEND, filepath);
        }

        @Override
        public void sent(String address, int port, String message) {
            easyHeavenReplies.messageSent(address, port, message);
        }

        @Override
        public void sentFile(String address, int port, String filepath) {
            easyHeavenReplies.fileSent(address, port, filepath);
        }
    };

    private boolean tcpServerSocketCreated;
    private TcpClientsSocket tcpClientsSocket;
    private TcpClientsSocket.Callback tcpClientsSocketCallback = new TcpClientsSocket.Callback() {
        @Override
        public String send(String target, int port, String message) {
            return sendTcpSocket(target, port , message);
        }

        @Override
        public String sendFile(String target, int port,  String filePath) {
            return sendTcpFileSocket(target, port, false, filePath);
        }

        @Override
        public void sent(String address, int port, String message) {
            easyHeavenReplies.messageSent(address, port, message);
        }

        @Override
        public void sentFile(String address, int port, String filePath) {
            easyHeavenReplies.fileSent(address, port, filePath);
        }
    };

    private boolean udpServerCreated;
    private UdpClients udpClients;
    private UdpClients.Callback udpClientsCallback = new UdpClients.Callback() {
        @Override
        public void createClient(String target, int port) {
            easyClient.createUdpClient(target, port);
        }

        @Override
        public String send(String sourcePort, String message) {
            return easyClient.sendUdp(Integer.parseInt(sourcePort), message);
        }

        @Override
        public void sent(String address, int port, String message) {
            easyHeavenReplies.messageSent(address, port, message);
        }
    };

    private boolean udpServerSocketCreated;
    private UdpClientsSocket udpClientsSocket;
    private UdpClientsSocket.Callback udpClientsSocketCallback = new UdpClientsSocket.Callback() {
        @Override
        public void createClient(String target, int port) {
            easyClient.createUdpClientSocket(target, port);
        }

        @Override
        public String send(String clientId, String message) {
            return easyClient.sendUdpSocket(clientId, message);
        }

        @Override
        public void sent(String address, int port, String message) {
            easyHeavenReplies.messageSent(address, port, message);
        }
    };

    private String[] commands = new String[] {
            UNICAST_TCP,
            UNICAST_UDP,
            BROADCAST_UDP,
            UNICAST_TCP_SOCKET,
            UNICAST_UDP_SOCKET,
            BROADCAST_UDP_SOCKET
    };
    private String[] easyCommands;


    public EasyClient(Context context, EasyHeavenReplies easyHeavenReplies) {
        super();
        this.context = context;
        setHeavenReplies(heavenReplies);
        this.easyHeavenReplies = easyHeavenReplies;

        hasHeavenAddress = false;
        subscribedToPeersUpdate = false;

        tcpServerCreated = false;
        tcpClients = new TcpClients(tcpClientsCallback);

        tcpServerSocketCreated = false;
        tcpClientsSocket = new TcpClientsSocket(tcpClientsSocketCallback);

        udpServerCreated = false;
        udpClients = new UdpClients(udpClientsCallback);

        udpServerSocketCreated = false;
        udpClientsSocket = new UdpClientsSocket(udpClientsSocketCallback);


        handler.post(initClient);

        easyClient = this;
    }

    private Runnable initClient = new Runnable() {
        @Override
        public void run() {
            boolean restart = false;

            if (!hasHeavenAddress) {
                getHeavenAddress();
                restart = true;
            }

            if (!tcpServerCreated) {
                createTcpServer();
                restart = true;
            }

            if (!tcpServerSocketCreated) {
                createTcpServerSocket();
                restart = true;
            }

            if (!udpServerCreated) {
                createUdpServer();
                restart = true;
            }

            if (!udpServerSocketCreated) {
                createUdpServerSocket();
                restart = true;
            }

            if (!subscribedToPeersUpdate) {
                getPeersUpdate();
                restart = true;
            }

            if (restart) handler.postDelayed(initClient, Config.INIT_RESTART_DELAY);
        }
    };

    @Override
    public String[] getCommands() {
        if (easyCommands == null) {
            easyCommands = new String[HeavenCommands.commands.length + commands.length];
            System.arraycopy(
                    HeavenCommands.commands,
                    0,
                    easyCommands,
                    0,
                    HeavenCommands.commands.length);
            System.arraycopy(
                    commands,
                    0,
                    easyCommands,
                    HeavenCommands.commands.length,
                    commands.length);
        }

        return easyCommands;
    }

    public void startHeaven() {
        context.startService(new Intent(
                PythonService.ACTION_START_HEAVEN, null,
                context, PythonService.class));
    }

    /* TCP */
    public void createTcpServer() {
        createTcpServer(Config.DEFAULT_TCP_PORT);
    }

    public void createTcpClient(String serverHeavenAddress, int port) {
        createTcpClient(serverHeavenAddress, port, Config.DEFAULT_ROUTING_METHOD);
    }

    public void unicastTcp(String target, String message) {
        tcpClients.send(target, Config.DEFAULT_TCP_PORT, message);
    }

    public void unicastTcp(String target, int port, String message) {
        tcpClients.send(target, port, message);
    }

    public void unicastTcpFile(String target, String filepath) {
        tcpClients.sendFile(target, Config.DEFAULT_TCP_PORT, filepath);
    }

    public void unicastTcpFile(String target, int port, String filepath) {
        tcpClients.sendFile(target, port, filepath);
    }


    /* UDP */
    private void createUdpServer() {
        createUdpServer(Config.DEFAULT_UDP_PORT);
    }

    void setDefaultUdpClientMtu(int mtu) {
        Config.DEFAULT_UDP_MTU = mtu;
    }

    void setDefaultUdpClientMaxHop(int maxHop) {
        Config.DEFAULT_UDP_MAX_HOP = maxHop;
    }

    void setDefaultUdpClientPacketRate(int packetRate) {
        Config.DEFAULT_UDP_PACKET_RATE = packetRate;
    }

    void setDefaultUdpClientRoutingMethod(int method) {
        Config.DEFAULT_ROUTING_METHOD = method;
    }

    private void createUdpClient(String serverHeavenAddress, int port) {
        createUdpClient(serverHeavenAddress,
                port,
                Config.DEFAULT_UDP_MTU,
                Config.DEFAULT_UDP_MAX_HOP,
                Config.DEFAULT_UDP_PACKET_RATE,
                Config.DEFAULT_ROUTING_METHOD);
    }

    public void unicastUdp(String target, String message) {
        udpClients.send(target, Config.DEFAULT_UDP_PORT, message);
    }

    public void unicastUdp(String target, int port, String message) {
        udpClients.send(target, port, message);
    }

    void broadcastUdp(String message) {
        udpClients.send(Config.HEAVEN_BROADCAST_ADDRESS, Config.DEFAULT_UDP_PORT, message);
    }

    void broadcastUdp(int port, String message) {
        udpClients.send(Config.HEAVEN_BROADCAST_ADDRESS, port, message);
    }


    /* TCP SOCKET */
    public void createTcpServerSocket() {
        createTcpServerSocket(Config.DEFAULT_TCP_SOCKET_PORT);
    }

    public void unicastTcpSocket(String target, String message) {
        tcpClientsSocket.send(target, Config.DEFAULT_TCP_SOCKET_PORT, message);
    }

    public void unicastTcpSocket(String target, int port, String message) {
        tcpClientsSocket.send(target, port, message);
    }

    public void unicastTcpSocketFile(String target, String filePath) {
        tcpClientsSocket.sendFile(target, Config.DEFAULT_TCP_SOCKET_PORT, filePath);
    }

    public void unicastTcpSocketFile(String target, int port, String filePath) {
        tcpClientsSocket.sendFile(target, port, filePath);
    }


    /* UDP SOCKET */
    public void createUdpServerSocket() {
        createUdpServerSocket(
                Config.DEFAULT_UDP_SOCKET_PORT,
                Config.DEFAULT_UDP_SOCKET_BUFFER_SIZE,
                Config.DEFAULT_UDP_SOCKET_QUEUE_LEN
        );
    }

    public void createUdpServerSocket(int port) {
        createUdpServerSocket(
                port,
                Config.DEFAULT_UDP_SOCKET_BUFFER_SIZE,
                Config.DEFAULT_UDP_SOCKET_QUEUE_LEN
        );
    }

    public void createUdpClientSocket(String serverAddress, int port) {
        createUdpClientSocket(
                serverAddress,
                port,
                Config.DEFAULT_UDP_SOCKET_TIMEOUT
        );
    }

    public void unicastUdpSocket(String target, String message) {
        udpClientsSocket.send(target, Config.DEFAULT_UDP_SOCKET_PORT, message);
    }
    public void unicastUdpSocket(String target, int port, String message) {
        udpClientsSocket.send(target, port, message);
    }
}
