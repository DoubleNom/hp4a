package a.humanitas.hp4a;

public class StoredMessage {
    public static final int MESSAGE = 0;
    public static final int FILE = 1;

    String address;
    String clientId;
    int port;
    String message;
    String messageId;
    String requestId;
    int messageKind;

    StoredMessage(String address, String message) {
        this.address = address;
        this.message = message;
    }

    StoredMessage(String address, String message, int kind) {
        this(address, message);
        messageKind = kind;
    }

}
