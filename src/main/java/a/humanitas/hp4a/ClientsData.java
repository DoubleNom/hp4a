package a.humanitas.hp4a;

import java.util.ArrayList;

public class ClientsData {
    private ArrayList<ClientData> clients;

    ClientsData() {
        clients = new ArrayList<>();
    }

    boolean add(String address, int port) {
        if (hasClient(address, port)) return false;
        clients.add(new ClientData(address, port));
        return true;
    }

    void setId(String address, int port, String id) {
        if (address == null || port == 0 || id == null) return;
        for (ClientData client: clients) {
            if (address.equals(client.address) && port == client.port) {
                client.id = id;
                return;
            }
        }
    }

    void remove(String address, int port) {
        if (address == null || port == 0) return;
        for(ClientData client: clients) {
            if (address.equals(client.address) && port == client.port) {
                clients.remove(client);
                break;
            }
        }
    }

    void removeFromId(String id) {
        if (id == null) return;
        for (ClientData client: clients) {
            if (id.equals(client.id)) {
                clients.remove(client);
                break;
            }
        }
    }

    boolean hasClient(String address, int port) {
        if (address == null || port == 0) return false;
        for(ClientData client: clients) {
            if (address.equals(client.address) && port == client.port)
                return true;
        }
        return false;
    }

    ClientData getClient(String address, String id, int port) {
        if (address != null) {
            for (ClientData client : clients) {
                if (address.equals(client.address) && port == client.port)
                    return client;
            }
        }
        else if (id != null) {
            for (ClientData client: clients) {
                if (id.equals(client.id) && port == client.port)
                    return client;
            }
        }
        return null;
    }

    String getId(String address, int port) {
        if (address == null || port == 0) return null;
        for(ClientData client: clients) {
            if (address.equals(client.address) && port == client.port)
                return client.id;
        }
        return null;
    }

    String getAddress(String id, int port) {
        if (id == null || port == 0) return null;
        for(ClientData client: clients) {
            if (id.equals(client.id) && port == client.port)
                return client.address;
        }
        return null;
    }
}
