package a.humanitas.hp4a;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;

import static a.humanitas.hp4a.PythonBridge.TAG;

/**
 * Handles the extraction of assets from an APK file.
 *
 * When the run() method is called, it compares the APK timestamp
 * with the timestamp in the extracted assets. If the APK is newer
 * (or there are no extracted assets), it extracts them to the
 * application's data folder and resets the extracted assets' timestamp.
 *
 * The APK is newer whenever there is a new installation or an update.
 *
 */
class AssetExtractor {
    private final Context context;
    private final String rootFolder;
    private final AssetManager mAssetManager;
    static final String mAssetModifiedPath = "/lastmodified.txt";


    AssetExtractor(Context context, String rootFolder) {
        this.context = context;
        this.rootFolder = rootFolder;
        mAssetManager = context.getAssets();
    }

    /* Sets the asset modification time  */
    private void setAssetLastModified(long time) {
        String filename = rootFolder + mAssetModifiedPath;
        try {
            BufferedWriter bwriter = new BufferedWriter(new FileWriter(new File(filename)));
            bwriter.write(String.format(Locale.ENGLISH, "%d", time));
            bwriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* Returns the asset modification time  */
    private long getAssetLastModified() {
        String filename = rootFolder + mAssetModifiedPath;
        try {
            BufferedReader breader = new BufferedReader(new FileReader(new File(filename)));
            String contents = breader.readLine();
            breader.close();
            return Long.valueOf(contents);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /*
     * Returns the time of this app's last update
     * Considers new installs and updates
     */
    private long getAppLastUpdate() {
        PackageManager pm = context.getPackageManager();
        try {
            PackageInfo pkgInfo = pm.getPackageInfo(context.getPackageName(),
                    PackageManager.GET_PERMISSIONS);
            return pkgInfo.lastUpdateTime;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    /* Recursively deletes the contents of a folder*/
    private void recursiveDelete(final File file) {
        if (file.isDirectory()) {
            if(file.listFiles() != null) {
                for (File f : file.listFiles())
                    recursiveDelete(f);
            }
        }
        if (!file.delete()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Could not erase file: " + file.getAbsolutePath());
                }
            });
        }
    }

    /**
     * Copy the asset at the specified path to this app's data directory. If the
     * asset is a directory, its contents are also copied.
     *
     * @param path
     * Path to asset, relative to app's assets directory.
     */
    private void copyAssets(String path) {
        // Ignore the following asset folders
        if (path.equals("images")
                || path.equals("sounds")
                || path.equals("webkit")
                || path.equals("databases") // Motorola
                || path.equals("kioskmode")) // Samsung
            return;
        try {
            String[] assetList = mAssetManager.list(path);
            if (assetList == null || assetList.length == 0)
                throw new IOException();

            // Make the directory.
            final File dir = new File(rootFolder, path);
            if (!dir.mkdirs()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Could not create dir: " + dir.getAbsolutePath());
                    }
                });
            }

            // Recurse on the contents.
            for (String entry : assetList) {
                if (path.isEmpty())
                    copyAssets(entry);
                else
                    copyAssets(path + "/" + entry);
            }
        } catch (IOException e) {
            copyFileAsset(path);
        }
    }

    /**
     * Copy the asset file specified by path to app's data directory. Assumes
     * parent directories have already been created.
     *
     * @param path
     * Path to asset, relative to app's assets directory.
     */
    private void copyFileAsset(String path) {
        File file = new File(rootFolder, path);
        try {
            InputStream in = mAssetManager.open(path);
            OutputStream out = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int read = in.read(buffer);
            while (read != -1) {
                out.write(buffer, 0, read);
                read = in.read(buffer);
            }
            out.close();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void run() {
        // Get the times of last modifications
        long appLastUpdate = this.getAppLastUpdate();
        long assetLastModified = this.getAssetLastModified();
        // TODO: Improve python modification detection
        // There is no need to erase python if we only change Android code
        // Use checksum on python folder?
        if (appLastUpdate > assetLastModified) {
            // Clear previous assets
            final File file = new File(rootFolder);
            this.recursiveDelete(file);
            if (!file.mkdirs()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Could not create dir: " + file.getAbsolutePath());
                    }
                });
            }
            // Extract new assets
            this.copyAssets("");
            // Update extract asset's timestamp
            this.setAssetLastModified(appLastUpdate);
        } //else {
            // Extracted assets are up to date
            //copyFileAsset("python/heaven_config.ini");
        //}
    }
}
