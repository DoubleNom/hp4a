package a.humanitas.hp4a;

public class ClientData {
    String address;
    int port;
    String id; // client_id / source_port / session_id

    ClientData(String address) {
        this.address = address;
    }

    ClientData(String address, int port) {
        this.address = address;
        this.port = port;
        this.id = "";
    }

    ClientData(String address, int port, String id) {
        this.address = address;
        this.port = port;
        this.id = id;
    }
}


