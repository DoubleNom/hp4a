#include <jni.h>
#include <string.h>
#include <Python.h>
#include <android/log.h>
#include "pyjni.h"

char *pTAG;

#define LOGI(...) \
  ((void)__android_log_print(ANDROID_LOG_INFO, pTAG, __VA_ARGS__))
#define LOGD(...) \
  ((void)__android_log_print(ANDROID_LOG_DEBUG, pTAG, __VA_ARGS__))

/**
 * Divert Python log to Android log
 */
static PyObject *androidembed_log(PyObject *self, PyObject *args) {
    char *logStr = NULL;

    if (!PyArg_ParseTuple(args, "s", &logStr)) {
        return NULL;
    }

    if(logStr[0] == 'y') Py_RETURN_NONE;

    LOGI("%s", logStr);
    Py_RETURN_NONE;
}


/**
 * Execute Python unix command into Android unix command
 */
static PyObject *androidembed_command(PyObject *self, PyObject *args) {

    FILE *fp;
    char buffer[PATH_MAX];
    char *result;
    int resultSize;

    char *arg;
    PyArg_ParseTuple(args, "s", &arg);

    fp = popen(arg, "r");
    if (fp == NULL) {
        return PyString_FromString("");
    }

    resultSize = PATH_MAX;
    result = calloc(PATH_MAX, sizeof(char));

    while(fgets(buffer, PATH_MAX, fp) != NULL) {
        if(strlen(result) + strlen(buffer) + 1 >= resultSize) {
            resultSize += PATH_MAX;
            result = (char *) realloc(result, sizeof(char*) * resultSize);
        }
        strcat(result, buffer);
    }

    return PyString_FromString(result);
}

/**
 * List AndroidEmbed methods for its module
 */
static PyMethodDef AndroidEmbedMethods[] = {
        {"log", androidembed_log, METH_VARARGS, "Log on android platform"},
        {"command", androidembed_command, METH_VARARGS, "Execute command"},
        {NULL, NULL, 0, NULL}
};

/**
 * Initialise AndroidEmbed module
 */
PyMODINIT_FUNC init_androidembed(void) {
    (void) Py_InitModule("androidembed", AndroidEmbedMethods);
    /* inject our bootstrap code to redirect python stdin/stdout */
    PyRun_SimpleString(
            "import sys\n" \
            "import androidembed\n" \
            "class LogFile(object):\n" \
            "    def __init__(self):\n" \
            "        self.buffer = ''\n" \
            "    def write(self, s):\n" \
            "        s = self.buffer + s\n" \
            "        lines = s.split(\"\\n\")\n" \
            "        for l in lines[:-1]:\n" \
            "            androidembed.log(l)\n" \
            "        self.buffer = lines[-1]\n" \
            "    def flush(self):\n" \
            "        return\n" \
            "sys.stdout = sys.stderr = LogFile()\n"
    );
}

/**
 * Set Python path
 * Needed in order to tell interpreter where it's ran
 */
void setPath( JNIEnv *env, jstring datapath) {
    jboolean isCopy;
    const char *data_path = (*env)->GetStringUTFChars(env, datapath, &isCopy);
    setenv("PYTHONHOME", data_path, 1);
    setenv("PYTHONPATH", data_path, 1);
    LOGI("%s", getenv("PYTHONHOME"));
    LOGI("%s", getenv("PYTHONPATH"));
}

/**
 * Run Python Script
 * Call only once at a time, never twice
 */
JNIEXPORT void JNICALL
Java_a_humanitas_hp4a_PythonWrapper_run(JNIEnv *env, jclass type,
                                       jstring pythonpath, jstring scriptpath, jstring TAG) {
#pragma clang diagnostic pop

    pTAG = (char *) (*env)->GetStringUTFChars(env, TAG, 0);

    LOGI("Initializing the Python interpreter");
    // Set new python home (for loading modules)
    setPath(env, pythonpath);
    LOGI("Path set");
    Py_OptimizeFlag = 1;
    LOGI("Flag set");
    Py_Initialize();    // Initialize python interpreter

    LOGI("Python interpreter initialized");
    // Redirect python stdout as android stdout
    init_androidembed();
    LOGI("Module loaded");


    const char *path = (*env)->GetStringUTFChars(env, scriptpath, 0);

    FILE *fp = fopen(path, "r");
    LOGD("Starting script %s", path);
    PyRun_SimpleFile(fp, path);
    LOGD("Script %s ended, will now finalize python", path);
    Py_Finalize();
    LOGD("Python finalized");
}


