class Network:
    """
    Represent the network as a set of edges objects
    In the network we can have only one Edge linking two nodes.
    An Edge is valid only when confirmed by both vertexes
    """
    def __init__(self, bad_weight_threshold):
        """
        :param bad_weight_threshold:  int, minimum weight of edges to consider 'bad' liks
        """
        self.edges = {}  # {(n1, n2): Edge} with n1 < n2
        self.fixed_nodes = {}  # {n: fixed} 0/1
        self.bad_weight_threshold = bad_weight_threshold

    def get_valid_tuples(self):
        return [[e.source, e.dest, e.weight] for e in self.edges.itervalues() if e.is_valid]

    def __str__(self):
        return str({'Network': ''.join(map(str, self.edges.itervalues())), 'Fixed Nodes': self.fixed_nodes})

    def api_dict(self):
        return {'Network': [str(e) for e in self.edges.values()],
                'Fixed Nodes': self.fixed_nodes,
                'Links': [e.get_repr() for e in self.edges.values() if e.is_valid]}

    # ---------------------------------- EDGES ---------------------------------- #

    def add_node(self, node, fixed):
        """
        # save fixed/mobile status for the advertiser node
        :param node: heaven address of the node
        :param fixed: 1/0
        :return:
        """
        if node not in self.fixed_nodes:
            self.fixed_nodes[node] = int(fixed)

    def add_edge(self, source, dest, weight, advertiser):
        """
        Add an edge to the network or update it
        :param source:
        :param dest:
        :param weight:
        :param advertiser:
        :return: True if something changed (new link or new weight of existing edge), False if this action
        should not trigger any routing table update because nothing relevant changed
        """
        key = (source, dest) if source < dest else (dest, source)

        if key in self.edges:
            return self.edges[key].update_info(weight, advertiser)
        else:
            new_edge = Edge(source, dest, weight, advertiser, self.fixed_nodes)
            self.edges[key] = new_edge
            return False

    def add_trusty_edge(self, source, dest, weight, fixed_s, fixed_d):
        """
        Used for testing routing algorithms, add a valid edge with a single call
        :param source:
        :param dest:
        :param weight:
        :param fixed_s:
        :param fixed_d:
        :return:
        """
        self.add_node(source, fixed_s)
        self.add_node(dest, fixed_d)
        self.add_edge(source, dest, weight, source)
        self.add_edge(source, dest, weight, dest)

    def remove_edge(self, source, dest, advertiser):
        """
        Called when a node does not see its neighbor anymore
        :param source:
        :param dest:
        :param advertiser: who is not seeing the other
        :return: True if something changed and the routing table must be recomputed, False otherwise
        """
        key = (source, dest) if source < dest else (dest, source)

        if key not in self.edges:
            return False

        removed = self.edges[key].remove_advertiser(advertiser)

        # The Edge has already only one advertiser and it is not the one passed
        # The Edge was already not valid so nothing changed
        if not removed:
            return False

        # If removed, the edge can have one or zero advertisers
        # If it has zero advertiser, remove it from the network
        if not self.edges[key].has_advertisers:
            del self.edges[key]

        # A link has become invalid or deleted, so something changed
        return True

    # ---------------------------------- NODES ---------------------------------- #

    def get_edge(self, source, dest):
        try:
            return self.edges[(source, dest) if source < dest else (dest, source)]
        except KeyError:
            return None

    def get_nodes(self):
        return set(self.fixed_nodes.keys())

    def get_neighbors(self, node):
        return set(e.other_node(node) for key, e in self.edges.iteritems() if node in key)

    def get_valid_neighbors(self, node):
        return set(e.other_node(node) for key, e in self.edges.iteritems()
                   if node in key
                   and e.is_valid)

    def get_valid_edges_keys(self):
        return set(key for key, e in self.edges.iteritems() if e.is_valid)

    def get_edge_weight(self, node1, node2):
        key = (node1, node2) if node1 < node2 else (node2, node1)
        try:
            return self.edges[key].weight
        except KeyError:
            return self.bad_weight_threshold

    # used by dijkstra, tree 0
    def get_good_edges_in_nodes_set_n_fixed(self, nodes_set, n_fixed):
        return {(s, d): e for (s, d), e in self.edges.iteritems()
                if (s in nodes_set or d in nodes_set)
                and not (s in nodes_set and d in nodes_set)
                and e.is_valid
                and e.n_fixed == n_fixed
                and e.weight < self.bad_weight_threshold}

    def get_all_edges_in_nodes_set_n_fixed(self, nodes_set, n_fixed):
        return {(s, d): e for (s, d), e in self.edges.iteritems()
                if (s in nodes_set or d in nodes_set)
                and not (s in nodes_set and d in nodes_set)
                and e.is_valid
                and e.n_fixed == n_fixed}

    # used by dijkstra, tree > 0
    def get_good_edges_in_nodes_set(self, nodes_set):
        return {(s, d): e for (s, d), e in self.edges.iteritems()
                if (s in nodes_set or d in nodes_set)
                and not (s in nodes_set and d in nodes_set)
                and e.is_valid
                and e.weight < self.bad_weight_threshold}

    def get_all_edges_in_nodes_set(self, nodes_set):
        return {(s, d): e for (s, d), e in self.edges.iteritems()
                if (s in nodes_set or d in nodes_set)
                and not (s in nodes_set and d in nodes_set)
                and e.is_valid}

    def remove_node(self, node):
        """
        Remove all the edges in which the node is in
        :param node:
        :return:
        """
        node_neighbors = self.get_neighbors(node)
        for neigh in node_neighbors:
            edge_id = (node, neigh) if node < neigh else (neigh, node)
            del self.edges[edge_id]

    def remove_advertiser(self, node):
        """
        Remove all the advertisement done by a node
        :param node:
        :return:
        """
        node_neighbors = self.get_neighbors(node)
        for neigh in node_neighbors:
            edge_id = (node, neigh) if node < neigh else (neigh, node)
            self.edges[edge_id].remove_advertiser(node)
            if not self.edges[edge_id].has_advertisers:
                del self.edges[edge_id]


class Edge:
    """
    Represent a bidirectional link of the network
    Valid only when advertised by both source and destination
    The two nodes could advertise a different weight for the link.
    We always take the bigger one.
    """
    def __init__(self, source, dest, weight, advertiser, fixed_nodes):

        # save in alphabetical order
        self.source, self.dest = (source, dest) if source < dest else (dest, source)

        if advertiser not in (source, dest):
            raise KeyError('Advertiser of edge must be a vertex of the edge')

        # We take the most recent one.
        # advertisers are the key of self.weights
        self.weights = {advertiser: weight}
        self.weight = weight
        self.fixed_nodes = fixed_nodes

    def update_info(self, weight, advertiser):
        """
        Complete this edge with the info coming from the other vertex.
        :param weight:
        :param advertiser:
        :return: True if a new info (advertiser/weight) has been added, False otherwise (nothing changed)
        """
        # The edge is now confirmed by both nodes
        if advertiser not in self.weights:
            self.weights[advertiser] = weight
            self.weight = max(self.weights.values())
            return True

        # We can only update the weight for an existing advertiser
        # updating the weight of one advertiser
        if self.weights[advertiser] != weight:
            old_weight = self.weight
            self.weights[advertiser] = weight
            self.weight = max(self.weights.values())
            # If the weight changed, update True
            if self.weight != old_weight:
                return True

        # nothing relevant changed
        return False

    def remove_advertiser(self, advertiser):
        """
        The vertex is not publicizing this vertex anymore, so remove his info
        :param advertiser: one of the vertexes
        :return: True if the advertiser has been deleted
        """
        # cannot remove if already removed, nothing changed
        if advertiser not in self.weights:
            return False

        # Removed an advertiser, now the edge become non valid or to delete
        del self.weights[advertiser]
        return True

    @property
    def is_valid(self):
        return len(self.weights) == 2

    @property
    def has_advertisers(self):
        return len(self.weights) != 0

    @property
    def n_fixed(self):
        try:
            return self.fixed_nodes[self.source] + self.fixed_nodes[self.dest]
        except KeyError:
            return 0

    def other_node(self, node):
        if node == self.source:
            return self.dest
        elif node == self.dest:
            return self.source
        else:
            return None

    def __str__(self):
        return '[{}-{}-{}]'.format(
            self.source, self.dest, self.weights)

    def get_repr(self):
        return self.source, self.dest, self.weight
