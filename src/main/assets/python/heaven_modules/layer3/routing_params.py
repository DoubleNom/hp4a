class RoutingParams(object):

    DEFAULT_WEIGHT = 1000

    """
    DISTANCE CALCULATION:
    - base_weight = max(weights)
    - fixed to fixed: distance = base_weight
    - fixed to mobile: distance = base_weight * FIXED_MOBILE_PENALTY
    - mobile to mobile: weight = base_weight * MOBILE_MOBILE_PENALTY
    """

    def __init__(self, config_reader):
        rp = config_reader.get_config_dict('routing_layer')
        self.DEFAULT_HOP_COUNTER = int(rp['default_hop_counter'])
        self.MAX_HOP = int(rp['max_hop'])
        self.LSP_INTERVAL = int(rp['lsp_interval'])
        self.BROADCAST_HEAVEN_ADDRESS = rp['broadcast_heaven_address']
        self.NUMBER_OF_ROUTING_TREES = int(rp['number_of_routing_trees'])
        self.EDGE_WEIGHT_INCREMENT = int(rp['edge_weight_increment'])
        self.CACHE_MAX_PACKET_AGE = int(rp['cache_max_packet_age'])
        self.ROUTING_TABLE_MGR_QLEN = int(rp['routing_table_mgr_qlen'])
        try:
            self.FIXED_NODE = int(config_reader.get_config_dict('main')['fixed'])
        except KeyError:
            self.FIXED_NODE = 0

        self.MAX_PACKET_AGE = int(rp['cache_max_packet_age'])
        self.DO_USE_MULTI_TREE = bool(int(rp['do_use_multi_tree']))
