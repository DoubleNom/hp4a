from heaven_modules.common.routing_header import RoutingHeader
from heaven_modules.common.packet import Packet, PacketError
import logging


class GossipRoutingManager:

    def __init__(self, send_to_upper_layer, link_layer_broadcast, config_reader, rp, profiler):
        self.send_to_upper_layer = send_to_upper_layer
        self.link_layer_broadcast = link_layer_broadcast
        self.rp = rp
        self.profiler = profiler
        self.heaven_address = config_reader.get_config_dict('main')['heaven_address']

    def receive_packet_from_layer2(self, packet):
        """
        Receive a packet from Layer2.
        This function is called by the routing wrapper.
        The packet is already checked against source, hop counter and duplicates
        :param packet:
        :return:
        """
        sent_up = False

        try:
            # packet for my upper layer
            if (packet.get_routing_protocol() == RoutingHeader.ROUTING_PROTOCOL_NONE
                    and packet.get_routing_destination() in [self.heaven_address, self.rp.BROADCAST_HEAVEN_ADDRESS]):

                packet.set_proc_by_l3_up_ts()
                self.send_to_upper_layer(packet)
                sent_up = True

            # Do not forward packets only for me
            if packet.get_routing_destination() == self.heaven_address:
                if not sent_up:
                    packet.set_proc_by_l3_up_ts()
                    self.profiler.profile_packet(packet, Packet.ROUTING_DROP)
                return

            # A packet received with hop counter equal to one cannot be forwarded
            if packet.get_hop_counter() == 1:
                if not sent_up:
                    packet.set_proc_by_l3_up_ts()
                    self.profiler.profile_packet(packet, Packet.HOP_COUNTER_DROP)
                return

        except PacketError as e:
            self.profiler.profile_packet(packet, Packet.WRONG_ROUTING_HEADER + '_gossip')
            logging.getLogger(__name__).exception('Dropped by Gossip: {}'.format(e))
            return

        # Forward the packet
        packet.decrease_hop_counter()
        packet.set_proc_by_l3_up_ts()
        packet.set_proc_by_l3_down_ts()
        self.link_layer_broadcast(packet)

    def send(self, packet):
        """
        Function called by the routing wrapper when the upper layer has a packet to send.
        Called also directly from MultiTree to send LSP packets
        :param packet:
        :return:
        """
        packet.set_rx_by_l3_down_ts()
        # ------------------- Apply default values where not specified ------------------- #
        hop_counter = packet.requested_max_hop
        if hop_counter is None:
            hop_counter = self.rp.DEFAULT_HOP_COUNTER

        routing_protocol = packet.requested_routing_protocol
        if routing_protocol is None:
            routing_protocol = RoutingHeader.ROUTING_PROTOCOL_NONE

        # -------------------------------------------------------------------------------- #
        packet.add_routing_header(hop_counter, self.heaven_address, packet.requested_destination, routing_protocol,
                                  RoutingHeader.ROUTING_METHOD_GOSSIP, RoutingHeader.ROUTING_TREE_GOSSIP)

        packet.set_proc_by_l3_down_ts()
        self.link_layer_broadcast(packet)
