def get_edges_set_1st_tree(network, nodes_set, used_edges, n_tree):
    """
    Edges with exactly one vertex in nodes_set. Given with a certain preference (mobile/fixed)
    :param network:
    :param nodes_set:
    :param used_edges:
    :param n_tree:
    :return:
    """
    for n_fixed in (2, 1, 0):
        edges = network.get_good_edges_in_nodes_set_n_fixed(nodes_set, n_fixed)
        if edges:
            return edges

    for n_fixed in (2, 1, 0):
        edges = network.get_all_edges_in_nodes_set_n_fixed(nodes_set, n_fixed)
        if edges:
            return edges

    return {}


def get_edges_set_other_tree(network, nodes_set, used_edges, n_tree):
    """
    Edges with exactly one vertex in nodes_set. Given with a certain preference (used n times)
    :param network:
    :param nodes_set:
    :param used_edges:
    :param n_tree:
    :return:
    """
    # first, try to look at neighbors used less times and with good links
    # if we are at n_tree = 1, max in used_edges is n_tree (edge used in tree #0 and in tree #1)
    used_n_times = {}
    valid_edges = network.get_good_edges_in_nodes_set(nodes_set)
    valid_edges_key = set(valid_edges.keys())
    for n_used in xrange(n_tree + 1):
        used_n_times[n_used] = set(key for key, value in used_edges.iteritems() if value == n_used)
        key_set = used_n_times[n_used].intersection(valid_edges_key)
        if key_set:
            return {key: valid_edges[key] for key in key_set}

    valid_edges = network.get_all_edges_in_nodes_set(nodes_set)
    valid_edges_key = set(valid_edges.keys())
    for n_used in xrange(n_tree + 1):
        key_set = used_n_times[n_used].intersection(valid_edges_key)
        if key_set:
            return {key: valid_edges[key] for key in key_set}

    return {}


def humanitas_dijkstra_tree(network, source, used_edges, n_tree):
    """
    First Tree, sorting criteria:
    1. Min distance neighbor on links Fixed-Fixed with w != max_w
    2. Min distance neighbor on links FM or MF with w != max_w
    3. Min distance neighbor on links Mobile-Mobile with w != max_w
    4. Min distance neighbor on all links Fixed-Fixed
    5. Min distance neighbor on all links FM or MF
    6. Min distance neighbor on all links Mobile-Mobile

    Other Trees, sorting criteria:
    1. Min distance neighbor on links used 0 times with w != max_w
    2. Min distance neighbor on links used 1 time  with w != max_w
    3. Min distance neighbor on all links used 0 times
    4. Min distance neighbor on all links used 1 time

    :param used_edges:
    :param n_tree:
    :param network:
    :param source:
    :return:
    """
    BIG_M = 10 ** 4  # infinite distance

    if n_tree == 0:
        get_best_edges = get_edges_set_1st_tree
    else:
        get_best_edges = get_edges_set_other_tree

    q = network.get_nodes()  # non-definitive nodes
    s = {source}  # selected/definitive nodes set, complementary of q
    distance = {node: BIG_M for node in q}  # {node: dist from source}
    distance[source] = 0
    previous = {node: None for node in q}  # {node: previous hop to source}
    q.remove(source)

    while q:
        edges = get_best_edges(network, s, used_edges, n_tree)
        # print('possible edges: {}'.format(edges.keys()))

        if not edges:
            # print('Unconnected nodes, break')
            break

        # update the distances of possible neighbors it they improve
        neighbors = set()
        for (n1, n2), edge in edges.iteritems():
            neighbor, node_in_s = (n1, n2) if n2 in s else (n2, n1)
            neighbors.add(neighbor)

            if distance[node_in_s] + edge.weight < distance[neighbor]:
                distance[neighbor] = distance[node_in_s] + edge.weight
                previous[neighbor] = node_in_s

        # add the best neighbor to s
        weight_dict = {node: distance[node] for node in neighbors}
        u = min(weight_dict, key=weight_dict.get)
        # print('Selected node: {}'.format(u))
        selected_edge = (u, previous[u]) if u < previous[u] else (previous[u], u)
        # print('Selected edge: {}'.format(selected_edge))
        used_edges[selected_edge] += 1

        q.remove(u)
        s.add(u)
        # print('previous: {}'.format(previous))
        # print('distance: {}'.format(distance))
        # print('used edge: {}'.format(used_edges))

    return used_edges, previous, distance


def humanitas_dijkstra(network, source, n_routing_trees):
    """
    This variation of dijkstra allows the selection of nodes with different
    criterion, e.g. not considering only the distance from the source.
    :param n_routing_trees:
    :param network:
    :param source:
    :return:
    """
    previous = {}  # {routing_tree_n: previous}
    routing_table = {}
    distance = {}
    used_edges = {key: 0 for key in network.get_valid_edges_keys()}
    for n_tree in xrange(0, n_routing_trees):
        used_edges, previous[n_tree], distance[n_tree] = humanitas_dijkstra_tree(
            network, source, used_edges, n_tree)

        # print('Routing Tree # {}'.format(n_tree))
        # print('Used edges: {}'.format(used_edges))
        # print('Previous: {}'.format(previous[n_tree]))

        routing_table[n_tree] = {}
        for dest in network.get_nodes():
            path = get_path(dest, previous[n_tree])
            if path:
                # print('Reversed path to {}: {}'.format(dest, path))
                routing_table[n_tree][dest] = path[-1]

    # print('Routing Table:  {}'.format(routing_table))
    return previous, routing_table, distance


def standard_dijkstra(network, source):

    q = network.get_nodes()  # non-definitive nodes
    distance = {}  # {node: dist from source}
    previous = {}  # {node: previous hop to source}
    used_edges = []
    BIG_M = 10**4  # infinite distance

    # initialize dicts
    for vertex in q:
        distance[vertex] = BIG_M
        previous[vertex] = None
    distance[source] = 0

    while q:
        # select the vertex (in q) with minimum distance from source
        dist_q = {key: distance[key] for key in q}
        u = min(dist_q, key=dist_q.get)
        q.remove(u)

        # If this and other nodes are not reachable, stop
        if distance[u] == BIG_M:
            break

        if previous[u] is not None:
            used_edges.append([previous[u], u, 1])

        # update its neighbors (in q)
        neighbors = network.get_valid_neighbors(u)
        for v in neighbors.intersection(q):
            new_dist = distance[u] + network.get_edge_weight(u, v)
            if new_dist < distance[v]:
                distance[v] = new_dist
                previous[v] = u

    return used_edges, previous


def get_path(dest, previous):
    path = []
    u = dest
    while previous[u] is not None:
        path.append(u)
        u = previous[u]
    return path
