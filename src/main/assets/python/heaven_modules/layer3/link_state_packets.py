import threading
import logging
from heaven_modules.layer3.routing_neighbors import RoutingNeighbor, RoutingNeighbors


class LinkStatePackets:
    """
    Used by MultiTree routing manager to keep an updated collection of received LSPs
    """

    def __init__(self):
        self.lsps = {}  # source heaven address --> last LSP object
        self.l_lock = threading.RLock()

    def updated_lsp(self, lsp):
        """
        :param lsp: LinkStatePacket
        :return: True if the timestamp of lsp is bigger than the saved one or we have a new lsp
        """
        if lsp.source in self.lsps and lsp.timestamp <= self.lsps[lsp.source].timestamp:
            return False
        return True

    def get_lsp_by_ha(self, heaven_address):
        """
        used by iptables
        :param heaven_address:
        :return:
        """
        try:
            return self.lsps[heaven_address]
        except KeyError:
            return None

    def add_lsp(self, lsp):
        with self.l_lock:
            self.lsps[lsp.source] = lsp

    def get_sources(self):
        return self.lsps.keys()

    def delete_lsps(self, sources):
        """
        :param sources: [heaven addresses]
        :return:
        """
        with self.l_lock:
            for source in sources:
                try:
                    del self.lsps[source]
                except KeyError:
                    continue

    def get_ha_to_ip_set_map(self):
        """
        return the dict {ha: set(ip)} for all peers in the network
        :return:
        """
        ha_to_ip_map_tot = {}
        with self.l_lock:
            for lsp in self.lsps.itervalues():
                for ha, ip in lsp.routing_neighbors.get_ha_to_ip_map().iteritems():
                    if ha not in ha_to_ip_map_tot:
                        ha_to_ip_map_tot[ha] = set()
                    ha_to_ip_map_tot[ha].add(ip)
        return ha_to_ip_map_tot

    def get_ip_by_ha(self, heaven_address):
        """
        :return: ip address of the peer having that heaven address
        """
        ha_ip_map = self.get_ha_to_ip_set_map()
        if heaven_address in ha_ip_map:
            return ha_ip_map[heaven_address].pop()  # pops a random address if many
        return None

    def get_mac_ip_map(self):
        mac_ip_map = {}
        with self.l_lock:
            for lsp in self.lsps.itervalues():
                mac_ip_map.update(lsp.routing_neighbors.get_mac_to_ip_map())
        return mac_ip_map

    def get_networks_set(self):
        networks = set()
        with self.l_lock:
            for lsp in self.lsps.values():
                if lsp.network:
                    networks.add(lsp.network)
        return networks

    def __str__(self):
        with self.l_lock:
            # return ','.join(str(lsp) for lsp in self.lsps.itervalues())
            return str({k: str(v) for k, v in self.lsps.iteritems()})

    def api_dict(self):
        with self.l_lock:
            # return ','.join(str(lsp) for lsp in self.lsps.itervalues())
            return {k: v.api_dict() for k, v in self.lsps.iteritems()}


class LinkStatePacket:
    """
    ------------------------ Routing info packet (Payload of a routing packet) ------------------------
    timestamp (float); fixed (0/1); internet_gw (0/1); network (e.g. 192.168.0.1/24); routing_neighbors;
    -------------------------------------------------------------------------------------------------
    """
    def __init__(self, source='', timestamp=0.0, fixed=0, internet_gw=0, network='', routing_neighbors=None):
        self.source = source
        self.timestamp = float(timestamp)
        self.fixed = int(fixed)
        self.internet_gw = int(internet_gw)
        self.network = network

        if routing_neighbors:
            self.routing_neighbors = routing_neighbors
        else:
            self.routing_neighbors = RoutingNeighbors()

    def update_from_str(self, source, l3_payload):
        """
        Receive a string representing a LSP, parse it and update attributes of this object accordingly
        :return: False if error, True if ok
        """
        self.source = source

        split_packet = l3_payload.split(';')

        if len(split_packet) < 5:
            logging.getLogger(__name__).error('Missing fields')
            return True

        try:
            self.timestamp = float(split_packet[0])
            self.fixed = int(split_packet[1])
            self.internet_gw = int(split_packet[2])
            self.network = split_packet[3]

            neighbors = [i.split('|') for i in split_packet[4:] if i]  # [[ha|ip|mac|dist]]

            if not neighbors:
                return True

            for i in neighbors:
                try:
                    routing_neighbor = RoutingNeighbor(i[0], i[1], i[2], i[3])
                except (IndexError, ValueError):
                    logging.getLogger(__name__).exception('Error in LSP {}'.format(split_packet))
                    return False

                self.routing_neighbors.add_neighbor(routing_neighbor)
            
        except ValueError:
            logging.getLogger(__name__).exception('Error in LSP {}'.format(split_packet))
            return False

        return True

    def lsp_to_str(self):
        """
        Create the string to be transmitted
        """
        return '{};{};{};{};{}'.format(
            self.timestamp,
            self.fixed,
            self.internet_gw,
            self.network,
            self.routing_neighbors.neighbors_to_str()
        )

    def get_neighbors_heaven_addresses(self):
        """
        Called by routing_table_manager when updating the routing table

        :return: [ha] of neighbors publicized in this lsp
        """
        return self.routing_neighbors.get_neighbors_heaven_addresses()

    def get_neighbors_list(self):
        """
        Called by routing_table_manager when updating the routing table

        :return: [RoutingNeighbor] of neighbors publicized in this lsp
        """
        return self.routing_neighbors.neighbors_list

    def __str__(self):
        return str({'src': self.source,
                    'ts': self.timestamp,
                    'fix': self.fixed,
                    'igw': self.internet_gw,
                    'net': self.network,
                    'neighbors': str(self.routing_neighbors)})

    def api_dict(self):
        return {'src': self.source,
                'ts': self.timestamp,
                'fix': self.fixed,
                'igw': self.internet_gw,
                'net': self.network,
                'neighbors': self.routing_neighbors.api_dict()}
