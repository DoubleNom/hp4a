import logging


def humanitas_spanning_trees(full_network, heaven_address, weight_increment, n_of_routing_trees):
    """
    A modified spanning tree that tries to minimize node cardinality and that produces different trees, each one
    different from the previous (if possible)
    :param full_network:
    :param heaven_address:
    :param weight_increment:
    :param n_of_routing_trees:
    :return: (new_routing_trees, new_routing_table)
    """

    nodes = full_network.get_nodes()
    network_tuples = full_network.get_valid_tuples()  # [[source, dest, weight]]
    logging.getLogger(__name__).debug('Valid tuples are: {}'.format(network_tuples))

    new_routing_trees = {}
    new_routing_table = {}

    for tree_n in xrange(n_of_routing_trees):

        # sort tuples by weight and then by alphabetical order
        network = sorted(network_tuples, key=lambda element: (element[2], element[0], element[1]))

        sources = {node: 0 for node in nodes}  # how many times a node has been chosen as source

        selected_edges = []
        tree = []
        routing_table = {}

        # Select the best edges from network
        while len(routing_table) < len(nodes) - 1:

            # if there are no edges
            if not network:
                break

            # find the lighter weight available
            selected_edge = min(network, key=lambda t: t[2])
            weight = selected_edge[2]

            # if there are more edges with the same weight,
            # give priority to edges whose source has been used less
            if ([edge[2] for edge in network]).count(weight) > 1:
                min_weight_edges = [edge for edge in network[:] if edge[2] == weight]

                candidates = []
                for edge in min_weight_edges:
                    candidates.append([edge[0], edge[1], edge[2], sources[edge[0]]])

                # sort the subset by number of usage
                selected_edge = min(candidates, key=lambda t: t[3])[:3]

            # if the link creates a loop, discard it
            if creates_loop(selected_edge[:2], tree):
                network.remove(selected_edge)
                continue

            selected_edges.append(selected_edge)
            sources[selected_edge[0]] += 1
            network.remove(selected_edge)

            # Take only source and dest from selected edges
            # tree = [[edge[0], edge[1]] for edge in selected_edges]
            tree.append([selected_edge[0], selected_edge[1]])
            predecessors = find_predecessors(tree[:], heaven_address)

            for dest in nodes:
                if dest == heaven_address:
                    continue
                next_hop = find_next_hop(predecessors, heaven_address, dest)

                if next_hop:
                    routing_table[dest] = next_hop

        new_routing_trees[tree_n] = selected_edges  # only for printing

        for edge in selected_edges:
            network_tuples[network_tuples.index(edge)][2] += weight_increment

        new_routing_table[tree_n] = routing_table

    return new_routing_trees, new_routing_table


def find_next_hop(predecessors, source, dest):
    """
    :param predecessors: dict of predecessors of each node
    :param source: starting node
    :param dest: destination node
    :return: next hop from the source to reach the destination
    """
    if dest not in predecessors:
        return None
    if predecessors[dest] == source:
        return dest
    return find_next_hop(predecessors, source, predecessors[dest])


def find_predecessors(tree, source):
    """
    :param tree: list of edges [vertex1, vertex2]
    :param source: source from which to evaluate predecessors
    :return: dict [node]: predecessor along the route from node to source
    """
    predecessors = {}

    adjacent_links = [edge for edge in tree if edge[0] == source or edge[1] == source]
    for edge in adjacent_links:
        if edge[0] == source:
            neighbor = edge[1]
        else:
            neighbor = edge[0]

        predecessors[neighbor] = source
        tree.remove(edge)

        predecessors.update(find_predecessors(tree, neighbor))

    return predecessors


def creates_loop(edge, tree):
    """
    Returns True if there is already a path between the two vertexes of the edge

    :param edge: [vertex1, vertex2] to be evaluated
    :param tree: List of edges
    :return: True if by adding the edge to the tree a loop is created, False otherwise.
    """
    if len(tree) == 0:
        return False

    source = edge[0]
    dest = edge[1]
    predecessors = find_predecessors(tree[:], source)
    next_hop = find_next_hop(predecessors, source, dest)
    if next_hop:
        return True
    return False
