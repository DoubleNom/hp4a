import time


class PacketStringCache:

    DEFAULT_MAX_PACKET_AGE = 5

    def __init__(self, cache_name, max_packet_age=DEFAULT_MAX_PACKET_AGE):
        """
        :param cache_name: str, name of the cache
        :param max_packet_age: int/float, packet older than max_age will be deleted
        """
        # set is the fastest data structure to check if an element in it
        self.packets = {}  # arrival ts (int) --> set of packets
        self.cache_name = cache_name
        self.max_packet_age = int(max_packet_age)

    def add_packet_string(self, packet_string_str):
        """
        :param packet_string_str:
        :return: True if added (packet not already in cache), False if not (duplicated packet)
        """
        now = int(time.time())

        if now not in self.packets:
            self.packets[now] = set()

            # here we need the list of keys because we modify the dict during the iteration
            for ts in self.packets.keys():
                if ts < now - self.max_packet_age:
                    del self.packets[ts]

        # hit = False
        # for packet_set in self.packets.values():
        #     if packet_string_str in packet_set:
        #         hit = True
        #         break
        #
        # if not hit:
        #     self.packets[now].add(packet_string_str)
        # return not hit

        for packet_set in self.packets.itervalues():
            if packet_string_str in packet_set:
                return False

        self.packets[now].add(packet_string_str)
        return True
