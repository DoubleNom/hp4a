"""
This routing layer allows one to one communications relying on a unicast high-speed layer 2
In order to have next-hop information, this layer receive update on neighbors and calculate the optimal
routing tree using Kruskal algorithm.
"""
import time
import logging
import Queue
import pprint
from threading import RLock
from packet_cache_manager import PacketStringCache
from heaven_modules.common.routing_header import RoutingHeader
from heaven_modules.common.packet import Packet, PacketError
from heaven_modules.common.standard_threads import PeriodicalCaller, EventsSerializer
from heaven_modules.layer3.link_state_packets import LinkStatePackets, LinkStatePacket
from network import Network
from dijkstra import humanitas_dijkstra
from routing_neighbors import RoutingNeighbors
from heaven_modules.neighboring.neighbors_params import NeighborsParams
from heaven_modules.common.network_tools import NetworkTools as nt
from heaven_modules.common.common_parameters import CommonParameters as cp

try:
    from heaven_modules.api.heaven_api_replies import Replies
except ImportError:
    """
    if this import fails, it means there are no upper layers
    so no requests can be done and replies will not be called.
    """
    pass
    

class MultiTreeRoutingManager:

    dns_servers = '8.8.8.8 8.8.4.4'

    def __init__(self, send_to_upper_layer, neighbors, gossip_send,
                 link_layer_unicast, config_reader, rp, profiler):
        """
        :param send_to_upper_layer:
        :param neighbors:
        :param gossip_send: function used to send lsp (they are sent only with gossip!)
        :param link_layer_unicast: function to send a packet to the lower layer
        :param config_reader: ConfigReader
        :param rp: RoutingParams
        :param profiler:
        """
        self.send_to_upper_layer = send_to_upper_layer
        self.neighbors = neighbors
        self.gossip_send = gossip_send
        self.link_layer_unicast = link_layer_unicast
        self.rp = rp
        self.profiler = profiler

        self.pp = pprint.PrettyPrinter(indent=2)
        self.peers = set([])
        self.mac_ip_map = {}

        # self.pending_kernel_routing_update = False

        self.heaven_address = config_reader.get('main', 'heaven_address')
        self.internet_gw = int(config_reader.get('main', 'internet_gw'))
        self.lan_network_address = config_reader.get('main', 'lan_network_address')
        self.networks = config_reader.get('main', 'networks').split(',')
        self.ad_hoc_network_address = config_reader.get('main', 'ad_hoc_network_address')
        self.ap_network_address = config_reader.get('main', 'ap_network_address')
        self.ap_gw_ip = nt.get_network_gw(self.ap_network_address)

        self.link_state_packets = LinkStatePackets()

        # update the timestamp only upon neighbors changes
        self.my_lsp_timestamp = MultiTreeRoutingManager.get_routing_ts()

        self.periodical_lsp_sender = PeriodicalCaller(self.send_link_state_packet, self.rp.LSP_INTERVAL)
        self.periodical_lsp_sender.setName('periodical lsp sender')

        self.peers_update_requests = {}  # {request_id: Request} to be called every time peers changes
        self.peers_update_requests_lock = RLock()  # protect alteration during iteration

        self.neighbors.updates_listener = self.update_neighbors  # set a listener for neighbors changes
        self.upper_layer_cache = PacketStringCache('MultiTreeUpperLayerCache', max_packet_age=self.rp.MAX_PACKET_AGE)

        # -------------------------- routing table management ------------------------- #
        self.routing_neighbors = RoutingNeighbors()  # used only to create the LSP
        self.routing_table = {}  # {tree_number --> {destination --> next_hop}}
        self.routing_trees = {}  # {tree_number --> [edges]} Note: USED ONLY FOR PRINTING
        self.distance = {}  # {tree_number --> {destination: distance}}
        self.network = Network(NeighborsParams.DISTANCE_MAX)
        self.network.add_node(self.heaven_address, self.rp.FIXED_NODE)

        # We need to save the name of the ap interface (if any) to apply kernel rules
        if cp.AP_NETWORK_NAME in self.networks:
            self.ap_intf = config_reader.get('ap_network', 'interface')
            # set leasetime
            lease_string = 'dhcp.wlan.leasetime'
            desired_lease_time = '30s'
            lease_time = nt.uci_get(lease_string)
            if lease_time != desired_lease_time:
                nt.uci_set(lease_string, desired_lease_time)
                nt.uci_commit()
            nt.restart_dns_service()
        else:
            self.ap_intf = None

        self.known_networks = {self.ad_hoc_network_address, self.ap_network_address,
                               nt.get_next_network(self.ad_hoc_network_address),
                               nt.get_next_network(self.ap_network_address)}

        # print 'Known networks: ', self.known_networks

        self.events_serializer = EventsSerializer(Queue.Queue())
        self.events_serializer.start()

    def start(self):
        MultiTreeRoutingManager.update_dns_servers()
        self.periodical_lsp_sender.start()

        if self.internet_gw:
            nt.restart_dns_service()

    def stop(self):
        self.periodical_lsp_sender.stop()
        self.events_serializer.stop()

    @staticmethod
    def update_dns_servers():
        current_dns_server = nt.uci_get('dhcp.@dnsmasq[0].server')
        if not current_dns_server or current_dns_server != MultiTreeRoutingManager.dns_servers:
            nt.replace_dns_server(MultiTreeRoutingManager.dns_servers, '/etc/config/dhcp')
            # print('DNS [{}] --> [{}], restarting dnsmasq'.format(
            #     current_dns_server, MultiTreeRoutingManager.dns_servers))
            nt.restart_dns_service()
        else:
            pass
            #print('DNS servers are ok')

    # --------------------------------------------------------------------------------------------------- #
    # -------------------------------------- Api calls -------------------------------------------------- #
    # --------------------------------------------------------------------------------------------------- #

    def get_peers(self):
        """
        This must return a list since set is not json serializable
        :return:
        """
        try:
            peers = self.routing_table[0].keys()
            return peers
        except KeyError:
            return []

    def get_peers_updates(self, request):
        subscription_id = '{:.4f}'.format(time.time())
        with self.peers_update_requests_lock:
            self.peers_update_requests[subscription_id] = request
        return True, subscription_id, self.get_peers()

    def notify_peers_update(self, peers):
        with self.peers_update_requests_lock:
            for request in self.peers_update_requests.values():
                request.reply(Replies.reply_get_peers(list(peers)))

    def stop_peers_updates(self, request, subscription_request_id):
        with self.peers_update_requests_lock:
            if subscription_request_id not in self.peers_update_requests:
                return False, subscription_request_id
            self.peers_update_requests[subscription_request_id].end()
            del self.peers_update_requests[subscription_request_id]
            return True, subscription_request_id

    def get_lsp(self):
        return self.link_state_packets.api_dict()

    def get_routing_table(self):
        """
        Called by API. Not a str, used as dict by other modules!
        :return: dict
        """
        return self.routing_table.copy()

    def get_full_network(self):
        return self.network.api_dict()

    def get_routing_trees(self):
        return self.routing_trees.copy()

    def mask_ip_address(self, ip_address):
        if ip_address and (nt.ip_in_network(ip_address, self.ad_hoc_network_address) or nt.ip_in_network(ip_address, self.ap_network_address)):
            return nt.get_routed_address(ip_address)
        return ip_address

    def get_peer_ip(self, heaven_address):

        # if the node is a neighbor, return its address
        peer_ip = self.neighbors.get_ip_by_ha(heaven_address)
        if peer_ip:
            return peer_ip

        # if a node is a peer, it must have one or more ip addresses
        ha_ip_map = self.link_state_packets.get_ha_to_ip_set_map()
        if heaven_address not in ha_ip_map:
            return ''

        peer_ip_set = ha_ip_map[heaven_address]

        # if the peer has only on ip, return it
        if len(peer_ip_set) == 1:
            return self.mask_ip_address(peer_ip_set.pop())

        # if the peer has more addresses, discard bad ones
        if self.ap_gw_ip in peer_ip_set:
            peer_ip_set.remove(self.ap_gw_ip)

        if len(peer_ip_set) == 1:
            return self.mask_ip_address(peer_ip_set.pop())

        # if the peer is on the ethernet network and me too, use that address
        for peer_ip in peer_ip_set:
            if self.lan_network_address and nt.ip_in_network(peer_ip, self.lan_network_address):
                return peer_ip

        # if the peer is on the adhoc network, return that masked address
        for peer_ip in peer_ip_set:
            if nt.ip_in_network(peer_ip, self.ad_hoc_network_address):
                return nt.get_routed_address(peer_ip)

        # if the peer is on the apcli network, return that masked address
        for peer_ip in peer_ip_set:
            if nt.ip_in_network(peer_ip, self.ap_network_address):
                return nt.get_routed_address(peer_ip)

        # otherwise, return a random ip
        return peer_ip_set.pop()

    def get_my_ip_for_peer(self, heaven_address):

        # 1. get the next hop to the destination
        next_hop = self.get_next_hop_unicast(heaven_address, 0)
        if not next_hop:
            return ''

        # 2. get the ip of the intf to use to reach the next hop
        next_hop_link_layer = self.neighbors.get_neighbor_by_ha(next_hop).link_layer
        if not next_hop_link_layer:
            return ''

        my_ip = next_hop_link_layer.ip_address_parser.get_ip_address(next_hop_link_layer.intf)

        return self.mask_ip_address(my_ip)

    # --------------------------------------------------------------------------------------------------- #
    # ------------------------------------- Routing calls ----------------------------------------------- #
    # --------------------------------------------------------------------------------------------------- #
    def send(self, packet):
        """
        function called by the routing wrapper when the upper layer has a packet to send.
        Called also to send new LSP
        :param packet:
        :return:
        """
        packet.set_rx_by_l3_down_ts()

        # ------------------- Apply default values where not specified ------------------- #

        hop_counter = packet.requested_max_hop
        if hop_counter is None:
            hop_counter = self.rp.DEFAULT_HOP_COUNTER

        routing_protocol = packet.requested_routing_protocol
        if routing_protocol is None:
            routing_protocol = RoutingHeader.ROUTING_PROTOCOL_NONE

        # -------------------------------------------------------------------------------- #

        for routing_tree in xrange(self.rp.NUMBER_OF_ROUTING_TREES):

            packet_copy = packet.__copy__()

            packet_copy.add_routing_header(
                hop_counter, self.heaven_address, packet.requested_destination, routing_protocol,
                RoutingHeader.ROUTING_METHOD_MULTI_TREE, routing_tree)

            if packet.requested_destination != self.rp.BROADCAST_HEAVEN_ADDRESS:
                self.send_on_tree(packet_copy)
            else:
                self.broadcast_on_tree(packet_copy)

    def broadcast_on_tree(self, packet):
        """
        Ask to the routing table manager the next hops to broadcast this packet.
        Called by upper layer, by self to forward and to send new lsp
        :param packet: only passed around, not touched by routing table manager
        :return:
        """
        next_hops = self.get_next_hops_broadcast(packet.get_routing_tree())

        packet.set_proc_by_l3_down_ts()

        if not next_hops:
            self.profiler.profile_packet(packet, Packet.NO_ROUTE)
            return

        valid_next_hops = next_hops.difference({packet.previous_hop})

        if not valid_next_hops:
            self.profiler.profile_packet(packet, Packet.ROUTING_DROP + 'nexth_is_prevh')
            return

        for next_hop in valid_next_hops:
            self.link_layer_unicast(packet, next_hop)

    def send_on_tree(self, packet):
        """
        Ask to the routing table manager the next hop to send this packet
        Called to forward packet and to send ul packets
        :param packet: only passed around, not touched by routing table manager
        :return:
        """
        next_hop = self.get_next_hop_unicast(
            packet.get_routing_destination(), packet.get_routing_tree())

        packet.set_proc_by_l3_down_ts()

        if not next_hop:
            self.profiler.profile_packet(packet, Packet.NO_ROUTE)
            return

        if packet.previous_hop == next_hop:
            self.profiler.profile_packet(packet, Packet.ROUTING_DROP + 'nexth_is_prevh')
            return

        self.link_layer_unicast(packet, next_hop)
        # print 'LinkLayer sending {}, nexthop is {}'.format(packet, next_hop)

    # --------------------------------------------------------------------------------------------------- #
    # ------------------------------------- Routing management ------------------------------------------ #
    # --------------------------------------------------------------------------------------------------- #

    @staticmethod
    def get_routing_ts():
        return float(str(time.time()))

    def send_link_state_packet(self, neighbors_updated=False):
        """
        :param neighbors_updated: True only if neighbors are been updated
        :return:
        """

        if neighbors_updated:
            self.my_lsp_timestamp = MultiTreeRoutingManager.get_routing_ts()

        lsp = LinkStatePacket(source=self.heaven_address,
                              timestamp=self.my_lsp_timestamp,
                              fixed=self.rp.FIXED_NODE,
                              internet_gw=self.internet_gw,
                              network=self.lan_network_address,
                              routing_neighbors=self.routing_neighbors)

        payload = lsp.lsp_to_str()

        # if neighbors_updated:
        #     print '---> Sending New LSP {}'.format(payload)
        # else:
        #     print '-> Sending Periodic LSP {}'.format(payload)

        packet = Packet(rx_str=payload, entry_point=Packet.LSP_BROADCAST)
        packet.set_created_by_l4_ts()
        packet.requested_destination = self.rp.BROADCAST_HEAVEN_ADDRESS
        packet.requested_routing_method = RoutingHeader.ROUTING_METHOD_GOSSIP
        packet.requested_routing_protocol = RoutingHeader.ROUTING_PROTOCOL_WRAPPER_NEIGHBOR_LIST
        packet.set_proc_by_l4_down_ts()
        self.gossip_send(packet)

        # BROADCAST OF LSP ON MTREE CAUSES FLOODING BECAUSE MTREE HAS NO CACHE
        # packet2 = Packet(rx_str=payload, entry_point=Packet.LSP_UNICAST)
        # packet2.set_created_by_l4_ts()
        # packet2.requested_destination = self.rp.BROADCAST_HEAVEN_ADDRESS
        # packet2.requested_routing_method = RoutingHeader.ROUTING_METHOD_MULTI_TREE
        # packet2.requested_routing_protocol = RoutingHeader.ROUTING_PROTOCOL_WRAPPER_NEIGHBOR_LIST
        # packet2.set_proc_by_l4_down_ts()
        # self.send(packet2)

    # --------------------------------------------------------------------------------------------------- #
    # ----------------------------------- Calls from lower layer ---------------------------------------- #
    # --------------------------------------------------------------------------------------------------- #

    def receive_packet_from_layer2(self, packet):

        # logging.getLogger(__name__).debug('MultiTree received {}'.format(packet))
        sent_up = False
        duplicated = False

        try:

            # if the packet is for my upper layer, pass it up
            if (packet.get_routing_protocol() == RoutingHeader.ROUTING_PROTOCOL_NONE and
                    packet.get_routing_destination() in [self.heaven_address, self.rp.BROADCAST_HEAVEN_ADDRESS]):

                # Duplicate-cache check for packets for my upper layer
                # Only non-duplicate packets can go up
                # duplicate packets can be forwarded anyway
                if self.upper_layer_cache.add_packet_string(packet.get_str_for_cache()):
                    packet.set_proc_by_l3_up_ts()
                    self.send_to_upper_layer(packet)
                    sent_up = True
                else:
                    duplicated = True

            # if the packet was only for me, it cannot be forwarded
            if packet.get_routing_destination() == self.heaven_address:

                if sent_up:
                    return
                else:
                    packet.set_proc_by_l3_up_ts()

                if duplicated:
                    self.profiler.profile_packet(packet, Packet.CACHE_HIT_DROP + '_mtree')
                    packet.packet_for_cache = packet.get_str_for_cache()
                    logging.getLogger(__name__).info('MultiTree dropped duplicated packet {}'.format(packet))
                else:
                    self.profiler.profile_packet(packet, Packet.ROUTING_DROP + '_mtree')
                    logging.getLogger(__name__).warning('MultiTree dropped useless packet  {}'.format(packet))

                return

            # I cannot forward a packet received with hop counter equal to one, so drop
            if packet.get_hop_counter() == 1:
                if not sent_up:
                    packet.set_proc_by_l3_up_ts()
                    self.profiler.profile_packet(packet, Packet.HOP_COUNTER_DROP + '_mtree')
                return

        except PacketError as e:
            self.profiler.profile_packet(packet, Packet.WRONG_ROUTING_HEADER + '_mtree')
            logging.getLogger(__name__).exception('Dropped by multitree: {}'.format(e))
            return

        # now the packet can be forwarded...

        # update the routing header (hop counter)
        packet.decrease_hop_counter()
        packet.set_proc_by_l3_up_ts()
        packet.set_rx_by_l3_down_ts()
        if packet.get_routing_destination() == self.rp.BROADCAST_HEAVEN_ADDRESS:
            self.broadcast_on_tree(packet)
        else:
            self.send_on_tree(packet)

    def receive_link_state_packet(self, packet):
        self.events_serializer.enqueue(None, self.do_receive_link_state_packet, packet)

    def do_receive_link_state_packet(self, packet):
        """
        Called by routing wrapper, packet coming from layer2
        :param packet:
        :return:
        """
        lsp = LinkStatePacket()
        # drop bad format lsp
        if not lsp.update_from_str(packet.get_routing_source(), packet.get_routing_payload()):
            packet.set_proc_by_l3_up_ts()
            self.profiler.profile_packet(packet, Packet.RECEIVED_LSP_ERROR)
            print '\n*************  Bad format LSP from {} *************\n'.format(packet.get_routing_source())
            print str(packet)
            return

        if self.link_state_packets.updated_lsp(lsp):
            # print '<--- Received new LSP from {}'.format(packet.get_routing_source())
            # self.pp.pprint(lsp.api_dict())
            # print '\n^^^^^^^^^^^^ PROCESSING NEW LSP: {}'.format(str(lsp))
            self.process_new_lsp(packet, lsp)
        else:
            packet.set_proc_by_l3_up_ts()
            # print '<- Received periodic LSP from {}'.format(packet.get_routing_source())
            self.profiler.profile_packet(packet, Packet.RECEIVED_OLD_LSP)

            # if self.pending_kernel_routing_update:
            #     # print '\n^^^^^^^^^^^^ PENDING UPDATES: {}'.format(str(lsp))
            #     self.update_kernel_routing()

    # --------------------------------------------------------------------------------------------------- #
    # --------------------------------- Routing table management ---------------------------------------- #
    # --------------------------------------------------------------------------------------------------- #

    def update_neighbors(self, routing_neighbors):
        """
        Listener for neighbors updates, called by Neighbors
        :param routing_neighbors:
        :return:
        """
        self.events_serializer.enqueue(None, self.do_update_neighbors, routing_neighbors)

    def do_update_neighbors(self, routing_neighbors):

        # print '<--- Received new neighbors {}'.format(routing_neighbors.api_dict())

        self.routing_neighbors = routing_neighbors.__copy__()

        # send lsp true
        self.send_link_state_packet(neighbors_updated=True)

        # Add all edges contained in routing_neighbors (updating the distance of existing)
        diffs = False

        for neighbor in routing_neighbors.get_heaven_neighbors_list():

            result = self.network.add_edge(
                source=self.heaven_address, dest=neighbor.heaven_address,
                weight=neighbor.distance, advertiser=self.heaven_address)

            if result:
                diffs = True
                # print 'Neighbor triggers new edge'

        # Remove the edges that are not anymore in neighbors = all_neighbors - new_neighbors
        neighbors_heaven_addresses = set(routing_neighbors.get_neighbors_heaven_addresses())  # new_neighbors
        neighbors_to_remove = self.network.get_neighbors(self.heaven_address).difference(neighbors_heaven_addresses)

        for neighbor in neighbors_to_remove:
            result = self.network.remove_edge(self.heaven_address, neighbor, advertiser=self.heaven_address)
            if result:
                diffs = True
                # print 'Neighbor triggers remove edge'

        if diffs:
            # print 'Neighbor triggers routing table update'
            self.update_routing_table()

        self.update_kernel_routing()

    def process_new_lsp(self, lsp_packet, lsp):

        diffs = False

        # send a new LSP to update the new-coming node in zero time
        self.send_link_state_packet()

        # Add to the network all the edges advertised in the lsp
        # lsp_source_neighbor are the neighbors of lsp.source advertised in the lsp
        for lsp_source_neighbor in lsp.routing_neighbors.get_heaven_neighbors_list():

            self.network.add_node(lsp.source, lsp.fixed)

            result = self.network.add_edge(
                source=lsp.source, dest=lsp_source_neighbor.heaven_address,
                weight=lsp_source_neighbor.distance, advertiser=lsp.source)

            if result:
                diffs = True
                # print 'New LSP triggers new edge'

        # Remove edges between lsp.source and X that are not publicized in this LSP.
        neighbors_heaven_addresses = set(lsp.get_neighbors_heaven_addresses())
        neighbors_to_remove = self.network.get_neighbors(lsp.source).difference(
            neighbors_heaven_addresses)

        for neighbor in neighbors_to_remove:
            result = self.network.remove_edge(lsp.source, neighbor, advertiser=lsp.source)
            if result:
                diffs = True
                # print 'New LSP triggers remove edge'

        self.link_state_packets.add_lsp(lsp)  # add lsp to the LinkStatePackets

        if diffs:
            # print 'New LSP triggers routing table update'
            self.update_routing_table()

        # if a mac-only node who is my neighbor appears in the lsp, remove it from my neighbors and send a new lsp
        lsp_mac_only_ip_set = lsp.routing_neighbors.get_mac_only_ip_set()
        neigh_mac_only_ip_set = self.routing_neighbors.get_mac_only_ip_set()
        neighbors_to_remove_ip_set = lsp_mac_only_ip_set.intersection(neigh_mac_only_ip_set)
        if neighbors_to_remove_ip_set:
            self.neighbors.remove_neighbors_ip_set(neighbors_to_remove_ip_set)

        """
        Always update kernel routes because either a route changed or a mac-only neighbor changed.
        Exception: do not trigger updates if the LSP comes from a node that is only apcli.
        """
        neighbors_ip = lsp.routing_neighbors.get_ip_list()
        if self.ap_gw_ip in neighbors_ip and len(neighbors_ip) == 1:
            # print 'Not updating kernel rules for LSP coming from apcli-only node\n'
            pass
        else:
            self.update_kernel_routing()

        # profile the lsp packet
        lsp_packet.set_proc_by_l3_up_ts()
        self.profiler.profile_packet(lsp_packet, Packet.RECEIVED_LSP)

    def update_routing_table(self):

        logging.getLogger(__name__).debug('Updating routing table using Network: {}'.format(self.network))

        self.routing_trees, self.routing_table, self.distance = humanitas_dijkstra(
            network=self.network,
            source=self.heaven_address,
            n_routing_trees=self.rp.NUMBER_OF_ROUTING_TREES)

        # print('New Routing table: {}\nDistances:{}'.format(self.routing_table, self.distance))

        new_peers = set(self.get_peers())
        if new_peers != self.peers:
            # print '************* Peers changed {} ***************'.format(new_peers)
            self.notify_peers_update(new_peers)
            # peers that disappeared: old - new
            unreachable_peers = self.peers.difference(new_peers)
            self.link_state_packets.delete_lsps(unreachable_peers)
            for ex_peer in unreachable_peers:
                self.network.remove_advertiser(ex_peer)

            self.peers = new_peers

    def get_next_hop_unicast(self, destination, routing_tree_number):

        try:
            return self.routing_table[routing_tree_number][destination]
        except (ValueError, KeyError):
            return None

    def get_next_hops_broadcast(self, routing_tree_number):

        try:
            all_next_hops = self.routing_table[routing_tree_number].values()
        except KeyError:
            all_next_hops = []

        return set(all_next_hops)

    def get_distance(self, node):
        try:
            return self.distance[0][node]
        except KeyError:
            return 1000

    def update_kernel_routing(self):

        # if you are apcli and not igw, the automatic default rule is enough (so do nothing)
        if cp.AP_CLI_NETWORK_NAME in self.networks and not self.internet_gw:
            return

        try:
            routing_table = self.get_routing_table()[0]
            # self.pending_kernel_routing_update = False
        except KeyError:
            # print 'Routing table still not ready'
            routing_table = {}  # go on, at least set rules for neighbors
            # self.pending_kernel_routing_update = True

        ha_to_ip_map = {}
        ha_to_ip_set_map = self.link_state_packets.get_ha_to_ip_set_map()  # {ha: set(ip)}

        # print 'HA to IP set from LSP:'
        # self.pp.pprint(ha_to_ip_set_map)
        for ha, ip_set in ha_to_ip_set_map.iteritems():

            if self.ap_gw_ip in ip_set:
                ip_set.remove(self.ap_gw_ip)

            try:
                ha_to_ip_map[ha] = ip_set.pop()
            except KeyError:
                # empty set
                pass

        ha_to_ip_map.update(self.neighbors.get_ha_to_ip_map())

        # print 'HA to IP for kernel rules:'
        # self.pp.pprint(ha_to_ip_map)

        # ----------------------------- Phase 1: Rules computation ---------------------------- #
        rules = {}
        net_rules = {}  # {dest_net: (next_hop, distance)} including default. Updated only with the closest

        # get all heaven networks
        heaven_networks = self.known_networks.copy()  # adhoc and ap and routed versions
        heaven_networks = heaven_networks.union(self.link_state_packets.get_networks_set())
        # do not alter rules for your LAN
        if self.lan_network_address and self.lan_network_address in heaven_networks:
            heaven_networks.remove(self.lan_network_address)

        # print 'Heaven networks: {}'.format(heaven_networks)

        # if you are only apcli and igw, just route all heaven networks to the ap
        if cp.AP_CLI_NETWORK_NAME in self.networks and self.internet_gw and len(self.networks) == 1:

            if not routing_table:
                return

            for net in heaven_networks:
                rules[net] = self.ap_gw_ip

            # print 'Desired kernel rules (apcli)'
            # self.pp.pprint(rules)
            self.apply_routing_rules(rules, heaven_networks)
            return

        # if you are not apcli, compute all rules
        for dest_ha, next_hop_ha in routing_table.iteritems():

            try:
                dest_ip = ha_to_ip_map[dest_ha]
                next_hop_ip = ha_to_ip_map[next_hop_ha]
            except KeyError:
                # print 'Missing IP for destination or next hop peer'
                continue

            # print 'Considering {}:{} via {}:{}'.format(dest_ha, dest_ip, next_hop_ha, next_hop_ip)

            # do not add rules to apcli directly, get them from the lsp of the ap
            if nt.ip_in_network(dest_ip, self.ap_network_address):
                # print 'Not adding rule to apcli destination'
                continue

            dest_lsp = self.link_state_packets.get_lsp_by_ha(dest_ha)
            if not dest_lsp:
                # print 'Missing LSP from destination {}'.format(dest_ha)
                continue

            # on the adhoc network, use only ".11.0/24" addresses
            if nt.ip_in_network(dest_ip, self.ad_hoc_network_address):
                dest_routed_ip = nt.get_routed_address(dest_ip)
            else:
                dest_routed_ip = dest_ip
            if nt.ip_in_network(next_hop_ip, self.ad_hoc_network_address):
                next_hop_routed_ip = nt.get_routed_address(next_hop_ip)
            else:
                next_hop_routed_ip = next_hop_ip

            # print 'Evaluating {} LSP'.format(dest_ha)

            # if the node is a gw for a network different from yours, consider the rule
            if dest_lsp.network and dest_lsp.network != self.lan_network_address:
                dest_distance = self.get_distance(dest_ha)
                # add the node as gw if you do not have one or if its distance is the best until now
                if dest_lsp.network not in net_rules or dest_distance < net_rules[dest_lsp.network][1]:
                    net_rules[dest_lsp.network] = (next_hop_routed_ip, dest_distance)

            # if the node is a internet gw and you are not, consider the rule
            if dest_lsp.internet_gw and not self.internet_gw:
                dest_distance = self.get_distance(dest_ha)
                # add the node as default gw if you do not have one or if it is the closes until now
                if cp.DEFAULT not in net_rules or dest_distance < net_rules[cp.DEFAULT][1]:
                    net_rules[cp.DEFAULT] = (dest_routed_ip, dest_distance)

            # do not add rule to reach a node on your lan (but consider the lsp anyway)
            if self.lan_network_address and nt.ip_in_network(dest_ip, self.lan_network_address):
                # print 'Not adding rule to my LAN destination'
                pass
            else:
                # print 'Adding rule to peer {} via {}'.format(dest_routed_ip, next_hop_routed_ip)
                rules[dest_routed_ip] = next_hop_routed_ip

            # set rules for nodes connected to ap
            # iterate on routing neighbors from dest_lsp packet
            for neighbor in dest_lsp.routing_neighbors.neighbors_list:
                if nt.ip_in_network(neighbor.ip_address, self.ap_network_address):
                    rules[nt.get_routed_address(neighbor.ip_address)] = next_hop_routed_ip
                    # print 'Adding rule to apcli {} via {}'.format(
                    # nt.get_routed_address(neighbor.ip_address), next_hop_routed_ip)
        """
        if you have a rule for a LAN network
        do not put single rules for hosts on that LAN 
        if the specific next hop is the same of the next hop to the LAN
        """
        rules_to_delete = []
        for dest_ip in rules:
            for dest_ip_net in net_rules.keys():
                if nt.ip_in_network(dest_ip, dest_ip_net) and rules[dest_ip] == net_rules[dest_ip_net][0]:
                    # print 'Removing rule to ({} via {}) because there is ({} via {})'.format(
                    #     dest_ip, rules[dest_ip], dest_ip_net, net_rules[dest_ip_net][0])
                    rules_to_delete.append(dest_ip)

        for dest in rules_to_delete:
            del rules[dest]

        # if you are ap, compute natting rules
        if self.ap_intf:
            snat_rules = {}
            dnat_rules = {}
            connected_ips = self.neighbors.get_neighbors_ip_on_link_layer(cp.AP_NETWORK_NAME)
            for ip in connected_ips:
                routed_ip = nt.get_routed_address(ip)
                rules[routed_ip] = ip
                snat_rules[ip] = routed_ip
                dnat_rules[routed_ip] = ip

            self.apply_iptables_rules(snat_rules, dnat_rules)
            self.update_ethers_file()

        # Add net rules to rules
        # print 'Net rules: {}'.format(net_rules)
        for net, t in net_rules.iteritems():
            rules[net] = t[0]

        # print 'Kernel rules to be applied:'
        # self.pp.pprint(rules)
        # print '\n'
        self.apply_routing_rules(rules, heaven_networks)

    def apply_routing_rules(self, rules, heaven_networks):
        """
        1. Read current kernel rules
        2. Do not consider rules not related to heaven
        3. Modify, add, remove next hop for existing destinations

        If the node has some other rules not related to heaven (e.g. virtual interfaces for docker...)
        we must not alter them.

        :param rules: dict {destination_ip, next_hop_ip} to be set
        :param heaven_networks: list of network_addresses advertised and related to heaven
        :return:
        """
        #  -------------- 1. read rules currently used in the system ------------- #
        dest_gw_tuples = nt.get_ip_route()  # tuples (dest, next_hop)
        dest_gw_dict = {t[0]: t[1] for t in dest_gw_tuples}  # dict {dest, next_hop}
        current_destinations = set(dest_gw_dict.keys())

        #  -------------- 2. do not consider rules not related to heaven --------- #

        """
        We remove from current_destinations all the rules not related to heaven.        
        Then we intersect and subtract current_destinations and rules 
        to decide which rule do add/modify/delete.
        """

        # do not consider default gw rule
        if self.internet_gw and cp.DEFAULT in current_destinations:
            current_destinations.remove(cp.DEFAULT)

        for dest in current_destinations.copy():  # copy for iteration

            # keep the new default route
            if dest == cp.DEFAULT:
                continue

            # network in lsp
            if dest in heaven_networks:
                continue

            # if the destination ip belongs to a heaven network, keep it
            for netw in heaven_networks:
                if nt.ip_in_network(dest, netw):
                    break
            # the previous for exited without the break
            # so the address does not belong to a know heaven network, do not alter it
            else:
                current_destinations.remove(dest)

        #  -------------- 3. Ip routes alterations  --------------------------------- #
        new_destinations = set(rules.keys())

        rules_to_modify = current_destinations.intersection(new_destinations)
        for dest in rules_to_modify:
            if dest_gw_dict[dest] != rules[dest]:
                nt.ip_route_change(dest, rules[dest])

        rules_to_add = new_destinations.difference(current_destinations)
        for dest in rules_to_add:
            nt.ip_route_add(dest, rules[dest])

        rules_to_delete = current_destinations.difference(new_destinations)
        for dest in rules_to_delete:
            nt.ip_route_del(dest, dest_gw_dict[dest])

        nt.ip_route_flush_cache()

    def apply_iptables_rules(self, snat_rules, dnat_rules):

        # ----------------- apply snat rules ----------------- #
        snat_tuples = nt.get_snat_rules()
        snat_dict = {t[0]: t[1] for t in snat_tuples}

        current_snatted = set(snat_dict.keys())
        new_snatted = set(snat_rules.keys())

        rules_to_add = new_snatted.difference(current_snatted)
        # print 'Adding snat rules: {}'.format(rules_to_add)
        for source_addr in rules_to_add:
            nt.snat_ip_address(self.ap_intf, source_addr, snat_rules[source_addr])

        rules_to_delete = current_snatted.difference(new_snatted)
        # print 'Deleting snat rules: {}'.format(rules_to_delete)
        for source_addr in rules_to_delete:
            nt.snat_ip_address_del(self.ap_intf, source_addr, snat_dict[source_addr])

        # ----------------- apply dnat rules ----------------- #
        dnat_tuples = nt.get_dnat_rules()
        dnat_dict = {t[0]: t[1] for t in dnat_tuples}

        current_dnatted = set(dnat_dict.keys())
        new_dnatted = set(dnat_rules.keys())

        rules_to_add = new_dnatted.difference(current_dnatted)
        # print 'Adding dnat rules: {}'.format(rules_to_add)
        for dest_addr in rules_to_add:
            nt.dnat_ip_address(self.ap_intf, dest_addr, dnat_rules[dest_addr])

        rules_to_delete = current_dnatted.difference(new_dnatted)
        # print 'Deleting dnat rules: {}'.format(rules_to_delete)
        for dest_addr in rules_to_delete:
            nt.dnat_ip_address_del(self.ap_intf, dest_addr, dnat_dict[dest_addr])

    def update_ethers_file(self):
        new_mac_ip_map = self.link_state_packets.get_mac_ip_map()
        new_mac_ip_map.update(self.neighbors.get_mac_to_ip_map())
        # print '-- Ethers should be {}'.format(new_mac_ip_map)
        if new_mac_ip_map != self.mac_ip_map:
            nt.write_ethers(new_mac_ip_map)
            # print '-- Ethers updated!'
            self.mac_ip_map = new_mac_ip_map
