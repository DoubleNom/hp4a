"""
This class provides a general layer 3:
- Allows to the upper layer to choose how to send a packet (choose between different routing options)
- Allows the communication between different routing protocols (intra-layer communication)
CACHE:
pkt from l2 (gossip) --> cache --> routing (up, fw ord drop)
pkt from l2 (mtree) --> routing (up, fw ord drop) --> cache for up
"""
from heaven_modules.common.tail_drop_queue import TailDropQueue
from gossip_routing_layer import GossipRoutingManager
from multi_tree_routing_layer import MultiTreeRoutingManager
from heaven_modules.layer2.link_layer_wrapper import LinkLayerWrapper
from heaven_modules.common.routing_header import RoutingHeader
from packet_cache_manager import PacketStringCache
from heaven_modules.common.packet import Packet, PacketError
from heaven_modules.neighboring.neighbors import Neighbors
from routing_params import RoutingParams
import logging
from heaven_modules.common.standard_threads import QueueServer
from heaven_modules.common.network_tools import NetworkTools


class L2ToL3Server(QueueServer):
    """
    1) Read packets coming from Layer 2
    2) Check source, if it is me drop
    3) Check the hop counter, if zero drop
    4) If the packet is for the upper layer, cache it and drop duplicates
    5) Pass the packet to a specific routing layer
    """

    def __init__(self, heaven_address, do_use_multi_tree, profiler):
        QueueServer.__init__(self, TailDropQueue())
        self.heaven_address = heaven_address
        self.do_use_multi_tree = do_use_multi_tree
        self.profiler = profiler
        self.gossip_receive_packet = None
        self.mtree_receive_packet = None
        self.mtree_receive_lsp = None
        self.gossip_cache = None

    def handle(self, x):
        packet = x

        logging.getLogger(__name__).debug('Routing wrapper received: {}'.format(packet))

        packet.set_rx_by_l3_up_ts()

        try:
            # Filter on hop counter
            if packet.get_hop_counter() <= 0:
                packet.set_proc_by_l3_up_ts()
                self.profiler.profile_packet(packet, Packet.HOP_COUNTER_DROP + '_wr')
                logging.getLogger(__name__).debug('Dropped because hop counter = 0')
                return

            # Filter on source
            if packet.get_routing_source() == self.heaven_address:
                packet.set_proc_by_l3_up_ts()
                self.profiler.profile_packet(packet, Packet.ROUTING_DROP + '_wr')
                logging.getLogger(__name__).debug('Dropped because source = myself')
                return

            # Duplicate-cache check for all incoming gossip packets
            if packet.get_routing_method() == RoutingHeader.ROUTING_METHOD_GOSSIP:
                if not self.gossip_cache.add_packet_string(packet.get_str_for_cache()):
                    packet.set_proc_by_l3_up_ts()
                    self.profiler.profile_packet(packet, Packet.CACHE_HIT_DROP + '_gossip')
                    logging.getLogger(__name__).debug('Dropped because of gossip cache hit')
                    return

            # Pass LSP received from every routing layer to MultiTree, then go on
            if self.do_use_multi_tree and packet.get_routing_protocol() == RoutingHeader.ROUTING_PROTOCOL_WRAPPER_NEIGHBOR_LIST:
                self.mtree_receive_lsp(packet)

            # Now send to Gossip or Multi Tree for standard processing
            if packet.get_routing_method() == RoutingHeader.ROUTING_METHOD_GOSSIP:
                self.gossip_receive_packet(packet)
            elif self.do_use_multi_tree:
                self.mtree_receive_packet(packet)

        except PacketError as e:
            self.profiler.profile_packet(packet, Packet.WRONG_ROUTING_HEADER + '_wrapper')
            logging.getLogger(__name__).exception('Dropped by RoutingWrapper: {}'.format(e))
            return


class RoutingWrapper:
    def __init__(self, send_to_upper_layer, config_reader, profiler, delegates):

        self.send_to_upper_layer = send_to_upper_layer
        self.neighbors = Neighbors(config_reader)
        self.rp = RoutingParams(config_reader)
        self.profiler = profiler
        self.delegates = delegates

        self.ad_hoc_network_address = config_reader.get('main', 'ad_hoc_network_address')
        self.ap_network_address = config_reader.get('main', 'ap_network_address')

        # Create a server to receive packets from Layer2
        self.l2_to_l3_server = L2ToL3Server(
            config_reader.get('main', 'heaven_address'),
            self.rp.DO_USE_MULTI_TREE,
            self.profiler)

        # create the layer 2
        self.link_layer_wrapper = LinkLayerWrapper(self.l2_to_l3_server, self.neighbors,
                                                   config_reader, self.profiler, self.delegates)

        # Create Gossip Routing Layer
        self.gossip_routing_layer = GossipRoutingManager(
            self.send_to_upper_layer, self.link_layer_wrapper.send_broadcast, config_reader, self.rp, self.profiler)
        self.l2_to_l3_server.gossip_receive_packet = self.gossip_routing_layer.receive_packet_from_layer2
        self.l2_to_l3_server.gossip_cache = PacketStringCache('GossipCache', max_packet_age=self.rp.MAX_PACKET_AGE)
        
        # Create Multi Tree Routing Layer
        if self.rp.DO_USE_MULTI_TREE:
            self.multi_tree_routing_layer = MultiTreeRoutingManager(
                self.send_to_upper_layer,
                self.neighbors,
                self.gossip_routing_layer.send,
                self.link_layer_wrapper.send_unicast,
                config_reader, self.rp, self.profiler)
            self.l2_to_l3_server.mtree_receive_packet = self.multi_tree_routing_layer.receive_packet_from_layer2
            self.l2_to_l3_server.mtree_receive_lsp = self.multi_tree_routing_layer.receive_link_state_packet

    # ------------------------------- API SERVER CALLS: ROUTING ---------------------------------#

    def get_ip_address(self, heaven_address):
        """
        Called by tcp/udp layer to create direct connections

        :param heaven_address:
        :return: routed ip address of the peer having that heaven_address
        """
        if not self.rp.DO_USE_MULTI_TREE:
            return None

        # lookup in neighbors
        peer_ip = self.neighbors.get_ip_by_ha(heaven_address)
        if not peer_ip:
            peer_ip = self.multi_tree_routing_layer.link_state_packets.get_ip_by_ha(heaven_address)

        if not peer_ip:
            return None

        # if ip is in ad_hoc network, you must convert the address from 10.0/24 to 11.0/24
        if NetworkTools.ip_in_network(peer_ip, self.ad_hoc_network_address):
            peer_ip = NetworkTools.get_routed_address(peer_ip)

        if NetworkTools.ip_in_network(peer_ip, self.ap_network_address) and peer_ip != NetworkTools.get_network_gw(self.ap_network_address):
            peer_ip = NetworkTools.get_routed_address(peer_ip)

        return peer_ip

    def get_peers(self):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.get_peers()
        else:
            return []

    def get_peers_updates(self, request):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.get_peers_updates(request)
        else:
            return False, '', []

    def stop_peers_updates(self, request, subscription_request_id):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.stop_peers_updates(request, subscription_request_id)
        else:
            return False, ''

    def get_neighbors_strings(self):
        return self.neighbors.get_neighbors_strings()

    def get_lsp(self):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.get_lsp()
        else:
            return {}

    def get_routing_table(self):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.get_routing_table()
        else:
            return {}

    def get_routing_trees(self):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.get_routing_trees()
        else:
            return {}

    def get_full_network(self):
        if self.rp.DO_USE_MULTI_TREE:
            return self.multi_tree_routing_layer.get_full_network()
        else:
            return {}

    def block_neighbor(self, heaven_address):
        return self.neighbors.block_neighbor(heaven_address)

    def unblock_neighbor(self, heaven_address):
        return self.neighbors.unblock_neighbor(heaven_address)

    def get_peer_ip(self, heaven_address):
        return self.multi_tree_routing_layer.get_peer_ip(heaven_address)

    def get_my_ip_for_peer(self, heaven_address):
        return self.multi_tree_routing_layer.get_my_ip_for_peer(heaven_address)

    # ------------------------------- ROUTING MANAGEMENT ---------------------------------#

    def start_routing_layers(self):
        self.l2_to_l3_server.start()
        self.link_layer_wrapper.start()
        if self.rp.DO_USE_MULTI_TREE:
            self.multi_tree_routing_layer.start()

    def stop(self):

        self.l2_to_l3_server.stop()

        if self.rp.DO_USE_MULTI_TREE:
            logging.getLogger(__name__).debug('Stopping Multi-Tree...')
            self.multi_tree_routing_layer.stop()

        logging.getLogger(__name__).debug('Stopping Link Layer...')
        self.link_layer_wrapper.stop()

        logging.getLogger(__name__).debug('Stopping neighbors...')
        self.neighbors.stop()

    def send(self, packet):
        """
        Called by the upper layer to send a packet
        :param packet:
        :return:
        """
        if packet.requested_routing_method == RoutingHeader.ROUTING_METHOD_MULTI_TREE and self.rp.DO_USE_MULTI_TREE:
            self.multi_tree_routing_layer.send(packet)
        elif packet.requested_routing_method == RoutingHeader.ROUTING_METHOD_GOSSIP:
            self.gossip_routing_layer.send(packet)
