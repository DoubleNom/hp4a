from heaven_modules.layer3.routing_wrapper import RoutingWrapper
from heaven_modules.common.transport_header import TransportHeader as th
from tcp_layer import TcpLayer
from udp_layer import UdpLayer
from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.api.heaven_api_request import VoidRequest
import logging
from heaven_modules.common.profiler import Profiler


class TransportManager:
    """
    Class that instantiates the lower layer and gives to it a queue to put packets.
    The class then instantiate a thread to serve that queue.

    The Transport Manager can be used to instantiate many
    - TCP server: listen on a port for connections and send incoming data to the app. layer
    - TCP client: connect to A server to send messages

    The class expose some methods to the upper layer
    We can have only a Transport manager running on the PC,
    different app will use it through sockets = INTF TO UPPER LAYERS
    """

    def __init__(self, config_reader, enable_packet_profiler, delegates):
        """
        :param config_reader:
        :param enable_packet_profiler:
        :raise ConfigParser.NoSectionError or ConfigParser.NoOptionError
        """
        self.heaven_address = config_reader.get_config_dict('main')['heaven_address']
        self.profiler = Profiler(self.heaven_address, enable_packet_profiler)
        self.delegates = delegates
        self.routing_layer = RoutingWrapper(self.receive_packet_from_lower_layer, config_reader, self.profiler, self.delegates)

        self.tcp_layer = TcpLayer(self.heaven_address, self.routing_layer.send, self.routing_layer.get_ip_address,
                                  config_reader, self.profiler)
        self.udp_layer = UdpLayer(self.heaven_address, self.routing_layer.send, self.routing_layer.get_ip_address,
                                  config_reader, self.profiler)

        self.routing_layer.start_routing_layers()

        debug_server_port = config_reader.get('test', 'debug_tcp_server_port')
        self.create_tcp_server(VoidRequest(), [0, 0, 0, debug_server_port])

        debug_server_socket_port = config_reader.get('test', 'debug_tcp_server_socket_port')
        self.create_tcp_server_socket(VoidRequest(), [0, 0, 0, debug_server_socket_port])

    def receive_packet_from_lower_layer(self, packet):
        """
        Dispatch packets received from lower layers.
        :param packet:
        :return: NA
        """

        if packet.get_transport_protocol() == th.TCP_TRANSPORT_TYPE:
            self.tcp_layer.receive_tcp_packet(packet)
        else:
            self.udp_layer.receive_udp_packet(packet)

    def stop(self):
        self.profiler.stop()
        logging.getLogger(__name__).debug('Stopping Transport Manager...')
        logging.getLogger(__name__).debug('Stopping UDP Layer...')
        self.udp_layer.stop()
        logging.getLogger(__name__).debug('Stopping TCP Layer...')
        self.tcp_layer.stop()
        logging.getLogger(__name__).debug('Stopping Routing Layer...')
        self.routing_layer.stop()
        logging.getLogger(__name__).debug('Heaven stack stopped! ')
        return True

    """
    The following methods can raise ValueError due to data_split assignment and string to int/float conversion.
    These exceptions are catch by the caller (HeavenApiServer).
    The methods called inside these methods (belonging to tcp_layer, udp_layer etc.) manage their own exceptions
    and return only True/False.
    """

    # -------------------------- HEAVEN --------------------------- #

    def get_heaven_address(self, request, _data_split):
        request.reply(Replies.reply_get_heaven_address(self.heaven_address))
        request.end()

    # -------------------------- NETWORK --------------------------- #

    def get_peers(self, request, _data_split):
        request.reply(Replies.reply_get_peers(self.routing_layer.get_peers()))
        request.end()

    def get_peers_updates(self, request, _data_split):
        result, subscription_id, peers = self.routing_layer.get_peers_updates(request)
        request.reply(Replies.reply_get_peers_updates(int(result), subscription_id, peers))

    def stop_peers_updates(self, request, data_split):
        subscription_request_id = data_split[3]
        result, subscription_id = self.routing_layer.stop_peers_updates(request, subscription_request_id)
        request.reply(Replies.reply_stop_peers_updates(int(result), subscription_id))
        request.end()

    def get_neighbors(self, request, _data_split):
        request.reply(Replies.reply_get_neighbors(self.routing_layer.get_neighbors_strings()))
        request.end()

    def get_lsp(self, request, _data_split):
        request.reply(Replies.reply_get_lsp(self.routing_layer.get_lsp()))
        request.end()

    def get_routing_table(self, request, _data_split):
        request.reply(Replies.reply_get_routing_table(self.routing_layer.get_routing_table()))
        request.end()

    def get_routing_trees(self, request, _data_split):
        request.reply(Replies.reply_get_routing_trees(self.routing_layer.get_routing_trees()))
        request.end()

    def get_full_network(self, request, _data_split):
        request.reply(Replies.reply_get_full_network(self.routing_layer.get_full_network()))
        request.end()

    def block_neighbor(self, request, data_split):
        heaven_address = data_split[3]
        request.reply(Replies.reply_block_neighbor(heaven_address, self.routing_layer.block_neighbor(heaven_address)))

    def unblock_neighbor(self, request, data_split):
        heaven_address = data_split[3]
        request.reply(Replies.reply_unblock_neighbor(heaven_address, self.routing_layer.unblock_neighbor(heaven_address)))

    def get_peer_ip(self, request, data_split):
        heaven_address = data_split[3]
        request.reply(Replies.reply_get_peer_ip(heaven_address, self.routing_layer.get_peer_ip(heaven_address)))
        request.end()

    def get_my_ip_for_peer(self, request, data_split):
        heaven_address = data_split[3]
        request.reply(Replies.reply_get_peer_ip(heaven_address, self.routing_layer.get_my_ip_for_peer(heaven_address)))
        request.end()

    # ------------------------- TCP SERVER ------------------------- #

    def create_tcp_server(self, request, data_split):
        port = int(data_split[3])
        created, error_description = self.tcp_layer.create_server(port, request)
        request.reply(Replies.reply_create_tcp_server(port, int(created), error_description))
        # otherwise this request ends when the stop is called
        if not created:
            request.end()

    def stop_tcp_server(self, request, data_split):
        port = int(data_split[3])
        stopped, error_description = self.tcp_layer.stop_server(port)
        request.reply(Replies.reply_stop_tcp_server(port, int(stopped), error_description))
        request.end()

    def get_tcp_servers(self, request, _data_split):
        request.reply(Replies.reply_get_tcp_servers(self.tcp_layer.get_tcp_servers()))
        request.end()

    # ---------------------- TCP SERVER SOCKET --------------------- #

    def create_tcp_server_socket(self, request, data_split):
        port = int(data_split[3])
        created, error_description = self.tcp_layer.create_server_socket(port, request)
        request.reply(Replies.reply_create_tcp_server(port, int(created), error_description))
        if not created:
            request.end()

    def stop_tcp_server_socket(self, request, data_split):
        port = int(data_split[3])
        stopped, error_description = self.tcp_layer.stop_server_socket(port)
        request.reply(Replies.reply_stop_tcp_server(port, int(stopped), error_description))
        request.end()

    def get_tcp_servers_socket(self, request, _data_split):
        request.reply(Replies.reply_get_tcp_servers(self.tcp_layer.get_tcp_servers_socket()))
        request.end()

    # ------------------------- TCP CLIENT ------------------------- #

    def create_tcp_client(self, request, data_split):
        server_address = data_split[3]
        server_port = int(data_split[4])
        routing_method = int(data_split[5])
        session_id, error_description = self.tcp_layer.create_client(
            server_address, server_port, routing_method, request)

        if session_id:
            request.reply(Replies.reply_create_tcp_client(server_address, server_port, session_id, 1))
            # this request ends when the stop is called or session error
        else:
            request.reply(Replies.reply_create_tcp_client(server_address, server_port, '', 0, error_description))
            request.end()

    def send_tcp(self, request, data_split):
        session_id = data_split[3]
        message = ';'.join(data_split[4:])
        mex_id, error_description = self.tcp_layer.transfer_data(request, session_id, message)

        if mex_id:
            request.reply(Replies.reply_send_tcp(session_id, mex_id, 1))
        else:
            request.reply(Replies.reply_send_tcp(session_id, '', 0, error_description))
            request.end()

    def send_tcp_file(self, request, data_split):
        session_id = data_split[3]
        delete_after_send = int(data_split[4])
        file_path = ';'.join(data_split[5:])

        mex_id, error_description = self.tcp_layer.transfer_file(request, session_id, delete_after_send, file_path)

        if mex_id:
            request.reply(Replies.reply_send_tcp_file(session_id, mex_id, 1))
        else:
            request.reply(Replies.reply_send_tcp_file(session_id, '', 0, error_description))
            request.end()

    def stop_tcp_client(self, request, data_split):
        session_id = data_split[3]
        stopped, error_description = self.tcp_layer.stop_client(session_id)
        request.reply(Replies.reply_stop_tcp_client(session_id, int(stopped), error_description))
        request.end()

    def get_tcp_clients(self, request, _data_split):
        request.reply(Replies.reply_get_tcp_clients(self.tcp_layer.get_tcp_clients()))
        request.end()

    # ---------------------- TCP CLIENT SOCKET---------------------- #

    def send_tcp_socket(self, request, data_split):
        server_address = data_split[3]
        server_port = int(data_split[4])
        message = ';'.join(data_split[5:])

        session_id, error_description = self.tcp_layer.transfer_data_socket(
            request, server_address, server_port, message)

        if session_id:
            request.reply(Replies.reply_send_tcp_socket(session_id, 1))
        else:
            request.reply(Replies.reply_send_tcp_socket('', 0, error_description))
            request.end()

    def send_tcp_file_socket(self, request, data_split):
        server_address = data_split[3]
        server_port = int(data_split[4])
        delete_after_send = int(data_split[5])
        file_path = ';'.join(data_split[6:])

        session_id, error_description = self.tcp_layer.transfer_file_socket(
            request, server_address, server_port, delete_after_send, file_path)

        if session_id:
            request.reply(Replies.reply_send_tcp_socket(session_id, 1))
        else:
            request.reply(Replies.reply_send_tcp_socket('', 0, error_description))
            request.end()

    # ------------------------- UDP SERVER ------------------------- #

    def create_udp_server(self, request, data_split):
        port = int(data_split[3])
        created, error_description = self.udp_layer.create_server(port, request)
        request.reply(Replies.reply_create_udp_server(port, int(created), error_description))
        if not created:
            request.end()

    def stop_udp_server(self, request, data_split):
        port = int(data_split[3])
        stopped, error_description = self.udp_layer.stop_udp_server(port)
        request.reply(Replies.reply_stop_udp_server(port, int(stopped), error_description))
        request.end()

    def get_udp_servers(self, request, _data_split):
        request.reply(Replies.reply_get_udp_servers(self.udp_layer.get_udp_servers()))
        request.end()

    # ---------------------- UDP SERVER SOCKET --------------------- #

    def create_udp_server_socket(self, request, data_split):
        port = int(data_split[3])
        rx_buffer_size = int(data_split[4])
        rx_queue_len = int(data_split[5])
        created, error_description = self.udp_layer.create_server_socket(
            port, rx_buffer_size, rx_queue_len, request)
        request.reply(Replies.reply_create_udp_server(port, int(created), error_description))
        if not created:
            request.end()

    def stop_udp_server_socket(self, request, data_split):
        port = int(data_split[3])
        stopped, error_description = self.udp_layer.stop_udp_server_socket(port)
        request.reply(Replies.reply_stop_udp_server(port, int(stopped), error_description))
        request.end()

    def get_udp_servers_socket(self, request, _data_split):
        request.reply(Replies.reply_get_udp_servers(self.udp_layer.get_udp_servers_socket()))
        request.end()

    # ------------------------- UDP CLIENT ------------------------- #

    def create_udp_client(self, request, data_split):
        destination_address = data_split[3]
        destination_port = int(data_split[4])
        mtu = int(data_split[5])
        max_hop = int(data_split[6])
        packet_rate = float(data_split[7])
        routing_method = int(data_split[8])
        source_port, error_description = self.udp_layer.create_client(
            destination_address, destination_port, mtu, max_hop, packet_rate, routing_method, request)

        if source_port:
            request.reply(Replies.reply_create_udp_client(
                destination_address, destination_port, source_port, 1))
        else:
            request.reply(Replies.reply_create_udp_client(
                destination_address, destination_port, '', 0, error_description))

    def send_udp(self, request, data_split):
        source_port = int(data_split[3])
        message = ';'.join(data_split[4:])
        sent, error_description = self.udp_layer.send(message, source_port)
        request.reply(Replies.reply_send_udp(source_port, int(sent), error_description))
        request.end()

    def stop_udp_client(self, request, data_split):
        source_port = int(data_split[3])
        stopped, error_description = self.udp_layer.stop_udp_client(source_port)
        request.reply(Replies.reply_stop_udp_client(source_port, int(stopped), error_description))
        request.end()

    def get_udp_clients(self, request, _data_split):
        request.reply(Replies.reply_get_udp_clients(self.udp_layer.get_udp_clients()))
        request.end()

    # ---------------------- UDP CLIENT SOCKET---------------------- #

    def create_udp_client_socket(self, request, data_split):
        server_address = data_split[3]
        server_port = int(data_split[4])
        tx_timeout = float(data_split[5])
        client_id, error_description = self.udp_layer.create_client_socket(
            server_address, server_port, tx_timeout, request)

        if client_id:
            request.reply(Replies.reply_create_udp_client(server_address, server_port, client_id, 1))
        else:
            request.reply(Replies.reply_create_udp_client(server_address, server_port, '', 0, error_description))

    def send_udp_socket(self, request, data_split):
        client_id = data_split[3]
        message = ';'.join(data_split[4:])
        sent, error_description = self.udp_layer.send_socket(message, client_id)
        request.reply(Replies.reply_send_udp(client_id, int(sent), error_description))
        request.end()

    def stop_udp_client_socket(self, request, data_split):
        client_id = data_split[3]
        stopped, error_description = self.udp_layer.stop_udp_client_socket(client_id)
        request.reply(Replies.reply_stop_udp_client(client_id, int(stopped), error_description))
        request.end()

    def get_udp_clients_socket(self, request, _data_split):
        request.reply(Replies.reply_get_udp_clients(self.udp_layer.get_udp_clients_socket()))
        request.end()
