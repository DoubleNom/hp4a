from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.common.packet import Packet


class UdpServer:

    def __init__(self, port, request, profiler):
        self.port = port
        self.request = request
        self.profiler = profiler

    def receive_udp_packet(self, packet):

        packet.set_proc_by_l4_up_ts()
        self.profiler.profile_packet(packet, Packet.RECEIVED_UDP_DATA)

        msg = packet.get_udp_payload()

        self.request.reply(Replies.reply_udp_server_received_message(
            self.port,
            packet.get_transport_source(),
            packet.get_udp_source_port(),
            msg,
            len(msg)
        ))

    def stop(self):
        self.request.end()
