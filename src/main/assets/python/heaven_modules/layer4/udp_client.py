import time
from heaven_modules.common.packet import Packet
from heaven_modules.common.tail_drop_queue import TailDropQueue
from heaven_modules.common.transport_header import TransportHeader
import logging
from heaven_modules.common.standard_threads import QueueServer
from heaven_modules.api.heaven_api_replies import ErrorDescriptions as err


class PacketSender(QueueServer):

    def __init__(self, packet_rate, tx_queue, send_to_layer3):
        QueueServer.__init__(self, tx_queue)
        self.packet_rate = packet_rate
        self.send_to_layer3 = send_to_layer3
        self.sleep_time = 1.0 / float(self.packet_rate)

    def handle(self, x):
        packet = x
        packet.set_proc_by_l4_down_ts()
        self.send_to_layer3(packet)
        time.sleep(self.sleep_time)


class UdpClient:

    def __init__(self, heaven_address, server_address, server_port, source_port,
                 mtu, max_hop, packet_rate, send_to_layer3, routing_method, request, profiler):
        self.heaven_address = heaven_address
        self.server_address = server_address
        self.server_port = server_port
        self.source_port = source_port
        self.mtu = mtu
        self.max_hop = max_hop
        self.packet_rate = packet_rate
        self.send_to_layer3 = send_to_layer3
        self.routing_method = routing_method
        self.request = request
        self.profiler = profiler

        self.tx_queue = TailDropQueue()

        self.packet_sender = None

        if packet_rate:
            self.packet_sender = PacketSender(self.packet_rate,
                                              self.tx_queue,
                                              self.send_to_layer3)

            self.packet_sender.start()

    def send(self, message):

        if len(message) > self.mtu:
            logging.getLogger(__name__).debug('Message bigger than MTU, dropping packet')
            return False, err.FILE_TOO_BIG

        packet = Packet(rx_str=message, entry_point=Packet.UDP_DATA)
        packet.set_created_by_l4_ts()
        packet.add_udp_header(self.server_port, self.source_port, time.time())
        packet.add_transport_header(TransportHeader.UDP_TRANSPORT_TYPE, self.heaven_address)
        packet.requested_destination = self.server_address
        packet.requested_routing_method = self.routing_method
        packet.requested_max_hop = self.max_hop

        if self.packet_sender is not None:
            if not self.packet_sender.put(packet):
                packet.set_proc_by_l4_down_ts()
                self.profiler.profile_packet(packet, Packet.QUEUE_DROP + '_udp_tx_q')
        else:
            packet.set_proc_by_l4_down_ts()
            self.send_to_layer3(packet)

        return True, ''

    def stop(self):
        if self.packet_sender:
            self.packet_sender.stop()
            # self.packet_sender.join()
        self.request.end()

    def __str__(self):
        return 'dest:{}-{}, sp:{}, mh:{}, rm:{}'.format(self.server_address,
                                                        self.server_port,
                                                        self.source_port,
                                                        self.max_hop,
                                                        self.routing_method)
