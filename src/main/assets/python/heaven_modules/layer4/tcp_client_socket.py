import time
from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.common.utilities import send_tcp_msg, num_to_str
import threading
from heaven_modules.common.transport_header import TcpSocketHeader as tps


class TcpClientSocket:

    def __init__(self, heaven_address, server_address, server_ip_address, server_port, session_id, request,
                 client_stopped, file_str):
        self.heaven_address = heaven_address  # heaven address of the client
        self.server_address = server_address  # heaven address of the server
        self.server_ip_address = server_ip_address
        self.server_port = server_port  # port on which to contact the server
        self.session_id = session_id
        self.request = request
        self.client_stopped = client_stopped  # function to call after send
        self.file_str = file_str
        self.mex_thread = None  # run the communication on another thread

    def send_message(self):
        self.mex_thread = threading.Thread(target=self.do_send_message)
        self.mex_thread.start()
        return self.session_id

    def do_send_message(self):
        # print 'Sending {} to {}-{}:{}'.format(self.file_str, self.server_address, self.server_ip_address, self.server_port)
        file_str_len = len(self.file_str)
        data_to_send = ''.join((self.heaven_address,
                                self.session_id,
                                num_to_str(file_str_len, tps.MSG_LEN_LEN),
                                self.file_str))
        start_ts = time.time()
        result = send_tcp_msg((self.server_ip_address, self.server_port), data_to_send)
        elapsed_time = time.time() - start_ts

        if result:
            rate = file_str_len * 8 / elapsed_time
            self.request.reply(Replies.reply_tcp_socket_message_sent(self.session_id, 1, file_str_len, elapsed_time, rate))
        else:
            self.request.reply(Replies.reply_tcp_socket_error(self.server_address, self.server_port, self.session_id, 1))

        # if len(data_to_send) > 500:
        #     print('Sending {}...{} on tcp socket - {}B'.format(data_to_send[:200], data_to_send[-200:], len(data_to_send)))
        # else:
        #     print('Sending {} on tcp socket - {}B'.format(data_to_send, len(data_to_send)))

        self.request.end()
        self.client_stopped(self.session_id)


if __name__ == '__main__':

    class VoidRequest(object):

        def reply(self, *args):
            print('reply: {}'.format(args))

        def end(self):
            print('request end')

    def cli_stopped(*args):
        print(args)

    def send(msg):
        cli = TcpClientSocket('clie', 'serv', 'localhost', 30100, 'test', VoidRequest(), cli_stopped, msg)
        cli.send_message()

    def test_socket(start, end, step, ip='192.168.0.172', port=30100):
        for i in xrange(start, end, step):
            text = 'A' * i
            msg = ''.join(('luca', 'test', str(len(text)).zfill(tps.MSG_LEN_LEN), text))
            start_ts = time.time()
            result = send_tcp_msg((ip, port), msg)
            delta_t = time.time() - start_ts
            print('{} Bytes, result {}, rate: {:,} bps'.format(i, result, int(i*8/delta_t)))
            time.sleep(0.05)
            if not result:
                break
