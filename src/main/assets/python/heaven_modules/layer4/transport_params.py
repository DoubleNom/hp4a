class TransportParams(object):

    HEAVEN_ADDRESS_LENGTH = 4
    MIN_PORT_ALLOWED = 1000
    MAX_PORT_ALLOWED = 9999

    MAX_SOCKET_BUFFER_SIZE = 64000
    
    def __init__(self, config_reader):
        tl = config_reader.get_config_dict('transport_layer')
        self.DEFAULT_RETRANSM_TIMEOUT = float(tl['default_retransm_timeout'])
        self.DEFAULT_NULL_TIMEOUT = int(tl['default_null_timeout'])
        self.DEFAULT_MAX_RETRANSM = int(tl['default_max_retransm'])
        self.SSTRESH_START = int(tl['sstresh_start'])
        self.TCP_CONGESTION_WINDOW_START = int(tl['tcp_congestion_window_start'])
        self.TCP_CONGESTION_WINDOW_MIN = int(tl['tcp_congestion_window_min'])
        self.TCP_CONGESTION_WINDOW_MAX = int(tl['tcp_congestion_window_max'])
        self.TCP_CONGESTION_WINDOW_DECREASE_PARAM = float(tl['tcp_congestion_window_decrease_param'])
        self.DO_SAVE_FILES = int(tl['do_save_files'])
        self.BROADCAST_HEAVEN_ADDRESS = config_reader.get_config_dict('routing_layer')['broadcast_heaven_address']
        self.MAX_PAYLOAD_SIZE_GOSSIP = int(tl['max_payload_size_gossip'])
        self.MAX_PAYLOAD_SIZE_MTREE = int(tl['max_payload_size_mtree'])
        self.MAX_MSG_LEN = int(config_reader.get_config_dict('api_server')['max_msg_len'])
        self.SOCKET_MIN_PORT = int(tl['socket_min_port'])
        self.SOCKET_MAX_PORT = int(tl['socket_max_port'])

        try:
            self.FILES_PATH = tl['files_path']
        except:
            self.FILES_PATH = ''


    def is_valid_heaven_address(self, address, broadcast_address_allowed=False):

        if len(address) != self.HEAVEN_ADDRESS_LENGTH:
            return False

        if not broadcast_address_allowed and address == self.BROADCAST_HEAVEN_ADDRESS:
            return False

        return True

    def is_valid_port(self, port):

        if not isinstance(port, int):
            return False

        if port < self.MIN_PORT_ALLOWED or port > self.MAX_PORT_ALLOWED:
            return False

        return True

    def is_valid_socket_port(self, port):

        if not isinstance(port, int):
            return False

        if port < self.SOCKET_MIN_PORT or port > self.SOCKET_MAX_PORT:
            return False

        return True
