import time
from heaven_modules.common.tail_drop_queue import TailDropQueue
from heaven_modules.common.transport_header import TransportHeader, TcpHeader
from heaven_modules.common.packet import Packet
from heaven_modules.api.heaven_api_replies import Replies
import logging
from heaven_modules.common.routing_header import RoutingHeader
from heaven_modules.common.standard_threads import QueueServer, EventsSerializer


class TcpServerRxThread(QueueServer):
    """
    Thread instantiated from a TCP server in order to read from the rx queue
    Management of:
    - init data
    - data packets --> send ack
    - keep alive --> send keep alive ack
    - fin --> send fin ack
    Moreover these packets reset the session timer
    """

    def __init__(self, tcp_server, queue_len):
        QueueServer.__init__(self, TailDropQueue(maxsize=queue_len))
        self.tcp_server = tcp_server

        self.tcp_type_switch = {
            TcpHeader.TCP_DATA: self.tcp_server.receive_data,
            TcpHeader.TCP_INIT_DATA: self.tcp_server.receive_data_init,
            TcpHeader.TCP_SYN: self.tcp_server.receive_syn,
            TcpHeader.TCP_FIN: self.tcp_server.receive_fin
        }  # tcp type --> function to call with packet as param

    def handle(self, x):
        packet = x
        packet.set_rx_by_l4_up_ts()
        self.tcp_type_switch[packet.get_tcp_type()](packet)

    def receive_packet(self, packet):
        # print('+ PKT ID = {}; QSIZE = {}'.format(packet.id, self.rx_queue.qsize()))
        if not self.put(packet):
            # print('DROP')
            packet.set_rx_by_l4_up_ts()
            packet.set_proc_by_l4_up_ts()
            self.tcp_server.profiler.profile_packet(packet, Packet.QUEUE_DROP + '_tcp_server_rx_q')


class Session:
    """
    A Session is a connection with a client: withing a session, many messages can be sent
    """
    MAX_FINISHED_MSG = 30

    def __init__(self, session_id, retransm_timeout, null_timeout, max_retransm, source, ack_strategy):
        self.session_id = session_id
        self.retransm_timeout = retransm_timeout
        self.null_timeout = null_timeout
        self.max_retransm = max_retransm
        self.source = source
        self.ack_strategy = ack_strategy
        self.messages = {}  # mex_id --> message
        self.finished_messages = []  # [mex_id], used to send acks if data for finished messages arrives
        self.start_ts = time.time()

    def has_message(self, mex_id):
        return mex_id in self.messages

    def has_finished_message(self, mex_id):
        return mex_id in self.finished_messages

    def add_message(self, message):
        self.messages[message.mex_id] = message

    def get_message(self, mex_id):
        try:
            return self.messages[mex_id]
        except KeyError:
            return None

    def delete_message(self, mex_id):
        try:
            del self.messages[mex_id]
            self.finished_messages.append(mex_id)

            if len(self.finished_messages) > self.MAX_FINISHED_MSG:
                self.finished_messages.pop(0)

        except KeyError:
            pass


class Message:
    """
    A message is a set of packets to be received
    """

    def __init__(self, mex_id, n_blocks):
        self.mex_id = mex_id
        self.n_blocks = n_blocks
        self.received_packets = [None] * n_blocks  # used to store received packets
        self.blocks = range(n_blocks)  # used to check missing blocks / compute ack seq_n
        self.start_timestamp = time.time()
        self.end_timestamp = None
        self.reception_communicated = False

    def receive_data_packet(self, packet):
        """
        :param packet:
        :return: True if the message is complete, False otherwise
        """
        seq_n = packet.get_tcp_seq_n()
        try:
            self.blocks.remove(seq_n)
            self.received_packets[seq_n] = packet
        except ValueError:
            logging.getLogger(__name__).debug('TCP server received duplicate block {}/{}'.format(seq_n, self.n_blocks))
            return

        if self.is_message_complete() and not self.end_timestamp:
            self.end_timestamp = time.time()

    def get_complete_message(self):
        return ''.join(packet.get_tcp_payload() for packet in self.received_packets)

    def is_message_complete(self):
        return not self.blocks


class TcpServer:

    def __init__(self, tcp_layer, port, request, tp, profiler):
        self.tcp_layer = tcp_layer  # reference to the unique tcp layer
        self.port = port  # port on which this server is listening
        self.request = request  # request to which reply on events
        self.tp = tp
        self.profiler = profiler

        self.sessions = {}  # (session_id, source) --> Session
        self.rx_thread = TcpServerRxThread(self, self.tp.TCP_CONGESTION_WINDOW_START)  # thread that receive packets
        self.receive_commands = EventsSerializer(TailDropQueue())
        self.receive_commands.start()

    def receive_packet(self, packet):
        self.rx_thread.receive_packet(packet)

    def start(self):
        self.rx_thread.start()

    def stop(self):
        self.receive_commands.enqueue(None, self.stop_server)

    def stop_server(self):
        self.receive_commands.stop()
        # send FIN packets to all tcp clients
        sessions = self.sessions.keys()[:]
        for session in sessions:
            self.send_fin_ack(session)

        self.sessions = {}
        self.request.end()
        self.rx_thread.stop()
        # self.rx_thread.join()

    def send(self, packet, destination, routing_method):
        packet.set_proc_by_l4_down_ts()
        packet.requested_destination = destination
        packet.requested_routing_method = routing_method
        self.tcp_layer.send(packet)

    # --------------------------------- SESSION MANAGEMENT -----------------------------------

    def receive_syn(self, packet):
        """
        When a connection request is received, read and save the session information
        from the SYN packet
        """
        # logging.getLogger(__name__).debug('TCP Server received SYN: {}'.format(packet))

        # if an old session already exists, delete it first
        if (packet.get_tcp_session_id(), packet.get_transport_source()) in self.sessions:
            if time.time() - self.sessions[(packet.get_tcp_session_id(), packet.get_transport_source())].start_ts > 5:
                del self.sessions[(packet.get_tcp_session_id(), packet.get_transport_source())]

        # if the session does not exist, create it
        if (packet.get_tcp_session_id(), packet.get_transport_source()) not in self.sessions:
            self.sessions[(packet.get_tcp_session_id(), packet.get_transport_source())] = Session(
                packet.get_tcp_session_id(),
                packet.get_tcp_retransm_timeout(),
                packet.get_tcp_null_timeout(),
                packet.get_tcp_max_retransm(),
                packet.get_transport_source(),
                packet.get_tcp_ack_strategy())

        packet.set_proc_by_l4_up_ts()
        self.profiler.profile_packet(packet, Packet.RECEIVED_TCP_SYN)

        # send acks also for duplicated packets because they could get lost

        # Create syn ack packet
        syn_ack_packet = Packet(entry_point=Packet.TCP_SYN_ACK)

        syn_ack_packet.set_created_by_l4_ts()
        syn_ack_packet.add_tcp_syn_header(
            packet.get_tcp_retransm_timeout(),
            packet.get_tcp_null_timeout(),
            packet.get_tcp_max_retransm(),
            packet.get_tcp_ack_strategy())

        syn_ack_packet.add_tcp_header(
            packet.get_tcp_server_port(),
            TcpHeader.TCP_SYN_ACK,
            packet.get_tcp_session_id(),
            packet.get_tcp_seq_n(),
            packet.get_tcp_retransm_n(),
            packet.get_tcp_mex_id())

        syn_ack_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.tcp_layer.heaven_address)
        self.send(syn_ack_packet, packet.get_transport_source(), packet.get_routing_method())
        logging.getLogger(__name__).debug('TCP Server sent SYN ack')

    # create the data structure to receive data, reset the session timer and send init ack
    def receive_data_init(self, packet):
        """
        The value M of the Sequence number of the init data package is the number of packets that
        make up the message that will follow: the server prepares a data structure of M blocks
        The init message is not counted
        """
        if (packet.get_tcp_session_id(), packet.get_transport_source()) not in self.sessions:
            packet.set_proc_by_l4_up_ts()
            self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
            logging.getLogger(__name__).warning(
                'TCP server received data init for non existing session, sending fin_ack, {}'.format(packet))
            self.send_fin_ack((packet.get_tcp_session_id(), packet.get_transport_source()))
            return

        if not self.sessions[(packet.get_tcp_session_id(), packet.get_transport_source())].has_message(packet.get_tcp_mex_id()):
            self.sessions[(packet.get_tcp_session_id(), packet.get_transport_source())].add_message(
                Message(packet.get_tcp_mex_id(), packet.get_tcp_seq_n()))

        packet.set_proc_by_l4_up_ts()
        self.profiler.profile_packet(packet, Packet.RECEIVED_TCP_DATA_INIT)

        logging.getLogger(__name__).debug('TCP Server received data init: {}'.format(packet))

        data_init_ack_packet = Packet(entry_point=Packet.TCP_DATA_INIT_ACK)

        data_init_ack_packet.set_created_by_l4_ts()

        data_init_ack_packet.add_tcp_header(
            packet.get_tcp_server_port(),
            TcpHeader.TCP_INIT_DATA_ACK,
            packet.get_tcp_session_id(),
            packet.get_tcp_seq_n(),
            packet.get_tcp_retransm_n(),
            packet.get_tcp_mex_id())

        data_init_ack_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.tcp_layer.heaven_address)

        self.send(data_init_ack_packet, packet.get_transport_source(), packet.get_routing_method())

    # send data ack and reset session timer
    def receive_data(self, packet):
        """
        Note: you can receive data for completed messages!
        :param packet:
        :return:
        """
        try:
            session = self.sessions[(packet.get_tcp_session_id(), packet.get_transport_source())]
        except KeyError:
            packet.set_proc_by_l4_up_ts()
            self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
            logging.getLogger(__name__).warning(
                'TCP server received data for non existing session-source, {}'.format(packet))
            return

        if session.has_message(packet.get_tcp_mex_id()):
            message = session.get_message(packet.get_tcp_mex_id())
            message.receive_data_packet(packet)
        elif session.has_finished_message(packet.get_tcp_mex_id()):
            message = None
        else:
            packet.set_proc_by_l4_up_ts()
            self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
            logging.getLogger(__name__).warning(
                'TCP server received data for non existing message in existing session, {}'.format(packet))
            return

        packet.set_proc_by_l4_up_ts()
        self.profiler.profile_packet(packet, Packet.RECEIVED_TCP_DATA)

        # The seq_n in the ack is the same of the received packet

        # Create the ack
        ack_packet = Packet(entry_point=Packet.TCP_DATA_ACK)
        ack_packet.set_created_by_l4_ts()
        ack_packet.add_tcp_header(packet.get_tcp_server_port(), TcpHeader.TCP_ACK, packet.get_tcp_session_id(),
                                  packet.get_tcp_seq_n(), packet.get_tcp_retransm_n(), packet.get_tcp_mex_id())

        ack_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.tcp_layer.heaven_address)

        self.send(ack_packet, packet.get_transport_source(), packet.get_routing_method())

        # See if all data is received
        if message and message.is_message_complete() and not message.reception_communicated:

            complete_data = message.get_complete_message()
            message.reception_communicated = True
            data_len = len(complete_data)
            elapsed_time = message.end_timestamp - message.start_timestamp
            bitrate = data_len * 8.0 / elapsed_time

            if data_len > self.tp.MAX_MSG_LEN:

                if self.tp.DO_SAVE_FILES:
                    file_path = '{}/rx_f_{:.10f}.txt'.format(self.tp.FILES_PATH, time.time())
                    fh = open(file_path, 'wb')
                    fh.write(complete_data)
                    fh.close()

                    self.request.reply(Replies.reply_tcp_server_received_file(self.port,
                                                                              packet.get_transport_source(),
                                                                              packet.get_tcp_session_id(),
                                                                              file_path,
                                                                              data_len,
                                                                              elapsed_time,
                                                                              bitrate))
                else:
                    self.request.reply(Replies.reply_tcp_server_received_file(self.port,
                                                                              packet.get_transport_source(),
                                                                              packet.get_tcp_session_id(),
                                                                              'received_file_not_saved',
                                                                              data_len,
                                                                              elapsed_time,
                                                                              bitrate))
            else:
                self.request.reply(Replies.reply_tcp_server_received_message(self.port,
                                                                             packet.get_transport_source(),
                                                                             packet.get_tcp_session_id(),
                                                                             complete_data,
                                                                             data_len,
                                                                             elapsed_time,
                                                                             bitrate))

            self.remove_message(packet.get_tcp_session_id(), packet.get_transport_source(), packet.get_tcp_mex_id())

    def remove_message(self, session_id, source, mex_id):
        self.receive_commands.enqueue(None, self.do_remove_message, (session_id, source, mex_id))

    def do_remove_message(self, x):
        session_id, source, mex_id = x
        if (session_id, source) in self.sessions:
            self.sessions[(session_id, source)].delete_message(mex_id)

    def receive_fin(self, packet):
        self.receive_commands.enqueue(None, self.do_remove_session, (packet.get_tcp_session_id(),
                                                                     packet.get_transport_source()))
        packet.set_proc_by_l4_up_ts()
        self.profiler.profile_packet(packet, Packet.RECEIVED_TCP_FIN)

    def do_remove_session(self, x):
        session_id, source = x
        if (session_id, source) in self.sessions:
            del self.sessions[(session_id, source)]

    def send_fin_ack(self, session):
        # Create the fin ack packet
        session_id, session_source = session
        fin_ack_packet = Packet(entry_point=Packet.TCP_FIN_ACK)
        fin_ack_packet.set_created_by_l4_ts()
        fin_ack_packet.add_tcp_header(self.port, TcpHeader.TCP_FIN_ACK, session_id, 0, 0, '')
        fin_ack_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.tcp_layer.heaven_address)
        self.send(fin_ack_packet, session_source, RoutingHeader.ROUTING_METHOD_GOSSIP)




