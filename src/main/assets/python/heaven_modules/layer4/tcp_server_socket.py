import SocketServer
import time
import logging
from threading import Thread
from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.common.transport_header import TcpSocketHeader as tps
import six


class MyTcpServerHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        """
        Called when a client connects
        :return:
        """
        start_ts = time.time()
        # read only the header and check if the message can be received
        header = self.request.recv(tps.HEADER_LENGTH).strip()

        if not six.PY2:
            header = header.decode('utf8')

        if not header or len(header) < tps.HEADER_LENGTH:
            logging.getLogger(__name__).debug('Wrong header length: {}'.format(len(header)))
            return

        try:
            source, sessiond_id, message_len = header[:tps.SOURCE_END], \
                                               header[tps.SESSION_ID_START:tps.SESSION_ID_END], \
                                               header[tps.MSG_LEN_START:tps.MSG_LEN_END]
        except ValueError:
            logging.getLogger(__name__).debug('Wrong header format: {}'.format(header))
            return

        try:
            message_len = int(message_len)
        except ValueError:
            logging.getLogger(__name__).debug('Message length is not int: {}'.format(header))
            return

        if not message_len:
            logging.getLogger(__name__).debug('Message length is empty: {}'.format(header))
            return

        # # blocks are ecchanged, not appended!!!
        # try:
        #     rx_buffer = bytearray(message_len)
        # except OverflowError:
        #     logging.getLogger(__name__).debug('Cannot create a buffer so big: {}'.format(header))
        #     return
        #
        # bytes_received = 0
        # while bytes_received != message_len:
        #     received = self.request.recv_into(rx_buffer, message_len)  # receive the message in the prepared buffer
        #     if received == 0:
        #         logging.getLogger(__name__).debug('received 0 Bytes')
        #         return
        #     bytes_received += received

        rx_buffer = ''
        while len(rx_buffer) != message_len:

            # try to rx as musch as possible into a tmp string
            tmp_str = self.request.recv(message_len)

            if len(tmp_str) == 0:
                logging.getLogger(__name__).debug('received 0 Bytes')
                return

            rx_buffer += tmp_str  # receive the message in the prepared buffer

        logging.getLogger(__name__).debug('Received message: {}'.format(header))

        if six.PY2:
            rx_buffer = rx_buffer
        else:
            rx_buffer = rx_buffer.decode('utf8')

        self.server.message_received(rx_buffer, source, sessiond_id, time.time() - start_ts)


class MyTcpServer(SocketServer.TCPServer):

    def __init__(self, port, message_received):
        self.port = port
        self.message_received = message_received

        while True:
            try:
                SocketServer.TCPServer.__init__(self, ('', self.port), MyTcpServerHandler)
            except:
                time.sleep(1)
            else:
                break

    def stop(self):
        self.server_close()
        self.shutdown()


class TcpServerSocket:

    def __init__(self, tcp_layer, port, request, tp):
        self.tcp_layer = tcp_layer  # reference to the unique tcp layer
        self.port = port  # port on which this server is listening
        self.request = request  # request to which reply on events
        self.tp = tp
        self.server = MyTcpServer(port, self.message_received)
        self.data_server_thread = Thread(target=self.server.serve_forever, name='TCP_SERVER_{}'.format(port))

    def start(self):
        self.data_server_thread.start()

    def message_received(self, data, source, session_id, elapsed_time):

        data_len = len(data)
        bitrate = data_len * 8 / elapsed_time

        if len(data) > self.tp.MAX_MSG_LEN:

            if self.tp.DO_SAVE_FILES:
                file_path = '{}/rx_f_{:.10f}.txt'.format(self.tp.FILES_PATH, time.time())
                with open(file_path, 'wb') as fh:
                    fh.write(data)
                self.request.reply(Replies.reply_tcp_server_received_file(
                    self.port, source, session_id, file_path, data_len, elapsed_time, bitrate))
            else:
                self.request.reply(Replies.reply_tcp_server_received_file(
                    self.port, source, session_id, 'received_file_not_saved', data_len, elapsed_time, bitrate))
        else:
            self.request.reply(Replies.reply_tcp_server_received_message(
                self.port, source, session_id, data, data_len, elapsed_time, bitrate))

    def stop(self):
        self.server.stop()
        self.request.end()


if __name__ == '__main__':

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(module)s %(name)s.%(funcName)s +%(lineno)s: %(levelname)-8s [%(process)d] %(message)s')

    def void_request(*args):
        print('RX message callback called')


    server = MyTcpServer(30100, void_request)
    data_server_thread = Thread(target=server.serve_forever, name='pippo')
    data_server_thread.start()
    try:
        while 1:
            time.sleep(60)
    except KeyboardInterrupt:
        server.stop()



