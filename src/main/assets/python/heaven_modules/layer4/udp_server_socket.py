from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.common.standard_threads import HeavenSocketServer
from heaven_modules.common.transport_header import UdpSocketHeader as ush
import logging
import socket


class DataServer(HeavenSocketServer):

    def __init__(self, data_port, rx_buffer_size, rx_queue_len, request):
        HeavenSocketServer.__init__(self, ('', data_port), rx_buffer_size, rx_queue_len,
                                    socket_family=socket.AF_INET, socket_type=socket.SOCK_DGRAM,
                                    name='Udp Server Socket')
        self.port = data_port
        self.request = request

    def handle(self, x):

        data, client_address = x

        if len(data) < ush.HEADER_LENGTH:
            return

        try:
            source, client_id = data[ush.SOURCE_START:ush.SOURCE_END], data[ush.CLIENT_ID_START:ush.CLIENT_ID_END]
        except ValueError:
            return

        logging.getLogger(__name__).debug('From {}, Session {}, Received {} Bytes message: {}...'.format(
            source, client_id, len(data[ush.PAYLOAD_START:]), data[ush.PAYLOAD_START:ush.PAYLOAD_START + 100]))

        msg = data[ush.PAYLOAD_START:]

        self.request.reply(Replies.reply_udp_server_received_message(
            self.port, source, client_id, msg, len(msg)))


class UdpServerSocket:

    def __init__(self, port, rx_buffer_size, rx_queue_len, request):
        self.request = request
        self.server = DataServer(port, rx_buffer_size, rx_queue_len, request)
        self.server.start()

    def stop(self):
        self.server.stop()
        self.request.end()


if __name__ == '__main__':

    import time

    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(module)s %(name)s.%(funcName)s +%(lineno)s: %(levelname)-8s [%(process)d] %(message)s')

    class VoidRequest(object):

        def reply(self, *args):
            print('reply received')

        def end(self):
            print('request end')

    def test_server(port=30200, mtu=4096, queue_len=100):
        server = UdpServerSocket(port, mtu, queue_len, VoidRequest())
        try:
            while 1:
                time.sleep(60)
        except KeyboardInterrupt:
            server.stop()

