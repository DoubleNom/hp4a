from transport_params import TransportParams
from heaven_modules.layer3.routing_params import RoutingParams
from heaven_modules.common.routing_header import RoutingHeader
from udp_server import UdpServer
from udp_client import UdpClient
import threading
from heaven_modules.common.utilities import IdGenerator
from heaven_modules.common.packet import Packet
from udp_client_socket import UdpClientSocket
from udp_server_socket import UdpServerSocket
from heaven_modules.api.heaven_api_replies import ErrorDescriptions as err


class UdpLayer:

    def __init__(self, heaven_address, send_to_layer3, get_ip_address, config_reader, profiler):
        self.heaven_address = heaven_address
        self.send_to_layer3 = send_to_layer3
        self.get_ip_address = get_ip_address
        self.profiler = profiler

        self.udp_clients = {}  # source_port --> client object
        self.udp_servers = {}  # server_port --> server object
        self.clients_lock = threading.Lock()  # protect dicts from modification during iteration
        self.servers_lock = threading.Lock()  # protect dicts from modification during iteration

        self.udp_clients_socket = {}  # source_port --> client object
        self.udp_servers_socket = {}  # server_port --> server object
        self.clients_socket_lock = threading.Lock()  # protect dicts from modification during iteration
        self.servers_socket_lock = threading.Lock()  # protect dicts from modification during iteration

        self.tp = TransportParams(config_reader)
        self.rp = RoutingParams(config_reader)
        self.do_use_multi_tree = bool(int(config_reader.get_config_dict('routing_layer')['do_use_multi_tree']))

        self.source_port_generator = IdGenerator(None, self.tp.MIN_PORT_ALLOWED, self.tp.MAX_PORT_ALLOWED)

        self.stopping_layer = False

    def stop(self):

        self.stopping_layer = True

        with self.clients_lock:
            for client in self.udp_clients.itervalues():
                client.stop()
            self.udp_clients = {}

        with self.servers_lock:
            for server in self.udp_servers.itervalues():
                server.stop()
            self.udp_servers = {}

        with self.clients_socket_lock:
            for client in self.udp_clients_socket.itervalues():
                client.stop()
            self.udp_clients_socket = {}

        with self.servers_socket_lock:
            for server in self.udp_servers_socket.itervalues():
                server.stop()
            self.udp_servers_socket = {}

    """
    Functions called by the transport manager because of API calls
    """
    # ------------------------- UDP SERVER ------------------------- #

    def create_server(self, server_port, request):

        if not self.tp.is_valid_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        with self.servers_lock:

            if self.stopping_layer:
                return False, err.HEAVEN_IS_STOPPING

            if server_port in self.udp_servers:
                return False, err.SERVER_PORT_NOT_AVAILABLE

            self.udp_servers[server_port] = UdpServer(server_port, request, self.profiler)

        return True, ''

    def stop_udp_server(self, server_port):

        with self.servers_lock:

            if self.stopping_layer:
                return True, ''

            if server_port not in self.udp_servers:
                return False, err.SERVER_PORT_NOT_FOUND

            del self.udp_servers[server_port]

        return True, ''

    def get_udp_servers(self):
        return self.udp_servers.keys()

    # ---------------------- UDP SERVER SOCKET --------------------- #

    def create_server_socket(self, server_port, rx_buffer_size, rx_queue_len, request):

        if not self.tp.is_valid_socket_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        if rx_buffer_size > self.tp.MAX_SOCKET_BUFFER_SIZE:
            return False, err.BUFFER_SIZE_TOO_BIG

        with self.servers_socket_lock:

            if self.stopping_layer:
                return False, err.HEAVEN_IS_STOPPING

            if server_port in self.udp_servers_socket:
                return False, err.SERVER_PORT_NOT_AVAILABLE

            self.udp_servers_socket[server_port] = UdpServerSocket(
                server_port, rx_buffer_size, rx_queue_len, request)

        return True, ''

    def stop_udp_server_socket(self, server_port):

        with self.servers_socket_lock:

            if self.stopping_layer:
                return True, ''

            if server_port not in self.udp_servers_socket:
                return False, err.SERVER_PORT_NOT_FOUND

            self.udp_servers_socket[server_port].stop()
            del self.udp_servers_socket[server_port]

        return True, ''

    def get_udp_servers_socket(self):
        return self.udp_servers_socket.keys()

    # ------------------------- UDP CLIENT ------------------------- #

    def create_client(self, destination_heaven_address, server_port, mtu, max_hop, packet_rate,
                      routing_method, request):

        if not self.tp.is_valid_heaven_address(destination_heaven_address, broadcast_address_allowed=True):
            return False, err.HEAVEN_ADDRESS_NOT_VALID

        if not self.tp.is_valid_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        if not self.do_use_multi_tree and routing_method == RoutingHeader.ROUTING_METHOD_MULTI_TREE:
            return False, err.MULTI_TREE_DISABLED

        if routing_method == RoutingHeader.ROUTING_METHOD_MULTI_TREE:
            max_payload_size = self.tp.MAX_PAYLOAD_SIZE_MTREE
        elif routing_method == RoutingHeader.ROUTING_METHOD_GOSSIP:
            max_payload_size = self.tp.MAX_PAYLOAD_SIZE_GOSSIP
        else:
            return False, err.ROUTING_METHOD_NOT_VALID

        if mtu > max_payload_size:
            return False, err.MTU_TOO_BIG

        if mtu == 0:
            mtu = max_payload_size

        if max_hop > self.rp.MAX_HOP:
            return False, err.MAX_HOP_TOO_BIG

        if max_hop == 0:
            max_hop = self.rp.MAX_HOP

        with self.clients_lock:

            if self.stopping_layer:
                return False, err.HEAVEN_IS_STOPPING

            # Generate client port
            source_port = int(self.source_port_generator.get_id())

            self.udp_clients[source_port] = UdpClient(self.heaven_address,
                                                      destination_heaven_address,
                                                      server_port,
                                                      source_port,
                                                      mtu,
                                                      max_hop,
                                                      packet_rate,
                                                      self.send_to_layer3,
                                                      routing_method,
                                                      request,
                                                      self.profiler)

        return source_port, ''

    def send(self, message, source_port):
        try:
            return self.udp_clients[source_port].send(message)
        except KeyError:
            return False, err.SOURCE_PORT_NOT_FOUND

    def stop_udp_client(self, source_port):

        with self.clients_lock:

            if self.stopping_layer:
                return True, ''

            if source_port not in self.udp_clients:
                return False, err.SOURCE_PORT_NOT_FOUND

            self.udp_clients[source_port].stop()
            del self.udp_clients[source_port]
            self.source_port_generator.del_id(source_port)

        return True, ''

    def get_udp_clients(self):
        return [str(self.udp_clients[k]) for k in self.udp_clients.keys()]

    # ---------------------- UDP CLIENT SOCKET---------------------- #

    def create_client_socket(self, server_address, server_port, tx_timeout, request):

        if not self.tp.is_valid_heaven_address(server_address):
            return False, err.HEAVEN_ADDRESS_NOT_VALID

        if not self.tp.is_valid_socket_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        if not self.rp.DO_USE_MULTI_TREE:
            return False, err.MULTI_TREE_DISABLED

        server_ip_address = self.get_ip_address(server_address)
        if server_ip_address is None:
            return False, err.IP_LOOKUP_FAILED

        with self.clients_socket_lock:

            if self.stopping_layer:
                return False, err.HEAVEN_IS_STOPPING

            # Generate client port
            client_id = self.source_port_generator.get_id()

            self.udp_clients_socket[client_id] = UdpClientSocket(
                self.heaven_address, server_ip_address, server_port, client_id, tx_timeout, request)

        return client_id, ''

    def send_socket(self, message, client_id):

        if len(message) > self.tp.MAX_SOCKET_BUFFER_SIZE:
            return False, err.MESSAGE_TOO_BIG

        try:
            self.udp_clients_socket[client_id].send(message)
            return True, ''
        except KeyError:
            return False, err.CLIENT_ID_NOT_FOUND

    def stop_udp_client_socket(self, client_id):

        with self.clients_socket_lock:

            if self.stopping_layer:
                return True, ''

            if client_id not in self.udp_clients_socket:
                return False, err.CLIENT_ID_NOT_FOUND

            self.udp_clients_socket[client_id].stop()
            del self.udp_clients_socket[client_id]
            self.source_port_generator.del_id(client_id)

        return True, ''

    def get_udp_clients_socket(self):
        return self.udp_clients_socket.keys()

    """
    Functions called by heaven internally
    """
    def receive_udp_packet(self, packet):
        """
        Calld by the lower layer when a packet is received from the network
        :param packet:
        :return:
        """
        try:
            self.udp_servers[packet.get_udp_destination_port()].receive_udp_packet(packet)
            packet.set_rx_by_l4_up_ts()
        except KeyError:
            packet.set_proc_by_l4_up_ts()
            self.profiler.profile_packet(packet, Packet.WRONG_UDP_SESSION_DATA)
