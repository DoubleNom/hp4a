import time
import math
from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.common.transport_header import TransportHeader, TcpHeader, TcpSynHeader
from heaven_modules.common.packet import Packet
import Queue
from heaven_modules.common.routing_header import RoutingHeader as rh
from heaven_modules.common.standard_threads import PeriodicalCaller, EventsSerializer, QueueServer
from heaven_modules.common.utilities import IdGenerator
import logging
from heaven_modules.common.tail_drop_queue import PriorityTailDropQueue
from heaven_modules.api.heaven_api_replies import ErrorDescriptions as err


class RtoEstimator:
    """
    As defined by IETF in RFC6298 https://tools.ietf.org/html/rfc6298

    ALPHA: reactivity of srtt updates 0<x<1, recommended 1.0/8 = 0.125
    0: srtt does not update with new rtt.
    1: srtt = new rtt

    BETA: reactivity of rttvar updates 0<x<1, recommended 1.0/4 = 0.25
    0: rttvar does not update with new rtt.
    1: rttvar = abs(srtt - new_rtt)

    K: margin over srtt, int, recommended 4

    RTO = SRTT + K*RTTVAR
    """
    K = 4
    ALPHA = 0.25
    BETA = 0.25
    MIN_RTO = 0.2
    MAX_RTO = 7

    def __init__(self, starting_rto):
        self.srtt = None  # smoothed RTT
        self.rttvar = None  # variance of RTT
        self.rto = starting_rto

    # noinspection PyAttributeOutsideInit
    def new_rtt(self, rtt):
        """
        Set RTO value after the first RTT
        :param rtt:
        :return:
        """
        self.srtt = rtt
        self.rttvar = rtt/2.0
        self.rto = max(self.MIN_RTO, self.srtt + self.K * self.rttvar)
        self.new_rtt = self.new_rtt2  # redefine this function at runtime

    def new_rtt2(self, rtt):
        """
        Update RTO value with a new RTT
        After the first time, this function will be run instead of new_rtt
        :param rtt:
        :return:
        """
        self.rttvar = (1 - self.BETA) * self.rttvar + self.BETA * abs(self.srtt - rtt)
        self.srtt = (1 - self.ALPHA) * self.srtt + self.ALPHA * rtt
        self.rto = int(max(self.MIN_RTO, self.srtt + self.K * self.rttvar))
        self.rto = min(self.MAX_RTO, self.rto)


class TransmissionRegulator(QueueServer):
    """
    Calls the send function for a packet as soon as a transmission token is available.
    Once a packet has been transmitted for the first time, retransmissions do not need new tokens.
    """
    def __init__(self, tcp_client):
        QueueServer.__init__(self, Queue.Queue())
        self.tcp_client = tcp_client
        self.tx_tokens = Queue.Queue()

    def handle(self, x):
        packet = x
        packet.set_created_by_l4_ts()

        self.tx_tokens.get()  # Blocks here until a token is available

        # Maybe the thread has been stopped while waiting for the token
        if not self.do_run:
            self.tx_tokens.task_done()
            return

        self.tcp_client.send_packet(packet)
        self.tcp_client.unacked_packets += 1
        self.tx_tokens.task_done()

    def stop(self):
        QueueServer.stop(self)
        self.add_tokens(1)

    def reset_queues(self):
        while not self.tx_tokens.empty():
            self.tx_tokens.get()
            self.tx_tokens.task_done()
        while not self.queue.empty():
            self.queue.get()
            self.queue.task_done()

    def add_packet(self, packet):
        self.put(packet)

    def add_packets(self, packets):
        map(self.put, packets)

    def add_tokens(self, n_tokens):
        for i in xrange(n_tokens):
            self.tx_tokens.put(1)


class Message:
    """
    Manages all the parameters and the segments of a message to be transmitted
    """

    def __init__(self, tcp_client, request, mex_id, file_str):
        self.tcp_client = tcp_client
        self.request = request
        self.mex_id = mex_id
        self.file_size = len(file_str)
        self.n_segments = int(math.ceil(float(self.file_size) / self.tcp_client.max_payload_size))
        self.unacked_list = range(self.n_segments)  # this must be a list, order is important
        self.data_init_packet = None
        self.last_clock_event = 0
        self.start_timestamp = None  # set when the data init ack has been received
        self.end_timestamp = None  # set when all segments have been acked

        # create and store a Packet for each segment to be transmitted
        self.packets = []
        i = 0
        payload_size = self.tcp_client.max_payload_size  # save reference to save time
        for seq_n in xrange(self.n_segments):
            data_packet = Packet(rx_str=file_str[i:i + payload_size], entry_point=Packet.TCP_DATA)
            data_packet.add_tcp_header(self.tcp_client.server_port, TcpHeader.TCP_DATA, self.tcp_client.session_id, seq_n, 0, self.mex_id)
            data_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.tcp_client.heaven_address)
            self.packets.append(data_packet)
            i += payload_size

    def start(self):
        """
        send data init packet
        :return:
        """
        # create the data init packet
        self.data_init_packet = Packet(entry_point=Packet.TCP_DATA_INIT)
        # self.data_init_packet.set_created_by_l4_ts()
        self.data_init_packet.add_tcp_header(self.tcp_client.server_port, TcpHeader.TCP_INIT_DATA,
                                             self.tcp_client.session_id, self.n_segments, 0, self.mex_id)
        self.data_init_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.tcp_client.heaven_address)

        # send the data init packet
        self.last_clock_event = time.time()
        self.tcp_client.add_tokens(1)  # Add a token to tx the data init
        self.tcp_client.add_packet(self.data_init_packet)

    def receive_init_data_ack(self, packet):

        # discard duplicate acks or acks for completed messages
        if self.start_timestamp or self.end_timestamp:
            packet.set_proc_by_l4_up_ts()
            self.tcp_client.profiler.profile_packet(packet, Packet.OLD_TCP_SESSION_DATA)
            logging.getLogger(__name__).warning(
                'TCP client received duplicated data init ack for existing message: {}'.format(packet))
            return

        logging.getLogger(__name__).debug('TCP client received data init ack: {}'.format(packet))

        self.start_timestamp = time.time()
        self.data_init_packet.acked_ts = self.start_timestamp
        self.tcp_client.unacked_packets = 0
        self.tcp_client.increase_congestion_window()
        self.tcp_client.add_tokens(self.tcp_client.congestion_window)
        self.tcp_client.rto_estimator.new_rtt(self.start_timestamp - self.data_init_packet.start_ts)
        self.tcp_client.add_packets(self.packets)  # put data packet in the transmission queue
        packet.set_proc_by_l4_up_ts()
        self.tcp_client.profiler.profile_packet(packet, Packet.RECEIVED_TCP_DATA_INIT_ACK)

    def receive_data_ack(self, packet):

        seq_n = packet.get_tcp_seq_n()
        logging.getLogger(__name__).debug('TCP client received ack {}/{}, len(unacked): {}'.format(
            seq_n, self.n_segments, len(self.unacked_list)))

        # if the ack is duplicated, exit immediately
        try:
            self.unacked_list.remove(seq_n)
        except ValueError:
            logging.getLogger(__name__).debug('TCP client received duplicated ack {}/{} ...'.format(
                seq_n, self.n_segments))
            packet.set_proc_by_l4_up_ts()
            self.tcp_client.profiler.profile_packet(packet, Packet.OLD_TCP_SESSION_DATA)
            return

        data_packet = self.packets[seq_n]  # take a local reference to gain speed
        data_packet.acked_ts = time.time()

        # if this was the last ack
        if not self.unacked_list:
            self.end_timestamp = time.time()
            # redefine this function at runtime to discard acks for messages already sent
            # noinspection PyAttributeOutsideInit
            self.receive_data_ack = self.receive_data_ack_after_end

            elapsed_time = self.end_timestamp - self.start_timestamp
            rate = self.file_size * 8 / elapsed_time
            self.request.reply(Replies.reply_tcp_message_sent(self.tcp_client.session_id, self.mex_id, 1,
                                                              self.file_size, elapsed_time, rate))
            self.stop()

        # update the estimator only for packets that have not been retransmitted
        if data_packet.get_tcp_header().retransm_n == 0:
            self.tcp_client.rto_estimator.new_rtt(data_packet.acked_ts - data_packet.start_ts)

        self.tcp_client.unacked_packets -= 1
        self.tcp_client.increase_congestion_window()

        # add tokens due to the new congestion window and to replace this packet
        new_tokens = self.tcp_client.congestion_window - self.tcp_client.unacked_packets
        self.tcp_client.add_tokens(new_tokens)

        packet.set_proc_by_l4_up_ts()
        self.tcp_client.profiler.profile_packet(packet, Packet.RECEIVED_TCP_DATA_ACK)

    def receive_data_ack_after_end(self, packet):
        packet.set_proc_by_l4_up_ts()
        self.tcp_client.profiler.profile_packet(packet, Packet.OLD_TCP_SESSION_DATA)
        logging.getLogger(__name__).debug('TCP client received ack for a completed message'.format(packet))

    def do_clock_event(self):
        """
        Called every DeltaT from another thread, it will trigger the check of timeout expiration of each packet
        transmitted and not acked.
        :return:
        """
        logging.getLogger(__name__).debug('TCP client doing clock event')
        now = time.time()
        delta_t = now - self.last_clock_event
        self.last_clock_event = now
        if self.start_timestamp is None and self.data_init_packet is not None:
            self.tcp_client.check_packet_timeout(self.data_init_packet, delta_t)
            # Cannot redefine the function because client serializer has a reference to this very function
        else:
            # loop on [seq_n] of packets that has been transmitted at least once (start_timestamp is set)
            # and that has not been acked yet
            # Note: 5 is a margin
            for seq_n in self.unacked_list[:self.tcp_client.unacked_packets + 5]:
                if self.packets[seq_n].start_ts is not None:
                    self.tcp_client.check_packet_timeout(self.packets[seq_n], delta_t)

    def stop(self):
        self.request.end()
        self.tcp_client.request_remove_message(self.mex_id)


class TcpClient:
    """
    Class that instantiate a TCP client in order to establish a session with a server.
    The class allows then to send messages through the established connection,
    managing transmission speed and retransmissions.
    A client can send multiple messages (mex id) to the server, but can have only ONE session to a server.
    So a Client is a TCP session.

    Only One Message at a time can be sent. Messages are stored in a queue until it is possible to send them.
    """

    # Events serializer constants. Lower numbers are served first

    N_PRIORITIES = 6
    PRIORITY_STOP_CLIENT = 0
    PRIORITY_RX_ACK = 1
    PRIORITY_CLOCK_EVENT = 2
    PRIORITY_SEND_RETX = 3
    PRIORITY_SEND_NEW = 4
    PRIORITY_OTHER = 5

    def __init__(self, tcp_layer, heaven_address, server_address, server_port, session_id, routing_method,
                 request, tp, profiler):
        
        self.tcp_layer = tcp_layer  # used to pass data to the lower layer
        self.heaven_address = heaven_address  # heaven address of the client
        self.server_address = server_address  # heaven address of the server
        self.server_port = server_port  # port on which to contact the server
        self.session_id = session_id  # id for the connection, randomly generated by the source
        self.routing_method = routing_method
        self.request = request
        self.profiler = profiler

        # TCP params
        self.tp = tp
        self.congestion_window = self.tp.TCP_CONGESTION_WINDOW_START
        self.sstresh = self.tp.SSTRESH_START
        self.unacked_packets = 0
        self.filling_counter = 0
        self.n_tokens = 0
        self.consecutive_time_outs = 0

        # serializer
        self.events_serializer = EventsSerializer(PriorityTailDropQueue(maxsize=self.tp.TCP_CONGESTION_WINDOW_MAX,
                                                                        n_queues=self.N_PRIORITIES))
        self.events_serializer.start()

        # TCP Session Params
        self.session_null_timeout = self.tp.DEFAULT_NULL_TIMEOUT  # should be used to send keepalives, NOT USED
        self.session_max_retransm = self.tp.DEFAULT_MAX_RETRANSM
        self.session_ack_strategy = TcpSynHeader.SINGLE_ACK_STRATEGY

        self.rto_estimator = RtoEstimator(self.tp.DEFAULT_RETRANSM_TIMEOUT)
        self.clock_granularity = RtoEstimator.MIN_RTO
        self.clock_events_generator = PeriodicalCaller(self.request_clock_event, self.clock_granularity)

        if self.routing_method == rh.ROUTING_METHOD_MULTI_TREE:
            self.max_payload_size = self.tp.MAX_PAYLOAD_SIZE_MTREE
        else:
            self.max_payload_size = self.tp.MAX_PAYLOAD_SIZE_GOSSIP

        self.max_file_size = self.max_payload_size * TcpHeader.TCP_MAX_SEQ_N

        # SYN packet
        self.syn_packet = None
        self.session_established = False

        # Create a thread that start transmission of packets when tokens are received
        self.transmission_regulator = TransmissionRegulator(self)

        # Messages
        """
        Only one message at a time can be sent (the first of the list)
        Once the message has been sent, it is removed from the list
        """
        self.message_id_generator = IdGenerator(TcpHeader.MEX_ID_LEN, 0, 0)
        self.messages = {}  # mex_id : Message
        self.messages_list = []  # list of mex_id to be sent, used to serve them in arrival order
        self.is_stopping = False  # used to not send many timeout events

    # ----------------------------------------- USER COMMANDS  ------------------------------------------------------- #

    def stop(self, _packet=None):
        self.is_stopping = True
        self.events_serializer.enqueue(self.PRIORITY_STOP_CLIENT, self.do_stop, None)

    def do_stop(self):

        for message in self.messages.values():
            message.stop()

        self.events_serializer.stop()  # cannot join because it is called by this thread!

        self.transmission_regulator.stop()
        # self.transmission_regulator.join()

        self.clock_events_generator.stop()
        # self.clock_events_generator.join()

        # try to send a FIN packet to the server, so it can delete session data and save some space
        fin_packet = Packet(entry_point=Packet.TCP_FIN)
        fin_packet.set_created_by_l4_ts()
        fin_packet.add_tcp_header(self.server_port, TcpHeader.TCP_FIN, self.session_id, 0, 0, 0)
        fin_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.heaven_address)
        self.send_packet(fin_packet)
        self.request.end()

    def start_session(self):
        """
        Called just after the Client object has been created
        :return:
        """
        # Create the SYN packet
        self.syn_packet = Packet(entry_point=Packet.TCP_SYN)
        self.syn_packet.set_created_by_l4_ts()
        self.syn_packet.add_tcp_syn_header(self.rto_estimator.rto, self.session_null_timeout, self.session_max_retransm, self.session_ack_strategy)
        self.syn_packet.add_tcp_header(self.server_port, TcpHeader.TCP_SYN, self.session_id, 0, 0, '')
        self.syn_packet.add_transport_header(TransportHeader.TCP_TRANSPORT_TYPE, self.heaven_address)

        # Start transmission threads
        self.clock_events_generator.start()
        self.transmission_regulator.start()

        # Put the SYN packet in TX
        self.add_packet(self.syn_packet)
        self.add_tokens(1)

        return self.session_id

    def send_message(self, request, file_str):

        if self.is_stopping:
            return False, 'Tcp client is stopping'

        if len(file_str) > self.max_file_size:
            logging.getLogger(__name__).debug('File string bigger than maximum allowed, dropping send request')
            return False, err.FILE_TOO_BIG

        mex_id = self.message_id_generator.get_id()
        # add a message but do not start its transmission yet
        self.request_add_message(mex_id, Message(self, request, mex_id, file_str))
        return mex_id, ''

    # ----------------------------------------- MESSAGES ------------------------------------------------------------- #

    def request_add_message(self, mex_id, message):
        if self.is_stopping:
            message.stop()
            return
        self.events_serializer.enqueue(self.PRIORITY_OTHER, self.do_add_message, (mex_id, message))

    def do_add_message(self, x):

        mex_id, message = x

        if self.is_stopping:
            message.stop()
            return

        if mex_id in self.messages_list:
            return

        self.messages_list.append(mex_id)
        self.messages[mex_id] = message

        # if this is the only message in list, start it
        if len(self.messages_list) == 1:
            self.start_message()

    def request_remove_message(self, mex_id):
        self.events_serializer.enqueue(self.PRIORITY_OTHER, self.do_remove_message, mex_id)

    def do_remove_message(self, mex_id):
        self.messages_list.remove(mex_id)
        del self.messages[mex_id]
        self.message_id_generator.del_id(mex_id)

        # if there is a message to be transmitted, reset params and start it
        if self.messages_list:
            self.start_message()

    def start_message(self):
        # reset TCP params
        self.congestion_window = self.tp.TCP_CONGESTION_WINDOW_START
        self.sstresh = self.tp.SSTRESH_START
        self.unacked_packets = 0
        self.filling_counter = 0
        self.transmission_regulator.reset_queues()

        # start the first message in list
        self.messages[self.messages_list[0]].start()

    # ----------------------------------------- PACKET RECEPTION ----------------------------------------------------- #

    def receive_packet(self, packet):
        packet.set_rx_by_l4_up_ts()
        logging.getLogger(__name__).debug('Enqueuing packet for TCP client')
        self.events_serializer.enqueue(self.PRIORITY_RX_ACK, self.do_receive_packet, packet)

    def do_receive_packet(self, packet):
        logging.getLogger(__name__).debug('Managing a received packet of type: {}'.format(packet.get_tcp_type()))
        pt = packet.get_tcp_type()
        if pt == TcpHeader.TCP_ACK:
            try:
                self.messages[packet.get_tcp_mex_id()].receive_data_ack(packet)
            except KeyError:
                packet.set_proc_by_l4_up_ts()
                self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
                logging.getLogger(__name__).warning(
                    'TCP client received data ack for non existing message (already completed?) : {}'.format(packet))
        elif pt == TcpHeader.TCP_INIT_DATA_ACK:
            try:
                self.messages[packet.get_tcp_mex_id()].receive_init_data_ack(packet)
            except KeyError:
                packet.set_proc_by_l4_up_ts()
                self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
                logging.getLogger(__name__).warning(
                    'TCP client received DATA_INIT_ACK for non existing message: {}'.format(packet))
        elif pt == TcpHeader.TCP_SYN_ACK:
            logging.getLogger(__name__).debug('TCP client received SYN_ACK: {}'.format(packet))
            # check that the ACK matches the SYN and that it is not duplicated
            if packet.get_transport_source() == self.server_address \
                    and packet.get_tcp_session_id() == self.session_id and not self.session_established:
                # Used to discard other SYN ack that may arrive
                self.session_established = True
                # Notify the user that the client has been created
                self.request.reply(Replies.reply_tcp_session_established(
                    self.server_address, self.server_port, self.session_id, 1))
                # Remove the SYN from unacked packets
                self.unacked_packets = 0

            packet.set_proc_by_l4_up_ts()
            self.profiler.profile_packet(packet, Packet.RECEIVED_TCP_SYN_ACK)
        elif pt == TcpHeader.TCP_FIN_ACK:
            logging.getLogger(__name__).debug('TCP client received FIN_ACK: {}'.format(packet))
            self.request.reply(Replies.reply_tcp_session_abort(
                self.server_address, self.server_port, self.session_id, 1,
                error_description='Connection closed by the server'))
            # self.stop() alone does not delete the session_id from tcp_layer
            self.tcp_layer.stop_client(self.session_id)
        else:
            logging.getLogger(__name__).error('Cannot manage packet type: {}'.format(packet.get_tcp_type()))

    # ----------------------------------------- TIMEOUTS ------------------------------------------------------------- #

    def request_clock_event(self):
        self.events_serializer.enqueue(self.PRIORITY_CLOCK_EVENT, self.do_clock_event, None)

    def do_clock_event(self):
        """
        :return:
        """
        if not self.session_established:
            self.check_packet_timeout(self.syn_packet, self.clock_granularity)
        else:
            try:
                self.messages[self.messages_list[0]].do_clock_event()
            except (KeyError, IndexError):
                pass

    def check_packet_timeout(self, packet, delta_t):

        # the packet is not ready to be transmitted again, increase its timeout and return
        if packet.timeout < self.rto_estimator.rto * (packet.get_tcp_header().retransm_n + 1):
            packet.timeout += delta_t
            return

        # the packet is in timeout and must be retransmitted if possible
        if packet.get_tcp_header().retransm_n < self.session_max_retransm:
            packet.set_created_by_l4_ts()
            packet.timeout = 0
            packet.get_tcp_header().retransm_n += 1
            packet.remove_routing_header()
            self.retx_packet(packet)
            self.update_window_for_time_out()
            logging.getLogger(__name__).debug(
                'Retransmitting packet having retransm_n={}: {}'.format(packet.get_tcp_header().retransm_n, packet))
        elif not self.is_stopping:
            logging.getLogger(__name__).debug('Reached max-retransm for seq_n {}, aborting session'.format(packet.get_tcp_header().seq_n))
            self.request.reply(Replies.reply_tcp_session_abort(
                self.server_address, self.server_port, self.session_id, 1,
                error_description='Max-retransm reached'))
            # self.stop() alone does not delete the session_id from tcp_layer
            self.tcp_layer.stop_client(self.session_id)

    # ----------------------------------------- OTHERS --------------------------------------------------------------- #

    def add_tokens(self, x):
        self.transmission_regulator.add_tokens(x)

    def add_packet(self, packet):
        self.transmission_regulator.add_packet(packet)

    def add_packets(self, packets):
        self.transmission_regulator.add_packets(packets)

    def send_packet(self, packet):
        """
        Called:
        - by the transmission regulator once the packet can be sent for the first time
        - after a timeout event
        :param packet:
        :return:
        """
        self.events_serializer.enqueue(self.PRIORITY_SEND_NEW, self.do_send_packet, packet)

    def retx_packet(self, packet):
        self.events_serializer.enqueue(self.PRIORITY_SEND_RETX, self.do_send_packet, packet)

    def do_send_packet(self, packet):
        """
        Called:
        - by the transmission regulator once the packet can be sent for the first time
        - after a timeout event
        :param packet:
        :return:
        """
        # set the packet start timestamp and routing params only at the first transmission
        if packet.start_ts is None:
            packet.start_ts = time.time()
            packet.requested_destination = self.server_address
            packet.requested_routing_method = self.routing_method

        packet.set_proc_by_l4_down_ts()
        self.tcp_layer.send(packet)

    def increase_congestion_window(self):

        """
        Called after a data init ack or a data ack has been received
        Note: tokens are not added here so cumulative acks can be implemented.
        :return:
        """
        self.consecutive_time_outs = 0
        if self.congestion_window < self.sstresh:
            """
            Slow start phase: increase the window of 1 for each received ack.
            Note: the token for the received ack is already added externally.
            """
            logging.getLogger(__name__).debug('Increasing W = {} in Slow Start'.format(self.congestion_window))
            self.congestion_window = min(self.tp.TCP_CONGESTION_WINDOW_MAX, self.congestion_window + 1)
        else:
            """
            Congestion Avoidance phase: increase the window of 1 unit only every consecutive W acks received.
            """
            logging.getLogger(__name__).debug('Increasing W = {} in Congestion Avoidance'.format(self.congestion_window))
            self.filling_counter += 1
            if self.filling_counter == self.congestion_window:
                self.congestion_window = min(self.tp.TCP_CONGESTION_WINDOW_MAX, self.congestion_window + 1)
                self.filling_counter = 0

        logging.getLogger(__name__).debug('Window increased to W = {}, SSTHRESH = {}, Unacked = {}, tokens = {}'.format(
            self.congestion_window, self.sstresh, self.unacked_packets, self.n_tokens))

    def update_window_for_time_out(self):
        """
        Called every time a packet timeout expires.
        Decrease the congestion_window (up to a minimum) and update sstresh
        :return:
        """
        self.filling_counter = 0
        logging.getLogger(__name__).debug('Consecutive timeouts {}'.format(self.consecutive_time_outs))
        if self.consecutive_time_outs == 0:
            self.sstresh = max(self.tp.TCP_CONGESTION_WINDOW_MIN, int(self.congestion_window / 2.0))
            self.congestion_window = max(self.tp.TCP_CONGESTION_WINDOW_MIN,
                                         int(self.congestion_window * self.tp.TCP_CONGESTION_WINDOW_DECREASE_PARAM))

            logging.getLogger(__name__).debug('Packet time out, decreased window to W = {}, SSTHRESH = {}, Unacked = {}, tokens = {}'.format(
                 self.congestion_window, self.sstresh, self.unacked_packets, self.n_tokens))

        self.consecutive_time_outs += 1
        if self.consecutive_time_outs > self.congestion_window:
            self.consecutive_time_outs = 0

