from tcp_client import TcpClient
from tcp_server import TcpServer
from transport_params import TransportParams
from heaven_modules.layer3.routing_params import RoutingParams
from heaven_modules.common.utilities import IdGenerator
from heaven_modules.common.routing_header import RoutingHeader
import os
import threading
import logging
from heaven_modules.common.packet import Packet
from heaven_modules.common.transport_header import TcpHeader
from tcp_server_socket import TcpServerSocket
from tcp_client_socket import TcpClientSocket
from heaven_modules.api.heaven_api_replies import ErrorDescriptions as err


class TcpLayer:

    def __init__(self, heaven_address, send, get_ip_address, config_reader, profiler):
        self.send = send
        self.get_ip_address = get_ip_address
        self.heaven_address = heaven_address
        self.profiler = profiler

        self.tp = TransportParams(config_reader)
        self.rp = RoutingParams(config_reader)

        # lock are used only to create/delete clients and servers
        self.clients = {}  # session_id --> TcpClient object
        self.servers = {}  # listening port (int) --> TcpServer object
        self.clients_lock = threading.Lock()
        self.servers_lock = threading.Lock()

        # tcp sockets
        self.clients_socket = {}  # session_id --> TcpClient object
        self.servers_socket = {}  # listening port (int) --> TcpServer object
        self.servers_socket_lock = threading.Lock()
        self.clients_socket_lock = threading.Lock()

        # generate sequential session_id
        self.sessions_generator = IdGenerator(TcpHeader.SESSION_ID_LEN, 0, 0)
        self.do_run = True  # tell if a stop layer command has been issued

    def stop(self):

        self.do_run = False

        logging.getLogger(__name__).debug('Stopping TCP servers...')
        with self.servers_lock:
            for server_port in self.servers:
                logging.getLogger(__name__).debug('Stopping TCP server {}...'.format(server_port))
                self.stop_server(server_port)
            self.servers = {}

        logging.getLogger(__name__).debug('Stopping TCP clients...')
        with self.clients_lock:
            for session_id in self.clients:
                logging.getLogger(__name__).debug('Stopping TCP client {}...'.format(session_id))
                self.stop_client(session_id)
            self.clients = {}

        logging.getLogger(__name__).debug('Stopping TCP socket servers...')
        with self.servers_socket_lock:
            for server in self.servers_socket.itervalues():
                server.stop()
        self.servers_socket = {}

        # socket clients stop by themselves

    """
    Functions called by the transport manager because of API calls
    """

    # ------------------------- TCP SERVER ------------------------- #

    def create_server(self, server_port, request):

        if not self.tp.is_valid_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        with self.servers_socket_lock:

            if not self.do_run:
                return False, err.HEAVEN_IS_STOPPING

            if server_port in self.servers:
                return False, err.SERVER_PORT_NOT_AVAILABLE

            self.servers[server_port] = TcpServer(self, server_port, request, self.tp, self.profiler)

        self.servers[server_port].start()

        return True, ''

    def stop_server(self, server_port):

        # we are already stopping the tcp layer (the lock is already taken) # called here
        if not self.do_run:
            self.servers[server_port].stop()
            return

        # it is an async call (we have to gain the lock)
        if not self.tp.is_valid_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        with self.servers_lock:

            if server_port not in self.servers:
                return False, err.SERVER_PORT_NOT_FOUND

            self.servers[server_port].stop()
            del self.servers[server_port]

        return True, ''

    def get_tcp_servers(self):
        return self.servers.keys()

    # ---------------------- TCP SERVER SOCKET --------------------- #

    def create_server_socket(self, server_port, request):

        if not self.tp.is_valid_socket_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        with self.servers_lock:

            if not self.do_run:
                return False, err.HEAVEN_IS_STOPPING

            if server_port in self.servers_socket:
                return False, err.SERVER_PORT_NOT_AVAILABLE

            self.servers_socket[server_port] = TcpServerSocket(self, server_port, request, self.tp)

        self.servers_socket[server_port].start()
        return True, ''

    def stop_server_socket(self, server_port):

        # we are already stopping the tcp layer (the lock is already taken)
        if not self.do_run:
            self.servers_socket[server_port].stop()
            return

        # it is an async call (we have to gain the lock)
        if not self.tp.is_valid_socket_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        with self.servers_socket_lock:

            if server_port not in self.servers_socket:
                return False, err.SERVER_PORT_NOT_FOUND

            self.servers_socket[server_port].stop()
            del self.servers_socket[server_port]

        return True, ''

    def get_tcp_servers_socket(self):
        return self.servers_socket.keys()

    # ------------------------- TCP CLIENT ------------------------- #

    def create_client(self, server_address, server_port, routing_method, request):
        # generate random port and session id

        if not self.tp.is_valid_heaven_address(server_address):
            return False, err.HEAVEN_ADDRESS_NOT_VALID

        if not self.tp.is_valid_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        if routing_method not in RoutingHeader.ROUTING_METHODS:
            return False, err.ROUTING_METHOD_NOT_VALID

        if not self.rp.DO_USE_MULTI_TREE and routing_method == RoutingHeader.ROUTING_METHOD_MULTI_TREE:
            return False, err.MULTI_TREE_DISABLED

        with self.clients_lock:

            if not self.do_run:
                return False, err.HEAVEN_IS_STOPPING

            new_session_id = self.sessions_generator.get_id()
            self.clients[new_session_id] = TcpClient(self, self.heaven_address,
                                                     server_address, server_port, new_session_id,
                                                     routing_method, request, self.tp, self.profiler)

        return self.clients[new_session_id].start_session(), ''  # session_id

    def transfer_data(self, request, session_id, message):

        if session_id not in self.clients:
            logging.getLogger(__name__).debug('Session_id not valid')
            return False, err.SESSION_ID_NOT_FOUND

        return self.clients[session_id].send_message(request, message)  # mex_id, '' or False, error

    def transfer_file(self, request, session_id, delete_after_send, file_path):

        try:
            with open(file_path, 'rb') as messageFile:
                message = messageFile.read()

            if delete_after_send:
                os.remove(file_path)

        except IOError:
            logging.getLogger(__name__).debug('File {} not Found, cannot send data'.format(file_path))
            return False, err.FILE_NOT_FOUND

        logging.getLogger(__name__).debug('Starting to send a message from file...')
        return self.transfer_data(request, session_id, message)

    def stop_client(self, session_id):

        # we are stopping the tcp layer (the lock is already taken)
        if not self.do_run:
            self.clients[session_id].stop()
            self.sessions_generator.del_id(session_id)
            return

        # it is an async call (we have to gain the lock)
        with self.clients_lock:

            if session_id not in self.clients:
                return False, err.HEAVEN_IS_STOPPING

            self.clients[session_id].stop()
            del self.clients[session_id]
            self.sessions_generator.del_id(session_id)

        return True, ''

    def get_tcp_clients(self):
        return self.clients.keys()

    # ---------------------- TCP CLIENT SOCKET---------------------- #

    def transfer_data_socket(self, request, server_address, server_port, message):

        if not self.tp.is_valid_heaven_address(server_address):
            return False, err.HEAVEN_ADDRESS_NOT_VALID

        if not self.tp.is_valid_socket_port(server_port):
            return False, err.PORT_OUT_OF_RANGE

        if not self.rp.DO_USE_MULTI_TREE:
            return False, err.MULTI_TREE_DISABLED

        server_ip_address = self.get_ip_address(server_address)
        if server_ip_address is None:
            return False, err.IP_LOOKUP_FAILED

        with self.clients_socket_lock:

            if not self.do_run:
                return False, err.HEAVEN_IS_STOPPING

            client_id = self.sessions_generator.get_id()

            self.clients_socket[client_id] = TcpClientSocket(
                self.heaven_address, server_address, server_ip_address,
                server_port, client_id, request, self.socket_client_stopped, message)

        return self.clients_socket[client_id].send_message(), ''  # session_id

    def transfer_file_socket(self, request, server_address, server_port, delete_after_send, file_path):

        try:
            with open(file_path, 'rb') as messageFile:
                message = messageFile.read()

            if delete_after_send:
                os.remove(file_path)

        except IOError:
            print('File {} not Found, cannot send data'.format(file_path))
            logging.getLogger(__name__).debug('File {} not Found, cannot send data'.format(file_path))
            return False, err.FILE_NOT_FOUND

        logging.getLogger(__name__).debug('Starting to send a message from file...')
        return self.transfer_data_socket(request, server_address, server_port, message)

    """
    Functions called by heaven internally
    """
    def socket_client_stopped(self, session_id):
        """
        Called by TcpClientSocket after the message has been sent/failed
        :param session_id:
        :return:
        """
        with self.clients_socket_lock:
            del self.clients_socket[session_id]

    def receive_tcp_packet(self, packet):
        """
        Calld by the lower layer when a packet is received from the network
        :param packet:
        :return:
        """

        # Packet for a TCP Client: it can receive only acks!
        if packet.is_tcp_ack_type():
            try:
                self.clients[packet.get_tcp_session_id()].receive_packet(packet)
                logging.getLogger(__name__).debug('Received packet for TCP client')
            except KeyError:
                packet.set_rx_by_l4_up_ts()
                packet.set_proc_by_l4_up_ts()
                self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
                logging.getLogger(__name__).warning(
                    'TCP layer received ack for non existing session, {}'.format(packet))

        # Packet for a TCP Server: it does not receive acks!
        else:
            try:
                self.servers[packet.get_tcp_server_port()].receive_packet(packet)
                logging.getLogger(__name__).debug('Received packet for TCP server')
            except KeyError:
                packet.set_rx_by_l4_up_ts()
                packet.set_proc_by_l4_up_ts()
                self.profiler.profile_packet(packet, Packet.WRONG_TCP_SESSION_DATA)
                logging.getLogger(__name__).warning(
                    'TCP layer received non-ack packet for non existing session, {}'.format(packet))
