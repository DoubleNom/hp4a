from heaven_modules.common.standard_threads import QueueServer
import Queue
from heaven_modules.common.utilities import get_udp_socket
import socket
import six


class UdpSender(QueueServer):

    def __init__(self, server_ip_address, server_port, tx_timeout, header):
        QueueServer.__init__(self, Queue.Queue())
        self.server_ip_address = server_ip_address
        self.server_port = server_port
        self.tx_timeout = tx_timeout
        self.header = header
        self.sock = None

    def handle(self, x):

        try:
            if self.sock is None:
                self.sock = get_udp_socket((self.server_ip_address, self.server_port), self.tx_timeout)

            msg = self.header + x

            if not six.PY2:
                msg = bytes(msg, 'utf8')

            self.sock.sendto(msg, (self.server_ip_address, self.server_port))
        except (socket.error, socket.timeout):
            if self.sock is not None:
                self.sock.close()
                self.sock = None
            return

    def cleanup(self):
        if self.sock is not None:
            self.sock.close()


class UdpClientSocket:

    def __init__(self, heaven_address, server_ip_address, server_port, client_id, tx_timeout, request):
        self.client_id = client_id
        self.request = request
        self.sending_thread = UdpSender(server_ip_address, server_port, tx_timeout, heaven_address + client_id)
        self.sending_thread.start()

    def send(self, message):
        self.sending_thread.put(message)
        return True

    def stop(self):
        self.sending_thread.stop()
        self.request.end()


if __name__ == '__main__':

    import time

    class VoidRequest(object):

        def reply(self, *args):
            print('reply: {}'.format(args))

        def end(self):
            print('request end')

    def test_socket(start, end, step, dest='gitl', ip='192.168.0.172', port=30200):

        cli = UdpClientSocket(dest, ip, port, 'test', 1, VoidRequest())

        try:
            for i in xrange(start, end, step):
                text = 'A' * i
                start_ts = time.time()
                cli.send(text)
                delta_t = time.time() - start_ts
                print('{} Bytes sent rate: {:,} bps'.format(i, int(i*8/delta_t)))
                time.sleep(0.05)
        except KeyboardInterrupt:
            pass
        finally:
            cli.stop()
