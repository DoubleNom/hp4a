class Commands(object):
    """
    Command values for the API.
    """
    # -------------------------- HEAVEN ---------------------------- #
    GET_HEAVEN_ADDRESS = 'get_heaven_address'
    STOP_HEAVEN = 'stop_heaven'
    # -------------------------- NETWORK --------------------------- #
    GET_PEERS = 'get_peers'
    GET_PEERS_UPDATES = 'get_peers_updates'
    STOP_PEERS_UPDATES = 'stop_peers_updates'
    GET_NEIGHBORS = 'get_neighbors'
    GET_LSP = 'get_lsp'
    GET_ROUTING_TABLE = 'get_routing_table'
    GET_ROUTING_TREES = 'get_routing_trees'
    GET_FULL_NETWORK = 'get_full_network'
    BLOCK_NEIGHBOR = 'block_neighbor'
    UNBLOCK_NEIGHBOR = 'unblock_neighbor'
    GET_PEER_IP = 'get_peer_ip'
    GET_MY_IP_FOR_PEER = 'get_my_ip_for_peer'
    # ------------------------- TCP SERVER ------------------------- #
    CREATE_TCP_SERVER = 'create_tcp_server'
    STOP_TCP_SERVER = 'stop_tcp_server'
    GET_TCP_SERVERS = 'get_tcp_servers'
    # ---------------------- TCP SERVER SOCKET --------------------- #
    CREATE_TCP_SERVER_SOCKET = 'create_tcp_server_socket'
    STOP_TCP_SERVER_SOCKET = 'stop_tcp_server_socket'
    GET_TCP_SERVERS_SOCKET = 'get_tcp_servers_socket'
    # ------------------------- TCP CLIENT ------------------------- #
    CREATE_TCP_CLIENT = 'create_tcp_client'
    SEND_TCP = 'send_tcp'
    SEND_TCP_FILE = 'send_tcp_file'
    STOP_TCP_CLIENT = 'stop_tcp_client'
    GET_TCP_CLIENTS = 'get_tcp_clients'
    # ---------------------- TCP CLIENT SOCKET---------------------- #
    SEND_TCP_SOCKET = 'send_tcp_socket'
    SEND_TCP_FILE_SOCKET = 'send_tcp_file_socket'
    # ------------------------- UDP SERVER ------------------------- #
    CREATE_UDP_SERVER = 'create_udp_server'
    STOP_UDP_SERVER = 'stop_udp_server'
    GET_UDP_SERVERS = 'get_udp_servers'
    # ---------------------- UDP SERVER SOCKET --------------------- #
    CREATE_UDP_SERVER_SOCKET = 'create_udp_server_socket'
    STOP_UDP_SERVER_SOCKET = 'stop_udp_server_socket'
    GET_UDP_SERVERS_SOCKET = 'get_udp_servers_socket'
    # ------------------------- UDP CLIENT ------------------------- #
    CREATE_UDP_CLIENT = 'create_udp_client'
    SEND_UDP = 'send_udp'
    STOP_UDP_CLIENT = 'stop_udp_client'
    GET_UDP_CLIENTS = 'get_udp_clients'
    # ---------------------- UDP CLIENT SOCKET---------------------- #
    CREATE_UDP_CLIENT_SOCKET = 'create_udp_client_socket'
    SEND_UDP_SOCKET = 'send_udp_socket'
    STOP_UDP_CLIENT_SOCKET = 'stop_udp_client_socket'
    GET_UDP_CLIENTS_SOCKET = 'get_udp_clients_socket'
