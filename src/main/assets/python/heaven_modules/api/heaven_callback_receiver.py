from heaven_api_commands import Commands
from heaven_api_replies import Replies
import logging
from heaven_modules.common.standard_threads import QueueServer
from heaven_modules.common.tail_drop_queue import TailDropQueue

"""
Collection of Threads that listen for specific callbacks from Heaven and distinguish between different replies.
The functions called are abstract and must be implemented by the user.
Yous should pass receive_packet as callback function for heaven api calls.
"""


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- PEERS -------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class PeersReceiver(QueueServer):
    """
    Replies from peers and peers subscribe
    """

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        if Replies.KEY_PEERS in reply:
            self.peers_received(reply[Replies.KEY_PEERS])

        elif Replies.KEY_PEERS_SUBSCRIPTION in reply:
            if reply[Replies.KEY_PEERS_SUBSCRIPTION]:
                self.subscribe_success()
            else:
                self.subscribe_fail()

    def peers_received(self, peers):
        pass

    def subscribe_success(self):
        pass

    def subscribe_fail(self):
        pass


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- TCP ---------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class TcpServerReceiver(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        logging.getLogger(__name__).debug('TcpServerReceiver received {}'.format(reply))

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        if reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_SERVER and Replies.KEY_CREATED in reply:

            if reply[Replies.KEY_CREATED]:
                self.server_created(reply)
            else:
                self.server_not_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_TCP_SERVER and Replies.KEY_STOPPED in reply:
            if reply[Replies.KEY_STOPPED]:
                self.server_stopped(reply)
            else:
                self.server_not_stopped(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_SERVER and Replies.KEY_MESSAGE in reply:
            logging.getLogger(__name__).debug('Calling message received')
            self.message_received(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_SERVER and Replies.KEY_FILE_PATH in reply:
            self.file_received(reply)

    def server_created(self, reply):
        pass

    def server_not_created(self, reply):
        pass

    def server_stopped(self, reply):
        pass

    def server_not_stopped(self, reply):
        pass

    def message_received(self, reply):
        pass

    def file_received(self, reply):
        pass


class TcpClientReceiver(QueueServer):
    """
    Replies from create,stop tcp client and send tcp
    """

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        logging.getLogger(__name__).debug('TCP client received {}'.format(reply))

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_CLIENT and Replies.KEY_REQUEST_ACCEPTED in reply:

            if reply[Replies.KEY_REQUEST_ACCEPTED]:
                self.client_creation_accepted(reply)
            else:
                self.client_creation_rejected(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_CLIENT and Replies.KEY_CREATED in reply:

            if reply[Replies.KEY_CREATED]:
                self.client_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_CLIENT and Replies.KEY_ERROR in reply:

            self.session_aborted(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_TCP_CLIENT and Replies.KEY_STOPPED in reply:

            if reply[Replies.KEY_STOPPED]:
                self.client_stopped(reply)
            else:
                self.client_not_stopped(reply)

        elif reply[Replies.KEY_COMMAND] in [Commands.SEND_TCP, Commands.SEND_TCP_FILE] and Replies.KEY_REQUEST_ACCEPTED in reply:
            self.message_accepted(reply)

        elif reply[Replies.KEY_COMMAND] in [Commands.SEND_TCP, Commands.SEND_TCP_FILE] and Replies.KEY_MESSAGE_SENT in reply:

            if reply[Replies.KEY_MESSAGE_SENT]:
                self.message_sent(reply)

    def client_creation_accepted(self, reply):
        pass

    def client_creation_rejected(self, reply):
        pass

    def client_created(self, reply):
        pass

    def session_aborted(self, reply):
        pass

    def client_stopped(self, reply):
        pass

    def client_not_stopped(self, reply):
        pass

    def message_accepted(self, reply):
        pass

    def message_sent(self, reply):
        pass


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- TCP SOCKET --------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class TcpServerSocketReceiver(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        logging.getLogger(__name__).debug('TcpServerSocketReceiver received {}'.format(reply))

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        if reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_SERVER_SOCKET and Replies.KEY_CREATED in reply:

            if reply[Replies.KEY_CREATED]:
                self.server_created(reply)
            else:
                self.server_not_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_TCP_SERVER_SOCKET and Replies.KEY_STOPPED in reply:
            if reply[Replies.KEY_STOPPED]:
                self.server_stopped(reply)
            else:
                self.server_not_stopped(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_SERVER_SOCKET and Replies.KEY_MESSAGE in reply:
            logging.getLogger(__name__).debug('Calling message received')
            self.message_received(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_TCP_SERVER_SOCKET and Replies.KEY_FILE_PATH in reply:
            self.file_received(reply)

    def server_created(self, reply):
        pass

    def server_not_created(self, reply):
        pass

    def server_stopped(self, reply):
        pass

    def server_not_stopped(self, reply):
        pass

    def message_received(self, reply):
        pass

    def file_received(self, reply):
        pass


class TcpClientSocketReceiver(QueueServer):
    """
    Replies from send tcp socket

    - Replies.reply_send_tcp_socket(session_id, 1)
    - Replies.reply_send_tcp_socket('', 0, error_description)

    - Replies.reply_tcp_socket_message_sent(self.session_id, 1, file_str_len, elapsed_time, rate)
    - Replies.reply_tcp_socket_error(self.server_address, self.server_port, self.session_id, 1)
    """

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        logging.getLogger(__name__).debug('TCP client socket received {}'.format(reply))

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        if reply[Replies.KEY_COMMAND] not in [Commands.SEND_TCP_SOCKET, Commands.SEND_TCP_FILE_SOCKET]:
            return

        if Replies.KEY_REQUEST_ACCEPTED in reply and int(reply[Replies.KEY_REQUEST_ACCEPTED]) == 1:
            self.message_accepted(reply)

        elif Replies.KEY_REQUEST_ACCEPTED in reply and int(reply[Replies.KEY_REQUEST_ACCEPTED]) == 0:
            self.message_rejected(reply)

        elif Replies.KEY_MESSAGE_SENT in reply:
            self.message_sent(reply)

        elif Replies.KEY_ERROR in reply:
            self.message_failed(reply)

    def message_accepted(self, reply):
        pass

    def message_rejected(self, reply):
        pass

    def message_sent(self, reply):
        pass

    def message_failed(self, reply):
        pass


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- UDP ---------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class UdpServerReceiver(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        if reply[Replies.KEY_COMMAND] == Commands.CREATE_UDP_SERVER and Replies.KEY_CREATED in reply:

            if reply[Replies.KEY_CREATED]:
                self.server_created(reply)
            else:
                self.server_not_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_UDP_SERVER and Replies.KEY_STOPPED in reply:
            if reply[Replies.KEY_STOPPED]:
                self.server_stopped(reply)
            else:
                self.server_not_stopped(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_UDP_SERVER and Replies.KEY_MESSAGE in reply:
            self.message_received(reply)

    def server_created(self, reply):
        pass

    def server_not_created(self, reply):
        pass

    def server_stopped(self, reply):
        pass

    def server_not_stopped(self, reply):
        pass

    def message_received(self, reply):
        pass


class UdpClientReceiver(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_UDP_CLIENT:

            if reply[Replies.KEY_CREATED]:
                self.client_created(reply)
            else:
                self.client_not_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.SEND_UDP:

            if reply[Replies.KEY_MESSAGE_SENT]:
                self.message_sent(reply)
            else:
                self.message_not_sent(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_UDP_CLIENT:

            if reply[Replies.KEY_STOPPED]:
                self.client_stopped(reply)
            else:
                self.client_not_stopped(reply)

    def client_created(self, reply):
        pass

    def client_not_created(self, reply):
        pass

    def message_sent(self, reply):
        pass

    def message_not_sent(self, reply):
        pass

    def client_stopped(self, reply):
        pass

    def client_not_stopped(self, reply):
        pass


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- UDP SOCKET --------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class UdpServerSocketReceiver(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):

        reply = x

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        if reply[Replies.KEY_COMMAND] == Commands.CREATE_UDP_SERVER_SOCKET and Replies.KEY_CREATED in reply:

            if reply[Replies.KEY_CREATED]:
                self.server_created(reply)
            else:
                self.server_not_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_UDP_SERVER_SOCKET and Replies.KEY_STOPPED in reply:
            if reply[Replies.KEY_STOPPED]:
                self.server_stopped(reply)
            else:
                self.server_not_stopped(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_UDP_SERVER_SOCKET and Replies.KEY_MESSAGE in reply:
            self.message_received(reply)

    def server_created(self, reply):
        pass

    def server_not_created(self, reply):
        pass

    def server_stopped(self, reply):
        pass

    def server_not_stopped(self, reply):
        pass

    def message_received(self, reply):
        pass


class UdpClientSocketReceiver(QueueServer):
    """
    command == Create client
    - Replies.reply_create_udp_client(destination_address, destination_port, source_port, 1)
    - Replies.reply_create_udp_client(destination_address, destination_port, '', 0, error_description)

    command == send udp
    - Replies.reply_send_udp(client_id, int(sent), error_description)

    command == stop client
    - Replies.reply_stop_udp_client(client_id, int(stopped), error_description)

    """

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):
        reply = x

        if Replies.KEY_DELETE_REQUEST in reply:
            return

        elif reply[Replies.KEY_COMMAND] == Commands.CREATE_UDP_CLIENT_SOCKET:

            if reply[Replies.KEY_CREATED]:
                self.client_created(reply)
            else:
                self.client_not_created(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.SEND_UDP_SOCKET:

            if reply[Replies.KEY_MESSAGE_SENT]:
                self.message_sent(reply)
            else:
                self.message_not_sent(reply)

        elif reply[Replies.KEY_COMMAND] == Commands.STOP_UDP_CLIENT_SOCKET:

            if reply[Replies.KEY_STOPPED]:
                self.client_stopped(reply)
            else:
                self.client_not_stopped(reply)

    def client_created(self, reply):
        pass

    def client_not_created(self, reply):
        pass

    def message_sent(self, reply):
        pass

    def message_not_sent(self, reply):
        pass

    def client_stopped(self, reply):
        pass

    def client_not_stopped(self, reply):
        pass
