from heaven_api_replies import Replies
from heaven_modules.common.utilities import IdGenerator


class Request:

    def __init__(self, request_id, command, reply_sender):
        self.request_id = request_id
        self.command = command
        self.reply_sender = reply_sender

    def reply(self, reply):
        self.reply_sender.receive_reply((self.request_id, self.command, reply))

    def end(self):
        self.reply_sender.receive_reply((self.request_id, self.command, Replies.reply_delete_request()))


# used only for debug
class ReplySender:

    REQUEST_ID_LENGTH = 5

    def __init__(self):
        self.requests = {}
        self.request_id_generator = IdGenerator(self.REQUEST_ID_LENGTH, 0, 0)

    def get_new_request_id(self):
        return self.request_id_generator.get_id()

    def add_request(self, request):
        self.requests[request.request_id] = request

    def add_new_request(self):
        new_req = Request(self.get_new_request_id(), 'test', self)
        self.add_request(new_req)
        return new_req

    def receive_reply(self, x):

        request_id, command, reply = x

        print('<< Callback function ({})'.format(x))

        if Replies.KEY_DELETE_REQUEST in reply:
            try:
                del self.requests[request_id]
                self.request_id_generator.del_id(request_id)
            except KeyError:
                pass


class VoidRequest:

    def __init__(self):
        pass

    def reply(self, reply):
        pass

    def end(self):
        pass




