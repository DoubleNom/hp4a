import socket
import logging
from heaven_modules.common.transport_header import TransportHeader as tpkt
from heaven_modules.common.routing_header import RoutingHeader as rpkt
from heaven_modules.api.heaven_api_commands import Commands as hc

try:
    import simplejson as json
except ImportError:
    import json
from heaven_modules.api.heaven_api_replies import Replies
from heaven_modules.common.standard_threads import HeavenSocketServer
from heaven_modules.common.utilities import send_tcp_msg, send_udp_msg
from heaven_modules.layer4.transport_params import TransportParams
import six


class HeavenClient(HeavenSocketServer):
    TCP = tpkt.TCP_TRANSPORT_TYPE
    UDP = tpkt.UDP_TRANSPORT_TYPE
    TRANSPORT_TYPES = [UDP, TCP]
    MTREE = rpkt.ROUTING_METHOD_MULTI_TREE
    GOSSIP = rpkt.ROUTING_METHOD_GOSSIP
    BROADCAST_ADDRESS = '&&&&'

    def __init__(self, heaven_server_address, my_server_address, config_reader, socket_family, socket_type):
        self.heaven_address = config_reader.get('main', 'heaven_address')
        trp = TransportParams(config_reader)
        api_config_dict = config_reader.get_config_dict('api_server')
        self.MAX_PAYLOAD_SIZE_GOSSIP = trp.MAX_PAYLOAD_SIZE_GOSSIP
        self.MAX_PAYLOAD_SIZE_MTREE = trp.MAX_PAYLOAD_SIZE_MTREE
        self.SOCKET_BUFFER_SIZE = int(api_config_dict['socket_buffer_size'])
        self.RX_QUEUE_LEN = int(api_config_dict['rx_queue_len'])
        self.TX_TIMEOUT = float(api_config_dict['tx_timeout'])
        self.MAX_MSG_LEN = int(api_config_dict['max_msg_len'])

        self.requests = {}  # request_id --> Request
        self.heaven_server_address = heaven_server_address  # e.g. (127.0.0.1, 9999)
        HeavenSocketServer.__init__(self, my_server_address, self.SOCKET_BUFFER_SIZE, self.RX_QUEUE_LEN,
                                    socket_family=socket_family, socket_type=socket_type,
                                    name='Heaven Api Client')
        self.start()

    def handle(self, x):
        """
        Handle replies from Heaven
        :param x: (tuple) data, client_address
        :return: 
        """
        data, client_address = x

        logging.getLogger(__name__).debug('Received reply from HeavenServer: {}'.format(data))
        try:
            reply = json.loads(data)
            request_id = reply[Replies.KEY_REQUEST_ID]
            request = self.requests[request_id]
            request(reply)
        except (AttributeError, TypeError, KeyError, ValueError):
            return

        if Replies.KEY_DELETE_REQUEST in reply:
            try:
                del self.requests[request_id]
            except KeyError:
                logging.getLogger(__name__).exception('Request in api_client APIReplyServer was already deleted!')
                return

    def _send_to_heaven(self, request_str):
        """
        Open a socket to the server and send the passed string
        :param request_str: string to be sent
        :return: True on message sent, False on error
        """
        if not request_str:
            return False

        if self.socket_type == socket.SOCK_STREAM:
            return send_tcp_msg(self.heaven_server_address, request_str, self.socket_family)
        elif self.socket_type == socket.SOCK_DGRAM:
            return send_udp_msg(self.heaven_server_address, request_str, self.socket_family)

    def _create_request(self, request_id, callback_function, command_str, *args):
        """
        Create the string to be sent to the heaven server
        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is receivedthe function to be called when data is received
        :param command_str: heaven command_str
        :param args: parameters
        :return: requestID;port;command_str;parameter1;par2....
        """
        if request_id in self.requests:
            return False

        self.requests[request_id] = callback_function

        if self.socket_family == socket.AF_INET:
            addr = self.address[1]
        else:
            addr = self.address

        request_str = '{};{};{}'.format(request_id, addr, command_str)

        # if necessary, add other command arguments
        if args:
            return request_str + ';' + ';'.join(map(str, args))
        else:
            return request_str

    # -------------------------- HEAVEN ---------------------------- #

    def get_heaven_address(self, request_id, callback_function):
        """
        Get the heaven address in use

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_heaven_address`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_HEAVEN_ADDRESS))

    def stop_heaven(self, request_id, callback_function):
        """
        Request to stop heaven

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_HEAVEN))

    # -------------------------- NETWORK --------------------------- #

    def get_peers(self, request_id, callback_function):
        """
        Get the list of heaven addresses of reachable nodes in the network

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_peers`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_PEERS))

    def get_peers_updates(self, request_id, callback_function):
        """
        Start to receive the peer list each time that it changes

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_peers_updates`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_peers`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_PEERS_UPDATES))

    def stop_peers_updates(self, request_id, callback_function, subscription_request_id):
        """
        Stop to receive peers updates

        :param request_id: (str) unique id of the request
        :param callback_function:
        :param subscription_request_id: (str) The request id used to subscribe to peers updates
        :return:
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_peers_updates`
        """
        return self._send_to_heaven(self._create_request(
            request_id, callback_function, hc.STOP_PEERS_UPDATES, subscription_request_id))

    def get_neighbors(self, request_id, callback_function):
        """
        Get details about neighbors (nodes directly connected to this device)

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_neighbors`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_NEIGHBORS))

    def get_lsp(self, request_id, callback_function):
        """
        Get details about Link State Packets (routing information) used to compute routes

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_lsp`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_LSP))

    def get_routing_table(self, request_id, callback_function):
        """
        Get the routing table used by heaven

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_routing_table`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_ROUTING_TABLE))

    def get_routing_trees(self, request_id, callback_function):
        """
        Get the routing paths used by heaven

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_routing_trees`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_ROUTING_TREES))

    def get_full_network(self, request_id, callback_function):
        """
        Get the nodes and links of the network

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_full_network`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_FULL_NETWORK))

    def block_neighbor(self, request_id, callback_function, heaven_address):
        """
        Prevent a certain heaven address to become a neighbor

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param heaven_address: (str) heaven address of the node to block
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_block_neighbor`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.BLOCK_NEIGHBOR, heaven_address))

    def unblock_neighbor(self, request_id, callback_function, heaven_address):
        """
        Allow a certain heaven address (previously blocked) to become a neighbor

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param heaven_address: (str) heaven address of the node to unblock
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_unblock_neighbor`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.UNBLOCK_NEIGHBOR, heaven_address))

    def get_peer_ip(self, request_id, callback_function, heaven_address):
        """
        Get the IP address that I can use to reach a peer

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param heaven_address: (str) heaven address of the peer
        :return: (str) ip address that I can use to communicate with the peer
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_peer_ip`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_PEER_IP, heaven_address))

    def get_my_ip_for_peer(self, request_id, callback_function, heaven_address):
        """
        Get one of my IP addresses (the address that the peer can use to reach me)

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param heaven_address: (str) heaven address of the peer
        :return: (str) ip address that the peer can use to communicate with me
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_my_ip_for_peer`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_MY_IP_FOR_PEER, heaven_address))

    # ------------------------- TCP SERVER ------------------------- #

    def create_tcp_server(self, request_id, callback_function, port):
        """
        Create a Tcp server to receive messages from Tcp clients.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which to listen for connections (1000 < port < 9999)
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_tcp_server`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_server_received_message`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_server_received_file`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.CREATE_TCP_SERVER, port))

    def stop_tcp_server(self, request_id, callback_function, port):
        """
        Stop a Tcp Server.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which the server is listening
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_tcp_server`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_TCP_SERVER, port))

    def get_tcp_servers(self, request_id, callback_function):
        """
        Get a list of ports of Heaven Tcp Servers currently in use

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_tcp_servers`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_TCP_SERVERS))

    # ---------------------- TCP SERVER SOCKET --------------------- #

    def create_tcp_server_socket(self, request_id, callback_function, port):
        """
        Create a Tcp Server that receive packets directly from a socket (packets do not traverse the Heaven stack).

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which to listen for connections (30100 < port < 30999)
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_tcp_server`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_server_received_message`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_server_received_file`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.CREATE_TCP_SERVER_SOCKET, port))

    def stop_tcp_server_socket(self, request_id, callback_function, port):
        """
        Stop a Tcp Server Socket.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which the server is listening (30100 < port < 30999)
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_tcp_server`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_TCP_SERVER_SOCKET, port))

    def get_tcp_servers_socket(self, request_id, callback_function):
        """
        Get the list of ports of Tcp Server Socket in use

        :param request_id: (str) unique id of the request
        :param callback_function:
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_tcp_servers`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_TCP_SERVERS_SOCKET))

    # ------------------------- TCP CLIENT ------------------------- #

    def create_tcp_client(self, request_id, callback_function, server_heaven_address, server_port, routing_method):
        """
        Create a client to send data to a Tcp Server.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param server_heaven_address: (str) heaven address of the tcp server
        :param server_port: (int) port of the tcp server
        :param routing_method: (int) 0 for gossip, 1 for mtree
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_tcp_client`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_session_established`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_session_abort`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.CREATE_TCP_CLIENT,
                                                       server_heaven_address, server_port, routing_method))

    def send_tcp(self, request_id, callback_function, session_id, message):
        """
        Send data to a Tcp server using a Tcp client

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param session_id: (str) identifier of the tcp client
        :param message: (str) message to be sent
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_send_tcp`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_message_sent`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.SEND_TCP, session_id, message))

    def send_tcp_file(self, request_id, callback_function, session_id, delete_after_send, file_path):
        """
        Send a file to a Tcp server using a Tcp client

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param session_id: (str) identifier of the tcp client
        :param delete_after_send: (int) 1 if the file must be deleted after being sent, 0 otherwise
        :param file_path: (str) absolute path of the file to send
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_send_tcp_file`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_message_sent`
        """
        return self._send_to_heaven(
            self._create_request(request_id, callback_function, hc.SEND_TCP_FILE, session_id, delete_after_send,
                                file_path))

    def stop_tcp_client(self, request_id, callback_function, session_id):
        """
        Stop a tcp client.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param session_id: (str) identifier of the tcp client
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_tcp_client`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_TCP_CLIENT, session_id))

    def get_tcp_clients(self, request_id, callback_function):
        """
        Get the list of session id of the tcp clients in use

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_tcp_clients`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_TCP_CLIENTS))

    # ---------------------- TCP CLIENT SOCKET---------------------- #

    def send_tcp_socket(self, request_id, callback_function, server_heaven_address, server_port, message):
        """
        Send a message to a Tcp Server Socket.
        Packets will be sent directly on sockets without traversing the Heaven stack.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param server_heaven_address: (str) heaven address of the tcp server socket
        :param server_port: (int) port of the tcp server socket
        :param message: (str) the message to send
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_send_tcp_socket`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_socket_message_sent`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_socket_error`
        """
        return self._send_to_heaven(self._create_request(
            request_id, callback_function, hc.SEND_TCP_SOCKET, server_heaven_address, server_port, message))

    def send_tcp_file_socket(self, request_id, callback_function, server_heaven_address, server_port, delete_after_send,
                             file_path):
        """
        Send a file to a Tcp Server Socket.
        Packets will be sent directly on sockets without traversing the Heaven stack.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param server_heaven_address: (str) heaven address of the tcp server socket
        :param server_port: (int) port of the tcp server socket
        :param delete_after_send: (int) 1 if the file must be deleted after being sent, 0 otherwise
        :param file_path: (str) absolute path of the file to be sent
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_send_tcp_socket`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_socket_message_sent`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_tcp_socket_error`
        """
        return self._send_to_heaven(self._create_request(
            request_id, callback_function, hc.SEND_TCP_FILE_SOCKET, server_heaven_address, server_port, delete_after_send, file_path))

    # ------------------------- UDP SERVER ------------------------- #

    def create_udp_server(self, request_id, callback_function, port):
        """
        Create a Udp server to receive messages from Udp clients.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which to listen for connections (1000 < port < 9999)
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_udp_server`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_udp_server_received_message`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.CREATE_UDP_SERVER, port))

    def stop_udp_server(self, request_id, callback_function, port):
        """
        Stop a Udp server.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which the server is listening
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_udp_server`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_UDP_SERVER, port))

    def get_udp_servers(self, request_id, callback_function):
        """
        Get the list of ports of udp servers in use.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_udp_servers`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_UDP_SERVERS))

    # ---------------------- UDP SERVER SOCKET --------------------- #

    def create_udp_server_socket(self, request_id, callback_function, port, rx_buffer_size, rx_queue_len):
        """
        Create a Udp server socket to receive messages from Udp client sockets.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param port: (int) port on which to listen for packets (30100 < port < 30999)
        :param rx_buffer_size: (int) not used
        :param rx_queue_len: (int) length of the queue for received messages
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_udp_server`
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_udp_server_received_message`
        """
        return self._send_to_heaven(
            self._create_request(
                request_id, callback_function, hc.CREATE_UDP_SERVER_SOCKET, port, rx_buffer_size, rx_queue_len))

    def stop_udp_server_socket(self, request_id, callback_function, port):
        """
        Stop a Udp Server Socket.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is receivedTrue when the server is stopped, False on error.
        :param port: (int) port on which the server is listening
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_udp_server`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_UDP_SERVER_SOCKET, port))

    def get_udp_servers_socket(self, request_id, callback_function):
        """
        Get the list of ports of udp server sockets in use.

        :param request_id: (str) unique id of the request
        :param callback_function:
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_get_udp_servers`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_UDP_SERVERS_SOCKET))

    # ------------------------- UDP CLIENT ------------------------- #

    def create_udp_client(self, request_id, callback_function, server_address, server_port, mtu, max_hop, packet_rate,
                          routing_method):
        """
        Create a Udp client to send data to Udp Servers.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param server_address: (str) heaven address of the udp server
        :param server_port: (int) port of the udp server
        :param mtu: (int) Maximum Transfer Unit < 2000. If 0, the maximum value allowed by the network will be used
        :param max_hop: (int) int <= 9, maximum number of hop that the message can do in the network. If 0, the maximum value allowed by the network will be used
        :param packet_rate: (int) packets sent per second. If 0, the maximum value allowed by the network will be used
        :param routing_method: (int) 0 gossip, 1 mtree
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_udp_client`
        """
        return self._send_to_heaven(
            self._create_request(request_id, callback_function, hc.CREATE_UDP_CLIENT, server_address,
                                server_port, mtu, max_hop, packet_rate, routing_method))

    def send_udp(self, request_id, callback_function, source_port, message):
        """
        Send a message using a udp client.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param source_port: (str) identifier of the udp client
        :param message: (str) message to be sent
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_send_udp`
        """
        return self._send_to_heaven(
            self._create_request(request_id, callback_function, hc.SEND_UDP, source_port, message))

    def stop_udp_client(self, request_id, callback_function, source_port):
        """
        Stop a udp client.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param source_port: (str) identifier of the udp client
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_udp_client`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.STOP_UDP_CLIENT, source_port))

    def get_udp_clients(self, request_id, callback_function):
        """
        Get the list of source_port of the udp clients in use.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: heaven_api_replies.reply_get_udp_clients
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_UDP_CLIENTS))

    # ---------------------- UDP CLIENT SOCKET---------------------- #

    def create_udp_client_socket(self, request_id, callback_function, server_address, server_port, tx_timeout):
        """
        Create a Udp client socket to send data to Udp Server sockets.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param server_address: (str) heaven address of the server
        :param server_port: (int) port of the udp server socket
        :param tx_timeout: (float) timeout to send a message
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_create_udp_client`
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.CREATE_UDP_CLIENT_SOCKET,
                                                       server_address, server_port, tx_timeout))

    def send_udp_socket(self, request_id, callback_function, client_id, message):
        """
        Send a message using a Udp client socket.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param client_id: (str) identifier of the udp client socket.
        :param message: (str) message to be sent
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_send_udp`
        """
        return self._send_to_heaven(self._create_request(
            request_id, callback_function, hc.SEND_UDP_SOCKET, client_id, message))

    def stop_udp_client_socket(self, request_id, callback_function, client_id):
        """
        Stop a Udp Client Socket.

        :param request_id: (str) unique id of the request
        :param callback_function: (callable) function to be called when a reply is received
        :param client_id: (str) identifier of the udp client socket.
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: :py:meth:`heaven_drones.heaven_modules.api.heaven_api_replies.Replies.reply_stop_udp_client`
        """
        return self._send_to_heaven(self._create_request(
            request_id, callback_function, hc.STOP_UDP_CLIENT_SOCKET, client_id))

    def get_udp_clients_socket(self, request_id, callback_function):
        """
        Get a list of the client_id of udp_socket_clients in use.

        :param request_id: (str) unique id of the request
        :param callback_function:
        :return: (bool) True if the request has been sent to Heaven, False otherwise
        :callback_message: heaven_api_replies.reply_get_udp_clients
        """
        return self._send_to_heaven(self._create_request(request_id, callback_function, hc.GET_UDP_CLIENTS_SOCKET))
