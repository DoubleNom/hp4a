import time
from heaven_modules.api.heaven_callback_receiver import TcpClientReceiver, UdpClientReceiver
from heaven_modules.api.heaven_callback_receiver import TcpClientSocketReceiver, UdpClientSocketReceiver
from heaven_modules.api.heaven_api_replies import Replies
import os
from heaven_modules.common.tail_drop_queue import TailDropQueue
import logging
from heaven_modules.common.standard_threads import QueueServer

"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- TCP ---------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class TcpClientsManager(QueueServer):
    """
    Creates many clients to send data to one server.
    This thread reads from a tx queue the messages to be sent and requests tcp clients to do the job.
    Once the message is sent, the manager deletes the client.
    """
    DATA_MESSAGE = 1
    STOP_CLIENT = 3
    MAX_CLIENTS = 20

    def __init__(self, heaven_client, tcp_server_heaven_address, tcp_server_port, routing_method, sent_callback=None):
        QueueServer.__init__(self, TailDropQueue())
        self.heaven_client = heaven_client
        self.tcp_server_heaven_address = tcp_server_heaven_address
        self.tcp_server_port = tcp_server_port
        self.routing_method = routing_method
        self.sent_callback = sent_callback
        self.tcp_client_callback_receiver = MyTcpClientReceiver(self)
        self.client_callback = self.tcp_client_callback_receiver.receive_packet
        self.tcp_clients = {}  # client request_id --> tcp clients
        self.messages = {}  # message request_id --> client request_id , used to delete clients
        self.max_msg_len = heaven_client.MAX_MSG_LEN
        self.start()

    def handle(self, x):

        command, data = x

        if command == self.STOP_CLIENT and data in self.tcp_clients:
            self.stop_client(data)  # data is the client request id
            del self.tcp_clients[data]
            logging.getLogger(__name__).debug('Stopped client {}, now clients are {}'.format(data, self.tcp_clients))

        elif command == self.DATA_MESSAGE:

            if len(self.tcp_clients) >= self.MAX_CLIENTS:
                return

            request_id = self.generate_request_id()

            self.heaven_client.create_tcp_client(
                request_id,
                self.client_callback,
                self.tcp_server_heaven_address,
                self.tcp_server_port,
                self.routing_method)

            self.tcp_clients[request_id] = TcpClientMessageSender(
                request_id, self.heaven_client, self.client_callback, self.messages, self.max_msg_len)
            self.tcp_clients[request_id].send(data)

    def send(self, packet_str):
        self.put((self.DATA_MESSAGE, packet_str))
        return True

    def cleanup(self):
        for client_request_id in self.tcp_clients.keys():
            self.stop_client(client_request_id)

        self.tcp_clients = {}
        self.tcp_client_callback_receiver.stop()

    def request_stop_client_with_message_id(self, reply):  # called when a message is sent with success
        message_request_id = reply[Replies.KEY_REQUEST_ID]
        client_request_id = self.messages[message_request_id]
        logging.getLogger(__name__).debug('Request to stop client {} because message {} was sent'.format(client_request_id, message_request_id))
        self.put((self.STOP_CLIENT, client_request_id))

        if self.sent_callback:
            self.sent_callback(self.tcp_server_heaven_address, self.tcp_server_port, self.routing_method, reply)

    def stop_client(self, client_request_id):
        self.tcp_clients[client_request_id].stop()
        if self.tcp_clients[client_request_id].is_running():
            self.tcp_clients[client_request_id].join()
        else:
            logging.getLogger(__name__).debug('Cannot join this client!')

    def generate_request_id(self):
        return 'create_tcp_cli_{}_{}_{:.15f}'.format(self.tcp_server_heaven_address,
                                                self.tcp_server_port,
                                                time.time())

    def session_aborted(self, reply):
        if self.sent_callback:
            self.sent_callback(self.tcp_server_heaven_address, self.tcp_server_port, self.routing_method, reply)


class TcpClientMessageSender(QueueServer):
    """
    The thread read from a tx queue (put by tx manager) and wait his session id to send messages in the queue.
    Typically the queue will have only a message since after the first the client it is stopped.
    """
    def __init__(self, tcp_client_request_id, heaven_client, client_callback, messages, max_msg_len):
        QueueServer.__init__(self, TailDropQueue())
        self.tcp_client_request_id = tcp_client_request_id
        self.heaven_client = heaven_client
        self.client_callback = client_callback
        self.messages = messages
        self.max_msg_len = max_msg_len
        self.session_id = ''

    def receive_session_id(self, session_id):
        self.session_id = session_id
        self.start()

    def handle(self, x):
        packet_str = x

        if packet_str is None:
            return

        request_id = self.generate_request_id_send()

        if len(packet_str) > self.max_msg_len:
            this_folder = os.path.dirname(os.path.abspath(__file__))
            file_path = '{}/sending_file_{:.15f}.htf'.format(this_folder, time.time())
            fh = open(file_path, 'wb')
            fh.write(packet_str)
            fh.close()

            logging.getLogger(__name__).debug('Sending TCP file '.format(file_path))
            self.heaven_client.send_tcp_file(
                request_id, self.client_callback, self.session_id, 1, file_path)
        elif packet_str[:10] == 'send_file;':
            self.heaven_client.send_tcp_file(
                request_id, self.client_callback, self.session_id, 0, packet_str[10:])
        else:
            logging.getLogger(__name__).debug('Sending TCP message')
            self.heaven_client.send_tcp(
                request_id, self.client_callback, self.session_id, packet_str)

        self.messages[request_id] = self.tcp_client_request_id

    def generate_request_id_send(self):
        return 'send_tcp_{}_{:.15f}'.format(self.session_id, time.time())

    def generate_request_id_stop(self):
        return 'stop_tcp_client_{}_{:.15f}'.format(self.session_id, time.time())

    def send(self, x):
        self.put(x)

    def is_running(self):
        return self.do_run

    def stop(self):
        QueueServer.stop(self)

        if self.session_id:
            logging.getLogger(__name__).debug('Asking heaven to stop tcp client')
            self.heaven_client.stop_tcp_client(
                'stop_tcp_client_{}'.format(self.generate_request_id_stop()),
                self.client_callback,
                self.session_id)
        else:
            logging.getLogger(__name__).debug('No session id, cannot stop client')


class MyTcpClientReceiver(TcpClientReceiver):

    def __init__(self, tcp_clients_manager):
        TcpClientReceiver.__init__(self)
        self.tcp_clients_manager = tcp_clients_manager
        self.start()

    def client_created(self, reply):
        request_id = reply[Replies.KEY_REQUEST_ID]
        session_id = reply[Replies.KEY_SESSION_ID]

        try:
            self.tcp_clients_manager.tcp_clients[request_id].receive_session_id(session_id)
        except KeyError:
            logging.getLogger(__name__).exception('Request id not found')

    def message_sent(self, reply):
        logging.getLogger(__name__).debug('TCP Client sent: {}'.format(reply))
        self.tcp_clients_manager.request_stop_client_with_message_id(reply)

    def session_aborted(self, reply):
        self.tcp_clients_manager.session_aborted(reply)


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- TCP SOCKET --------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class TcpSocketClient(QueueServer):
    """
    Used to send data to a specific server
    """

    SEND_MSG = 0
    SEND_FILE = 1
    DEFAULT_DELETE = 0

    def __init__(self, heaven_client, server_address, server_port, delete_after_send=0, callback_receiver=None):
        QueueServer.__init__(self, TailDropQueue())
        self.heaven_client = heaven_client
        self.server_address = server_address
        self.server_port = server_port
        self.delete_after_send = delete_after_send

        # use a standard callback receiver
        if callback_receiver is None:
            self.callback_receiver = TcpClientSocketReceiver()
        else:
            self.callback_receiver = callback_receiver

        self.callback_receiver.start()
        self.callback = self.callback_receiver.receive_packet
        self.request_counter = 0
        self.start()

    def send(self, data_str):
        request_id = self.generate_request_id()
        self.put((TcpSocketClient.SEND_MSG, request_id, data_str))
        return request_id

    def send_file(self, file_path, delete_after_send=0):
        request_id = self.generate_request_id()
        self.put((TcpSocketClient.SEND_FILE, request_id, (delete_after_send, file_path)))
        return request_id

    def generate_request_id(self):
        self.request_counter += 1

        if self.request_counter > 1000:
            self.request_counter = 1

        return 'send_tcp_sock_{}_{:.15f}'.format(self.request_counter, time.time())

    def handle(self, x):

        cmd, request_id, params = x

        if cmd == TcpSocketClient.SEND_MSG:
            self.heaven_client.send_tcp_socket(
                request_id, self.callback, self.server_address, self.server_port, params)

        elif cmd == TcpSocketClient.SEND_FILE:
            self.heaven_client.send_tcp_file_socket(
                request_id, self.callback, self.server_address, self.server_port, params[0], params[1])

    def cleanup(self):
        self.callback_receiver.stop()


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- UDP ---------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class UdpClientManager(object):
    """
    Creates a UDP client to a specific server and manages callbacks
    """

    def __init__(self, heaven_client, server_address, server_port, mtu=0, max_hop=0, packet_rate=0, routing_method=0):
        self.heaven_client = heaven_client
        self.server_address = server_address
        self.server_port = int(server_port)
        self.mtu = int(mtu)
        self.max_hop = int(max_hop)
        self.packet_rate = int(packet_rate)
        self.routing_method = int(routing_method)
        self.udp_client_callback_receiver = MyUdpClientReceiver(self)
        self.client_callback = self.udp_client_callback_receiver.receive_packet
        self.source_port = None
        self.create_client()

    def create_client(self):

        self.heaven_client.create_udp_client(
            'create_udp_cli_{}_{}_{:.15f}'.format(self.server_address, self.server_port, time.time()),
            self.udp_client_callback_receiver.receive_packet,
            self.server_address, self.server_port, self.mtu, self.max_hop, self.packet_rate, self.routing_method)

    def receive_source_port(self, source_port):
        self.source_port = int(source_port)

    def send(self, message):

        if not self.source_port:
            return False

        return self.heaven_client.send_udp(
            'send_udp_{}_{}_{:.15f}'.format(self.server_address, self.server_port, time.time()),
            self.client_callback,
            self.source_port,
            message)

    def stop(self):

        result = self.heaven_client.stop_udp_client(
            'stop_udp_client_{}_{}_{:.15f}'.format(self.server_address, self.server_port, time.time()),
            self.client_callback,
            self.source_port)

        if not result:
            return False

        self.udp_client_callback_receiver.stop()


class MyUdpClientReceiver(UdpClientReceiver):
    """
    Implements methods of UdpClientReceiver in order to manage various stateful callbacks
    e.g. receiving and storing the udp source port for future use
    """

    def __init__(self, udp_client_manager):
        UdpClientReceiver.__init__(self)
        self.udp_client_manager = udp_client_manager
        self.start()

    def client_created(self, reply):

        logging.getLogger(__name__).debug('UDP Client created: {}'.format(reply))

        if reply[Replies.KEY_SERVER_ADDRESS] != self.udp_client_manager.server_address:
            return

        if int(reply[Replies.KEY_PORT]) != self.udp_client_manager.server_port:
            return

        self.udp_client_manager.receive_source_port(reply[Replies.KEY_SOURCE_PORT])


"""
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------- UDP SOCKET --------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
"""


class MyUdpClientSocketReceiver(UdpClientSocketReceiver):

    def __init__(self, udp_socket_client):
        UdpClientSocketReceiver.__init__(self)
        self.udp_socket_client = udp_socket_client

    def client_created(self, reply):
        self.udp_socket_client.set_client_id(int(reply[Replies.KEY_SOURCE_PORT]))


class UdpSocketClient(QueueServer):
    """
    Used to send data to a specific server
    """

    def __init__(self, heaven_client, server_address, server_port, tx_timeout):
        QueueServer.__init__(self, TailDropQueue())
        self.heaven_client = heaven_client
        self.server_address = server_address
        self.server_port = server_port
        self.tx_timeout = tx_timeout
        self.callback_receiver = MyUdpClientSocketReceiver(self)
        self.callback = self.callback_receiver.receive_packet
        self.callback_receiver.start()
        self.client_id = None
        self.request_counter = 0

        # create client
        self.heaven_client.create_udp_client_socket(
            self.generate_request_id(), self.callback, self.server_address, self.server_port, self.tx_timeout)

    def send_msg(self, data_str):
        self.put(data_str)

    def set_client_id(self, client_id):
        """
        After you have a client id you can start serving the tx queue
        :param client_id:
        :return:
        """
        self.client_id = client_id
        self.start()

    def generate_request_id(self):
        self.request_counter += 1

        if self.request_counter > 1000:
            self.request_counter = 1

        return 'send_udp_sock_{}_{:.15f}'.format(self.request_counter, time.time())

    def handle(self, x):
        message = x
        self.heaven_client.send_udp_socket(self.generate_request_id(), self.callback, self.client_id, message)

    def cleanup(self):
        self.heaven_client.stop_udp_client_socket(self.generate_request_id(), self.callback, self.client_id)
        self.callback_receiver.stop()
        self.callback_receiver.join()
