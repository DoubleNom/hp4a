class Replies(object):
    """
    When Heaven has a reply for a request, it calls the methods of this class
    to obtain the dictionary that will be json-serialized and sent as reply.

    .. Note:: Every Reply dictionary will contain also the triggering RequestId and Command
    """

    KEY_HEAVEN_ADDRESS = 'heaven_address'
    KEY_STOPPING = 'stopping'
    KEY_PEERS = 'peers'
    KEY_PEERS_SUBSCRIPTION = 'subscription'
    KEY_PEERS_UNSUBSCRIPTION = 'unsubscription'
    KEY_PEERS_SUBSCRIPTION_ID = 'subscription_id'
    KEY_NEIGHBORS = 'neighbors'
    KEY_IP_ADDRESS = 'ip'
    KEY_LSP = 'lsp'
    KEY_ROUTING_TABLE = 'routing_table'
    KEY_PORT = 'port'
    KEY_CREATED = 'created'
    KEY_ERROR = 'error'
    KEY_STOPPED = 'stopped'
    KEY_SERVER_ADDRESS = 'server_address'
    KEY_SESSION_ID = 'session_id'
    KEY_SOURCE_PORT = 'source_port'
    KEY_REQUEST_ACCEPTED = 'request_accepted'
    KEY_MESSAGE_ID = 'message_id'
    KEY_MESSAGE_SENT = 'sent'
    KEY_SOURCE = 'source'
    KEY_MESSAGE = 'message'
    KEY_FILE_PATH = 'file_path'
    KEY_TCP_SERVERS = 'tcp_servers'
    KEY_TCP_CLIENTS = 'tcp_clients'
    KEY_UDP_SERVERS = 'udp_servers'
    KEY_UDP_CLIENTS = 'udp_clients'
    KEY_FILE_SIZE = 'file_size'
    KEY_FULL_NETWORK = 'full_network'
    KEY_ROUTING_TREES = 'routing_trees'
    KEY_ELAPSED_TIME = 'elapsed_time'
    KEY_BPS = 'bps'
    KEY_ERROR_DESCRIPTION = 'error_description'

    KEY_BLOCKED_NEIGHBOR = 'blocked_neighbor'
    KEY_UNBLOCKED_NEIGHBOR = 'unblocked_neighbor'

    KEY_REQUEST_ID = 'request_id'
    KEY_COMMAND = 'command'
    KEY_DELETE_REQUEST = 'delete_request'

    # ---------------------------- ROUTING ----------------------------------

    @staticmethod
    def reply_get_heaven_address(heaven_address, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_heaven_address`. Only one reply per request.

        :param heaven_address: (str) The heaven_address used by this instance of Heaven
        :param error_description: (str)
        :return: {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_HEAVEN_ADDRESS: heaven_address, 
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_stop_heaven(stopping, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_heaven`. Only one reply per request.

        :param stopping: (bool) True if stopping, False otherwise
        :param error_description: (str)
        :return: {Replies.KEY_STOPPING: stopping,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_STOPPING: stopping,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_peers(peers, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_peers` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.reply_get_peers_updates`.
        Only one reply if the command is get_peers.
        Many replies if the command is get_peers_updates.

        :param peers: (list) list of peers
        :param error_description: (str)
        :return: {Replies.KEY_PEERS: peers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PEERS: peers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_peers_updates(subscription_success, subscription_id, peers, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_peers_updates`. Only one reply per request.

        :param subscription_success: (int) 1 on success, 0 on fail
        :param subscription_id: (str) id used to retrieve and stop the subscription
        :param peers: (list) list of peers
        :param error_description: (str)
        :return: {Replies.KEY_PEERS_SUBSCRIPTION: subscription_success,
                Replies.KEY_PEERS_SUBSCRIPTION_ID: subscription_id,
                Replies.KEY_PEERS: peers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PEERS_SUBSCRIPTION: subscription_success,
                Replies.KEY_PEERS_SUBSCRIPTION_ID: subscription_id,
                Replies.KEY_PEERS: peers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_stop_peers_updates(unsubscription_success, subscription_id, error_description=''):
        """
        reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_peers_updates`. Only one reply per request.

        :param unsubscription_success: (int) 1 on success, 0 on fail
        :param subscription_id: (str) id used to retrieve and stop the subscription
        :param error_description: (str)
        :return: {Replies.KEY_PEERS_UNSUBSCRIPTION: unsubscription_success,
                Replies.KEY_PEERS_SUBSCRIPTION_ID: subscription_id,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PEERS_UNSUBSCRIPTION: unsubscription_success,
                Replies.KEY_PEERS_SUBSCRIPTION_ID: subscription_id,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_neighbors(neigh, error_description=''):
        """
        reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_neighbors`. Only one reply per request.

        :param neigh: (dict) representation of the current Neighbors
        :param error_description: (str)
        :return: {Replies.KEY_NEIGHBORS: neigh,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_NEIGHBORS: neigh,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_lsp(lsp, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_lsp`. Only one reply per request.

        :param lsp: (dict) representation of the current Link State Packets
        :param error_description: (str)
        :return: {Replies.KEY_LSP: lsp,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_LSP: lsp,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_routing_table(rt, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_routing_table`. Only one reply per request.

        :param rt: (dict) representation of the routing table {tree_n: {dest: next_hop}}
        :param error_description: (str)
        :return:  {Replies.KEY_ROUTING_TABLE: rt,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_ROUTING_TABLE: rt,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_routing_trees(rt, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_routing_trees`. Only one reply per request.

        :param rt: (dict) representation of the routing tree {node: previous_hop}
        :param error_description: (str)
        :return: {Replies.KEY_ROUTING_TREES: rt,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_ROUTING_TREES: rt,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_full_network(fn, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_full_network`. Only one reply per request.

        :param fn: (dict) representation of the Network (advertised nodes and links)
        :param error_description: (str)
        :return: {Replies.KEY_FULL_NETWORK: fn,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

        """
        return {Replies.KEY_FULL_NETWORK: fn,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_block_neighbor(heaven_address, blocked, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.block_neighbor`. Only one reply per request.

        :param heaven_address: (str) heaven address of the neighbor.
        :param blocked: (int) 1 if blocked, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_BLOCKED_NEIGHBOR: blocked,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_BLOCKED_NEIGHBOR: blocked,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_unblock_neighbor(heaven_address, unblocked, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.unblock_neighbor`. Only one reply per request.

        :param heaven_address: (str) heaven address of the neighbor.
        :param unblocked: (int) 1 if unblocked, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_UNBLOCKED_NEIGHBOR: unblocked,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_UNBLOCKED_NEIGHBOR: unblocked,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_peer_ip(heaven_address, ip_address, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_peer_ip`. Only one reply per request.

        :param heaven_address: (str) heaven address of the peer
        :param ip_address: (str) ip address of the peer or an empty string
        :param error_description: (str)
        :return: {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_IP_ADDRESS: ip_address,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_IP_ADDRESS: ip_address,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_my_ip_for_peer(heaven_address, ip_address, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_my_ip_for_peer`. Only one reply per request.

        :param heaven_address: (str) heaven address of the peer
        :param ip_address: (str) my ip address to reach the peer or an empty string
        :param error_description: (str)
        :return: {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_IP_ADDRESS: ip_address,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_HEAVEN_ADDRESS: heaven_address,
                Replies.KEY_IP_ADDRESS: ip_address,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    # ---------------------------- TCP SERVER AND SERVER SOCKET ---------------------------------- #

    @staticmethod
    def reply_create_tcp_server(port, created, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_server_socket`.

        :param port: (int) port on which the server will listen
        :param created: (int) 1 if created, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_server_received_message(port, source, session_id, message, message_len, elapsed_time, bps, error_description=''):
        """
        Eventual reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_server_socket`.

        :param port: (int) port on which the server is listening
        :param source: (str) source of the received message
        :param session_id: (str) id of the communication session
        :param message: (str)
        :param message_len: (int) number of bytes of the message
        :param elapsed_time: (float) duration of message reception
        :param bps: (int) bitrate [bit/s] of message reception
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_SOURCE: source,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE: message,
                Replies.KEY_FILE_SIZE: message_len,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_SOURCE: source,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE: message,
                Replies.KEY_FILE_SIZE: message_len,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_server_received_file(port, source, session_id, file_path, file_size, elapsed_time, bps, error_description=''):
        """
        Eventual reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_server_socket`. One reply for each received message.

        :param port: (int) port on which the server is listening
        :param source: (str) source of the received message
        :param session_id: (str) id of the communication session
        :param file_path: (str) filename (with path) into which the message has been saved
        :param file_size: (int) number of bytes of the message
        :param elapsed_time: (float) duration of message reception
        :param bps: (int) bitrate [bit/s] of message reception
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_SOURCE: source,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_FILE_PATH: file_path,
                Replies.KEY_FILE_SIZE: file_size,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_SOURCE: source,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_FILE_PATH: file_path,
                Replies.KEY_FILE_SIZE: file_size,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_stop_tcp_server(port, stopped, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_tcp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_tcp_server_socket`. Only one reply per request.

        :param port: (int) port on which the server is (was) listening
        :param stopped: (int) 1 if stopped, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_tcp_servers(servers, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_tcp_servers` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_tcp_servers_socket`. Only one reply per request.

        :param servers:  (list) ports of opened servers.
        :param error_description: (str)
        :return: {Replies.KEY_TCP_SERVERS: servers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_TCP_SERVERS: servers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    # ---------------------------- TCP CLIENT ---------------------------------- #
    @staticmethod
    def reply_create_tcp_client(server_address, server_port, session_id, accepted, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_client`.
        Note: After receiving this message, the session is not established and confirmed yet.

        :param server_address: (str) heaven_address of the server
        :param server_port: (int) port on which the server is listening
        :param session_id: (str) id of the communication session
        :param accepted: (int) 1 if the request has been accepted, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_session_established(server_address, server_port, session_id, created, error_description=''):
        """
        Second reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_client` (if no error occurs). The session_id now can be used to send messages.

        :param server_address: (str) heaven_address of the server
        :param server_port: (int) port on which the server is listening
        :param session_id: (str) id of the tcp session
        :param created: (int) 1 if the session has been established, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_session_abort(server_address, server_port, session_id, error, error_description=''):
        """
        Eventual reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_tcp_client`.

        :param server_address: (str) heaven_address of the server
        :param server_port: (int) port on which the server is listening
        :param session_id: (str) id of the communication session
        :param error: (str)
        :param error_description: (str)
        :return: {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_ERROR: error,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_ERROR: error,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_send_tcp(session_id, message_id, accepted, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp`. Only one reply per request.
        Note: After receiving this message, the message is being sent but not yet received by the server.

        :param session_id: (str) id of the communication session
        :param message_id: (str) id of the message
        :param accepted: (int) 1 if the request to send has been accepted, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_ID: message_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_ID: message_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_send_tcp_file(session_id, message_id, accepted, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_file`.
        Note: After receiving this message, the message is being sent but not yet received by the server.

        :param session_id: (str) id of the communication session
        :param message_id: (str) id of the message
        :param accepted: (int) 1 if the request to send has been accepted, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_ID: message_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_ID: message_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_message_sent(session_id, message_id, sent, file_size, elapsed_time, bps, error_description=''):
        """
        Second reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_file` (if no error occurs).

        :param session_id: (str) id of the communication session
        :param message_id: (str) id of the message
        :param sent: (int) 1 if the message has been sent and received by the server, 0 otherwise
        :param file_size: (int) number of bytes of the message
        :param elapsed_time: (float) duration of message reception
        :param bps: (int) bitrate [bit/s] of message reception
        :param error_description: (str)
        :return: {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_ID: message_id,
                Replies.KEY_MESSAGE_SENT: sent,
                Replies.KEY_FILE_SIZE: file_size,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_ID: message_id,
                Replies.KEY_MESSAGE_SENT: sent,
                Replies.KEY_FILE_SIZE: file_size,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_stop_tcp_client(session_id, stopped, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_tcp_client`. Only one reply per request.

        :param session_id: (str) id of the communication session
        :param stopped: (int) 1 if the client has been stopped, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_tcp_clients(clients, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_tcp_clients`. Only one reply per request.

        :param clients: (list) session ids of the created clients
        :param error_description: (str)
        :return: {Replies.KEY_TCP_CLIENTS: clients,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_TCP_CLIENTS: clients,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    # ---------------------------- TCP CLIENT SOCKET ---------------------------------- #

    @staticmethod
    def reply_send_tcp_socket(session_id, accepted, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_socket` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_file_socket`.
        Note: After receiving this message, the message is being sent but not yet received by the server.

        :param session_id: (str) id of the communication session 
        :param accepted: (int) 1 if the message has been enqueued for transmission, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_REQUEST_ACCEPTED: accepted,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_socket_message_sent(session_id, sent, file_size, elapsed_time, bps, error_description=''):
        """
        Second reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_socket` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_file_socket` (if no error occurs).

        :param session_id: (str) id of the communication session
        :param sent: (int) 1 if the message has been sent and received by the server, 0 otherwise
        :param file_size: (int) number of bytes of the message
        :param elapsed_time: (float) duration of message reception
        :param bps: (int) bitrate [bit/s] of message reception
        :param error_description: (str)
        :return: {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_SENT: sent,
                Replies.KEY_FILE_SIZE: file_size,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_MESSAGE_SENT: sent,
                Replies.KEY_FILE_SIZE: file_size,
                Replies.KEY_ELAPSED_TIME: elapsed_time,
                Replies.KEY_BPS: bps,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_tcp_socket_error(server_address, server_port, session_id, error, error_description=''):
        """
        Eventual reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_socket` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_tcp_file_socket`.

        :param server_address: (str) heaven address of the server.
        :param server_port: (int) port of the server
        :param session_id: (str) id of the communication session
        :param error:
        :param error_description: (str)
        :return: {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_ERROR: error,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SESSION_ID: session_id,
                Replies.KEY_ERROR: error,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    # ---------------------------- UDP SERVER ---------------------------------- #

    @staticmethod
    def reply_create_udp_server(port, created, error_description=''):
        """
        First reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_udp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_udp_server_socket`. Only one reply per request.

        :param port: (int) port on which the server will listen
        :param created: (int) 1 if created, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_udp_server_received_message(port, source, source_port, message, message_len, error_description=''):
        """
        Eventual reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_udp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_udp_server_socket`. One reply for each message received.

        :param message_len: (int) number of bytes of the received message
        :param port: (int) port on which the server is listening
        :param source: (str) heaven address of the sender
        :param source_port: (str) id of the communication session
        :param message: (str) received message
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_SOURCE: source,
                Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_MESSAGE: message,
                Replies.KEY_FILE_SIZE: message_len,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_SOURCE: source,
                Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_MESSAGE: message,
                Replies.KEY_FILE_SIZE: message_len,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_stop_udp_server(port, stopped, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_udp_server` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.and` stop_udp_server_socket. Only one reply per request.

        :param port: (int) port on which the server is listening
        :param stopped: (int) 1 if stopped, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_PORT: port,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_PORT: port,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_udp_servers(servers, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_udp_servers` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_udp_servers_socket`. Only one reply per request.

        :param servers: (list) ports of opened servers.
        :param error_description: (str)
        :return: {Replies.KEY_UDP_SERVERS: servers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_UDP_SERVERS: servers,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    # ---------------------------- UDP CLIENT ---------------------------------- #
    @staticmethod
    def reply_create_udp_client(server_address, server_port, source_port, created, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_udp_client` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.create_udp_client_socket`. Only one reply per request.

        :param server_address: (str) heaven_address of the server
        :param server_port: (int) port on which the server is listening
        :param source_port: (str) id of the communication session
        :param created: (int) 1 if the client has been created, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SERVER_ADDRESS: server_address,
                Replies.KEY_PORT: server_port,
                Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_CREATED: created,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_send_udp(source_port, sent, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_udp` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.send_udp_socket`. Only one reply per request.

        :param source_port: (str) id of the communication session
        :param sent: (int) 1 if the message has been sent, 0 otherwise (Note: udp does not guarantee that the server has received the message)
        :param error_description: (str)
        :return: {Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_MESSAGE_SENT: sent,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_MESSAGE_SENT: sent,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_stop_udp_client(source_port, stopped, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_udp_client` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.stop_udp_client_socket`. Only one reply per request.

        :param source_port: (str) id of the communication session
        :param stopped: int) 1 if the client has been stopped, 0 otherwise
        :param error_description: (str)
        :return: {Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_SOURCE_PORT: source_port,
                Replies.KEY_STOPPED: stopped,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    @staticmethod
    def reply_get_udp_clients(clients, error_description=''):
        """
        Reply to :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_udp_clients` and :py:meth:`heaven_drones.heaven_modules.api.heaven_api_client.HeavenClient.get_udp_clients_socket`. Only one reply per request.

        :param clients: (list) source ports of created clients
        :param error_description: (str)
        :return: {Replies.KEY_UDP_CLIENTS: clients,
                Replies.KEY_ERROR_DESCRIPTION: error_description}
        """
        return {Replies.KEY_UDP_CLIENTS: clients,
                Replies.KEY_ERROR_DESCRIPTION: error_description}

    # ---------------------------- API LEVEL ----------------------------------
    @staticmethod
    def reply_delete_request():
        """
        Message sent to signal that a request will not produce other replies.

        :return: {Replies.KEY_DELETE_REQUEST: 1}
        """
        return {Replies.KEY_DELETE_REQUEST: 1}

    @staticmethod
    def update_reply(request_id, command):
        """
        These two fields are always added to replies by the Heaven API Server Message sender

        :param request_id: (str) request identifier
        :param command: (str) command that triggered the reply.
        :return: {Replies.KEY_REQUEST_ID: request_id, Replies.KEY_COMMAND: command}
        """
        return {Replies.KEY_REQUEST_ID: request_id, Replies.KEY_COMMAND: command}


class ErrorDescriptions(object):
    """
    Values or the field error_description used in Replies
    """

    PORT_OUT_OF_RANGE = 'port out of range'
    HEAVEN_IS_STOPPING = 'heaven is stopping'
    SERVER_PORT_NOT_FOUND = 'server port not found'
    SERVER_PORT_NOT_AVAILABLE = 'server port not available'
    HEAVEN_ADDRESS_NOT_VALID = 'unacceptable Heaven Address'
    ROUTING_METHOD_NOT_VALID = 'unacceptable routing method'
    MULTI_TREE_DISABLED = 'multi tree routing is disabled'
    FILE_TOO_BIG = 'file too big'
    SESSION_ID_NOT_FOUND = 'session_id not found'
    FILE_NOT_FOUND = 'file not found'
    IP_LOOKUP_FAILED = 'ip lookup failed'
    BUFFER_SIZE_TOO_BIG = 'buffer size too big'
    MTU_TOO_BIG = 'mtu too big'
    MAX_HOP_TOO_BIG = 'max hop too big'
    SOURCE_PORT_NOT_FOUND = 'source port not found'
    MESSAGE_TOO_BIG = 'message bigger than the socket buffer size'
    CLIENT_ID_NOT_FOUND = 'client id not found'
    CONNECTION_CLOSED_BY_SERVER = 'connection closed by server'
