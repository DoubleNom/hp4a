from heaven_modules.layer4.transport_layer import TransportManager
import socket
from heaven_api_request import Request
from heaven_api_commands import Commands
try:
    import simplejson as json
except ImportError:
    import json
from heaven_api_replies import Replies
from heaven_modules.common.tail_drop_queue import TailDropQueue
from heaven_modules.common.standard_threads import HeavenSocketServer, QueueServer
from heaven_modules.common.utilities import send_tcp_msg, send_udp_msg
from heaven_modules.common.delegates import Delegates
import logging
import threading


class HeavenAPIServer(HeavenSocketServer):

    def __init__(self, socket_address, config_reader, enable_packet_profiler,
                 socket_family=socket.AF_INET, socket_type=socket.SOCK_STREAM):
        """
        :param socket_address: 'ip:port' on which to listen
        :param config_reader: ConfigReade object
        :param enable_packet_profiler: boolean
        :raise KeyError, ValueError, socket.error
        """
        # reading config may rise KeyError or ValueError
        api_config_dict = config_reader.get_config_dict('api_server')
        buffer_size = int(api_config_dict['socket_buffer_size'])
        rx_queue_len = int(api_config_dict['rx_queue_len'])
        self.tx_timeout = float(api_config_dict['tx_timeout'])
        self.delegates = Delegates()

        # start Heaven, may rise socket error if another instance of heaven is running
        HeavenSocketServer.__init__(
            self, socket_address, buffer_size, rx_queue_len, socket_family, socket_type, name='Heaven Api Server')

        # start Heaven stack
        # can raise ConfigParser.NoSectionError or ConfigParser.NoOptionError, catch by heaven launcher

        self.transport_manager = TransportManager(config_reader, enable_packet_profiler, self.delegates)

        # object variables
        self.reply_senders = {}  # destination socket {(ip, port): ReplySender}
        self.is_stopping = False
        self.start()

    def stop(self, *args):
        print('Stopping HEAVEN...')
        self.is_stopping = True
        self.transport_manager.stop()
        HeavenSocketServer.stop(self)
        for reply_sender in self.reply_senders.itervalues():
            reply_sender.stop()
        self.reply_senders = {}
        print('Waiting {} threads to stop [this may take up to 30s]...'.format(threading.enumerate()))

    def handle(self, x):
        """
        Receives request from Heaven Clients as strings in the format:
        requestID;port;command;parameter1;par2;
        0         1    2       3          4

        - request_id: unique id of the request
        - port: port on which the reply must be sent. The IP address will be the same of reception
        - command: e.g. 'create_tcp_server'
        - parameter: parameters regarding the command

        If a unix socket is used, port will be socket address

        :param x: a tuple (received_data_string, client_address)
        :return: -
        """
        if not self.do_run:
            return

        data, client_address = x
        data_split = data.split(';')

        # try:
        logging.getLogger(__name__).debug('HeavenServer received {} from {}'.format(data[:100], client_address))

        # Parse the first part of the request
        try:
            request_id, port, command = data_split[:3]

            if self.socket_family == socket.AF_INET:
                destination_socket_address = (client_address[0], int(port))
            else:
                destination_socket_address = port
        except ValueError:
            return

        # Start a thread to send replies to the Heaven client
        if destination_socket_address not in self.reply_senders:
            self.reply_senders[destination_socket_address] = ReplySender(
                self, destination_socket_address)

        reply_sender = self.reply_senders[destination_socket_address]

        # create a new request object and add it to the reply sender
        request = Request(request_id, command, reply_sender)

        if not reply_sender.add_request(request):
            logging.getLogger(__name__).error('HeavenServer received a duplicate request_id: {}'.format(data))
            return

        if command == Commands.STOP_HEAVEN:
            request.reply(Replies.reply_stop_heaven(True))
            self.stop()

        # now serve the request by calling the right function of the transport manager
        try:
            getattr(self.transport_manager, command)(request, data_split)
        except:
            logging.getLogger(__name__).exception('HeavenServer received a bad request: {}'.format(data))
            request.end()


class ReplySender(QueueServer):
    """
    For each heaven client (ip address, server port) a callback reader is created
    in order to open a socket to the client and send him all the replies.

    When the socket fails, all requests are deleted
    """

    def __init__(self, api_request_server, destination_address):
        """
        :param api_request_server: HeavenSocketServer receiving requests from the client
        :param destination_address: socket to reach the client, a tuple (ip, port)
        """
        QueueServer.__init__(self, TailDropQueue())
        self.api_request_server = api_request_server
        self.destination_address = destination_address
        self.requests = {}  # request_id --> request
        self.socket_family = api_request_server.socket_family
        self.socket_type = api_request_server.socket_type
        self.start()

    def add_request(self, request):
        """
        Called by HeavenServer
        :param request:
        :return: True if added, False is not (request id must be unique)
        """
        if request.request_id not in self.requests:
            self.requests[request.request_id] = request
            return True

        return False

    def handle(self, x):

        # read the reply
        request_id, command, reply = x

        try:
            reply.update(Replies.update_reply(request_id, command))
        except AttributeError:
            return

        # try to send the reply to the client
        logging.getLogger(__name__).debug('ReplySender sending reply to {}: {}'.format(self.destination_address, reply))

        if self.socket_type == socket.SOCK_STREAM:
            send_tcp_msg(self.destination_address, json.dumps(reply), self.socket_family)
        else:
            send_udp_msg(self.destination_address, json.dumps(reply), self.socket_family)

        # if necessary, delete the request
        if Replies.KEY_DELETE_REQUEST in reply:
            try:
                del self.requests[request_id]
            except KeyError:
                logging.getLogger(__name__).exception('ReplySender cannot delete request, not found!')
                pass

    def cleanup(self):

        # signal to the HeavenServer that this client is not reachable anymore
        if self.api_request_server.do_run and self.destination_address in self.api_request_server.reply_senders:
            del self.api_request_server.reply_senders[self.destination_address]

        logging.getLogger(__name__).debug('HeavenServer: ReplySender to {} finished'.format(self.destination_address))

    def receive_reply(self, x):
        self.put(x)
