class CommonParameters(object):

    IOS_NETWORK_NAME = 'ios_network'
    ANDROID_NETWORK_NAME = 'android_network'
    AP_NETWORK_NAME = 'ap_network'
    AP_CLI_NETWORK_NAME = 'ap_cli_network'
    AD_HOC_NETWORK_NAME = 'ad_hoc_network'
    ETHERNET_NETWORK_NAME = 'ethernet_network'
    STANDARD_NETWORKS = [AP_NETWORK_NAME, AP_CLI_NETWORK_NAME, ETHERNET_NETWORK_NAME, AD_HOC_NETWORK_NAME]

    DEFAULT = 'default'