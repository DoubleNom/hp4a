
"""
PACKET ROUTING FORMAT
| HOP COUNTER | SOURCE  | DEST.  | ROUTING PROTOCOL | ROUTING METHOD | ROUTING TREE | PAYLOAD | TOTAL = 250 bytes
| 1 char      | 4 char  | 4 char |     1 char       |  1 char        |     1 char   | 239 char|
0             1         5        9                  10               11             12
\_rx_str:          9gitlluca01002gitl123400003000000000000000.4060.00100,
packet_for_cache:  gitlluca0102gitl123400003000000000000000.4060.00100,
\9gitlluca01002gitl123400003000000000000000.4060.00100,
\*gitlluca01*02gitl123400003000000000000000.4060.00100,
\*gitlluca01*02gitl123440003001770000002
"""


class RoutingHeader:

    ROUTING_HEADER_START = 0
    ROUTING_HEADER_LENGTH = 12
    ROUTING_PAYLOAD_START = ROUTING_HEADER_START + ROUTING_HEADER_LENGTH

    HOP_COUNTER_LEN = 1
    HOP_COUNTER_INDEX = ROUTING_HEADER_START

    SOURCE_LEN = 4
    SOURCE_START = HOP_COUNTER_INDEX + HOP_COUNTER_LEN
    SOURCE_END = SOURCE_START + SOURCE_LEN
    
    DESTINATION_LEN = 4
    DESTINATION_START = SOURCE_END
    DESTINATION_END = DESTINATION_START + DESTINATION_LEN

    ROUTING_PROTOCOL_LEN = 1
    ROUTING_PROTOCOL_INDEX = DESTINATION_END

    ROUTING_METHOD_LEN = 1
    ROUTING_METHOD_INDEX = ROUTING_PROTOCOL_INDEX + ROUTING_PROTOCOL_LEN

    ROUTING_TREE_LEN = 1
    ROUTING_TREE_INDEX = ROUTING_METHOD_INDEX + ROUTING_METHOD_LEN

    ROUTING_PROTOCOL_NONE = 0
    ROUTING_PROTOCOL_PING = 1
    ROUTING_PROTOCOL_PING_ACK = 2
    ROUTING_PROTOCOL_PING_LOCALIZATION = 3
    ROUTING_PROTOCOL_PING_LOCALIZATION_ACK = 4
    ROUTING_PROTOCOL_PING_FOR_DESTINATION = 5
    ROUTING_PROTOCOL_PING_FOR_DESTINATION_ACK = 6
    ROUTING_PROTOCOL_WRAPPER_NEIGHBOR_LIST = 7
    
    ROUTING_PROTOCOLS = [
        ROUTING_PROTOCOL_NONE,
        ROUTING_PROTOCOL_PING,
        ROUTING_PROTOCOL_PING_ACK,
        ROUTING_PROTOCOL_PING_LOCALIZATION,
        ROUTING_PROTOCOL_PING_LOCALIZATION_ACK,
        ROUTING_PROTOCOL_PING_FOR_DESTINATION,
        ROUTING_PROTOCOL_PING_FOR_DESTINATION_ACK,
        ROUTING_PROTOCOL_WRAPPER_NEIGHBOR_LIST]
    
    ROUTING_METHOD_GOSSIP = 0
    ROUTING_METHOD_MULTI_TREE = 1
    
    ROUTING_METHODS = [ROUTING_METHOD_GOSSIP, ROUTING_METHOD_MULTI_TREE]

    ROUTING_TREE_GOSSIP = 0  # the RoutingTree value to put in Gossip Header
    
    def __init__(self, hop_counter, source, destination, routing_protocol, routing_method, routing_tree):
        self.hop_counter = hop_counter
        self.source = source
        self.destination = destination
        self.routing_protocol = routing_protocol
        self.routing_method = routing_method
        self.routing_tree = routing_tree

    def get_header_str(self):
        return '{:0>{hop_counter_len}}{:0>{source_len}}{:0>{destination_len}}{:0>{routing_protocol_len}}{:0>{routing_method_len}}{:0>{routing_tree_len}}'.format(
            self.hop_counter,
            self.source,
            self.destination,
            self.routing_protocol,
            self.routing_method,
            self.routing_tree,
            hop_counter_len=RoutingHeader.HOP_COUNTER_LEN,
            source_len=RoutingHeader.SOURCE_LEN,
            destination_len=RoutingHeader.DESTINATION_LEN,
            routing_protocol_len=RoutingHeader.ROUTING_PROTOCOL_LEN,
            routing_method_len=RoutingHeader.ROUTING_METHOD_LEN,
            routing_tree_len=RoutingHeader.ROUTING_TREE_LEN)

    def get_len(self):
        return self.ROUTING_HEADER_LENGTH

    def __str__(self):
        packet_str = '<{}> = '.format(self.__class__)
        for field in self.__dict__:
            packet_str += '{}:{},\n'.format(field, self.__dict__[field])
        return packet_str

    def __copy__(self):
        return RoutingHeader(self.hop_counter, self.source, self.destination, self.routing_protocol,
                             self. routing_method, self.routing_tree)

