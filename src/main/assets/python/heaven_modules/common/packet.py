import time
from heaven_modules.common.routing_header import RoutingHeader
from heaven_modules.common.transport_header import TransportHeader, TcpHeader, TcpSynHeader, UdpHeader


class PacketError(Exception):
    """
    Some calls on packets can cause this exception to be catch and managed
    """
    pass


class Packet:
    """
    RAW
    \----------------------------------------------------------------------------------------------------
    |      l3_header     |     l4_header   | tcp/udp_header | tcp_syn_header |      payload            |
    \----------------------------------------------------------------------------------------------------
    0                    l3_index          l4_index      tcp_index       tcp_syn_index
    l2_index
    """
    # entry points = point/reason of creation of the packet object
    RX_ON_SOCKET = 'rx socket'

    LSP_UNICAST = 'lsp unicast'
    LSP_BROADCAST = 'lsp broadcast'

    TCP_SYN = 'tcp syn'
    TCP_DATA_INIT = 'tcp data init'
    TCP_DATA = 'tcp data'
    TCP_FIN = 'tcp fin'
    TCP_FIN_ACK = 'tcp fin ack'
    TCP_SYN_ACK = 'tcp syn ack'
    TCP_DATA_INIT_ACK = 'tcp data init ack'
    TCP_DATA_ACK = 'tcp data ack'
    UDP_DATA = 'udp data'

    # exit points = point when the packet ends his processing
    RECEIVED_TCP_SYN = 'received tcp syn'
    RECEIVED_TCP_DATA_INIT = 'received tcp data init'
    RECEIVED_TCP_DATA = 'received tcp data'
    RECEIVED_TCP_FIN = 'received tcp fin'
    RECEIVED_TCP_SYN_ACK = 'received tcp syn ack'
    RECEIVED_TCP_DATA_INIT_ACK = 'received tcp data init ack'
    RECEIVED_TCP_DATA_ACK = 'received tcp data ack'
    WRONG_TCP_SESSION_DATA = 'wrong tcp session data'
    OLD_TCP_SESSION_DATA = 'old tcp session data'
    RECEIVED_UDP_DATA = 'received udp data'
    WRONG_UDP_SESSION_DATA = 'wrong udp session data'

    HOP_COUNTER_DROP = 'dropped hop counter'
    CACHE_HIT_DROP = 'dropped cache hit'
    ROUTING_DROP = 'routing drop'
    SOCKET_DROP = 'dropped on socket'
    QUEUE_DROP = 'dropped on queue'  # we put a leading space since exit point will be: queue_name + QUEUE_DROP
    RECEIVED_LSP_ERROR = 'received lsp error'
    RECEIVED_LSP = 'received lsp'
    RECEIVED_OLD_LSP = 'received old lsp'
    NO_ROUTE = 'no route'
    WRONG_ROUTING_HEADER = 'wrong routing header'

    SENT_ON_SOCKET = 'sent on socket'

    # indexes to store timestamps
    N_TS = 11
    RX_ON_SOCKET_I = 0
    RX_BY_L3_UP_I = 1
    PROC_BY_L3_UP_I = 2
    RX_BY_L4_UP_I = 3
    PROC_BY_L4_UP_I = 4

    CREATED_BY_L4_I = 5
    PROC_BY_L4_DOWN_I = 6
    RX_BY_L3_DOWN_I = 7
    PROC_BY_L3_DOWN_I = 8
    RX_BY_L2_DOWN_I = 9
    TX_ON_SOCKET_I = 10

    def __init__(self, rx_str='', previous_hop=None, is_stop_packet=False, entry_point=''):
        """
        :param rx_str: string as read from the network socket (if packet created by the link layer) or
        packet payload (if packet created by tcp/udp)
        :param previous_hop: if packet is read from a socket, it is the heaven address of the sender
        :param is_stop_packet: True/False : packet used to stop packet queue readers
        :param entry_point: point/reason of creation of the packet object (used by packet profiler)
        """
        self.rx_str = rx_str
        self.previous_hop = previous_hop
        self.is_stop_packet = is_stop_packet
        self.entry_point = entry_point

        self.id = '{:.6f}'.format(time.time())
        self.headers = []  # list of Headers objects in order e.g : [L3, L4, TCP, TCPSYN]
        self.has_routing_header = False
        self.has_syn_header = False  # the only packet with an extra header after TCP/UDP

        # values set when a new packet must be sent, decided by transport layers (routing requests)
        self.requested_destination = None
        self.requested_routing_protocol = None
        self.requested_routing_method = None
        self.requested_max_hop = None

        # values written by TCP client to manage retransmissions
        self.start_ts = None
        self.acked_ts = None
        self.timeout = 0
        
        # values used to profile the packet (time passed in each layer)
        self.profiler_ts = Packet.N_TS * [0.0]

    # --------------------- functions called to profile the packet -----------------------

    def set_rx_on_socket_ts(self):
        self.profiler_ts[Packet.RX_ON_SOCKET_I] = time.time()

    def set_rx_by_l3_up_ts(self):
        self.profiler_ts[Packet.RX_BY_L3_UP_I] = time.time()

    def set_proc_by_l3_up_ts(self):
        self.profiler_ts[Packet.PROC_BY_L3_UP_I] = time.time()

    def set_rx_by_l4_up_ts(self):
        self.profiler_ts[Packet.RX_BY_L4_UP_I] = time.time()

    def set_proc_by_l4_up_ts(self):
        self.profiler_ts[Packet.PROC_BY_L4_UP_I] = time.time()

    def set_created_by_l4_ts(self):
        self.profiler_ts[Packet.CREATED_BY_L4_I] = time.time()

    def set_proc_by_l4_down_ts(self):
        self.profiler_ts[Packet.PROC_BY_L4_DOWN_I] = time.time()

    def set_rx_by_l3_down_ts(self):
        self.profiler_ts[Packet.RX_BY_L3_DOWN_I] = time.time()

    def set_proc_by_l3_down_ts(self):
        self.profiler_ts[Packet.PROC_BY_L3_DOWN_I] = time.time()

    def set_rx_by_l2_down_ts(self):
        self.profiler_ts[Packet.RX_BY_L2_DOWN_I] = time.time()

    def set_tx_on_socket_ts(self):
        self.profiler_ts[Packet.TX_ON_SOCKET_I] = time.time()

    # --------------------- functions called to route the packet -----------------------

    def get_str_for_cache(self):
        """
        Exclude Hop Counter and Routing Tree from Routing Header. Take all the other Headers (L4, TCP)
        :return:
        """
        return self.rx_str[RoutingHeader.SOURCE_START:RoutingHeader.ROUTING_TREE_INDEX] \
               + self.rx_str[RoutingHeader.ROUTING_PAYLOAD_START:TcpSynHeader.SYN_HEADER_END]

    def get_tcp_header(self):
        if self.has_syn_header:
            return self.headers[-2]
        else:
            return self.headers[-1]

    # ------------------------------------ PARSING ROUTING LAYER --------------------------------------------
    
    # get methods extracts information directly from self.rx_str
    # todo exceptions management

    def get_hop_counter(self):
        try:
            return int(self.rx_str[RoutingHeader.HOP_COUNTER_INDEX])
        except ValueError:
            raise PacketError('Hop counter cannot be {}'.format(self.rx_str[RoutingHeader.HOP_COUNTER_INDEX]))

    def get_routing_source(self):
        return self.rx_str[RoutingHeader.SOURCE_START:RoutingHeader.SOURCE_END]

    def get_routing_destination(self):
        if self.previous_hop:
            return self.rx_str[RoutingHeader.DESTINATION_START:RoutingHeader.DESTINATION_END]
        else:
            return self.requested_destination

    def get_routing_protocol(self):
        try:
            return int(self.rx_str[RoutingHeader.ROUTING_PROTOCOL_INDEX])
        except ValueError:
            raise PacketError('Routing protocol cannot be {}'.format(self.rx_str[RoutingHeader.ROUTING_PROTOCOL_INDEX]))

    def get_routing_method(self):
        try:
            return int(self.rx_str[RoutingHeader.ROUTING_METHOD_INDEX])
        except ValueError:
            raise PacketError('Routing method cannot be {}'.format(self.rx_str[RoutingHeader.ROUTING_METHOD_INDEX]))

    def get_routing_tree(self):
        if self.previous_hop:
            try:
                return int(self.rx_str[RoutingHeader.ROUTING_TREE_INDEX])
            except ValueError:
                raise PacketError(
                    'Routing tree cannot be {}'.format(self.rx_str[RoutingHeader.ROUTING_TREE_INDEX]))
        else:
            try:
                return int(self.headers[0].routing_tree)
            except ValueError:
                raise PacketError(
                    'Routing tree cannot be {}'.format(self.headers[0].routing_tree))

    def get_routing_payload(self):
        return self.rx_str[RoutingHeader.ROUTING_PAYLOAD_START:]

    # ------------------------------------ PARSING TRANSPORT HEADER --------------------------------------------

    def get_transport_protocol(self):
        value = self.rx_str[TransportHeader.TRANSPORT_PROTOCOL_START:TransportHeader.TRANSPORT_PROTOCOL_END]

        if len(value) != TransportHeader.TRANSPORT_PROTOCOL_LEN:
            raise PacketError('Transport protocol field has wrong length')

        try:
            return int(value)
        except ValueError:
            raise PacketError('Transport protocol cannot be {}'.format(value))

    def get_transport_source(self):
        return self.rx_str[TransportHeader.TRANSPORT_SOURCE_START:TransportHeader.TRANSPORT_SOURCE_END]

    def get_transport_payload(self):
        return self.rx_str[TransportHeader.TRANSPORT_PAYLOAD_START:]

    # ------------------------------------ PARSING TCP HEADER --------------------------------------------

    def get_tcp_server_port(self):

        value = self.rx_str[TcpHeader.SERVER_PORT_START: TcpHeader.SERVER_PORT_END]

        if len(value) != TcpHeader.SERVER_PORT_LEN:
            raise PacketError('Tcp port field has wrong length')

        try:
            return int(value)
        except ValueError:
            raise PacketError('Tcp server port cannot be {}'.format(value))

    def get_tcp_type(self):
        try:
            return int(self.rx_str[TcpHeader.TCP_TYPE_INDEX])
        except ValueError:
            raise PacketError('Tcp type cannot be {}'.format(self.rx_str[TcpHeader.TCP_TYPE_INDEX]))

    def get_tcp_session_id(self):
        return self.rx_str[TcpHeader.SESSION_ID_START:TcpHeader.SESSION_ID_END]

    def get_tcp_seq_n(self):
        value = self.rx_str[TcpHeader.SEQ_N_START:TcpHeader.SEQ_N_END]

        if len(value) != TcpHeader.SEQ_N_LEN:
            raise PacketError('Tcp seq_n field has wrong length')

        try:
            return int(value)
        except ValueError:
            raise PacketError('Tcp seq_n cannot be {}'.format(value))

    def get_tcp_retransm_n(self):

        value = self.rx_str[TcpHeader.RETRANSM_N_START:TcpHeader.RETRANSM_N_END]

        if len(value) != TcpHeader.RETRANSM_N_LEN:
            raise PacketError('Tcp retransm_n field has wrong length')

        try:
            return int(value)
        except ValueError:
            raise PacketError('Tcp retransm_n cannot be {}'.format(value))

    def get_tcp_mex_id(self):
        return self.rx_str[TcpHeader.MEX_ID_START:TcpHeader.MEX_ID_END]

    def get_tcp_payload(self):
        return self.rx_str[TcpHeader.TCP_PAYLOAD_START:]

    def is_tcp_ack_type(self):
        return self.get_tcp_type() in TcpHeader.TCP_ACK_TYPES

    def is_tcp_non_ack_type(self):
        return self.get_tcp_type() in TcpHeader.TCP_NON_ACK_TYPES

    # ------------------------------------ PARSING TCP SYN HEADER --------------------------------------------

    def get_tcp_retransm_timeout(self):

        value = self.rx_str[TcpSynHeader.RETRANSM_TIMEOUT_START:TcpSynHeader.RETRANSM_TIMEOUT_END]

        if len(value) != TcpSynHeader.RETRANSM_TIMEOUT_LEN:
            raise PacketError('TcpSyn retransm_timeout field has wrong length')
        try:
            return float(value)
        except ValueError:
            raise PacketError('TcpSyn retransm_timeout cannot be {}'.format(value))

    def get_tcp_null_timeout(self):

        value = self.rx_str[TcpSynHeader.NULL_TIMEOUT_START:TcpSynHeader.NULL_TIMEOUT_END]

        if len(value) != TcpSynHeader.NULL_TIMEOUT_LEN:
            raise PacketError('TcpSyn null_timeout field has wrong length')
        try:
            return float(value)
        except ValueError:
            raise PacketError('TcpSyn null_timeout cannot be {}'.format(value))

    def get_tcp_max_retransm(self):

        value = self.rx_str[TcpSynHeader.MAX_RETRANSM_START:TcpSynHeader.MAX_RETRANSM_END]

        if len(value) != TcpSynHeader.MAX_RETRANSM_LEN:
            raise PacketError('TcpSyn max_retransm field has wrong length')
        try:
            return int(value)
        except ValueError:
            raise PacketError('TcpSyn max_retransm cannot be {}'.format(value))

    def get_tcp_ack_strategy(self):

        try:
            return int(self.rx_str[TcpSynHeader.ACK_STRATEGY_INDEX])
        except ValueError:
            raise PacketError('TcpSyn ack_strategy cannot be {}'.format(self.rx_str[TcpSynHeader.ACK_STRATEGY_INDEX]))

    # ------------------------------------ PARSING UDP HEADER --------------------------------------------

    def get_udp_destination_port(self):
        return int(self.rx_str[UdpHeader.UDP_DESTINATION_PORT_START:UdpHeader.UDP_DESTINATION_PORT_END])

    def get_udp_source_port(self):
        return int(self.rx_str[UdpHeader.UDP_SOURCE_PORT_START:UdpHeader.UDP_SOURCE_PORT_END])

    def get_udp_timestamp(self):
        return float(self.rx_str[UdpHeader.UDP_TIMESTAMP_START:UdpHeader.UDP_TIMESTAMP_END])

    def get_udp_payload(self):
        return self.rx_str[UdpHeader.UDP_PAYLOAD_START:]

    # ------------------------------------ WRITING HEADERS --------------------------------------------

    def decrease_hop_counter(self):
        self.rx_str = ''.join((self.rx_str[:RoutingHeader.HOP_COUNTER_INDEX],
                               str(self.get_hop_counter() - 1),
                               self.rx_str[RoutingHeader.HOP_COUNTER_INDEX + 1:]))

    def add_routing_header(self, *args):
        if not self.has_routing_header:
            self.has_routing_header = True
            self.headers.insert(0, RoutingHeader(*args))

    def remove_routing_header(self):
        if self.has_routing_header:
            self.has_routing_header = False
            self.headers.pop(0)

    def add_transport_header(self, *args):
        self.headers.insert(0, TransportHeader(*args))

    def add_tcp_header(self, *args):
        self.headers.insert(0, TcpHeader(*args))

    def add_tcp_syn_header(self, *args):
        self.has_syn_header = True
        self.headers.insert(0, TcpSynHeader(*args))

    def add_udp_header(self, *args):
        self.headers.insert(0, UdpHeader(*args))

    def packet_to_str(self):
        """
        Called by the link layer just before sending the packet
        :return:
        """
        return ''.join(header.get_header_str() for header in self.headers) + self.rx_str

    def get_len(self):
        """
        Called to profile the packet
        :return:
        """
        return sum(header.get_len() for header in self.headers) + len(self.rx_str)

    def __str__(self):
        packet_str = '<{} = '.format(self.__class__)
        try:
            for field in self.__dict__.keys():
                packet_str += '{}:{},\n'.format(field, str(self.__dict__[field])[:100])
        except RuntimeError:
            return ''
        return packet_str + '>'

    def __copy__(self):
        """
        Used by MultiTree to send a copy of the packet on each routing tree
        :return:
        """
        # keep fields written by layers > 3
        new_packet = Packet(rx_str=self.rx_str)
        new_packet.headers = [header.__copy__() for header in self.headers]
        new_packet.has_syn_header = self.has_syn_header
        new_packet.has_routing_header = self.has_routing_header

        # keep the routing needs of the packet
        new_packet.requested_destination = self.requested_destination
        new_packet.requested_routing_protocol = self.requested_routing_protocol
        new_packet.requested_routing_method = self.requested_routing_method
        new_packet.requested_max_hop = self.requested_max_hop
        
        # keep profiling timestamps
        new_packet.entry_point = self.entry_point
        new_packet.profiler_ts = self.profiler_ts[:]

        return new_packet
