import ConfigParser as cp
import logging
import six


class ConfigReader:
    """
    Configuration file parser
    """
    def __init__(self, configuration_file_path):
        """
        :param configuration_file_path: The absolute path of the configuration file.
        :raise IOError if the configuration file is not found.
        """
        self.configuration_file_path = configuration_file_path

        # load the configuration into the ConfigParser object
        if six.PY2:
            self.config = cp.RawConfigParser(allow_no_value=True)
        else:
            self.config = cp.ConfigParser()

        # load the file into self.config
        self.config.read(configuration_file_path)
        # now self.config has configurations already loaded in memory
        # Note: everything is read as a string.

    def get_config_dict(self, section):
        """
        Get a dict {section: value}

        :param section: section of the config file (written in squared brackets in the file)
        :return: the dict {key: value} of the specified section or {} if not found
        """
        try:
            my_dict = {a[0]: a[1] for a in self.config.items(section)}
            return my_dict
        except cp.NoSectionError:
            logging.getLogger(__name__).exception('Configuration section [{}] not found!'.format(section))
            return {}

    def get(self, section, key):
        """
        Get a single value

        :param section: section in the configuration file
        :param key: key in the section
        :return: the value found
        :raise: ConfigParser.NoSectionError or ConfigParser.NoOptionError
        """
        return self.config.get(section, key)
