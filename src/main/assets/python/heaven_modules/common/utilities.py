import socket
import six


class IdGenerator:

    def __init__(self, id_len, id_min, id_max):
        """
        :param id_len: number of digits of the id. If id_len is passed, id will be in range(10**id_len)
        :param id_min: minimum id number. If id_len is None, they will be in range [id_min, id_max]
        :param id_max: maximum id number
        """
        if id_len is not None:
            self.id_len = id_len
            self.id_min = 0
            self.id_max = 10**id_len - 1
        else:
            self.id_len = len(str(id_max))
            self.id_min = id_min
            self.id_max = id_max

        self.first_free_id = self.id_min
        self.used_ids = set()

    def get_id(self):
        new_id = None
        while new_id is None or new_id in self.used_ids:
            new_id = self.first_free_id
            self.first_free_id += 1

            # if necessary, reset the counter
            if self.first_free_id > self.id_max:
                self.first_free_id = self.id_min

        self.used_ids.add(new_id)
        return self.num_to_str(new_id)

    def del_id(self, num_str):
        try:
            self.used_ids.remove(int(num_str))
        except ValueError:
            pass

    def num_to_str(self, num):
        """
        Fill the id with leading zeros so that length equals id_len
        :param num:
        :return:
        """
        return str(num).rjust(self.id_len, '0')


def get_udp_socket(destination, tx_timeout, socket_family=socket.AF_INET):
    """
    Create a UDP socket to be used with .send()
    :param socket_family:
    :param destination: a tuple (ip, port)
    :param tx_timeout: blocking timeout. if zero, the socket is not blocking
    :return: socket object
    ??? sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    """
    sock = socket.socket(socket_family, socket.SOCK_DGRAM)
    sock.settimeout(tx_timeout)
    sock.connect(destination)
    return sock


def get_broadcast_udp_socket(destination, tx_timeout):
    """
    Create a UDP socket to be used with .send()
    :param destination: a tuple (ip, port)
    :param tx_timeout: blocking timeout. if zero, the socket is not blocking
    :return: socket object

    # AF_INET = use addresses in format (ipv4, port)
    # SOCK_DGRAM is for UDP
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # SOL_SOCKET = this socket object only
    # SO_BROADCAST = set the value for broadcast to 1
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    # OLD set a TX timeout
    # OLD sock.settimeout(self.neighboring_requests_timeout)
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.settimeout(tx_timeout)
    sock.connect(destination)
    return sock


def num_to_str(num_int, n_chars):
    return str(num_int).zfill(n_chars)


# noinspection PyArgumentList
def send_tcp_msg(destination, data_to_send, socket_family=socket.AF_INET, tx_timeout=5):
    # print 'sending {} to {}'.format(data_to_send, destination)
    sock = None
    data_len = len(data_to_send)

    if six.PY3:
        data_to_send = bytes(data_to_send, 'utf8')

    try:
        sock = socket.socket(socket_family, socket.SOCK_STREAM)
        sock.settimeout(tx_timeout)
        # Connect to server and send data
        sock.connect(destination)
        # print('Connected')

        # send message
        bytes_sent = 0
        while bytes_sent != data_len:
            sent = sock.send(data_to_send[bytes_sent:])
            if sent == 0:
                # print('Cannot send')
                sock.close()
                return False
            bytes_sent += sent

        # print('Sent {} Bytes'.format(data_len))

    except (socket.error, socket.timeout) as e:
        print('{} - cannot send {}[...] to {}'.format(e, data_to_send[:100], destination))
        if sock is not None:
            sock.close()
        return False
    else:
        if sock is not None:
            sock.close()
        return True


# noinspection PyArgumentList
def send_udp_msg(destination, data_to_send, socket_family=socket.AF_INET, tx_timeout=5):
    # print 'sending {} to {}'.format(data_to_send, destination)
    if six.PY3:
        data_to_send = bytes(data_to_send, 'utf8')

    sock = None
    try:
        sock = socket.socket(socket_family, socket.SOCK_DGRAM)
        sock.settimeout(tx_timeout)
        sock.sendto(data_to_send, destination)
    except (socket.error, socket.timeout) as e:
        print('{} - cannot send {}[...] to {}'.format(e, data_to_send[:100], destination))
        if sock is not None:
            sock.close()
        return False
    else:
        if sock is not None:
            sock.close()
        return True


def get_socket_from_strings(socket_type_str, socket_family_str, socket_address_str):

    sock_type_dict = {'tcp': socket.SOCK_STREAM, 'udp': socket.SOCK_DGRAM}
    try:
        socket_type = sock_type_dict[socket_type_str]
    except KeyError:
        return None, None, None, 'Api socket type not valid'

    try:
        socket_family_dict = {'af_inet': socket.AF_INET, 'af_unix': socket.AF_UNIX}
    except AttributeError:
        socket_family_dict = {'af_inet': socket.AF_INET}

    try:
        socket_family = socket_family_dict[socket_family_str]
    except KeyError:
        return None, None, None, 'Socket family {} not supported, cannot start Heaven'.format(socket_family_str)

    if socket_family == socket.AF_INET:
        ip, port = socket_address_str.split(':')
        try:
            socket_address = (ip, int(port))
        except(ValueError, TypeError):
            return None, None, None, '{} is not in the format IP:PORT, cannot start Heaven'.format(socket_address_str)
    else:
        socket_address = socket_address_str
        # todo check validity

    return socket_type, socket_family, socket_address, ''
