import os
import time
import socket
import Queue
import logging
from threading import Thread
from tail_drop_queue import TailDropQueue
import six
import SocketServer


class QueueServer(Thread):
    """
    Read elements from a queue and call a handler function using that element as parameter
    Use Case: Call the management function for received packets
    """

    PACKET_COMMAND = 0
    STOP_COMMAND = 1

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue
        self.do_run = False

    def run(self):
        self.do_run = True
        while self.do_run:
            command, x = self.queue.get()

            if command == self.STOP_COMMAND:
                self.queue.task_done()
                break

            self.handle(x)
            self.queue.task_done()

        self.cleanup()

    def put(self, x):
        return self.queue.put((self.PACKET_COMMAND, x))

    def stop(self):
        self.do_run = False
        return self.queue.put((self.STOP_COMMAND, None))

    def handle(self, x):
        pass

    def cleanup(self):
        pass


class EventsSerializer(Thread):
    """
    Put callback functions (with params) in a multi priority queue and call them in sequence.
    Use Case: Receiving packets (ack), events (timeouts) and commands (stop) for tcp client
    """

    def __init__(self, queue=Queue.Queue()):
        Thread.__init__(self)
        self.events_queue = queue
        self.do_run = True

    def run(self):
        self.do_run = True
        while self.do_run:

            priority, do_stop, callback, params = self.events_queue.get()

            if do_stop:
                self.events_queue.task_done()
                break

            if params is None:
                callback()
            else:
                callback(params)

            if priority is not None:
                self.events_queue.task_done(priority)
            else:
                self.events_queue.task_done()

    def stop(self):
        self.do_run = False
        self.events_queue.put((0, 1, None, None))

    def enqueue(self, priority, callback, params=None):
        return self.events_queue.put((priority, 0, callback, params))


# ---------------- NEW SERVER WITH QUEUE TO SERIALIZE REPLIES ---------------- #

class HeavenTcpSocketServerHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        # self.data = self.request.recv(1024)
        data = ''
        while 1:
            new_data = self.request.recv(1024)
            if not new_data:
                break
            else:
                data += new_data

        self.server.queue_server.put((data, self.client_address))


class HeavenTcpSocketServerWrapper(SocketServer.TCPServer):

    def __init__(self, address, handler, queue_server):
        self.queue_server = queue_server
        SocketServer.TCPServer.__init__(self, address, handler)


try:
    class HeavenTcpSocketServerWrapperUnix(SocketServer.UnixStreamServer):

        def __init__(self, address, handler, queue_server):
            self.queue_server = queue_server
            SocketServer.UnixStreamServer.__init__(self, address, handler)


    class HeavenUdpSocketServerWrapperUnix(SocketServer.UnixDatagramServer):

        def __init__(self, address, handler, queue_server):
            self.queue_server = queue_server
            SocketServer.UnixDatagramServer.__init__(self, address, handler)

except AttributeError:
    pass


class HeavenUdpSocketServerHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        self.server.queue_server.put((self.request[0], self.client_address))

class HeavenUdpSocketServerWrapper(SocketServer.UDPServer):

    def __init__(self, address, handler, queue_server):
        self.queue_server = queue_server
        SocketServer.UDPServer.__init__(self, address, handler)

class HeavenSocketServer(QueueServer):

    def __init__(self, address, buffer_size, queue_size, 
                 socket_family=socket.AF_INET, socket_type=socket.SOCK_STREAM, name=''):
        self.address = address
        self.buffer_size = buffer_size
        self.queue_size = queue_size
        self.socket_family = socket_family
        self.socket_type = socket_type
        QueueServer.__init__(self, Queue.Queue())
        self.name = name  # must be assigned after initializing the thread
        
        if socket_type == socket.SOCK_STREAM:
            if socket_family == socket.AF_INET:
                self.sock_server = HeavenTcpSocketServerWrapper(self.address, HeavenTcpSocketServerHandler, self)
            else:
                # Make sure the socket does not already exist
                try:
                    os.unlink(self.address)
                except OSError:
                    pass
                self.sock_server = HeavenTcpSocketServerWrapperUnix(self.address, HeavenTcpSocketServerHandler, self)
                
        elif socket_type == socket.SOCK_DGRAM:
            
            if socket_family == socket.AF_INET:
                self.sock_server = HeavenUdpSocketServerWrapper(self.address, HeavenUdpSocketServerHandler, self)
            else:
                try:
                    os.unlink(self.address)
                except OSError:
                    pass
                self.sock_server = HeavenUdpSocketServerWrapperUnix(self.address, HeavenUdpSocketServerHandler, self)
            
        self.server_thread = Thread(target=self.sock_server.serve_forever)
        self.server_thread.start()

        if socket_type == socket.SOCK_STREAM:
            print('TCP server {} - {}'.format(self.address, self.name))
        elif socket_type == socket.SOCK_DGRAM:
            print('UDP server {} - {}'.format(self.address, self.name))

    def stop(self):
        QueueServer.stop(self)
        self.sock_server.server_close()
        self.sock_server.shutdown()

        if self.socket_family != socket.AF_INET:
            try:
                os.remove(self.address)
            except OSError:
                pass


class PeriodicalCaller(Thread):
    """
    Call a function evey X seconds (no params)
    The sleep time auto-adjusts itself based on self.action duration
    """

    def __init__(self, action, interval):
        Thread.__init__(self)
        self.action = action
        self.interval = interval
        self.reference_ts = None
        self.do_run = False

    def run(self):
        self.do_run = True
        self.reference_ts = time.time()
        while self.do_run:
            self.action()
            time.sleep(self.interval)

    def stop(self):
        self.do_run = False
