import Queue


class TailDropQueue:
    """
    A queue of limited size with Tail Drop policy
    """
    DEFAULT_MAXSIZE = 10

    def __init__(self, maxsize=DEFAULT_MAXSIZE):

        self.queue = Queue.Queue(maxsize=maxsize)

    def put(self, packet):
        try:
            self.queue.put(packet, block=False)
            return True
        except Queue.Full:
            return False

    def get(self):
        return self.queue.get()

    def qsize(self):
        return self.queue.qsize()

    def task_done(self):
        self.queue.task_done()


class PriorityTailDropQueue:
    """
    A queue of limited size with Tail Drop policy
    The queue has many priorities (class). For each class, you can have the specified maximum number of packets
    """
    DEFAULT_MAXSIZE = 10
    DEFAULT_N_QUEUES = 1

    def __init__(self, maxsize=DEFAULT_MAXSIZE, n_queues=DEFAULT_N_QUEUES):
        self.maxsize = maxsize
        self.queue = Queue.PriorityQueue()
        self.counters = [0] * n_queues

    def put(self, x):
        # x = (priority, other, ....)
        if self.counters[x[0]] < self.maxsize:
            self.queue.put(x)
            self.counters[x[0]] += 1
            return True
        else:
            return False

    def get(self):
        return self.queue.get()

    def qsize(self):
        return self.queue.qsize()

    def task_done(self, priority):
        self.queue.task_done()
        self.counters[priority] -= 1

