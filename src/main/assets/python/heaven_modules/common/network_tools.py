import ipaddress
import re
import subprocess
import time
import os
import urllib2
import socket
import six


class NetworkTools(object):

    OS_LINUX = 'linux'
    OS_ANDROID = 'android'
    TEST_IP = '216.58.192.142'

    def __init__(self, config_reader=None):
        self.config_reader = config_reader
        if self.config_reader:
            main_c = config_reader.get_config_dict('main')
            self.os = main_c['os']
        else:
            self.os = NetworkTools.OS_LINUX

    # ---------------------------- Execute commands ------------------------------- #
    @staticmethod
    def run(command):
        """
        Run a command and return the resulting string
        :param command:
        :return:
        """
        # command.split(' ') fails with commands with spaces in params
        # If shlex fails:
        # https://stackoverflow.com/questions/79968/split-a-string-by-spaces-preserving-quoted-substrings-in-python
        try:
            p = subprocess.Popen(command.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
        except OSError as e:
            print('{} while running {}'.format(e, command))
            return ''

        if six.PY2:
            return out
        else:
            return out.decode('utf8')

    @staticmethod
    def run_shell(command):
        """
        Run a command and return the resulting string.
        To be used when the command contains parameters that are string with spaces
        :param command:
        :return:
        """
        try:
            p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
            out, err = p.communicate()
        except OSError as e:
            print(e)
            return ''

        if six.PY2:
            return out
        else:
            return out.decode('utf8')

    @staticmethod
    def run_bg(command):
        with open(os.devnull, 'w') as DEVNULL:
            try:
                subprocess.Popen(command.split(' '), stdout=DEVNULL, stderr=DEVNULL)
            except OSError as e:
                print(e)

    @staticmethod
    def run_os(command):
        try:
            os.system(command)
        except OSError as e:
            print(e)

    # ---------------------------- Others ----------------------------------------- #
    @staticmethod
    def internet_on():
        try:
            # try to get a page from google.com
            urllib2.urlopen('http://{}'.format(NetworkTools.TEST_IP), timeout=2)
            return True
        except (urllib2.URLError, socket.timeout):
            return False

    # ---------------------------- Subnetting ------------------------------------- #
    @staticmethod
    def is_valid_ip_address(ip_addr):
        """
        :param ip_addr: a string, e.g. '192.168.0.89'
        :return: True/False
        """
        try:
            ipaddress.ip_address(unicode(ip_addr))
        except ValueError:
            return False
        else:
            return True

    @staticmethod
    def ip_in_network(ip, network):
        """
        :param ip: e.g. '192.168.10.2'
        :param network: e.g. '192.168.10.0/24'
        :return: True/False
        """
        try:
            result = ipaddress.IPv4Address(unicode(ip)) in ipaddress.IPv4Network(unicode(network))
        except:
            result = False

        return result

    @staticmethod
    def get_network_gw(network_address):
        """
        :param network_address: e.g. '192.168.45.0/24'
        :return: 192.168.45.1
        """
        return str(ipaddress.ip_network(unicode(network_address))[1])

    # --------------------- Read network configuration ---------------------------- #

    @staticmethod
    def get_nic_addresses(nic):
        """
        return the ip address and network address used ny a nic
        :param nic: e.g. 'wlan0'
        :return: a list [ip_address, broadcast_address, network_address] or None if errors
        e.g. ['172.17.0.1', '0.0.0.0', u'172.17.0.0/16']

        wlp2s0    Link encap:Ethernet  HWaddr e0:94:67:b7:ba:51
              inet addr:192.168.2.181  Bcast:192.168.2.255  Mask:255.255.255.0

        """
        intf_data = NetworkTools.run('ip a show {}'.format(nic))
        time.sleep(1)

        try:
            # e.g. '192.168.10.12/24'
            full_ip_address = re.findall('inet\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2})', intf_data)[0]
            ip_address = full_ip_address.split('/')[0]
            bcast = re.findall('brd\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})', intf_data)[0]
            interface = ipaddress.IPv4Interface(unicode(full_ip_address))
            netw = str(interface.network)
            return ip_address, bcast, netw
        except:
            return None, None, None

    @staticmethod
    def get_ip_addr():
        """
        :return: [(intf, ip, bc, netw)]
        """
        ip_addr = NetworkTools.run('ip addr')
        ip_tuples = re.findall('^\d+:\s(\S+)(?:@\S+)?:.*\n.*\n\s+inet\s(\S+)\sbrd\s(\S+)', ip_addr, re.MULTILINE)
        # [(intf, ip, broadcast)] e.g. ('ra1', '192.168.10.36/24', '192.168.10.255')
        result = []
        for ip_t in ip_tuples:
            intf = ip_t[0]
            if intf.find('@') > 0:
                intf = intf[:intf.find('@')]
            ip_address = ip_t[1].split('/')[0]
            ip_intf = ipaddress.IPv4Interface(unicode(ip_t[1]))
            network_address = str(ip_intf.network)
            result.append((intf, ip_address, ip_t[2], network_address))
        return result

    @staticmethod
    def get_arp_tuples(intf=None):
        """
        Read the arp table
        :param intf: it is possible to filter on the interface. If None, results for all interfaces are returned
        :return: [(ip, mac, intf)] if intf is None, [(ip, mac)] if intf is specified
        """
        try:
            with open('/proc/net/arp', 'r') as f:
                arp_result = f.read()
        except IOError:
            return []

        try:
            if intf is None:
                return re.findall(r'\b([\d.]+)\s+.+([0-9a-fA-F:]{17}).+\s+(\S+)\n', arp_result)
            else:
                return re.findall(r'\b([\d.]+)\s+.+([0-9a-fA-F:]{17}).+%s' % intf, arp_result)
        except IndexError:
            return []

    def get_connected_ip_addresses(self, network):
        """
        Return the list of IP addresses reachable through a certain NIC
        :paran network: e.g. '192.168.10.0/24'
        :return: list of strings e.g. ['192.168.10.1', '192.168.10.2', ...]
        """
        if self.os == NetworkTools.OS_LINUX:
            arp_tuples = NetworkTools.get_arp_tuples()
            ip_network = ipaddress.ip_network(unicode(network))
            return [t[0] for t in arp_tuples if ipaddress.ip_address(unicode(t[0])) in ip_network]
        elif self.os == NetworkTools.OS_ANDROID:
            """
            Returns the gateway of the network
            :param network: e.g. '192.168.2.0/24'
            :return: e.g. ['192.168.2.1'] or [] if network is "wrong"
            """
            try:
                hosts = list(ipaddress.ip_network(unicode(network)).hosts())
                gateway = str(hosts[0])
            except (ValueError, IndexError):
                return []
            else:
                return [gateway]

    # --------------------- Read RSSI for adhoc neighboring ----------------------------- #
    # --------------------- mt7610u iwpriv methods -------------------------------------- #
    @staticmethod
    def get_ip_rssi_mt7610u(intf):
        """
        NOTE! BRING ALL MAC TO UPPER OR LOWER CASE
        :param intf:
        :return: dict {ip, rssi}
        Note; rssi is a string
        Note: if rssi is 0 there is a problem, use -100 instead
        """
        mac_rssi = NetworkTools.get_mac_rssi_mt7610u(intf)  # [(mac, rssi)]

        if not mac_rssi:
            return {}

        # convert to a dict, lower on macs and int on rssi
        mac_rssi_dict = {mac.lower(): int(rssi) for mac, rssi in mac_rssi}  # {mac: rssi}
        # set 0 rssi values to -100
        mac_rssi_dict_cleaned = {mac: rssi if rssi < 0 else -100 for mac, rssi in mac_rssi_dict.iteritems()}

        # get the ip to mac mapping
        ip_mac = NetworkTools.get_arp_tuples(intf=intf)  # [(ip, mac)]
        # convert to dict and lower macs
        ip_mac_dict = {ip: mac.lower() for ip, mac in ip_mac}  # {ip: mac}

        # create a dict ip: rssi only for entries that are in both dicts
        return {ip: mac_rssi_dict_cleaned[mac] for ip, mac in ip_mac_dict.iteritems() if mac in mac_rssi_dict_cleaned}

    @staticmethod
    def get_mac_rssi_mt7610u(intf):
        """
        ([0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2}:[0-9a-fA-F]{2})\s+\d+\s+\d+\s+(-\d{2})
        ([0-9a-fA-F:]{17})\s+\d+\s+\d+\s+(-\d{2})
        :param intf:
        :return: [(mac, rssi)]
        Note rssi is returned as string

        HT Operating Mode : 0

        MAC                AID BSS RSSI0  RSSI1  RSSI2  PhMd      BW    MCS   SGI   STBC
        C0:25:E9:13:77:1F  1   0   -78    0      -81    HTMIX     40M   0     0     0     0         , 39324, 100%

        50:3E:AA:75:5B:68  2   0   -77    0      -86    HTMIX     40M   1     0     0     0         , 8324, 100%

        7C:DD:90:D3:6C:6D  3   0   -67    0      -90    HTMIX     40M   5     0     0     0         , 10392, 100%

        50:3E:AA:37:AC:4F  4   0   -54    0      -89    HTMIX     40M   7     0     0     0         , 22542, 100%

        50:3E:AA:6A:61:64  5   0   -82    0      -86    HTMIX     40M   0     0     0     0         , 0, 0%

        50:3E:AA:71:B2:F5  6   0   -50    0      -92    HTMIX     40M   7     0     0     0         , 4141, 100%

        """
        iwpriv_result = NetworkTools.run('iwpriv {} adhocEntry'.format(intf))
        time.sleep(0.3)
        try:
            return re.findall('([0-9a-fA-F:]{17})\s+\d+\s+\d+\s+(-\d{2})', iwpriv_result)
        except IndexError:
            return []

    # --------------------- Read RSSI for adhoc neighboring ----------------------------- #
    # -------------------------------- iw station  -------------------------------------- #

    @staticmethod
    def get_ip_rssi_iw(intf):
        """
        NOTE! BRING ALL MAC TO UPPER OR LOWER CASE
        :param intf:
        :return: dict {ip, rssi}
        Note; rssi is a string
        """
        ip_mac = NetworkTools.get_arp_tuples(intf=intf)  # [(ip, mac)]
        mac_rssi = NetworkTools.get_mac_rssi_iw(intf)  # [(mac, rssi)]
        mac_ip_dict = {mac.lower(): ip for ip, mac in ip_mac}  # {mac: ip}
        mac_rssi_dict = {mac.lower(): rssi for mac, rssi in mac_rssi}  # {mac: rssi}
        return {ip: mac_rssi_dict[mac] for mac, ip in mac_ip_dict.iteritems() if mac in mac_rssi_dict}

    @staticmethod
    def get_mac_rssi_iw(intf):
        """
        NOTE! BRING ALL MAC TO UPPER OR LOWER CASE
        :param intf:
        :return: dict {mac, rssi}
        Note; rssi is a string
        """
        iw_result = NetworkTools.run('iw dev {} station dump'.format(intf))
        time.sleep(0.1)
        try:
            mac_list = re.findall('([\da-fA-F:]{17})', iw_result)
            rssi_list = re.findall('signal:\s+(-\d+)', iw_result)
            return zip(mac_list, rssi_list)
        except IndexError:
            return []

    # ---------------------------- Set routes ------------------------------------- #

    @staticmethod
    def enable_forwarding(source_network, dest_network):
        """
        if the routing rule is not listed, add it
        NOTE CHECK THAT sysctl net.ipv4.ip_forward = 1
        NOTE SET PERMANENTLY IN /etc/sysctl.conf
        :param source_network: e.g. '192.168.10.0/24'
        :param dest_network: e.g. '192.168.10.0/24'
        :return:
        """
        NetworkTools.run('sysctl -w net.ipv4.ip_forward=1')
        iptables = NetworkTools.run('iptables -L FORWARD')

        # if the rule is already in the iptables forwarding chain, exit
        result = re.findall(source_network + '\s+' + dest_network, iptables)
        if result:
            return

        # if the rule is missing, add it
        NetworkTools.run('iptables -I FORWARD -s {} -d {} -j ACCEPT'.format(source_network, dest_network))

    @staticmethod
    def enable_sys_forwarding():
        """
        if the routing rule is not listed, add it
        NOTE CHECK THAT sysctl net.ipv4.ip_forward = 1
        NOTE SET PERMANENTLY IN /etc/sysctl.conf
        :return:
        """
        NetworkTools.run('sysctl -w net.ipv4.ip_forward=1')
        NetworkTools.run('iptables -I FORWARD -j ACCEPT')

    # todo you could parse /proc/net/route to be faster and independent
    @staticmethod
    def get_next_network(network_address):
        """
        :param network_address: e.g. u'192.168.10.0/24'
        :return: e.g. '192.168.11.0/24'
        """
        new_net = unicode(ipaddress.ip_network(unicode(network_address))[255] + 1)
        mask = unicode(ipaddress.ip_network(unicode(network_address)).netmask)
        return str(ipaddress.ip_network((new_net, mask)))

    @staticmethod
    def get_routed_address(ip_addr):
        """
        :param ip_addr: e.g. '192.168.10.14'
        :return: e.g. '192.168.11.14'
        """
        return str(ipaddress.ip_address(unicode(ip_addr)) + 256)

    @staticmethod
    def create_virtual_interface(intf, ip_addr):
        """
        Creates wlan0:0 with the specified address

        :param intf: e.g. 'wlan0'
        :param ip_addr:  e.g. '192.168.11.12'
        :return:
        """
        veth = '{}:0'.format(intf)
        NetworkTools.run('ifconfig {} {}'.format(veth, ip_addr))
        return veth

    @staticmethod
    def stop_virtual_interface(intf):
        """
        :param intf: e.g. 'wlan0:0'
        :return:
        """
        NetworkTools.run('ifconfig {} down'.format(intf))

    # ------------------------------- uci  ------------------------------------- #

    @staticmethod
    def uci_get(path):
        """
        e.g. uci show network.wlan.ipaddr
        ---> dhcp.cfg01411c.server='8.8.8.8' '8.8.4.4'
        :param path:
        :return:
        """
        uci1 = NetworkTools.run('uci show {}'.format(path))

        if not uci1:
            return ''

        try:
            uci_res1 = uci1.strip().split('=')[1].split("'")
        except IndexError:
            return ''

        return ' '.join(i.strip() for i in uci_res1 if i.strip())

    @staticmethod
    def uci_set(path, value):
        NetworkTools.run('uci set {}={}'.format(path, value))

    @staticmethod
    def uci_commit():
        NetworkTools.run('uci commit')

    @staticmethod
    def replace_dns_server(server_address, filepath):
        """
        list server 'whatever' --> list server 'server_address'
        :param filepath: file of the dhcp file path, '/etc/config/dhcp' on openwrt
        :param server_address: (str) ip addresses separated by space e.g. '8.8.8.8 4.4.4.4'
        :return:
        """
        if not os.path.isfile(filepath):
            return None

        with open(filepath, 'r') as f:
            lines = f.readlines()

        new_file_str = ''

        for line in lines:

            # add servers after this line
            if 'option localservice' in line:
                new_file_str += line
                for server in server_address.split(' '):
                    new_file_str += "\tlist server '{}'\n".format(server)
                continue

            # do not consider old servers
            if 'list server' in line or 'option server' in line:
                continue

            new_file_str += line

        with open(filepath, 'w') as f:
            f.write(new_file_str)

    @staticmethod
    def restart_dns_service():
        service_path = '/etc/init.d/dnsmasq'
        if not os.path.isfile(service_path):
            return None
        NetworkTools.run_bg('{} restart'.format(service_path))

    @staticmethod
    def get_ap_clients(intf):
        """
        Return the
        :return: [(ip, mac, status)] of connected and active devices
        """
        ip_neigh = NetworkTools.run('ip neigh show dev {}'.format(intf))
        parsed = re.findall('^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s.+([0-9a-fA-F:]{17}).*\s(\w+)$',
                            ip_neigh, re.MULTILINE)
        return parsed

    @staticmethod
    def get_ip_route():
        """
        Returns all the destination ip addresses belonging to the specified network
        :return: list of tuples (dest, gw)
        root@Omega-BD9B:~# ip route
        default via 192.168.0.1 dev eth0  src 192.168.0.103
        192.168.0.0/24 dev eth0  src 192.168.0.103
        192.168.0.1 dev eth0  src 192.168.0.103
        192.168.10.0/24 dev ra1  src 192.168.10.103
        192.168.11.0/24 dev ra1  src 192.168.11.103
        192.168.11.25 via 192.168.11.25 dev ra1
        192.168.11.41 via 192.168.11.41 dev ra1
        192.168.25.0/24 via 192.168.11.25 dev ra1
        192.168.41.0/24 via 192.168.11.41 dev ra1

        '^(default|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/?\d{0,2}).*via\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        """
        ip_route = NetworkTools.run('ip route')
        return re.findall(
            '^(default|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/?\d{0,2}).*via\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})',
            ip_route, re.MULTILINE)

    @staticmethod
    def ip_route_change(dest, new_gw):
        NetworkTools.run('ip route change {} via {}'.format(dest, new_gw))

    @staticmethod
    def ip_route_add(dest, new_gw):
        NetworkTools.run('ip route add {} via {}'.format(dest, new_gw))

    @staticmethod
    def ip_route_del(dest, new_gw):
        NetworkTools.run('ip route del {} via {}'.format(dest, new_gw))

    @staticmethod
    def ip_route_flush_cache():
        NetworkTools.run('ip route flush cache')

    @staticmethod
    def read_ethers():
        """
        Returns all the destination ip addresses belonging to the specified network
        :return: {mac, ip}
        xx:yy:zz:pp:dd:aa    192.168.254.200
        aa:zz:dd:ll:mm:gg    192.168.254.100
        """
        file_str = ''
        with open('/etc/ethers', 'r') as f:
            f.read()

        lines = file_str.split('\n')
        mac_ip_tuples = (line.split(' ') for line in lines)
        return {t[0]: t[1] for t in mac_ip_tuples}

    @staticmethod
    def write_ethers(mac_ip_map):
        lines = ['{} {}\n'.format(k, v) for k, v in mac_ip_map.iteritems()]
        with open('/etc/ethers', 'w+') as f:
            f.writelines(lines)

    @staticmethod
    def ping_node(ip_address, timeout=1, intf=None):
        """
        Test the connectivity to a node. Timeout for reply 2 seconds
        :param ip_address: e.g. '192.168.0.3'
        :param timeout:
        :param intf:
        :return:
        """
        ping_str = 'ping -c 1 {} -W {}'.format(ip_address, timeout)
        
        if intf is not None:
            ping_str = '{} -I {}'.format(ping_str, intf)

        p = subprocess.Popen(ping_str.split(' '), stdout=subprocess.PIPE)
        p.wait()
        if p.poll():
            return False
        else:
            return True

    @staticmethod
    def delete_ip_neigh(ip_address, intf):
        NetworkTools.run('ip neigh del {} dev {}'.format(ip_address, intf))

    @staticmethod
    def flush_iptable(table_name):
        NetworkTools.run('iptables -t {} -F'.format(table_name))

    @staticmethod
    def snat_ip_address(intf, ip_address_1, ip_address_2):
        """
        Set the rule to snat outgoing traffic from an interface
        :param intf:
        :param ip_address_1: source ip address written in the packet (e.g. 3.x)
        :param ip_address_2: source ip address to write in the packet (e.g. 4.x)
        :return:
        """
        NetworkTools.run('iptables -t nat -I POSTROUTING -s {} -j SNAT --to {}'.format(
            ip_address_1, ip_address_2))

    @staticmethod
    def snat_ip_address_del(intf, ip_address_1, ip_address_2):
        """
        Set the rule to snat outgoing traffic from an interface
        :param intf:
        :param ip_address_1: source ip address written in the packet (e.g. 3.x)
        :param ip_address_2: source ip address to write in the packet (e.g. 4.x)
        :return:
        """
        NetworkTools.run('iptables -t nat -D POSTROUTING -s {} -j SNAT --to {}'.format(
            ip_address_1, ip_address_2))

    @staticmethod
    def get_snat_rules():
        """
        root@Omega-AE03:~# iptables -t nat -L POSTROUTING -n
        Chain POSTROUTING (policy ACCEPT)
        target     prot opt source               destination
        SNAT       all  --  192.168.3.120        0.0.0.0/0            to:192.168.4.120
        SNAT       all  --  192.168.3.181        0.0.0.0/0            to:192.168.4.181
        SNAT       all  --  192.168.3.120        0.0.0.0/0            to:192.168.4.120
        SNAT       all  --  192.168.3.181        0.0.0.0/0            to:192.168.4.181
        SNAT       all  --  192.168.3.181        0.0.0.0/0            to:192.168.4.181
        SNAT       all  --  192.168.3.181        0.0.0.0/0            to:192.168.4.181
        SNAT       all  --  192.168.3.181        0.0.0.0/0            to:192.168.4.181
        SNAT       all  --  192.168.3.181        0.0.0.0/0            to:192.168.4.181
        postrouting_rule  all  --  0.0.0.0/0            0.0.0.0/0
        zone_lan_postrouting  all  --  0.0.0.0/0            0.0.0.0/0
        zone_wan_postrouting  all  --  0.0.0.0/0            0.0.0.0/0

        """
        ip_tables = NetworkTools.run('iptables -t nat -L POSTROUTING -n')
        return re.findall(
            '^SNAT.+\s(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d{1,2})?)\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d{1,2})?\s+to:(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d{1,2})?)$',
            ip_tables, re.MULTILINE)

    @staticmethod
    def dnat_ip_address(intf, ip_address_1, ip_address_2):
        """
        Set the rule to dnat outgoing traffic on an interface
        :param intf:
        :param ip_address_1: destination ip address written in the packet e.g. 4.x
        :param ip_address_2: destination ip address to write in the packet e.g. 3.x
        :return:
        """
        NetworkTools.run('iptables -t nat -I PREROUTING -d {} -j DNAT --to {}'.format(ip_address_1, ip_address_2))

    @staticmethod
    def dnat_ip_address_del(intf, ip_address_1, ip_address_2):
        """
        Set the rule to dnat outgoing traffic on an interface
        :param intf:
        :param ip_address_1: destination ip address written in the packet e.g. 4.x
        :param ip_address_2: destination ip address to write in the packet e.g. 3.x
        :return:
        """
        NetworkTools.run('iptables -t nat -D PREROUTING -d {} -j DNAT --to {}'.format(ip_address_1, ip_address_2))

    @staticmethod
    def get_dnat_rules():
        """
        root@Omega-AE03:~# iptables -t nat -L PREROUTING -n
        Chain PREROUTING (policy ACCEPT)
        target     prot opt source               destination
        DNAT       all  --  0.0.0.0/0            192.168.4.120        to:192.168.3.120
        DNAT       all  --  0.0.0.0/0            192.168.4.181        to:192.168.3.181
        """
        ip_tables = NetworkTools.run('iptables -t nat -L PREROUTING -n')
        return re.findall(
            '^DNAT.+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d{1,2})?\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d{1,2})?)\s+to:(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}(?:\/\d{1,2})?)$',
            ip_tables, re.MULTILINE)
