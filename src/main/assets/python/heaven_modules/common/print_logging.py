"""
Library used on Android, i.e. where logging is not available
and the import of logging fails
"""
import time


class PrintLogger(object):

    def __init__(self, logger_name):
        self.logger_name = logger_name

    def debug(self, log_str):
        print('{:.4f}:DEBUG:{}:{}'.format(time.time(), self.logger_name, log_str))

    def warning(self, log_str):
        print('{:.4f}:WARNING:{}:{}'.format(time.time(), self.logger_name, log_str))

    def error(self, log_str):
        print('{:.4f}:ERROR:{}:{}'.format(time.time(), self.logger_name, log_str))

    def exception(self, log_str):
        print('{:.4f}:EXCEPTION:{}:{}'.format(time.time(), self.logger_name, log_str))


class PrintLogging(object):

    NOTSET = 0
    DEBUG = 10
    INFO = 20
    WARNING = 30
    ERROR = 40
    CRITICAL = 50

    def basicConfig(self, *argv, **kwargs):
        pass

    def getLogger(self, logger_name):
        return PrintLogger(logger_name)

# try:
#     import fff
# except ImportError:
#     print('Import failed, using print logging')
#     # from this file import PrintLogging()
#     #create the object for this file
#     logging = PrintLogging()
#
# # this is done only in heaven.py
# logging.basicConfig(level=logging.DEBUG)
#
# # how to log
# logging.getLogger(__name__).debug('culo')


