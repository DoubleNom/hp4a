import os
import Queue
from standard_threads import QueueServer


class RequestServer(QueueServer):

    def __init__(self, queue, profile_packet):
        QueueServer.__init__(self, queue)
        self.profile_packet = profile_packet

    def handle(self, x):
        self.profile_packet(x)


class Profiler:

    def __init__(self, name, enabled):
        self.enabled = enabled

        if enabled:

            self.packet_profiles_filename = 'packet_profiles_{}.csv'.format(name)
            self.requests_server = RequestServer(Queue.Queue(), self.do_profile_packet)

            try:
                os.remove(self.packet_profiles_filename)
            except OSError:
                pass
            self.packet_profiles_f = open(self.packet_profiles_filename, 'a+')
            self._write_line(self.packet_profiles_f,
                    'ID;RX-L3;L3_PROC_UP;L3-L4;L4_PROC_UP;L4_PROC_DOWN;L4-L3;L3_PROC_DOWN;L3_L2;L2_TX;TOT;LEN;IN;OUT')

            self.requests_server.start()

    def profile_packet(self, packet, exit_point=''):
        if not self.enabled:
            return

        self.requests_server.put((packet, exit_point))

    def do_profile_packet(self, x):
        packet, exit_point = x
        """
        Function called when the packet ends his life in Heaven
        :param packet:
        :param exit_point: how the processing of the packet terminated
        :return:
        """
        """
        Timestamps:
        
        0: Rx on socket
        1: Rx by L3 up
        2: Finished processing by L3 up
        3: Rx by L4
        4: Finished processing by L4
        -----------------------------------
        5: Created by L4
        6: Finished processing by L4 down
        7: Rx by L3 down
        8: Finished processing by L3 down
        9: Rx by L2 down
        10: Tx on socket
        
        Processing L4: 
        - If 4 and 3: 4-3
        - If 6 and 5: 6-5 
        
        1-0   2-1        3-2   4-3        6-5
        RX-L3;L3_PROC_UP;L3-L4;L4_PROC_UP;L4_PROC_DOWN;L4-L3;L3_PROC_DOWN;L3_L2;L2_TX;TOT;LEN;IN;OUT
        0     1          2     3          4            5     6            7     8     9   10  11 12
        
        For forwarded packets, L3 proc down would be big because we do not have rxdown
        """
        intervals = [max(0.0, packet.profiler_ts[i+1] - packet.profiler_ts[i]) for i in xrange(10)]
        intervals.pop(4)  # remove 5-4

        # remove dt for forwarded packets
        for i, v in enumerate(intervals):
            if v > 100:
                intervals[i] = 0.0

        line = '{};{};{};{};{}'.format(
            packet.id,
            self._delays_list_to_str(intervals + [sum(intervals)]),
            packet.get_len(),
            packet.entry_point,
            exit_point)

        self._write_line(self.packet_profiles_f, line)

    def stop(self):
        if self.enabled:
            self.requests_server.stop()
            self._close_file(self.packet_profiles_f)

    @staticmethod
    def _avg(values):
        return float(sum(values))/len(values)

    @staticmethod
    def _delays_list_to_str(values):
        return ';'.join('{:.5f}'.format(value) for value in values)

    @staticmethod
    def _write_line(f, line):
        try:
            f.write(line + '\n')
        except ValueError:
            # the file may be already closed
            pass

    @staticmethod
    def _close_file(f):
        f.flush()
        os.fsync(f.fileno())
        f.close()


