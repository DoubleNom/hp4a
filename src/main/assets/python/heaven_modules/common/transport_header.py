from heaven_modules.common.routing_header import RoutingHeader


class TransportHeader:
    """
    Transport layer packet format
    | PROTOCOL  | SOURCE |  PAYLOAD |
    0   (2B)   2   (4B)  6

    - Protocol: int constants for RUDP/TCP/UDP
    - Source: 4 chars heaven id of the sender.
        Used by the destination to discriminate among different sessions with the same SessionID

    02 gitl 123400003000000000000000.4060.00100

    02gitl 123440003001770000002
    """

    TRANSPORT_HEADER_START = RoutingHeader.ROUTING_PAYLOAD_START
    TRANSPORT_HEADER_LENGTH = 6
    TRANSPORT_PAYLOAD_START = TRANSPORT_HEADER_START + TRANSPORT_HEADER_LENGTH

    TRANSPORT_PROTOCOL_LEN = 2
    TRANSPORT_PROTOCOL_START = TRANSPORT_HEADER_START
    TRANSPORT_PROTOCOL_END = TRANSPORT_PROTOCOL_START + TRANSPORT_PROTOCOL_LEN

    TRANSPORT_SOURCE_LEN = 4
    TRANSPORT_SOURCE_START = TRANSPORT_PROTOCOL_END
    TRANSPORT_SOURCE_END = TRANSPORT_SOURCE_START + TRANSPORT_SOURCE_LEN

    RUDP_TRANSPORT_TYPE = 1
    TCP_TRANSPORT_TYPE = 2
    UDP_TRANSPORT_TYPE = 3
    TRANSPORT_PROTOCOL_TYPES = [RUDP_TRANSPORT_TYPE, TCP_TRANSPORT_TYPE, UDP_TRANSPORT_TYPE]

    # These 2 values are referred only to tcp/upd payload, not counting their headers
    # 1500 (MTU) - 12 (ROUTING HEADER) - 6 (TRANSPORT HEADER) - 21 (TCP HEADER) = 1461

    def __init__(self, transport_protocol, source):
        self.transport_protocol = transport_protocol
        self.source = source

    def get_header_str(self):
        return '{:0>{transport_protocol_len}}{:0>{source_len}}'.format(
            self.transport_protocol, self.source,
            transport_protocol_len=TransportHeader.TRANSPORT_PROTOCOL_LEN,
            source_len=TransportHeader.TRANSPORT_SOURCE_LEN)

    def get_len(self):
        return self.TRANSPORT_HEADER_LENGTH

    def __str__(self):
        packet_str = '<{}> = '.format(self.__class__)
        for field in self.__dict__:
            packet_str += '{}:{},\n'.format(field, self.__dict__[field])
        return packet_str

    def __copy__(self):
        return TransportHeader(self.transport_protocol, self.source)


class UdpHeader:
    """
    UDP packet format (Payload of a transport packet)
    | DEST. PORT | SOURCE PORT | TIMESTAMP    | PAYLOAD |
    0    (4B)    4    (4B)     8  xxx.xxxxxx 18

    """
    UDP_HEADER_START = TransportHeader.TRANSPORT_PAYLOAD_START
    UDP_HEADER_LENGTH = 18
    UDP_PAYLOAD_START = UDP_HEADER_START + UDP_HEADER_LENGTH

    UDP_DESTINATION_PORT_LEN = 4
    UDP_DESTINATION_PORT_START = UDP_HEADER_START
    UDP_DESTINATION_PORT_END = UDP_DESTINATION_PORT_START + UDP_DESTINATION_PORT_LEN

    UDP_SOURCE_PORT_LEN = 4
    UDP_SOURCE_PORT_START = UDP_DESTINATION_PORT_END
    UDP_SOURCE_PORT_END = UDP_SOURCE_PORT_START + UDP_SOURCE_PORT_LEN

    UDP_TIMESTAMP_LEN = 10
    UDP_TIMESTAMP_INT_PART_LEN = 3
    UDP_TIMESTAMP_DEC_PART_LEN = 6
    UDP_TIMESTAMP_START = UDP_SOURCE_PORT_END
    UDP_TIMESTAMP_END = UDP_TIMESTAMP_START + UDP_TIMESTAMP_LEN

    def __init__(self, destination_port, source_port, timestamp):
        self.destination_port = destination_port
        self.source_port = source_port

        int_part_total = int(timestamp)
        dec_part = timestamp - int_part_total
        int_part = int(str(int_part_total)[-UdpHeader.UDP_TIMESTAMP_INT_PART_LEN:])
        self.timestamp = int_part + dec_part

    def get_header_str(self):
        return '{:0>{destination_port_len}}{:0>{source_port_len}}{:0>{timestamp_len}.{timestamp_dec_len}f}'.format(
            self.destination_port, self.source_port, self.timestamp,
            destination_port_len=UdpHeader.UDP_DESTINATION_PORT_LEN,
            source_port_len=UdpHeader.UDP_SOURCE_PORT_LEN,
            timestamp_len=UdpHeader.UDP_TIMESTAMP_LEN,
            timestamp_dec_len=UdpHeader.UDP_TIMESTAMP_DEC_PART_LEN)

    def get_len(self):
        return self.UDP_HEADER_LENGTH

    def __str__(self):
        packet_str = '<{}> = '.format(self.__class__)
        for field in self.__dict__:
            packet_str += '{}:{},\n'.format(field, self.__dict__[field])
        return packet_str

    def __copy__(self):
        return UdpHeader(self.destination_port, self.source_port, self.timestamp)


class TcpHeader:
    """
    TCP packet format (Payload of a transport packet)
    | SERVER PORT | TYPE | SESSION ID | SEQ # | RETRANSM # | MEX ID | PAYLOAD |
    0     (4B)   4 (1B) 5   (4B)     9 (5B)  14  (2B)     16  (5B) 21

    9gitlluca010

    02gitl

    1234 5 0001 00016 00 00001

    - Server port: Port on which the destination (TCP server) is listening for packets of this session.
    - Type: int type of message (syn, fin, data, data init, acks....)
    - SessionID: 4 chars unique identifier of the transport session (generated randomly by the session source)
    - Sequence number: int, index of the packet wrt the whole message. Start from 0, the init packet does not count
    - Retransmission number: int, number of retransmission of the same packet. Used to discriminate copies.
    - MessageID: random string generated by the sender
    """
    TCP_HEADER_START = TransportHeader.TRANSPORT_PAYLOAD_START
    TCP_HEADER_LENGTH = 21
    TCP_PAYLOAD_START = TCP_HEADER_START + TCP_HEADER_LENGTH

    SERVER_PORT_LEN = 4
    SERVER_PORT_START = TCP_HEADER_START
    SERVER_PORT_END = SERVER_PORT_START + SERVER_PORT_LEN

    TCP_TYPE_LEN = 1
    TCP_TYPE_INDEX = SERVER_PORT_END

    SESSION_ID_LEN = 4
    SESSION_ID_START = TCP_TYPE_INDEX + TCP_TYPE_LEN
    SESSION_ID_END = SESSION_ID_START + SESSION_ID_LEN

    SEQ_N_LEN = 5
    SEQ_N_START = SESSION_ID_END
    SEQ_N_END = SEQ_N_START + SEQ_N_LEN

    RETRANSM_N_LEN = 2
    RETRANSM_N_START = SEQ_N_END
    RETRANSM_N_END = RETRANSM_N_START + RETRANSM_N_LEN

    MEX_ID_LEN = 5
    MEX_ID_START = RETRANSM_N_END
    MEX_ID_END = MEX_ID_START + MEX_ID_LEN

    TCP_SYN = 0
    TCP_SYN_ACK = 1
    TCP_INIT_DATA = 2
    TCP_INIT_DATA_ACK = 3
    TCP_DATA = 4
    TCP_ACK = 5
    TCP_KEEP_ALIVE = 6
    TCP_KEEP_ALIVE_ACK = 7
    TCP_FIN = 8
    TCP_FIN_ACK = 9

    TCP_PACKET_TYPES = [TCP_SYN, TCP_SYN_ACK, TCP_INIT_DATA, TCP_INIT_DATA_ACK,
                        TCP_DATA, TCP_ACK, TCP_KEEP_ALIVE, TCP_KEEP_ALIVE_ACK,
                        TCP_FIN, TCP_FIN_ACK]

    TCP_ACK_TYPES = [TCP_SYN_ACK, TCP_INIT_DATA_ACK, TCP_ACK, TCP_KEEP_ALIVE_ACK, TCP_FIN_ACK]  # Types for a tcp client
    TCP_NON_ACK_TYPES = [TCP_SYN, TCP_INIT_DATA, TCP_DATA, TCP_KEEP_ALIVE, TCP_FIN]  # Types for a tcp server

    TCP_MAX_SEQ_N = 10**(SEQ_N_END - SEQ_N_START + 1) - 1
    
    def __init__(self, server_port, tcp_type, session_id, seq_n, retransm_n, mex_id):
        self.server_port = server_port
        self.tcp_type = tcp_type
        self.session_id = session_id
        self.seq_n = seq_n
        self.retransm_n = retransm_n
        self.mex_id = mex_id
     
    def get_header_str(self):
        return '{:0>{server_port_len}}{:0>{tcp_type_len}}{:0>{session_id_len}}{:0>{seq_n_len}}{:0>{retransm_n_len}}{:0>{mex_id_len}}'.format(
            self.server_port, self.tcp_type, self.session_id, self.seq_n, self.retransm_n, self.mex_id,
            server_port_len=TcpHeader.SERVER_PORT_LEN,
            tcp_type_len=TcpHeader.TCP_TYPE_LEN,
            session_id_len=TcpHeader.SESSION_ID_LEN,
            seq_n_len=TcpHeader.SEQ_N_LEN,
            retransm_n_len=TcpHeader.RETRANSM_N_LEN,
            mex_id_len=TcpHeader.MEX_ID_LEN)

    def get_len(self):
        return self.TCP_HEADER_LENGTH

    def __str__(self):
        packet_str = '<{}> = '.format(self.__class__)
        for field in self.__dict__:
            packet_str += '{}:{},\n'.format(field, self.__dict__[field])
        return packet_str

    def __copy__(self):
        return TcpHeader(self.server_port, self.tcp_type, self.session_id, self.seq_n, self.retransm_n, self.mex_id)


class TcpSynHeader:
    """
    TCP SYN packet format (Payload of a TCP packet)
    | RETRANSM_TIMEOUT | NULL_TIMEOUT | MAX_RENTRANSM | ACK_STRATEGY | EMPTY PAYLOAD
    0      (5B)        5     (5B)     10    (3B)     13      (1B)   14

    9gitlluca01002gitl123400003000000000000000.4060.00100
    \*gitlluca010*2gitl123400003000000000000000.4060.00100


    \*gitl luca 0 1 0*2gitl123400003000000000000000.4060.00100

    - Retransmission timeout: float XXX.X, 5 chars.
        Seconds after which, if a packet is not acknowledged, will be retransmitted
    - Null timeout: float XXX.X, 5 chars.
        Seconds after which, if no packets are exchanged, a session is closed. Not implemented/wanted
    - Maximum retransmissions: int. Number of allowed retransmission of a packet, after which the whole message transmission
        is aborted
    - Ack Strategy: int. Cumulative or single ack strategy
    """
    SYN_HEADER_START = TcpHeader.TCP_PAYLOAD_START
    SYN_HEADER_LENGTH = 14
    SYN_HEADER_END = SYN_HEADER_START + SYN_HEADER_LENGTH
    
    RETRANSM_TIMEOUT_LEN = 5
    RETRANSM_TIMEOUT_START = SYN_HEADER_START
    RETRANSM_TIMEOUT_END = RETRANSM_TIMEOUT_START + RETRANSM_TIMEOUT_LEN

    NULL_TIMEOUT_LEN = 5
    NULL_TIMEOUT_START = RETRANSM_TIMEOUT_END
    NULL_TIMEOUT_END = NULL_TIMEOUT_START + NULL_TIMEOUT_LEN

    MAX_RETRANSM_LEN = 3
    MAX_RETRANSM_START = NULL_TIMEOUT_END
    MAX_RETRANSM_END = MAX_RETRANSM_START + MAX_RETRANSM_LEN

    ACK_STRATEGY_LEN = 1
    ACK_STRATEGY_INDEX = MAX_RETRANSM_END

    SINGLE_ACK_STRATEGY = 0
    CUMULATIVE_ACK_STRATEGY = 1
    ACK_STRATEGY_VALUES = [SINGLE_ACK_STRATEGY, CUMULATIVE_ACK_STRATEGY]

    def __init__(self, retransm_timeout, null_timeout, max_retransm, ack_strategy):
        self.retransm_timeout = retransm_timeout
        self.null_timeout = null_timeout
        self.max_retransm = max_retransm
        self.ack_strategy = ack_strategy

    def get_header_str(self):
        return '{:0>{retransm_timeout_len},.1f}{:0>{null_timeout_len}.1f}{:0>{max_retransm_len}}{:0>{ack_strategy_len}}'.format(
            self.retransm_timeout, self.null_timeout, self.max_retransm, self.ack_strategy,
            retransm_timeout_len=TcpSynHeader.RETRANSM_TIMEOUT_LEN,
            null_timeout_len=TcpSynHeader.NULL_TIMEOUT_LEN,
            max_retransm_len=TcpSynHeader.MAX_RETRANSM_LEN,
            ack_strategy_len=TcpSynHeader.ACK_STRATEGY_LEN)

    def get_len(self):
        return self.SYN_HEADER_LENGTH

    def __str__(self):
        packet_str = '<{}> = '.format(self.__class__)
        for field in self.__dict__:
            packet_str += '{}:{},\n'.format(field, self.__dict__[field])
        return packet_str

    def __copy__(self):
        return TcpSynHeader(self.retransm_timeout, self.null_timeout, self.max_retransm, self.ack_strategy)


class TcpSocketHeader(object):

    """
    Received message format:
    | SOURCE | SESSION_ID | MSG_LEN |   MESSAGE  |
    0   (4B) 4   (4B)     8 (16B)  24           24 + MSG_LEN
    """
    SOURCE_START = 0
    SOURCE_END = 4

    SESSION_ID_LEN = 4
    SESSION_ID_START = SOURCE_END
    SESSION_ID_END = SESSION_ID_START + SESSION_ID_LEN

    MSG_LEN_LEN = 16
    MSG_LEN_START = SESSION_ID_END
    MSG_LEN_END = MSG_LEN_START + MSG_LEN_LEN

    HEADER_LENGTH = MSG_LEN_END

    ACK_LEN = 1024
    ACK_CHAR = '1'


class UdpSocketHeader(object):

    """
    Received message format:
    | SOURCE | CLIENT_ID |    MESSAGE  |
    0   (4B) 4   (4B)    8 
    """
    HEADER_LENGTH = 8

    SOURCE_START = 0
    SOURCE_END = 4

    CLIENT_ID_LEN = 4
    CLIENT_ID_START = SOURCE_END
    CLIENT_ID_END = CLIENT_ID_START + CLIENT_ID_LEN

    PAYLOAD_START = CLIENT_ID_END

