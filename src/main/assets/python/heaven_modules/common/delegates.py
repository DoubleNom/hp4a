class Delegates(object):

    def did_start_network(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_receive_packet(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_receive_neighbors(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_add_rpi_with_heaven_name(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_delete_rpi_with_heaven_name(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_stop_network_with_log(self, msg):
        print('Exec method {}'.format(msg['method']))

    def find_catastrophic_error_in_oto(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_connect_to_linux_wifi(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_disconnect_from_linux_wifi(self, msg):
        print('Exec method {}'.format(msg['method']))