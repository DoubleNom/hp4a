"""
We put the imports in try statements because some heaven instances should be able to
work without files that they do no use (to save storage space)
"""
try:
    from heaven_modules.neighboring.neighbor_service_ap import ApNeighborService
except ImportError:
    pass

try:
    from heaven_modules.neighboring.neighbor_service_apcli import ApcliNeighborService
except ImportError:
    pass

try:
    from heaven_modules.neighboring.neighbor_service_ethernet import EthernetNeighborService
except ImportError:
    pass

try:
    from heaven_modules.neighboring.neighbor_service_adhoc import AdHocNeighborService
except ImportError:
    pass

from link_layer_utils import *
from heaven_modules.common.network_tools import NetworkTools
from heaven_modules.common.common_parameters import CommonParameters as cp


class LinkLayer(object):

    def __init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler):
        self.network_name = network_name
        self.l2_to_l3_queue = l2_to_l3_queue  # Queues (created by L3) to put data for the upper layer
        self.neighbors = neighbors
        self.config_reader = config_reader
        self.profiler = profiler

        # read configurations
        self.network_configs = config_reader.get_config_dict(network_name)
        self.link_layer_configs = config_reader.get_config_dict('link_layer')

    def start(self):
        pass

    def stop(self):
        pass

    def send_unicast(self, packet, next_hop):
        pass

    def send_broadcast(self, packet):
        pass

    def create_unicast_client(self, ip_address, heaven_address):
        return DummyClient()


class DummyClient(object):
    """
    Necessary because neighbor calls this function for his own unicast client but in this case
    it will be only one for all neighbors on this link layer
    """

    def start(self):
        pass

    def stop(self):
        pass

    def send(self, packet):
        pass


class StandardLinkLayer(LinkLayer):
    """
    Link layer used by Access Point or APCLI or Ethernet
    """

    def __init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, ip_address_parser, profiler):
        LinkLayer.__init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler)
        self.ip_address_parser = ip_address_parser

        # --------------------- network and ports ---------------------
        self.intf = self.network_configs['interface']
        self.tx_socket_timeout = float(self.link_layer_configs['tx_socket_timeout'])
        self.broadcast_support = int(self.network_configs['broadcast_support'])
        self.data_port = int(self.network_configs['data_port'])  # port used to receive data

        # --------------------- neighboring service ---------------------
        if self.network_name == cp.AP_NETWORK_NAME:
            self.neighbor_service = ApNeighborService(
                self, self.neighbors, self.network_name, self.ip_address_parser, config_reader)
        elif self.network_name == cp.AP_CLI_NETWORK_NAME:
            self.neighbor_service = ApcliNeighborService(
                self, self.neighbors, self.network_name, self.ip_address_parser, config_reader)
        elif self.network_name == cp.ETHERNET_NETWORK_NAME:
            self.neighbor_service = EthernetNeighborService(
                self, self.neighbors, self.network_name, self.ip_address_parser, config_reader)
        elif self.network_name == cp.AD_HOC_NETWORK_NAME:
            self.neighbor_service = AdHocNeighborService(
                self, self.neighbors, self.network_name, self.ip_address_parser, config_reader)

            # configure the system for adhoc forwarding
            while not self.ip_address_parser.is_network_configured(self.intf):
                time.sleep(2)
            self.routed_network = NetworkTools.get_next_network(self.ip_address_parser.get_network_address(self.intf))
            self.routed_address = NetworkTools.get_routed_address(self.ip_address_parser.get_ip_address(self.intf))
            self.routed_nic = NetworkTools.create_virtual_interface(self.intf, self.routed_address)
            NetworkTools.enable_forwarding(self.routed_network, self.routed_network)

        # --------------------- sending and receiving ---------------------
        self.data_server = DataServer(self.ip_address_parser, self.data_port, self.l2_to_l3_queue, self.neighbors,
                                      int(self.link_layer_configs['rx_buffer_size']),
                                      int(self.link_layer_configs['rx_queue_len']),
                                      self.profiler, self.intf, name='{} Link Layer'.format(self.network_name))

        if self.broadcast_support:
            self.broadcast_client = BroadcastClient(self.ip_address_parser,
                                                    self.data_port,
                                                    int(self.link_layer_configs['broadcast_n_of_transmissions']),
                                                    self.tx_socket_timeout,
                                                    self.profiler, self.intf)

    def create_unicast_client(self, ip_address, heaven_address):
        return UnicastClient((ip_address, self.data_port), self.tx_socket_timeout, self.profiler)

    def start(self):
        self.neighbor_service.start()
        self.data_server.start()

        if self.broadcast_support:
            self.broadcast_client.start()

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} data server...'.format(self.network_name))
        self.data_server.stop()
        # self.data_server.join()

        if self.broadcast_support:
            self.broadcast_client.stop()

        self.neighbor_service.stop()

    def send_unicast(self, packet, next_hop):
        self.neighbors.send(next_hop, packet)

    def send_broadcast(self, packet):
        if self.broadcast_support:
            self.broadcast_client.send(packet)
        else:
            self.neighbors.send_to_all(self.network_name, packet)
