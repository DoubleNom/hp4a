"""
We put the imports in try statements because some heaven instances should be able to
work without files that they do no use (to save storage space)
"""
try:
    from android_link_layer_intf import AndroidLinkLayerInterface
except ImportError:
    pass

try:
    from ios_link_layer_intf import IosLinkLayerInterface
except ImportError:
    pass

try:
    from link_layer import StandardLinkLayer
except ImportError as e:
    print e
    pass

from link_layer_utils import IpAddressParser
from heaven_modules.common.common_parameters import CommonParameters as cp
from heaven_modules.common.network_tools import NetworkTools


class LinkLayerWrapper:
    """
    This class is the interface with upper layers:
    - It creates and holds link layers
    - It dispatches outgoing packets to the right link layer
    """
    def __init__(self, l2_to_l3_queue, neighbors, config_reader, profiler, delegates):
        self.l2_to_l3_queue = l2_to_l3_queue  # Queues (created by L3) to put data for the upper layer
        self.neighbors = neighbors
        self.link_layers = {}  # network_name --> LinkLayer]

        self.ip_address_parser = IpAddressParser(
            config_reader.get('main', 'heaven_address'),
            float(config_reader.get('link_layer', 'network_config_check_interval')),
            NetworkTools(config_reader))

        for network_name in config_reader.get('main', 'networks').split(','):

            if network_name == cp.IOS_NETWORK_NAME:
                self.link_layers[network_name] = IosLinkLayerInterface(
                    network_name, self.l2_to_l3_queue, self.neighbors, config_reader, profiler, delegates)
            elif network_name == cp.ANDROID_NETWORK_NAME:
                self.link_layers[network_name] = AndroidLinkLayerInterface(
                    network_name, self.l2_to_l3_queue, self.neighbors, config_reader, profiler)
            elif network_name in cp.STANDARD_NETWORKS:
                self.link_layers[network_name] = StandardLinkLayer(
                    network_name, self.l2_to_l3_queue, self.neighbors, config_reader, self.ip_address_parser, profiler)

    def start(self):
        for ll in self.link_layers.itervalues():
            ll.start()

    def stop(self):
        self.ip_address_parser.stop()
        for ll in self.link_layers.itervalues():
            ll.stop()

    def send_unicast(self, packet, next_hop):
        packet.set_rx_by_l2_down_ts()
        ll = self.neighbors.get_neighbor_link_layer(next_hop)
        if ll is not None:
            ll.send_unicast(packet, next_hop)

    def send_broadcast(self, packet):
        packet.set_rx_by_l2_down_ts()
        for ll in self.link_layers.itervalues():
            ll.send_broadcast(packet)
