import time
import socket
import logging
import six

from heaven_modules.common.tail_drop_queue import TailDropQueue
from heaven_modules.common.packet import Packet
from heaven_modules.common.standard_threads import HeavenSocketServer, PeriodicalCaller, QueueServer
from heaven_modules.common.utilities import get_udp_socket, get_broadcast_udp_socket


class DataServer(HeavenSocketServer):

    def __init__(self, ip_address_parser, data_port, l2_to_l3_queue, neighbors, rx_buffer_size, rx_queue_len,
                 profiler, intf, name='LinkLayer'):
        HeavenSocketServer.__init__(self, ('', data_port), rx_buffer_size, rx_queue_len,
                                    socket_family=socket.AF_INET, socket_type=socket.SOCK_DGRAM,
                                    name=name)
        self.ip_address_parser = ip_address_parser
        self.l2_to_l3_queue = l2_to_l3_queue
        self.neighbors = neighbors
        self.profiler = profiler
        self.intf = intf

    def handle(self, x):

        data, client_address = x

        logging.getLogger(__name__).debug('<---------- Received data: {}'.format(x))

        if data is None or client_address is None:
            return

        client_ip_address = client_address[0]

        if not self.ip_address_parser.is_network_configured(self.intf):
            return

        # discard messages from myself
        if client_ip_address == self.ip_address_parser.get_ip_address(self.intf):
            logging.getLogger(__name__).debug('Dropping data because it comes from myself: {}'.format(x))
            return

        neighbor = self.neighbors.get_neighbor_by_ip(client_ip_address)
        if neighbor is None:
            logging.getLogger(__name__).debug('Dropping data because previous hop is unknown: {}'.format(x))
            return

        previous_hop = neighbor.heaven_address

        # pass data to the upper layer
        packet = Packet(rx_str=data, previous_hop=previous_hop, entry_point=Packet.RX_ON_SOCKET)
        packet.set_rx_on_socket_ts()
        if not self.l2_to_l3_queue.put(packet):
            self.profiler.profile_packet(packet, Packet.QUEUE_DROP + '_l2_to_l3_q')

        # update the last seen timestamp of the neighbor
        self.neighbors.update_last_seen_ts(previous_hop)


class IpAddressParser(PeriodicalCaller):
    """
    #------------------------------------------------------------------------------------------------------------------#
    #------------------------------------------ NETWORK READER --------------------------------------------------------#
    # Periodically reads from the OS network parameters and updates variables by consequence.
    #------------------------------------------------------------------------------------------------------------------#
    """
    IP_ADDR = 0
    NET_ADDR = 1
    BR_ADDR = 2

    def __init__(self, heaven_address, update_interval, network_tools):
        self.heaven_address = heaven_address  # useful to create neighboring string
        self.network_tools = network_tools
        self.intfs = {}  # {intf: {IP_ADDR: '192.168.0.2', NET_ADDR: '192.168.0.0/24', BR_ADDR: '192.168.0.255'}}
        PeriodicalCaller.__init__(self, self.periodical_function, update_interval)
        self.start()

    def periodical_function(self):
        ip_tuples = self.network_tools.get_ip_addr()  # [(intf, ip, bc, netw)]

        for intf, ip, bc, netw in ip_tuples:
            self.intfs[intf] = {self.IP_ADDR: ip, self.NET_ADDR: netw, self.BR_ADDR: bc}

    def get_ip_address(self, intf):
        try:
            return self.intfs[intf][self.IP_ADDR]
        except KeyError:
            return ''
    
    def get_network_address(self, intf):
        try:
            return self.intfs[intf][self.NET_ADDR]
        except KeyError:
            return ''
    
    def get_broadcast_address(self, intf):
        try:
            return self.intfs[intf][self.BR_ADDR]
        except KeyError:
            return ''            

    def get_neighboring_str(self, intf, ts=None):

        # in this case it is a neighboring request so ts is not an "id"
        if ts is None:
            ts = time.time()
        
        if intf not in self.intfs:
            return ''
        
        mystr = '{};{};{}'.format(ts, self.heaven_address, self.intfs[intf][self.IP_ADDR])

        if six.PY2:
           return mystr
        else:
            return bytes(mystr, 'utf8')

    def is_network_configured(self, intf):
        return intf in self.intfs


class UnicastClient(QueueServer):
    """
    #------------------------------------------------------------------------------------------------------------------#
    #------------------------------------------ UNICAST CLIENT ------------------------------------------------------#
    # Thread that read a tx queue and tx in unicast. This thread is kept into a neighbor object
    #------------------------------------------------------------------------------------------------------------------#
    """

    def __init__(self, destination, tx_socket_timeout, profiler):
        QueueServer.__init__(self, TailDropQueue())
        self.destination = destination
        self.tx_socket_timeout = tx_socket_timeout
        self.profiler = profiler
        self.sock = None

    def handle(self, x):
        packet = x

        try:
            if not self.sock:
                self.sock = get_udp_socket(self.destination, self.tx_socket_timeout)

            self.sock.sendto(packet.packet_to_str(), self.destination)
            logging.getLogger(__name__).debug('----------> Socket Sent to {}: {}'.format(self.destination, packet))
            packet.set_tx_on_socket_ts()
            self.profiler.profile_packet(packet, Packet.SENT_ON_SOCKET + '_uc')
        except (socket.error, socket.timeout):
            packet.set_tx_on_socket_ts()
            self.profiler.profile_packet(packet, Packet.SOCKET_DROP + '_uc')
            logging.getLogger(__name__).warning('Socket went on TimeOut while sending to {}: {}'.format(self.destination, packet))
            if self.sock is not None:
                self.sock.close()
                self.sock = None

    def cleanup(self):
        if self.sock is not None:
            self.sock.close()

    def send(self, packet):
        if not self.put(packet):
            packet.set_tx_on_socket_ts()
            self.profiler.profile_packet(packet, Packet.QUEUE_DROP + '_uc_cli_tx_q')


class BroadcastClient(QueueServer):
    """
    #------------------------------------------------------------------------------------------------------------------#
    #------------------------------------------ BROADCAST CLIENT ------------------------------------------------------#
    # Thread that reads a tx queue and tx in broadcast
    #------------------------------------------------------------------------------------------------------------------#
    """

    def __init__(self, ip_address_parser, data_port, broadcast_n_of_transmissions, tx_socket_timeout, profiler, intf):
        QueueServer.__init__(self, TailDropQueue())
        self.ip_address_parser = ip_address_parser
        self.data_port = data_port
        self.broadcast_n_of_transmissions = broadcast_n_of_transmissions
        self.tx_socket_timeout = tx_socket_timeout
        self.profiler = profiler
        self.intf = intf
        self.sock = None

    def handle(self, x):

        packet = x

        if not self.ip_address_parser.is_network_configured(self.intf):
            return

        packet_str = packet.packet_to_str()
        logging.getLogger(__name__).debug('BroadcastClient TX: {}'.format(packet_str))
        for retx in xrange(self.broadcast_n_of_transmissions):
            try:
                if self.sock is None:
                    self.sock = self.get_socket()
                self.sock.send(packet_str)
                packet.set_tx_on_socket_ts()
                self.profiler.profile_packet(packet, Packet.SENT_ON_SOCKET + '_bc')
            except (socket.error, socket.timeout):
                packet.set_tx_on_socket_ts()
                self.profiler.profile_packet(packet, Packet.SOCKET_DROP + '_bc')
                if self.sock is not None:
                    self.sock.close()
                    self.sock = None

    def cleanup(self):
        if self.sock is not None:
            self.sock.close()

    def get_socket(self):
        return get_broadcast_udp_socket(
            (self.ip_address_parser.get_broadcast_address(self.intf), self.data_port), self.tx_socket_timeout)

    def send(self, packet):
        if not self.put(packet):
            packet.set_tx_on_socket_ts()
            self.profiler.profile_packet(packet, Packet.QUEUE_DROP + '_bc_cli_tx_q')
