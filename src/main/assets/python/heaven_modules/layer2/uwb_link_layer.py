import logging
from threading import Thread

from heaven_modules.common.packet import Packet
from heaven_modules.common.standard_threads import QueueServer
from heaven_modules.common.tail_drop_queue import TailDropQueue


class DataServer(Thread):
    """
    1. receive data from gpio (maybe also aggregate data)
    2. pass data to the upper layer
    3. update the last seen

    # 2. pass data to the upper layer
    self.l2_to_l3_queue.put(
    Packet(rx_str=packet_str, previous_hop=previous_hop, entry_point=Packet.RX_ON_SOCKET))

    # 3. update the last seen timestamp of the neighbor
    self.neighbors.update_last_seen_ts(previous_hop)

    # When you discover a new neighbor or a neighbor send you a nieighboring message, call
    self.neighbors.update_neighbor(heaven_address, '1.1.1.1', self.link_layer)
    """
    pass

    def stop(self):
        pass


class DataClient(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())

    def handle(self, x):
        """
        :param x: the string to transmit
        :return:
        """
        # TODO tx the string on the gpio (and split it)
        print('Here you have to send {}'.format(x))
        pass

    def cleanup(self):
        # todo close gpio communication
        pass

    def send(self, packet):
        self.put(packet.packet_to_str())


class DummyClient(object):
    """
    Necessary because neighbor calls this function for his own unicast client but in this case
    it will be only one for all neighbors on this link layer
    """

    def start(self):
        pass

    def stop(self):
        pass

    def send(self, packet):
        pass


class DummyNeighbors(object):

    def update_last_seen_ts(self, previous_hop):
        print('Neighbor {}: update last seen ts'.format(previous_hop))

    def update_neighbor(self, name, ip, link_layer):
        print('update_neighbor {}, {}, {}'.format(name, ip, link_layer))


class UwbLinkLayer:
    """
    This link layer exchange data throught the gpio interface (e.g. the rpi send data to a UWB module with gpio).
    Layer2 methods to be called and received data are converted in/to strings sent on a UNIX DATAGRAM socket.

    Receive packets:
    previous_hop_heaven_address;packet_string

    Send (unicast) packets:
    0;next_hop_heaven_address;packet_string

    """

    def __init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler):
        self.network_name = network_name
        self.l2_to_l3_queue = l2_to_l3_queue  # Queues (created by L3) to put data for the upper layer
        self.neighbors = neighbors
        self.config_reader = config_reader
        self.profiler = profiler
        # --------------------- network and ports ---------------------
        # nc = self.config_reader.get_config_dict(network_name)
        # self.rx_server_address = nc['rx_data_server_address']  # port used to receive data
        # self.tx_server_address = nc['tx_data_server_address']  # port used to receive data

        # --------------------- sending and receiving ---------------------
        self.data_server = DataServer()
        self.data_client = DataClient()

        # todo. Use common.standard_threads.PeriodicalCaller to send periodical neighboring requests


    def create_unicast_client(self, _ip_address, _heaven_address):
        return DummyClient()



    def start(self):
        self.data_server.start()
        self.data_client.start()

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} data server...'.format(self.network_name))
        self.data_server.stop()
        # self.data_server.join()

        logging.getLogger(__name__).debug('Stopping {} broadcast client...'.format(self.network_name))
        self.data_client.stop()
        # self.data_client.join()

    def send_unicast(self, packet, _next_hop):
        self.data_client.send(packet)

    def send_broadcast(self, packet):
        self.data_client.send(packet)


if __name__ == '__main__':
    import Queue

    uwb_link_layer = UwbLinkLayer('uwb', Queue.Queue(), DummyNeighbors(), None, None)
    uwb_link_layer.start()

    # read commands from the user stdin

    # send my_msg


    command_str = ''
    try:
        while 1:
            try:

                command_str = str(raw_input('''
    --------------------------------------------------------------------
    Type your command
     > '''))
                command_split = command_str.split(' ')
                command = command_split[0]

                if command == 'exit':
                    break
                elif command == 'send':
                    msg = command_split[1]
                    pkt = Packet(rx_str=msg)
                    uwb_link_layer.send_broadcast(pkt)
                else:
                    print('unknown command')
            except IndexError as e:
                print('Wrong number of parameters, ', e)

    except (KeyboardInterrupt, SystemExit, SystemError):
        print('Forced shutdown')

    uwb_link_layer.stop()



