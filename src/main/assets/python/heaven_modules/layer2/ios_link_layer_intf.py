import socket
import logging
import six
import json


if __name__ == '__main__':
    import os
    import sys
    # add heaven package to sys paths
    this_folder = os.path.dirname(os.path.abspath(__file__))
    previous_folder = os.path.abspath(os.path.join(this_folder, os.pardir))
    heaven_folder = os.path.abspath(os.path.join(previous_folder, os.pardir))
    sys.path.append(heaven_folder)

from link_layer import LinkLayer
from heaven_modules.common.tail_drop_queue import TailDropQueue
from heaven_modules.common.packet import Packet
from heaven_modules.common.standard_threads import QueueServer, HeavenSocketServer, PeriodicalCaller


class DataServer(HeavenSocketServer):

    MSG_LEN = 5

    def __init__(self, address, buffer_size, queue_size, l2_to_l3_queue, neighbors, neighbors_list, delegates):
        HeavenSocketServer.__init__(self, address, buffer_size, queue_size,
                                    socket_family=socket.AF_INET, socket_type=socket.SOCK_STREAM,
                                    name='iOS Link Layer')
        self.l2_to_l3_queue = l2_to_l3_queue
        self.delegates = delegates
        self.neighbors = neighbors
        self.neighbors_list = neighbors_list

        self.methods = {
            'DidStartNetwork': self.did_start_network,
            'DidReceivePacket': self.did_receive_packet,
            'DidUpdateNeighbors': self.did_update_neighbors,
            'DidAddRPiWithHeavenName': self.did_add_rpi_with_heaven_name,
            'DidDeleteRPiWithHeavenName': self.did_delete_rpi_with_heaven_name,
            'DidStopNetworkWithLog': self.did_stop_network_with_log,
            'FindCatastrophicErrorInOTO': self.find_catastrophic_error_in_oto,
            'DidConnectToLinuxWifi': self.did_connect_to_linux_wifi,
            'DidDisconnectFromLinuxWifi': self.did_disconnect_from_linux_wifi
        }

    def handle(self, x):
        """
        format: msg_len;command;parameters
        msg_len must be equal to len from command (first ';' excluded) to the end
        NOTE: x has already been decoded to string by HeavenSocketServer
        :param x:
        :return:
        """
        print('Python get from rx queue: {}'.format(x))
        data = x[0]

        msg_len = data[:self.MSG_LEN]

        if len(msg_len) != self.MSG_LEN:
            print('Msg len must be of {} chars'.format(self.MSG_LEN))

        try:
            msg_len = int(msg_len)
        except ValueError:
            print('msg_len [{}] is not int'.format(msg_len))
            return

        msg = data[self.MSG_LEN:self.MSG_LEN + msg_len]

        if len(msg) != msg_len:
            print('Received incomplete data, msg_len={} but len(msg)={}'.format(msg_len, len(msg)))
            return

        try:
            msg = json.loads(msg)
        except ValueError as e:
            print('Cannod decode json: {}'.format(e))
            return
        
        try:
            method = msg['method']
        except KeyError:
            print('JSON dict has no method field: {}'.format(msg))
            return

        if method not in self.methods:
            print('Method {} is not valid'.format(method))
            return

        self.methods[method](msg)

    def did_start_network(self, msg):
        print('Exec method {}'.format(msg['method']))
        # todo propagate to heaven clients

    def did_receive_packet(self, msg):
        print('Exec method {}'.format(msg['method']))

        try:
            packet_str = msg['packet']
            previous_hop = msg['fromPeer']
        except KeyError as e:
            print('Wrong fields in packet: {}'.format(e))
            return

        # pass data to the upper layer
        self.l2_to_l3_queue.put(
            Packet(rx_str=packet_str, previous_hop=previous_hop, entry_point=Packet.RX_ON_SOCKET))

        # update the last seen timestamp of the neighbor
        self.neighbors.update_last_seen_ts(previous_hop)

    def did_update_neighbors(self, msg):
        print('Exec method {}'.format(msg['method']))

        try:
            neighbors = msg['neighbors']
        except KeyError as e:
            print('Wrong fields in packet: {}'.format(e))
            return

        self.neighbors_list = neighbors

    def did_add_rpi_with_heaven_name(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_delete_rpi_with_heaven_name(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_stop_network_with_log(self, msg):
        print('Exec method {}'.format(msg['method']))

    def find_catastrophic_error_in_oto(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_connect_to_linux_wifi(self, msg):
        print('Exec method {}'.format(msg['method']))

    def did_disconnect_from_linux_wifi(self, msg):
        print('Exec method {}'.format(msg['method']))


class DataClient(QueueServer):

    def __init__(self, tx_data_port):
        QueueServer.__init__(self, TailDropQueue())
        self.tx_data_port = tx_data_port
        self.sock = None

    def handle(self, x):
        print('Python sending: {}'.format(x))

        x = '{:05d}{}'.format(len(x), x)

        if not six.PY2:
            # noinspection PyArgumentList
            x = bytes(x, 'utf8')

        # Note: the conversion to bytes does not change the length of a string

        try:
            if self.sock is None:
                self.sock = self.get_socket()
            self.sock.send(x)
        except (socket.error, socket.timeout):
            if self.sock is not None:
                self.sock.close()
                self.sock = None

    def cleanup(self):
        if self.sock is not None:
            self.sock.close()

    def get_socket(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('localhost', self.tx_data_port))
        sock.setblocking(False)
        return sock

    def send(self, msg):
        self.put(msg)

    def send_initiateLayer2NetworkWithHeaven(self, name):
        """
        Called when the link layer interface has started
        :param name:
        :return:
        """
        msg = {
            'method': 'initiateLayer2NetworkWithHeaven',
            'name': name,
            'bonjourType': '_xanda',
            'blockedPeers': [],
            'log': False,
            'logIdentification': '',
            'otoQueueSize': 300,
            'BCAmountOfServicesPerPeer': 3,
            'BCMaximumPacketsPerFrame': 25,
            'BCMaximumTimeOfAdvertising': 5,
            'BCMaximumPacketsQueuedToTransmit': 3000,
            'raspberryPiAvailable': True,
            'ssid': 'OmegaNetwork',
            'pwd': '12345678'
        }

        self.put(json.dumps(msg))

    def send_attemptToConnectToLinuxHotspot_msg(self):
        msg = {
            'method': 'attemptToConnectToLinuxHotspot'
        }

        self.put(json.dumps(msg))

    def send_sendPacket_msg(self, destination, packet_str):
        """
        Called every time there is a packet to send
        :param destination:
        :param packet_str:
        :return:
        """
        msg = {
            'method': 'sendPacket',
            'destination': destination,
            'packet': packet_str
        }

        self.put(json.dumps(msg))

    def send_stopNetworkActivities_msg(self):
        """
        Called when stopping this link layer interface
        :return:
        """
        msg = {
            'method': 'stopNetworkActivities'
        }

        self.put(json.dumps(msg))


class IosLinkLayerInterface(LinkLayer):
    """
    This is a dummy link layer, since methods are implemented in iOS.
    Layer2 methods to be called and received data are converted in/to strings sent on a socket.

    Protocol:
    5 chars for payload length followed by json message

    """

    def __init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler, delegates):
        LinkLayer.__init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler)
        self.delegates = delegates
        # --------------------- network and ports ---------------------
        self.tx_data_port = int(self.network_configs['tx_data_port'])  # port used to receive data
        self.rx_data_port = int(self.network_configs['rx_data_port'])  # port used to receive data

        self.neighbors_list = []
        self.heaven_address = self.config_reader.get('main', 'heaven_address')

        # --------------------- sending and receiving ---------------------
        self.data_server = DataServer(('localhost', self.rx_data_port), 1024, 100,
                                       self.l2_to_l3_queue, self.neighbors, self.neighbors_list, self.delegates)

        self.data_client = DataClient(self.tx_data_port)

        neighboring_interval = self.config_reader.get('neighboring_service', 'neighboring_requests_interval')
        self.neighbors_updater = PeriodicalCaller(self.update_neighbors, neighboring_interval)

    def start(self):
        self.neighbors_updater.start()
        self.data_server.start()
        self.data_client.start()
        self.data_client.send_initiateLayer2NetworkWithHeaven(self.heaven_address)

    def stop(self):
        self.neighbors_updater.stop()
        self.data_client.send_stopNetworkActivities_msg()

        logging.getLogger(__name__).debug('Stopping {} data server...'.format(self.network_name))
        self.data_server.stop()
        # self.data_server.join()

        logging.getLogger(__name__).debug('Stopping {} data client...'.format(self.network_name))
        self.data_client.stop()
        # self.data_client.join()

    def send_unicast(self, packet, next_hop):
        self.data_client.send_sendPacket_msg(next_hop, packet.packet_to_str())

    def send_broadcast(self, packet):
        self.data_client.send_sendPacket_msg('', packet.packet_to_str())

    def update_neighbors(self):
        neighbors_list = self.neighbors_list[:]

        for heaven_address in neighbors_list:
            self.neighbors.update_neighbor((heaven_address, '', '', 0, self))


class MyQueueServer(QueueServer):

    def handle(self, x):
        print('MyQueueServer received: {}'.format(x))


if __name__ == '__main__':

    # from heaven_modules.neighboring.neighbors import Neighbors
    # from heaven_modules.common.config_reader import ConfigReader
    # from heaven_modules.common.profiler import Profiler
    #
    # my_server = MyQueueServer(TailDropQueue())
    #
    # cr = ConfigReader('{}/heaven_config.ini'.format(heaven_folder))
    # profiler = Profiler('ciao', False)
    # ios_link_layer_intf = IosLinkLayerInterface('ios_network', my_server, Neighbors(cr), cr, profiler)
    # my_server.start()
    # ios_link_layer_intf.start()
    #
    # print('ios link layer intf is running')
    # try:
    #     while 1:
    #         time.sleep(5)
    #         pkt1 = Packet(rx_str='this is a broadcast message')
    #         ios_link_layer_intf.send_broadcast(pkt1)
    #         time.sleep(5)
    #         pkt2 = Packet(rx_str='this is a unicast message')
    #         ios_link_layer_intf.send_unicast(pkt2, 'luca')
    # except (KeyboardInterrupt, SystemExit, SystemError) as e:
    #     print('\nStopping ios link layer intf because of KeyboardInterrupt')
    #     ios_link_layer_intf.stop()
    #     my_server.stop()
    # print('ios link layer intf stopped')

    port = 3453

    import time
    server = DataServer(('localhost', port), 1024, 100, None, None, [], None)
    server.start()

    client = DataClient(port)
    client.start()

    methods = server.methods.keys()

    for method in methods:

        full_method = json.dumps({'method': method})

        client.send(full_method)
        time.sleep(0.5)

    client.stop()
    server.stop()
