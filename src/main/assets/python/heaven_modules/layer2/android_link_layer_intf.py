import socket
import logging
from link_layer import LinkLayer
from heaven_modules.common.tail_drop_queue import TailDropQueue
from heaven_modules.neighboring.android_neighbor_service_intf import AndroidNeighborServiceIntf
from heaven_modules.common.packet import Packet
from heaven_modules.common.standard_threads import QueueServer, HeavenSocketServer


class DataServer(HeavenSocketServer):
    """
    Receives data from the Android application (received packets) and pass them to the upper layer
    """

    def __init__(self, address, buffer_size, queue_size, l2_to_l3_queue, neighbors):
        HeavenSocketServer.__init__(self, address, buffer_size, queue_size,
                                    socket_family=socket.AF_UNIX, socket_type=socket.SOCK_DGRAM,
                                    name='Android Link Layer')
        self.l2_to_l3_queue = l2_to_l3_queue
        self.neighbors = neighbors

    def handle(self, x):
        """
        Receive packets:
        previous_hop_heaven_address;packet_string
        :param x:
        :return:
        """
        data = x[0]
        previous_hop = data[:4]
        packet_str = data[5:]

        # pass data to the upper layer
        self.l2_to_l3_queue.put(
            Packet(rx_str=packet_str, previous_hop=previous_hop, entry_point=Packet.RX_ON_SOCKET))

        # update the last seen timestamp of the neighbor
        self.neighbors.update_last_seen_ts(previous_hop)


class DataClient(QueueServer):
    """
    Sends data to the Android application
    """

    def __init__(self, server_address):
        QueueServer.__init__(self, TailDropQueue())
        self.server_address = server_address
        self.sock = None

    def handle(self, x):
        try:
            if self.sock is None:
                self.sock = self.get_socket()
            self.sock.sendall(x)
        except (socket.error, socket.timeout):
            if self.sock is not None:
                self.sock.close()
                self.sock = None

    def cleanup(self):
        if self.sock is not None:
            try:
                self.sock.close()
            except socket.error:
                pass

    def get_socket(self):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
        sock.connect(self.server_address)
        return sock

    def send(self, packet, next_hop):
        self.put('0;{};{}'.format(next_hop, packet.packet_to_str()))

    def broadcast(self, packet):
        self.put('1;{}'.format(packet.packet_to_str()))


class AndroidLinkLayerInterface(LinkLayer):
    """
    This is a dummy link layer, since methods are implemented in Android.
    Layer2 methods to be called and received data are converted in/to strings sent on a UNIX DATAGRAM socket.

    Received packet (Android --> Py):
    previous_hop_heaven_address;packet_string

    Request to broadcast a packet (Py --> Android):
    1;packet_string

    Request to send a packet (Py --> Android):
    0;next_hop_heaven_address;packet_string

    """

    def __init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler):
        LinkLayer.__init__(self, network_name, l2_to_l3_queue, neighbors, config_reader, profiler)
        self.rx_server_address = self.network_configs['rx_data_server_address']  # port used to receive data
        self.tx_server_address = self.network_configs['tx_data_server_address']  # port used to receive data
        self.neighbor_service = AndroidNeighborServiceIntf(self, neighbors, network_name, config_reader)
        # todo hardcoded params
        self.data_server = DataServer(self.rx_server_address, 4096, 100, self.l2_to_l3_queue, self.neighbors)
        self.data_client = DataClient(self.tx_server_address)

    def start(self):
        self.neighbor_service.start()
        self.data_server.start()
        self.data_client.start()

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} data server...'.format(self.network_name))
        self.data_server.stop()
        # self.data_server.join()
        logging.getLogger(__name__).debug('Stopping {} broadcast client...'.format(self.network_name))
        self.data_client.stop()
        # self.data_client.join()
        self.neighbor_service.stop()

    def send_unicast(self, packet, next_hop):
        self.data_client.send(packet, next_hop)

    def send_broadcast(self, packet):
        self.data_client.broadcast(packet)
