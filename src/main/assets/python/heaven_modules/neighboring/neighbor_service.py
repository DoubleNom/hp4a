from neighbors_params import NeighborsParams as neighp


class NeighborService(object):

    def __init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader):
        self.link_layer = link_layer
        self.neighbors = neighbors  # neighbors object
        self.network_name = network_name  # name of the network
        self.ip_address_parser = ip_address_parser
        self.config_reader = config_reader

        self.network_configs = config_reader.get_config_dict(self.network_name)
        self.neighp = neighp(config_reader)

    def start(self):
        pass

    def stop(self):
        pass

