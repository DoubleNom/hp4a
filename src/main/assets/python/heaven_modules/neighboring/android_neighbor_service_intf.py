import logging
import socket
from neighbor_service import NeighborService
from heaven_modules.common.standard_threads import HeavenSocketServer


class AndroidNeighboringServer(HeavenSocketServer):

    def __init__(self, server_address, neighbors, link_layer):
        HeavenSocketServer.__init__(self, server_address, 1024, 20,
                                    socket_family=socket.AF_UNIX, socket_type=socket.SOCK_DGRAM,
                                    name='Android Neighboring')

        self.neighbors = neighbors
        self.link_layer = link_layer

    def handle(self, x):
        heaven_address = x[0]
        self.neighbors.update_neighbor((heaven_address, '', '', 0, self.link_layer))


class AndroidNeighborServiceIntf(NeighborService):
    """
    This is a dummy neighbor service, since methods are implemented in Android.
    Neighbors are updated with data received with a socket from Android neighboring service.
    Receive the heaven address of neighbors each time they are updated/seen in the network
    """

    def __init__(self, link_layer, neighbors, network_name,  config_reader):
        NeighborService.__init__(self, link_layer, neighbors, network_name, None, config_reader)
        self.neighboring_server = AndroidNeighboringServer(
            self.network_configs['neighboring_server_address'], self.neighbors, self.link_layer)

    def start(self):
        self.neighboring_server.start()

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} neighboring service...'.format(self.network_name))
        self.neighboring_server.stop()
        # self.neighboring_server.join()
        logging.getLogger(__name__).debug('Stopped {} neighboring service...'.format(self.network_name))
