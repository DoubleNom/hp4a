
class NeighborsParams(object):
    """
    Params
    Average (or median) RSSI to distance (link weight) assignment:
    ---------------- RSSI = 0
    | DISTANCE_0   |
    ---------------- RSSI = RSSI_THRESH_1
    | DISTANCE_1   |
    ---------------- RSSI = RSSI_THRESH_2
    | DISTANCE_MAX |
    |              |

    The switch between two distances happens with an hysteresis.

    e.g. distance == DISTANCE_0, a new rssi arrives and the median value changes.
    If the new median value is smaller than RSSI_THRESH_1 - HYSTERESIS_VALUE, distance = DISTANCE_1

    e.g. distance == DISTANCE_1, a new rssi arrives and the median value changes.
    If the new median value is bigger than RSSI_THRESH_1 + HYSTERESIS_VALUE, distance = DISTANCE_0

    from https://www.tp-link.com/ca/products/details/Archer-T2UH.html#specifications
    5GHz:
    11a 6Mbps: -94dBm
    11a 54Mbps: -78dBm
    11n HT20 MCS0: -94dBm
    11n HT20 MCS7: -77dBm
    11n HT40 MCS0: -92dBm
    11n HT40 MCS7: -74dBm
    11ac VHT80 MCS0: -89dBm
    11ac VHT80 MCS9: -64dBm
    """

    REQUESTS_QUEUE_LEN = 20  # queue length of UDP servers
    REQUESTS_BUFFER_SIZE = 1024  # how many bytes are read from udp sockets
    REPLIES_BUFFER_SIZE = 1024  # how many bytes to read from tcp sockets
    N_OF_RSSI = 7  # number of RSSI to average (averaging history) to compute the weight/distance
    DEFAULT_RSSI = 0
    RSSI_THRESH_1 = -60
    RSSI_THRESH_2 = -80
    DISTANCE_0 = 1
    DISTANCE_1 = 10
    DISTANCE_MAX = 100
    HYSTERESIS_VALUE = 5

    def __init__(self, config_reader):
        ns = config_reader.get_config_dict('neighboring_service')
        self.blacklist = ns['blacklist'].split(',')
        self.neighboring_requests_timeout = float(ns['neighboring_requests_timeout'])
        self.neighboring_requests_interval = float(ns['neighboring_requests_interval'])
        self.neighboring_replies_check_interval = float(ns['neighboring_replies_check_interval'])
        self.neighboring_max_last_seen = float(ns['neighboring_max_last_seen'])
