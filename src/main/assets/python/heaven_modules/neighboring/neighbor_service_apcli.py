import socket
import logging
import time
from neighbor_service import NeighborService
from heaven_modules.common.standard_threads import PeriodicalCaller
from heaven_modules.common.network_tools import NetworkTools
from neighbors_params import NeighborsParams as neighp
from heaven_modules.common.utilities import get_udp_socket


class ApcliNeighborService(NeighborService):
    """
    Class in charge of starting all the threads that send neighboring requests and receive neighboring replies.
    Each link layer has its own neighboring service.
    """

    def __init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader):
        NeighborService.__init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader)

        # read network configuration
        self.intf = self.network_configs['interface']
        self.neighboring_requests_server_port = int(self.network_configs['neighboring_requests_server_port'])

        self.sock = None

        # read neighboring service configuration
        self.request_sender = PeriodicalCaller(self.send_neighboring_request, self.neighp.neighboring_requests_interval)

    def start(self):
        self.request_sender.start()

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} neighboring service...'.format(self.network_name))
        self.request_sender.stop()
        # self.request_sender.join()
        logging.getLogger(__name__).debug('Stopped {} neighboring service...'.format(self.network_name))

    def send_neighboring_request(self):

        while not self.ip_address_parser.is_network_configured(self.intf):
            time.sleep(2)

        # get gw ip address
        network_address = self.ip_address_parser.get_network_address(self.intf)
        gw_ip = NetworkTools.get_network_gw(network_address)
        gw_dest = (gw_ip, self.neighboring_requests_server_port)

        neigh_request = self.ip_address_parser.get_neighboring_str(self.intf)

        # print 'Sending Neighboring request {} to {}'.format(neigh_request, gw_dest)

        try:
            if self.sock is None:
                self.sock = get_udp_socket(gw_dest, self.neighp.neighboring_requests_timeout)

            self.sock.sendall(neigh_request)
            reply = str(self.sock.recv(neighp.REPLIES_BUFFER_SIZE))
            try:
                ts, heaven_address, ip_address = reply.split(';')
                if ip_address == gw_dest[0]:
                    self.neighbors.update_neighbor((heaven_address, ip_address, '', 0, self.link_layer))
            except ValueError:
                logging.getLogger(__name__).debug('Wrong neighboring reply format: {}'.format(reply))
                pass
        except (socket.error, socket.timeout) as e:
            print('{} - cannot send apcli neigh request to {}'.format(e, gw_dest))
            if self.sock is not None:
                self.sock.close()
                self.sock = None
