import logging
import socket
from neighbor_service import NeighborService
from neighbors_params import NeighborsParams as neighp
from heaven_modules.common.standard_threads import HeavenSocketServer
from heaven_modules.common.standard_threads import PeriodicalCaller
from heaven_modules.common.utilities import get_broadcast_udp_socket, get_udp_socket
from heaven_modules.common.network_tools import NetworkTools

"""
# ------------------------------------------------------------------------------------------------------------------- #
# If we can use broadcast addresses, we do not need an arp reader, but we can have only one thread that sends
# every X seconds a neighboring request in broadcast.
# The reply cannot be sent on the same port that sent the message,
# but we need a dedicated server to receive unicast neighboring replies.

AAAA, 192.168.10.7  |                                                      |    BBBB, 192.168.10.16
-----------------------------------------------------------------------------------------------------------------
1) udp client for broadcast n.req. .255
--------- N Request = tsA;AAAA;192.168.10.7 ------->
                                                2) udp server for broadcast n.req. (port 12345)

We add a "useless" reply just to make ARP work 
(otherwise by using only broadcast messages the arp table remains empty)

"""


class BroadcastNeighboringRequestsClient:
    """
    1) tx broadcast requests
    """
    RSSI_UPDATE_N = 3

    def __init__(self, neighboring_service):
        self.ns = neighboring_service
        self.periodical_caller = PeriodicalCaller(
            self.tx_request, self.ns.neighp.neighboring_requests_interval)
        self.sock = None
        self.rssi_update_counter = 0  # update rssi dict every X requests

    def start(self):
        self.periodical_caller.start()

    def stop(self):
        self.periodical_caller.stop()

    def tx_request(self):

        # if you cannot read your own ip address, do nothing
        if not self.ns.ip_address_parser.is_network_configured(self.ns.intf):
            return

        # update rssi every x times
        if not self.rssi_update_counter % self.RSSI_UPDATE_N:
            self.ns.update_rssi_dict()
        self.rssi_update_counter = (self.rssi_update_counter + 1) % self.RSSI_UPDATE_N

        req = self.ns.ip_address_parser.get_neighboring_str(self.ns.intf)

        try:
            if self.sock is None:
                self.sock = self.create_socket()
            self.sock.sendall(req)

        except (socket.error, socket.timeout):
            if self.sock is not None:
                self.sock.close()
                self.sock = None

    def create_socket(self):
        addr = (self.ns.ip_address_parser.get_broadcast_address(self.ns.intf), self.ns.broadcast_neighboring_requests_server_port)
        return get_broadcast_udp_socket(addr, self.ns.neighp.neighboring_requests_timeout)


class BroadcastNeighboringRequestsServer(HeavenSocketServer):
    """
    2) rx broadcasts requests and send UDP unicast replies SERVER
    # Note: this is important to make ARP work (it does not work only with broadcast msgs)
    """

    def __init__(self, neighboring_service, ip_address_parser, broadcast_neighboring_requests_server_port,
                 broadcast_neighboring_replies_server_port, neighboring_requests_timeout, blacklist,
                 link_layer, neighbors, intf):
        HeavenSocketServer.__init__(self, ('', broadcast_neighboring_requests_server_port),
                                    neighp.REQUESTS_BUFFER_SIZE, neighp.REQUESTS_QUEUE_LEN,
                                    socket_family=socket.AF_INET, socket_type=socket.SOCK_DGRAM,
                                    name='AdHoc Neighboring')

        self.neighboring_service = neighboring_service
        self.ip_address_parser = ip_address_parser
        self.broadcast_neighboring_replies_server_port = broadcast_neighboring_replies_server_port
        self.neighboring_requests_timeout = neighboring_requests_timeout
        self.blacklist = blacklist
        self.link_layer = link_layer
        self.neighbors = neighbors
        self.intf = intf

    def handle(self, x):

        neighboring_request, client_address = x
        client_ip_address, client_port = client_address  # socket (ip, port)

        logging.getLogger(__name__).debug(
            'BroadcastNeighboringRequestsServer RX from {}: {}'.format(client_address, neighboring_request))

        if not self.ip_address_parser.is_network_configured(self.intf):
            logging.getLogger(__name__).debug(
                '... BroadcastNeighboringRequestsServer cannot reply, network still not configured or read')
            return

        # discard messages from yourself
        if client_ip_address == self.ip_address_parser.get_ip_address(self.intf):
            return

        if client_ip_address in self.blacklist:
            return

        try:
            ts, neighbor_heaven_address, neighbor_ip_address = neighboring_request.split(';')
        except ValueError:
            logging.getLogger(__name__).exception(
                'Wrong broadcast neighboring request format {}'.format(neighboring_request))
            return

        # neighbor advertising a wrong ip address
        if neighbor_ip_address != client_ip_address:
            return

        # add rssi to the network update
        # if this ip is not in the rssi dict, force a rssi update
        if neighbor_ip_address not in self.neighboring_service.ip_rssi_dict:
            self.neighboring_service.update_rssi_dict()
        try:
            rssi = self.neighboring_service.ip_rssi_dict[neighbor_ip_address]
        except KeyError:
            rssi = -100

        self.neighbors.update_neighbor((neighbor_heaven_address, neighbor_ip_address, '', rssi, self.link_layer))

        # send an ack (or neighboring reply to the sender in unicast)
        # Note: this is important to make ARP work (it does not work only with broadcast msgs)
        sock = None
        try:
            sock = get_udp_socket(
                (client_ip_address, self.broadcast_neighboring_replies_server_port),
                self.neighboring_requests_timeout)
            sock.sendall('ack')
        except (socket.error, socket.timeout):
            if sock is not None:
                sock.close()


class AdHocNeighborService(NeighborService):
    """
    Class in charge of starting all the threads that send neighboring requests and receive neighboring replies.
    Each link layer has its own neighboring service with its own strategy.

    Adhoc strategy:

    1) Every node broadcasts periodically: ts;heaven_address;ip_address (NEIGHBORING_REQUEST)
    2) Every node receives NEIGHBORING_REQUESTS, checks them and update his neighbors.

    Note/Todo: This strategy assumes that if A can talk to B ==> B can talk to A

    \+ lightweight
    \- bi-directionality can fail?


    """

    def __init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader):
        NeighborService.__init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader)
        
        self.intf = self.network_configs['interface']
        self.intf_type = self.network_configs['interface_type']
        self.broadcast_neighboring_requests_server_port = int(self.network_configs['broadcast_neighboring_requests_server_port'])
        self.broadcast_neighboring_replies_server_port = int(self.network_configs['broadcast_neighboring_replies_server_port'])

        # Extra data: RSSI
        self.ip_rssi_dict = {}

        # 1) Send broadcast requests
        self.broadcast_neighboring_requests_client = BroadcastNeighboringRequestsClient(self)

        # 2) Read broadcast requests and send unicast replies (ack)
        # Note: unicast acks are important to make ARP work (it does not work only with broadcast msgs)
        self.broadcast_neighboring_requests_server = BroadcastNeighboringRequestsServer(
            self,
            self.ip_address_parser, self.broadcast_neighboring_requests_server_port,
            self.broadcast_neighboring_replies_server_port,
            self.neighp.neighboring_requests_timeout, self.neighp.blacklist,
            self.link_layer, self.neighbors, self.intf)

        # 3) Receive acks but do nothing with them
        self.dummy_server_for_acks = HeavenSocketServer(
            ('', self.broadcast_neighboring_replies_server_port),
            1024, 10, socket_family=socket.AF_INET, socket_type=socket.SOCK_DGRAM,
            name='AdHocNeighboring UnicastAckReceiver')

    def start(self):
        logging.getLogger(__name__).debug('Starting {} neighboring service...'.format(self.network_name))
        self.broadcast_neighboring_requests_client.start()
        self.broadcast_neighboring_requests_server.start()
        self.dummy_server_for_acks.start()
        logging.getLogger(__name__).debug('Started {} neighboring service...'.format(self.network_name))

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} neighboring service...'.format(self.network_name))
        self.broadcast_neighboring_requests_client.stop()
        self.broadcast_neighboring_requests_server.stop()
        self.dummy_server_for_acks.stop()
        logging.getLogger(__name__).debug('Stopped {} neighboring service...'.format(self.network_name))

    def update_rssi_dict(self):
        if self.intf_type == 'mt7610u':
            self.ip_rssi_dict = NetworkTools.get_ip_rssi_mt7610u(self.intf)
        elif self.intf_type == 'iw':
            self.ip_rssi_dict = NetworkTools.get_ip_rssi_iw(self.intf)

