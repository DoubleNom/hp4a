import socket
import logging
import time
from neighbor_service import NeighborService
from heaven_modules.common.standard_threads import PeriodicalCaller
from heaven_modules.common.network_tools import NetworkTools
from neighbors_params import NeighborsParams as neighp
from threading import RLock, Thread
import SocketServer


class NeighboringRequestsServerHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        
        try:
            neighboring_request, client_address = self.request[0], self.client_address
            client_ip_address, client_port = client_address  # socket (ip, port)
        except (TypeError, ValueError) as e:
            print 'socker error', e
            return

        sock = self.request[1]

        if not self.server.ip_address_parser.is_network_configured(self.server.intf):
            print 'AP NeighboringRequestsServer cannot reply, network still not configured or read'
            logging.getLogger(__name__).debug('... NeighboringRequestsServer cannot reply, network still not configured or read')
            return

        try:
            ts, neighbor_heaven_address, neighbor_ip_address = neighboring_request.split(';')
        except ValueError:
            logging.getLogger(__name__).debug('Wrong unicast neighboring request format {}'.format(neighboring_request))
            print 'Wrong unicast neighboring request format {}'.format(neighboring_request)
            return

        if neighbor_ip_address != client_ip_address:
            print 'wrong ip'
            return

        if neighbor_ip_address in self.server.blacklist:
            print 'blacklisted neighbor'
            return

        print 'AP Neighboring sending reply to', neighboring_request

        try:
            sock.sendto(self.server.ip_address_parser.get_neighboring_str(self.server.intf, ts), client_address)
        except (socket.error, socket.timeout):
            return

        ip_mac_dict = self.server.get_ip_mac_dict()

        # if the mac is not in the dict, force the update
        if neighbor_ip_address not in ip_mac_dict:
            ip_mac_dict = self.server.update_ip_mac_dict(forced=True)
            # print 'Forced ip-mac dict update, now it is {}'.format(ip_mac_dict)

        # if it is still not in the dict, discard the request
        if neighbor_ip_address not in ip_mac_dict:
            # print 'Cannot get the mac of {}, exiting'.format(neighbor_ip_address)
            return
        else:
            mac = ip_mac_dict[neighbor_ip_address]
            # print 'Processing connected node {}-{} as heaven neighbor {}'.format(
            #     neighbor_ip_address, mac, neighbor_heaven_address)

        self.server.neighbors.update_neighbor((neighbor_heaven_address, neighbor_ip_address, mac, 0, self.server.link_layer))
        

class NeighboringRequestsServer(SocketServer.ThreadingUDPServer):
    """
    # ---------------------------------------------------------------------------------------------------------------- #
    # 1) Receive unicast neighboring requests sent by connected devices with a UDP server
    # 2) Reply on the same socket with your IP address and HA
    # ---------------------------------------------------------------------------------------------------------------- #
    """

    def __init__(self, link_layer, neighbors, neighboring_requests_server_port, ip_address_parser,
                 blacklist, get_ip_mac_dict, update_ip_mac_dict, intf):

        SocketServer.ThreadingUDPServer.__init__(self, ('', neighboring_requests_server_port), NeighboringRequestsServerHandler)

        self.address = ('', neighboring_requests_server_port)
        self.link_layer = link_layer
        self.neighbors = neighbors
        self.blacklist = blacklist
        self.ip_address_parser = ip_address_parser
        self.get_ip_mac_dict = get_ip_mac_dict
        self.update_ip_mac_dict = update_ip_mac_dict
        self.intf = intf

        self.server_thread = Thread(target=self.serve_forever)

    def start(self):
        self.server_thread.start()
        print('UDP server listening on {} - AP neighboring'.format(self.address))

    def stop(self):
        self.server_close()
        self.shutdown()


class ApNeighborService(NeighborService):
    """
    Class in charge of starting all the threads that send neighboring requests and receive neighboring replies.
    Each link layer has its own neighboring service.
    """
    IP_NEIGH_INTERVAL = 2.0

    def __init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader):
        NeighborService.__init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader)

        self.intf = self.network_configs['interface']
        self.neighboring_requests_interval = self.neighp.neighboring_requests_interval
        self.neighboring_requests_server_port = int(self.network_configs['neighboring_requests_server_port'])

        self.ip_mac_dict = {}
        self.ip_mac_lock = RLock()

        # Receive unicast neighboring requests and reply with your data
        self.neighboring_requests_server = NeighboringRequestsServer(
            self.link_layer, self.neighbors,
            self.neighboring_requests_server_port, self.ip_address_parser, self.neighp.blacklist,
            self.get_ip_mac_dict, self.update_ip_mac_dict, self.intf)

        self.ip_mac_updater = PeriodicalCaller(self.update_ip_mac_dict, self.IP_NEIGH_INTERVAL)
        self.last_ping_ts = time.time()

    def start(self):
        self.neighboring_requests_server.start()
        self.ip_mac_updater.start()

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} neighboring service...'.format(self.network_name))
        self.neighboring_requests_server.stop()
        self.ip_mac_updater.stop()
        logging.getLogger(__name__).debug('Stopped {} neighboring service...'.format(self.network_name))

    @staticmethod
    def check_connectivity(ip_address, intf, results):
        results[ip_address] = NetworkTools.ping_node(ip_address, intf=intf)

    def get_ip_mac_dict(self):
        return self.ip_mac_dict.copy()

    def update_ip_mac_dict(self, forced=False):
        """
        We need to be very fast in detecting new connected devices
        :param forced: True if called by the neighboring request sender, False if periodic
        :return:
        """
        with self.ip_mac_lock:
            old_connected_ips = set(self.ip_mac_dict.keys())
            ip_mac_status_tuples = NetworkTools.get_ap_clients(self.intf)
            self.ip_mac_dict = {t[0]: t[1] for t in ip_mac_status_tuples}

        if forced:
            return self.ip_mac_dict.copy()

        connected_ips = set(self.ip_mac_dict.keys())

        # signal new neighbors asap
        new_connected_ips = connected_ips - old_connected_ips
        for ip in new_connected_ips:
            self.neighbors.update_connected_device((ip, self.ip_mac_dict[ip], self.link_layer))

        # do not ping devices too often
        now = time.time()
        if now - self.last_ping_ts <= self.neighboring_requests_interval:
            return

        self.last_ping_ts = now

        # probe other neighbors with ping
        ips_to_probe = connected_ips - new_connected_ips
        ip_to_probe_results = {ip: False for ip in ips_to_probe}

        # Ping in parallel so not block for too many seconds
        ping_threads = []
        for ip in ip_to_probe_results:
            ping_thread = Thread(target=ApNeighborService.check_connectivity, args=[ip, self.intf, ip_to_probe_results])
            ping_thread.start()
            ping_threads.append(ping_thread)
        for ping_thread in ping_threads:
            ping_thread.join()
        # print 'Ping results = {}'.format(ip_to_probe_results)

        # update neighbors for non-heaven nodes that replied to the ping
        for ip, result in ip_to_probe_results.iteritems():
            if result:
                self.neighbors.update_connected_device((ip, self.ip_mac_dict[ip], self.link_layer))

    def delete_ip_neigh(self, ip_address):
        print '********* Not Deleting {} from ip neigh ***********'.format(ip_address)
        # NetworkTools.delete_ip_neigh(ip_address, self.intf)
