import time
import threading
import Queue
import logging
from collections import deque
from heaven_modules.common.standard_threads import PeriodicalCaller, EventsSerializer
from heaven_modules.layer3.routing_neighbors import RoutingNeighbor, RoutingNeighbors
from neighbors_params import NeighborsParams as neighp
from heaven_modules.common.common_parameters import CommonParameters as cp


class Neighbors:
    """
    Manage the collection of neighbors
    A neighbor is indexed by
    - heaven address (if any)
    - ip (if any)
    - mac (if any)
    Neighbors are stored in a list to count them and iterate.

    """

    def __init__(self, config_reader):

        self.neighbors_ha = {}  # {ha: NeighborNode}
        self.neighbors_ip = {}  # {ip: NeighborNode}
        self.neighbors_mac = {}  # {mac: NeighborNode}
        self.neighbors_list = []  # list of all neighbors [NeighborNode]
        self.blocked_neighbors = set()  # list of heaven_addresses
        self.n_lock = threading.RLock()  # protection for collections iteration and modification

        # events serializer for updates and deletion of neighbors
        self.events_serializer = EventsSerializer(Queue.Queue())
        self.events_serializer.start()

        # ------------- start a thread to clean old neighbors ------------- #
        ns = config_reader.get_config_dict('neighboring_service')
        self.neighboring_replies_check_interval = float(ns['neighboring_replies_check_interval'])
        self.neighbor_max_last_seen = float(ns['neighboring_max_last_seen'])
        self.neighbors_cleaner = PeriodicalCaller(self.delete_inactive_neighbors,
                                                  self.neighboring_replies_check_interval)
        self.neighbors_cleaner.setName('neighbors cleaner')
        self.neighbors_cleaner.start()
        # ----------------------------------------------------------------- #
        self.updates_listener = None  # a routing layer function to call when neighbors change

    def stop(self):
        self.neighbors_cleaner.stop()
        self.events_serializer.stop()

        # stop the unicast sender to heaven neighbors
        with self.n_lock:
            for neigh in self.neighbors_list:
                neigh.stop()

    # --------------------------------------- GET -------------------------------------------------------------------- #

    def block_neighbor(self, heaven_address):
        deleted = False
        with self.n_lock:
            # prevent it from becoming a neighbor again and to be updated
            self.blocked_neighbors.add(heaven_address)

            if heaven_address in self.neighbors_ha:
                print 'Deleting {} from neighbors'.format(heaven_address)
                deleted = True
                neigh = self.neighbors_ha[heaven_address]
                self.neighbors_ha[heaven_address].heaven_address = ''
                del self.neighbors_ha[heaven_address]

                # A neighbor can be deleted as heaven neighbor but remain as connected device
                if neigh.link_layer.network_name != cp.AP_NETWORK_NAME:
                    del self.neighbors_ip[neigh.ip_address]
                    self.neighbors_list.remove(neigh)

                neigh.stop()
            else:
                print '{} cannot not become a neighbor'.format(heaven_address)

        if deleted:
            self.do_signal_change()

        return True

    def unblock_neighbor(self, heaven_address):
        with self.n_lock:
            if heaven_address in self.blocked_neighbors:
                self.blocked_neighbors.remove(heaven_address)
        return True

    def ha_in_neighbors(self, heaven_address):
        return heaven_address in self.neighbors_ha

    def get_neighbors_strings(self):
        """
        Call for the API
        :return:
        """
        with self.n_lock:
            return [str(neigh) for neigh in self.neighbors_list]

    def get_neighbor_by_ha(self, heaven_address):
        """
        Used by link layer wrapper to send the packet only to the right link layer
        :param heaven_address:
        :return:
        """
        try:
            return self.neighbors_ha[heaven_address]
        except KeyError:
            return None

    def get_heaven_addresses(self):
        """
        return the list of neighbors heaven addresses
        :return: e.g. ['aaaa', 'bbbb']
        """
        return self.neighbors_ha.keys()

    def get_neighbor_by_ip(self, ip):
        """
        Return the neighbor having the specified ip address, None if not found
        :param ip: e.g. '192.168.10.10'
        :return: e.g. 'aaaa'
        """
        try:
            return self.neighbors_ip[ip]
        except KeyError:
            return None

    def get_mac_to_ip_map(self):
        with self.n_lock:
            return {n.mac_address: n.ip_address for n in self.neighbors_mac.values() if n.mac_address}

    def get_ip_mac_dict(self):
        with self.n_lock:
            return {n.ip_address: n.mac_address for n in self.neighbors_mac.values()}

    def get_ha_to_ip_map(self, network_name=None):
        """
        Return the mapping {ha:ip} for the neighbors of the specified network
        Called by iptables rules setter
        :param network_name:
        :return:
        # todo neighbors on many link layers
        """
        with self.n_lock:
            if network_name:
                return {ha: neigh.ip_address for ha, neigh in self.neighbors_ha.iteritems()
                        if neigh.link_layer.network_name == network_name}
            else:
                return {ha: neigh.ip_address for ha, neigh in self.neighbors_ha.iteritems()}

    def get_mac_list_of_heaven_nodes(self):
        """
        Return the list of mac addresses of heaven nodes.
        Used by AP neighbor service to update the ip mac dict
        :return:
        """
        with self.n_lock:
            return [neigh.mac_address for neigh in self.neighbors_ha.itervalues() if neigh.mac_address]

    def get_neighbors_ip_on_link_layer(self, link_layer_name):
        """
        :return: list of ip of neighbors on a certain link layer name
        """
        with self.n_lock:
            return [neigh.ip_address for neigh in self.neighbors_list
                    if neigh.link_layer.network_name == link_layer_name]

    def get_neighbor_link_layer(self, heaven_address):
        try:
            return self.neighbors_ha[heaven_address].link_layer
        except KeyError:
            return None

    def get_ip_by_ha(self, heaven_address):

        if heaven_address in self.neighbors_ha:
            return self.neighbors_ha[heaven_address].ip_address
        else:
            return ''

    # ----------------------------------------------- SET ------------------------------------------------------------ #

    def update_last_seen_ts(self, heaven_address):
        try:
            self.neighbors_ha[heaven_address].last_seen_ts = time.time()
        except KeyError:
            pass

    def update_last_ping_ts(self, mac_address):
        try:
            self.neighbors_mac[mac_address].last_ping_ts = time.time()
        except KeyError:
            pass

    def update_neighbor(self, x):
        """
        Called by neighboring services when a new neighboring reply is received
        x =  heaven_address, ip_address, mac_address, rssi, link_layer
        """
        self.events_serializer.enqueue(None, self.do_update_neighbor, x)

    def update_connected_device(self, x):
        """
        Called by neighboring services when a new neighboring reply is received
        x =  heaven_address, ip_address, mac_address, rssi, link_layer
        """
        self.events_serializer.enqueue(None, self.do_update_connected_device, x)

    def delete_inactive_neighbors(self):
        self.events_serializer.enqueue(None, self.do_delete_inactive_neighbors)

    def remove_neighbors_ip_set(self, ip_set):
        """
        Called by multi tree routing layer when it receives a LSP advertising a non-heaven node X connected to lsp.source
        who is a neighbor but now is connected to another node
        :param ip_set: set of ip addresses of the neighbors to remove
        :return:
        """
        self.events_serializer.enqueue(None, self.do_remove_neighbors_ip_set, ip_set)

    def do_remove_neighbors_ip_set(self, ip_set):
        """
        :param ip_set: ip_set: set of ip addresses of the neighbors to remove
        :return:
        """
        with self.n_lock:
            neighbors_to_delete = [neigh for neigh in self.neighbors_ip.values() if neigh.ip_address in ip_set]
            for neigh in neighbors_to_delete:

                if neigh.heaven_address:
                    del self.neighbors_ha[neigh.heaven_address]

                if neigh.ip_address:
                    del self.neighbors_ip[neigh.ip_address]

                if neigh.mac_address:
                    del self.neighbors_mac[neigh.mac_address]

                self.neighbors_list.remove(neigh)

                if neigh.link_layer.network_name == cp.AP_NETWORK_NAME:
                    neigh.link_layer.neighbor_service.delete_ip_neigh(neigh.ip_address)

        for neigh in neighbors_to_delete:
            neigh.stop()
            logging.getLogger(__name__).debug('Stopped and deleted neighbor {}'.format(neigh.get_id()))

        if neighbors_to_delete:
            # print 'Neighbors deleted: {}'.format([str(n) for n in neighbors_to_delete])
            self.do_signal_change()

    def do_update_neighbor(self, x):
        """
        Called only by event serializer
        Add new neighbors and update others
        :param x: a tuple (heaven_address, ip_address, mac_address, rssi, link_layer)
        :return:
        """
        logging.getLogger(__name__).debug('Updating neighbor with {} ... '.format(x))
        heaven_address, ip_address, mac_address, rssi, link_layer = x
        # print 'Updating neighbor ({}|{}|{}|{})'.format(heaven_address, ip_address, mac_address, rssi)
        new_neighbor = False
        ip_changed = False
        distance_changed = False

        if not heaven_address:
            return

        with self.n_lock:

            if heaven_address in self.blocked_neighbors:
                return

            # new heaven node
            if heaven_address not in self.neighbors_ha:

                # check if before this, the node was a connected device without heaven
                if ip_address in self.neighbors_ip:
                    neigh = self.neighbors_ip[ip_address]
                    neigh.heaven_address = heaven_address
                    self.neighbors_ha[heaven_address] = neigh
                else:  # brand new neighbor
                    if link_layer.network_name == cp.AD_HOC_NETWORK_NAME:
                        neigh = AdHocNeighbor(heaven_address, ip_address, mac_address, link_layer)
                    else:
                        neigh = Neighbor(heaven_address, ip_address, mac_address, link_layer)
                    self.neighbors_list.append(neigh)
                    self.neighbors_ha[heaven_address] = neigh
                    if ip_address:
                        self.neighbors_ip[ip_address] = neigh
                    if mac_address:
                        self.neighbors_mac[mac_address] = neigh
                new_neighbor = True

            # new ip address for a heaven node
            elif self.neighbors_ha[heaven_address].ip_address != ip_address:
                old_ip = self.neighbors_ha[heaven_address].ip_address
                self.neighbors_ha[heaven_address].ip_address = ip_address
                del self.neighbors_ip[old_ip]
                self.neighbors_ip[ip_address] = self.neighbors_ha[heaven_address]
                ip_changed = True

        if new_neighbor or ip_changed:
            logging.getLogger(__name__).debug('Signalling new neighbor {}'.format(heaven_address))
            self.neighbors_ha[heaven_address].create_unicast_client(ip_address)

        self.update_last_ping_ts(mac_address)
        self.update_last_seen_ts(heaven_address)

        # see if distance changed
        if rssi and heaven_address:
            distance_changed = self.neighbors_ha[heaven_address].update_rssi(rssi)

        logging.getLogger(__name__).debug('new_neighbor: {}, ip_changed: {}, distance_changed: {}'.format(
            new_neighbor, ip_changed, distance_changed))

        if new_neighbor or ip_changed or distance_changed:
            self.do_signal_change()

    def do_update_connected_device(self, x):
        """
        Called only by event serializer
        Add new neighbors and update others
        :param x: a tuple (ip_address, mac_address, link_layer)
        :return:
        """
        logging.getLogger(__name__).debug('Updating neighbor with {} ... '.format(x))
        ip_address, mac_address, link_layer = x
        # print 'Updating device ({}|{})'.format(ip_address, mac_address)
        new_neighbor = False
        ip_changed = False

        with self.n_lock:

            # new connected device
            if mac_address not in self.neighbors_mac:
                neigh = Neighbor('', ip_address, mac_address, link_layer)
                self.neighbors_list.append(neigh)
                self.neighbors_mac[mac_address] = neigh
                self.neighbors_ip[ip_address] = neigh
                new_neighbor = True

            # new ip for a connected device
            elif self.neighbors_mac[mac_address].ip_address != ip_address:
                old_ip = self.neighbors_mac[mac_address].ip_address
                self.neighbors_mac[mac_address].ip_address = ip_address
                del self.neighbors_ip[old_ip]
                self.neighbors_ip[ip_address] = self.neighbors_mac[mac_address]
                ip_changed = True

        self.update_last_ping_ts(mac_address)

        if new_neighbor or ip_changed:
            self.do_signal_change()

    def do_delete_inactive_neighbors(self):
        """
        Called periodically by neighbors_cleaner
        :return:
        """
        with self.n_lock:
            now = time.time()

            # delete heaven nodes
            # print 'Neighbors_ha: {}'.format({k: str(v) for k, v in self.neighbors_ha.iteritems()})
            neighbors_to_delete = [neigh for neigh in self.neighbors_ha.values()
                                   if neigh.last_seen_ts < now - self.neighbor_max_last_seen]
            for neigh in neighbors_to_delete:
                ha = neigh.heaven_address
                # print 'Deleting neighbor {}'.format(ha)
                try:
                    neigh.heaven_address = ''
                    del self.neighbors_ha[ha]
                    # print 'Neighbor removed from neighbors_ha'

                    # A neighbor can be deleted as heaven neighbor but remain as connected device
                    if neigh.link_layer.network_name != cp.AP_NETWORK_NAME:
                        del self.neighbors_ip[neigh.ip_address]
                        # print 'Neighbor removed from neighbors_ip'
                        self.neighbors_list.remove(neigh)
                        # print 'Neighbor removed from neighbors_list'

                except KeyError:
                    print '\n########## Key Error while deleting neighbor ########################'
                    print 'NHAs: {}, NIPs: {}, NMACs:{}, triggering neighbor:{}'.format(
                        self.neighbors_ha.keys(),
                        self.neighbors_ip.keys(),
                        self.neighbors_mac.keys(),
                        neigh)
                    print '#####################################################################\n'
                    pass

            devices_to_delete = [neigh for neigh in self.neighbors_mac.values()
                                 if neigh.last_ping_ts < now - self.neighbor_max_last_seen]
            for neigh in devices_to_delete:

                if neigh.heaven_address:
                    del self.neighbors_ha[neigh.heaven_address]
                del self.neighbors_ip[neigh.ip_address]
                del self.neighbors_mac[neigh.mac_address]
                self.neighbors_list.remove(neigh)
                neigh.link_layer.neighbor_service.delete_ip_neigh(neigh.ip_address)

        for neigh in neighbors_to_delete:
            neigh.stop()
            logging.getLogger(__name__).debug('Stopped and deleted neighbor {}'.format(neigh.get_id()))

        if neighbors_to_delete or devices_to_delete:
            # print 'Neighbors deleted: {}'.format([str(n) for n in neighbors_to_delete])
            # print 'Devices deleted: {}'.format([str(n) for n in devices_to_delete])
            self.do_signal_change()

    # ----------------------------------------------- SEND ----------------------------------------------------------- #

    def send(self, heaven_address, packet):
        try:
            self.neighbors_ha[heaven_address].send(packet)
        except (KeyError, AttributeError) as e:
            print 'Cannot send {} to {}: {}'.format(packet, heaven_address, e)
            pass

    def send_to_all(self, network_name, packet):

        with self.n_lock:
            all_ha = [ha for ha, neigh in self.neighbors_ha.iteritems()
                      if neigh.link_layer.network_name == network_name]

        for heaven_address in all_ha:
            self.send(heaven_address, packet)

    # ------------------------------------- ROUTING LAYER ------------------------------------------------------------ #

    def do_signal_change(self):
        """
        Called by delete_inactive_neighbors and update_neighbor when a neighbor changes.
        It passes the new RoutingNeighbors collection to the routing layer
        :return:
        """
        if self.updates_listener is None:
            return

        with self.n_lock:
            routing_neighbors = RoutingNeighbors([neigh.get_routing_neighbor() for neigh in self.neighbors_list])
            # print 'Updated RoutingNeighbors: {}'.format(routing_neighbors)

        try:
            self.updates_listener(routing_neighbors)
        except TypeError:
            pass

    def __str__(self):
        # do not use locks for prints! get the list of keys ant try
        my_str = 'Neighbors: \n'
        for neighbor in self.neighbors_list[:]:
            try:
                my_str += str(neighbor) + '\n'
            except:
                pass
        return my_str

    def repr_for_test(self):
        return self.__dict__


class Neighbor(object):

    def __init__(self, heaven_address, ip_address, mac_address, link_layer):
        self.heaven_address = heaven_address
        self.ip_address = ip_address
        self.link_layer = link_layer  # reference to the link layer, used to get socket parameters
        self.mac_address = mac_address
        self.last_seen_ts = time.time()
        self.last_ping_ts = time.time()
        self.distance = neighp.DISTANCE_0
        self.unicast_client = None

    def stop(self):
        if self.unicast_client is not None:
            self.unicast_client.stop()

    def send(self, packet):
        """
        Function called by Neighbors when the link layer has a packet to send
        :param packet:
        :return:
        """
        self.unicast_client.send(packet)

    def __str__(self):
        return '[ha:{}, ip:{}, mac:{}, dist:{}, ll: {}, last_seen:{:.3f}s ago]'.format(
            self.heaven_address,
            self.ip_address,
            self.mac_address,
            self.distance,
            self.link_layer.network_name,
            time.time() - self.last_seen_ts)

    # ----------------------------------------------- GET ------------------------------------------------------------ #

    def get_id(self):
        return '{}:{}:{}'.format(self.heaven_address, self.ip_address, self.mac_address)

    # ----------------------------------------------- SET ------------------------------------------------------------ #

    def get_dist(self, rssi):
        return self.distance

    def update_rssi(self, rssi):
        return False

    def create_unicast_client(self, ip_address):
        if self.unicast_client:
            self.unicast_client.stop()
        self.unicast_client = self.link_layer.create_unicast_client(ip_address, self.heaven_address)
        self.unicast_client.start()

    def get_routing_neighbor(self):
        """
        Return the RoutingNeighbor corresponding to self
        :return:
        """
        return RoutingNeighbor(self.heaven_address, self.ip_address, self.mac_address, self.distance)


class AdHocNeighbor(Neighbor):
    """
    Version of Neighbor to consider the distance.
    """

    def __init__(self, heaven_address, ip_address, mac_address, link_layer):
        Neighbor.__init__(self, heaven_address, ip_address, mac_address, link_layer)

        # Append to the right. When maxlen is reached, the append will also remove an element from the left
        self.rssi_deque = deque(maxlen=neighp.N_OF_RSSI)
        self.distance = neighp.DISTANCE_MAX

    def get_median_rssi(self):
        rssi_list = list(self.rssi_deque)
        if rssi_list:
            rssi_list.sort()
            return rssi_list[len(rssi_list) / 2]
        else:
            return 0

    def get_dist(self, rssi):
        """
        :param rssi: new median or average rssi value
        :return: the tuple distance, upper threshold, lower threshold w.r.t. the new distance

        ---------------- RSSI = 0
        | DISTANCE_0   |
        ---------------- RSSI = RSSI_THRESH_1
        | DISTANCE_1   |
        ---------------- RSSI = RSSI_THRESH_2
        | DISTANCE_MAX |

        e.g.
        rssi in DISTANCE_0 range: (DISTANCE_0, 0, RSSI_THRESH_1)
        rssi in DISTANCE_1 range: (DISTANCE_1, RSSI_THRESH_1, RSSI_THRESH_2)
        rssi in DISTANCE_2 range: (DISTANCE_2, RSSI_THRESH_2, 0)
        """
        if rssi > neighp.RSSI_THRESH_1:
            return neighp.DISTANCE_0, 0, neighp.RSSI_THRESH_1
        elif rssi > neighp.RSSI_THRESH_2:
            return neighp.DISTANCE_1, neighp.RSSI_THRESH_1, neighp.RSSI_THRESH_2
        else:
            return neighp.DISTANCE_MAX, neighp.RSSI_THRESH_2, -1000

    def update_rssi(self, rssi):
        """
        Add the new RTT to the deque.
        :param rssi: string e.g. '-79'
        :return: True if the distance changes, False otherwise
        If True, the routing layer will be notified
        """
        # once maxlen is reached, the oldest value is pushed out
        self.rssi_deque.append(int(rssi))
        med_rssi = self.get_median_rssi()
        new_dist, upper_thr, lower_thr = self.get_dist(med_rssi)

        # case 1: better channel condition = bigger rssi = new distance < old
        # case 2: worse channel condition = smaller rssi = new distance > old
        # update only with hysteresis
        if ((new_dist < self.distance and med_rssi > lower_thr + neighp.HYSTERESIS_VALUE) or
                (new_dist > self.distance and med_rssi < upper_thr - neighp.HYSTERESIS_VALUE)):
            logging.getLogger(__name__).debug('{} distance changed: {} --> {}, rssi = {}'.format(
                self.heaven_address, self.distance, new_dist, list(self.rssi_deque)))
            self.distance = new_dist
            return True

        return False
