import logging
import socket
from neighbor_service import NeighborService
from heaven_modules.common.standard_threads import HeavenSocketServer, PeriodicalCaller
from heaven_modules.common.utilities import get_broadcast_udp_socket, get_udp_socket
from neighbors_params import NeighborsParams as neighp

"""
# ------------------------------------------------------------------------------------------------------------------- #
# If we can use broadcast addresses, we do not need an arp reader, but we can have only one thread that sends
# every X seconds a neighboring request in broadcast.
# The reply cannot be sent on the same port that sent the message,
# but we need a dedicated server to receive unicast neighboring replies.

AAAA, 192.168.10.7  |                                                      |    BBBB, 192.168.10.16
-----------------------------------------------------------------------------------------------------------------
1) udp client for broadcast n.req.
--------- N Request = tsA;AAAA;192.168.10.7 ------->
                                                2) udp server for broadcast n.req. (port 12345)

We add a "useless" reply just to make ARP work 
(otherwise by using only broadcast messages the arp table remains empty)

"""


class BroadcastNeighboringRequestsClient:
    """
    1) tx broadcast requests
    """

    def __init__(self, ip_address_parser, broadcast_neighboring_requests_server_port,
                 neighboring_requests_timeout, neighboring_sending_interval, intf):
        self.ip_address_parser = ip_address_parser
        self.broadcast_neighboring_requests_server_port = broadcast_neighboring_requests_server_port
        self.neighboring_requests_timeout = neighboring_requests_timeout
        self.periodical_caller = PeriodicalCaller(self.tx_request, neighboring_sending_interval)
        self.intf = intf
        self.sock = None

    def start(self):
        self.periodical_caller.start()

    def stop(self):
        self.periodical_caller.stop()

    def tx_request(self):

        # if you cannot read your own ip address, do nothing
        if not self.ip_address_parser.is_network_configured(self.intf):
            return

        req = self.ip_address_parser.get_neighboring_str(self.intf)

        try:
            if self.sock is None:
                self.sock = self.create_socket()
            self.sock.sendall(req)
        except (socket.error, socket.timeout):
            if self.sock is not None:
                self.sock.close()
                self.sock = None

    def create_socket(self):
        addr = (self.ip_address_parser.get_broadcast_address(self.intf), self.broadcast_neighboring_requests_server_port)
        return get_broadcast_udp_socket(addr, self.neighboring_requests_timeout)


class BroadcastNeighboringRequestsServer(HeavenSocketServer):
    """
    2) rx broadcasts requests and send UDP unicast replies SERVER
    # Note: this is important to make ARP work (it does not work only with broadcast msgs)
    """

    def __init__(self, ip_address_parser, broadcast_neighboring_requests_server_port,
                 broadcast_neighboring_replies_server_port, neighboring_requests_timeout, blacklist,
                 link_layer, neighbors, intf):
        HeavenSocketServer.__init__(self, ('', broadcast_neighboring_requests_server_port),
                                    neighp.REQUESTS_BUFFER_SIZE, neighp.REQUESTS_QUEUE_LEN,
                                    socket_family=socket.AF_INET,
                                    socket_type=socket.SOCK_DGRAM,
                                    name='Ethernet Neighboring')

        self.ip_address_parser = ip_address_parser
        self.broadcast_neighboring_replies_server_port = broadcast_neighboring_replies_server_port
        self.neighboring_requests_timeout = neighboring_requests_timeout
        self.blacklist = blacklist
        self.link_layer = link_layer
        self.intf = intf
        self.neighbors = neighbors

    def handle(self, x):

        neighboring_request, client_address = x
        client_ip_address, client_port = client_address  # socket (ip, port)

        logging.getLogger(__name__).debug(
            'BroadcastNeighboringRequestsServer RX from {}: {}'.format(client_address, neighboring_request))

        if not self.ip_address_parser.is_network_configured(self.intf):
            logging.getLogger(__name__).debug(
                '... BroadcastNeighboringRequestsServer cannot reply, network still not configured or read')
            return

        # discard messages from yourself
        if client_ip_address == self.ip_address_parser.get_ip_address(self.intf):
            return

        if client_ip_address in self.blacklist:
            return

        try:
            ts, neighbor_heaven_address, neighbor_ip_address = neighboring_request.split(';')
        except ValueError:
            logging.getLogger(__name__).exception(
                'Wrong broadcast neighboring request format {}'.format(neighboring_request))
            return

        # neighbor advertising a wrong ip address
        if neighbor_ip_address != client_ip_address:
            return

        self.neighbors.update_neighbor((neighbor_heaven_address, neighbor_ip_address, '', 0, self.link_layer))

        # send an ack (or neighboring reply to the sender in unicast)
        # Note: this is important to make ARP work (it does not work only with broadcast msgs)
        sock = None
        try:
            sock = get_udp_socket(
                (client_ip_address, self.broadcast_neighboring_replies_server_port),
                self.neighboring_requests_timeout)
            sock.sendall('ack')
        except (socket.error, socket.timeout):
            if sock is not None:
                sock.close()


class EthernetNeighborService(NeighborService):
    """
    Class in charge of starting all the threads that send neighboring requests and receive neighboring replies.
    Each link layer has its own neighboring service.
    """

    def __init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader):
        NeighborService.__init__(self, link_layer, neighbors, network_name, ip_address_parser, config_reader)

        # Read broadcast specific configurations
        self.intf = self.network_configs['interface']
        self.broadcast_neighboring_requests_server_port = int(self.network_configs['broadcast_neighboring_requests_server_port'])
        self.broadcast_neighboring_replies_server_port = int(self.network_configs['broadcast_neighboring_replies_server_port'])

        # 1) Send broadcast requests
        self.broadcast_neighboring_requests_client = BroadcastNeighboringRequestsClient(
            self.ip_address_parser, self.broadcast_neighboring_requests_server_port,
            self.neighp.neighboring_requests_timeout, self.neighp.neighboring_requests_interval, self.intf)
        # 2) Read broadcast requests and send unicast replies
        self.broadcast_neighboring_requests_server = BroadcastNeighboringRequestsServer(
            self.ip_address_parser, self.broadcast_neighboring_requests_server_port,
            self.broadcast_neighboring_replies_server_port,
            self.neighp.neighboring_requests_timeout, self.neighp.blacklist,
            self.link_layer, self.neighbors, self.intf)

    def start(self):
        logging.getLogger(__name__).debug('Starting {} neighboring service...'.format(self.network_name))
        self.broadcast_neighboring_requests_client.start()
        self.broadcast_neighboring_requests_server.start()
        logging.getLogger(__name__).debug('Started {} neighboring service...'.format(self.network_name))

    def stop(self):
        logging.getLogger(__name__).debug('Stopping {} neighboring service...'.format(self.network_name))
        self.broadcast_neighboring_requests_client.stop()
        self.broadcast_neighboring_requests_server.stop()
        logging.getLogger(__name__).debug('Stopped {} neighboring service...'.format(self.network_name))
