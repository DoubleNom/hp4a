import socket
import pprint
import random
import time
from heaven_drones.api.heaven_api_commands import Commands as hc
from heaven_drones.common.utilities import IdGenerator
from heaven_drones.api.heaven_api_replies import Replies
from heaven_drones.common.tail_drop_queue import TailDropQueue
from heaven_drones.common.standard_threads import QueueServer
from heaven_drones.common.config_reader import ConfigReader
from heaven_drones.api.heaven_api_client import HeavenClient


def get_heaven_api_client(config_reader, my_ip='localhost'):

    socket_family = config_reader.get_config_dict('api_server')['api_server_socket_family']
    socket_address = config_reader.get_config_dict('api_server')['api_server_socket_address']

    if socket_family == 'af_inet':
        socket_family = socket.AF_INET
        ip, port = socket_address.split(':')
        socket_address = (ip, int(port))
    elif socket_family == 'af_unix':
        socket_family = socket.AF_UNIX
    else:
        print('Socket family not valid')
        return None

    if socket_family == socket.AF_INET:
        # we use a while because we pick a source port/filename randomly
        while 1:
            port = int(random.randint(1000, 8000))
            try:
                heaven_client = HeavenClient(socket_address, (my_ip, port), config_reader, socket_family)
                return heaven_client
            except socket.error as e:
                # print('Socket error, cannot connect to the server or create the client server: {}'.format(e))
                time.sleep(2)
    else:
        while 1:
            num = int(random.randint(1000, 8000))
            client_socket_address = 'api_client_{}.socket'.format(num)

            # if the file is already in use, try again
            if os.path.isfile(client_socket_address):
                continue

            try:
                heaven_client = HeavenClient(socket_address, client_socket_address, config_reader, socket_family)
                return heaven_client
            except socket.error as e:
                # print('Socket error, cannot connect to the server: {}'.format(e))
                # you (still) cannot reach the server, so the file created will not be used
                try:
                    os.remove(client_socket_address)
                except OSError:
                    pass
                time.sleep(2)


class PrintCallbackReceiver(QueueServer):

    def __init__(self):
        QueueServer.__init__(self, TailDropQueue())
        self.pp = pprint.PrettyPrinter(indent=2)
        self.requests = set()
        self.request_id_generator = IdGenerator(5, 0, 0)
        self.start()

    def receive_packet(self, x):
        self.put(x)

    def handle(self, x):
        reply = x
        request_id = reply[Replies.KEY_REQUEST_ID]

        if Replies.KEY_DELETE_REQUEST in reply and request_id in self.requests:
            self.requests.remove(request_id)
            self.request_id_generator.del_id(request_id)
            return

        self.pp.pprint(reply)

    def get_request_id(self):
        request_id = self.request_id_generator.get_id()
        self.requests.add(request_id)
        return request_id


if __name__ == '__main__':
    import ConfigParser
    import os

    # read the configuration file to know the socket address of the heaven api server
    this_folder = os.path.dirname(os.path.abspath(__file__))
    config_reader = ConfigReader(this_folder + '/heaven_config.ini')

    # get a client
    heaven_client = get_heaven_api_client(config_reader)
    if heaven_client is None:
        exit(0)

    # print all callback messages
    receiver = PrintCallbackReceiver()
    my_callback = receiver.receive_packet

    # get the image folder, if any
    try:
        image_folder = config_reader.get('test', 'image_folder')
    except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
        print('Image folder not configured, you will have to specify the full image path.')
        image_folder = ''

    # read commands from the user stdin
    command_str = ''
    try:
        while True:
            print('Active request: {}'.format(receiver.requests))

            try:

                command_str = str(raw_input('''
--------------------------------------------------------------------
Type your command followed by parameters, separated by spaces

method = gossip = 0 / mtree = 1

 > '''))

                command_str_split = command_str.split(' ')
                command = command_str_split[0]
                params = command_str_split[1:]

                if command == 'exit':
                    break

                # -------------------------- HEAVEN ---------------------------- #

                elif command == hc.GET_HEAVEN_ADDRESS:
                    print(heaven_client.get_heaven_address(receiver.get_request_id(), my_callback))
                elif command == hc.STOP_HEAVEN:
                    print(heaven_client.stop_heaven(receiver.get_request_id(), my_callback))

                # -------------------------- NETWORK --------------------------- #

                elif command == hc.GET_PEERS:
                    print(heaven_client.get_peers(receiver.get_request_id(), my_callback))
                elif command == hc.GET_PEERS_UPDATES:
                    print(heaven_client.get_peers_updates(receiver.get_request_id(), my_callback))
                elif command == hc.GET_NEIGHBORS:
                    print(heaven_client.get_neighbors(receiver.get_request_id(), my_callback))
                elif command == hc.GET_LSP:
                    print(heaven_client.get_lsp(receiver.get_request_id(), my_callback))
                elif command == hc.GET_ROUTING_TABLE:
                    print(heaven_client.get_routing_table(receiver.get_request_id(), my_callback))
                elif command == hc.GET_ROUTING_TREES:
                    print(heaven_client.get_routing_trees(receiver.get_request_id(), my_callback))
                elif command == hc.GET_FULL_NETWORK:
                    print(heaven_client.get_full_network(receiver.get_request_id(), my_callback))

                # ------------------------- TCP SERVER ------------------------- #

                elif command == hc.CREATE_TCP_SERVER:
                    print(heaven_client.create_tcp_server(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.STOP_TCP_SERVER:
                    print(heaven_client.stop_tcp_server(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.GET_TCP_SERVERS:
                    print(heaven_client.get_tcp_servers(receiver.get_request_id(), my_callback))

                # ---------------------- TCP SERVER SOCKET --------------------- #

                elif command == hc.CREATE_TCP_SERVER_SOCKET:
                    print(heaven_client.create_tcp_server_socket(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.STOP_TCP_SERVER_SOCKET:
                    print(heaven_client.stop_tcp_server_socket(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.GET_TCP_SERVERS_SOCKET:
                    print(heaven_client.get_tcp_servers_socket(receiver.get_request_id(), my_callback))

                # ------------------------- TCP CLIENT ------------------------- #

                elif command == hc.CREATE_TCP_CLIENT:
                    print(heaven_client.create_tcp_client(receiver.get_request_id(), my_callback, params[0], params[1], params[2]))
                elif command == hc.SEND_TCP:
                    print(heaven_client.send_tcp(receiver.get_request_id(), my_callback, params[0], params[1]))
                elif command == hc.SEND_TCP_FILE:
                    print(heaven_client.send_tcp_file(receiver.get_request_id(), my_callback,
                                                      params[0], 0, params[1]))
                elif command == 'image':
                    image = '{}{}'.format(image_folder, params[1])
                    print(heaven_client.send_tcp_file(receiver.get_request_id(), my_callback,
                                                      params[0], 0, image))
                elif command == hc.STOP_TCP_CLIENT:
                    print(heaven_client.stop_tcp_client(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.GET_TCP_CLIENTS:
                    print(heaven_client.get_tcp_clients(receiver.get_request_id(), my_callback))

                # ---------------------- TCP CLIENT SOCKET---------------------- #

                elif command == hc.SEND_TCP_SOCKET:
                    print(heaven_client.send_tcp_socket(receiver.get_request_id(), my_callback, params[0], params[1], params[2]))
                elif command == hc.SEND_TCP_FILE_SOCKET:
                    print(heaven_client.send_tcp_file_socket(
                        receiver.get_request_id(), my_callback, params[0], params[1], 0, params[2]))
                elif command == 'image_socket':
                    image = '{}{}'.format(image_folder, params[-1])
                    print(heaven_client.send_tcp_file_socket(
                        receiver.get_request_id(), my_callback, params[0], params[1], 0, image))

                # ------------------------- UDP SERVER ------------------------- #

                elif command == hc.CREATE_UDP_SERVER:
                    print(heaven_client.create_udp_server(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.STOP_UDP_SERVER:
                    print(heaven_client.stop_udp_server(receiver.get_request_id(), my_callback, params[0]))

                elif command == hc.GET_UDP_SERVERS:
                    print(heaven_client.get_udp_servers(receiver.get_request_id(), my_callback))

                # ---------------------- UDP SERVER SOCKET --------------------- #

                elif command == hc.CREATE_UDP_SERVER_SOCKET:
                    print(heaven_client.create_udp_server_socket(
                        receiver.get_request_id(), my_callback, params[0], params[1], params[2]))
                elif command == hc.STOP_UDP_SERVER_SOCKET:
                    print(heaven_client.stop_udp_server_socket(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.GET_UDP_SERVERS_SOCKET:
                    print(heaven_client.get_udp_servers_socket(receiver.get_request_id(), my_callback))

                # ------------------------- UDP CLIENT ------------------------- #

                elif command == hc.CREATE_UDP_CLIENT:
                    print(heaven_client.create_udp_client(receiver.get_request_id(), my_callback, params[0], params[1],
                                                          params[2], params[3], params[4], params[5]))
                elif command == hc.SEND_UDP:
                    print(heaven_client.send_udp(receiver.get_request_id(), my_callback, params[0], params[1]))
                elif command == hc.STOP_UDP_CLIENT:
                    print(heaven_client.stop_udp_client(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.GET_UDP_CLIENTS:
                    print(heaven_client.get_udp_clients(receiver.get_request_id(), my_callback))

                # ---------------------- UDP CLIENT SOCKET---------------------- #

                elif command == hc.CREATE_UDP_CLIENT_SOCKET:
                    print(heaven_client.create_udp_client_socket(
                        receiver.get_request_id(), my_callback, params[0], params[1], params[2]))
                elif command == hc.SEND_UDP_SOCKET:
                    print(heaven_client.send_udp_socket(receiver.get_request_id(), my_callback, params[0], params[1]))
                elif command == hc.STOP_UDP_CLIENT_SOCKET:
                    print(heaven_client.stop_udp_client_socket(receiver.get_request_id(), my_callback, params[0]))
                elif command == hc.GET_UDP_CLIENTS_SOCKET:
                    print(heaven_client.get_udp_clients_socket(receiver.get_request_id(), my_callback))
                else:
                    print('unknown command')
            except IndexError as e:
                print('Wrong number of parameters, ', e)

    except (KeyboardInterrupt, SystemExit, SystemError):
        print('Forced shutdown')

    print('Closing heaven client')
    receiver.stop()
    heaven_client.stop()
