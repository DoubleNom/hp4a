# heaven_drones

This project contains the source files related to the python part of the HEAVEN network middleware for Unix-based system.

Requirements:
- python2.7
- python-ipaddress

Requirement when running on Linux systems:
- iproute2 (command "ip")
- net-tools (command "ifconfig")
- wireless-tools (commands "iwpriv","iw", "iwconfig", "iwlist")
- iptables

See the Project Wiki for further information.