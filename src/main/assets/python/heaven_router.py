"""
This script launch heaven from the layer3, so layer4 and api are not used.
"""

import os
import time
from heaven_drones.common.config_reader import ConfigReader
from heaven_drones.layer3.routing_wrapper import RoutingWrapper
from heaven_drones.common.profiler import Profiler


def dummy_send_to_upper_layer(packet):
    pass


if __name__ == '__main__':

    heaven_config_path = os.path.dirname(os.path.abspath(__file__)) + '/heaven_config.ini'

    try:
        config_reader = ConfigReader(heaven_config_path)
    except IOError:
        print('Configuration file not found, cannot start Heaven')
        exit(0)

    profiler = Profiler('0000', False)

    rw = RoutingWrapper(dummy_send_to_upper_layer, config_reader, profiler)
    rw.start_routing_layers()

    print('HeavenRouter is running')
    try:
        while 1:
            time.sleep(10)
    except (KeyboardInterrupt, SystemExit, SystemError) as e:
        print('\nStopping HeavenRouter because of KeyboardInterrupt')
        rw.stop()

    print('HeavenRouter stopped')
