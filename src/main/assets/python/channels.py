class WirelessChannels:

    """
    iw list -->

    * 2412 MHz [1] (22.0 dBm)
    * 2417 MHz [2] (22.0 dBm)
    * 2422 MHz [3] (22.0 dBm)
    * 2427 MHz [4] (22.0 dBm)
    * 2432 MHz [5] (22.0 dBm)
    * 2437 MHz [6] (22.0 dBm)
    * 2442 MHz [7] (22.0 dBm)
    * 2447 MHz [8] (22.0 dBm)
    * 2452 MHz [9] (22.0 dBm)
    * 2457 MHz [10] (22.0 dBm)
    * 2462 MHz [11] (22.0 dBm)
    * 2467 MHz [12] (22.0 dBm)
    * 2472 MHz [13] (22.0 dBm)
    * 2484 MHz [14] (disabled)

    * 5180 MHz [36] (22.0 dBm) (no IR)
    * 5200 MHz [40] (22.0 dBm) (no IR)
    * 5220 MHz [44] (22.0 dBm) (no IR)
    * 5240 MHz [48] (22.0 dBm) (no IR)
    * 5260 MHz [52] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5280 MHz [56] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5300 MHz [60] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5320 MHz [64] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5500 MHz [100] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5520 MHz [104] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5540 MHz [108] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5560 MHz [112] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5580 MHz [116] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5600 MHz [120] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5620 MHz [124] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5640 MHz [128] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5660 MHz [132] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5680 MHz [136] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5700 MHz [140] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5720 MHz [144] (22.0 dBm) (no IR, radar detection)
      DFS state: usable (for 14302 sec)
      DFS CAC time: 60000 ms
    * 5745 MHz [149] (22.0 dBm) (no IR)
    * 5765 MHz [153] (22.0 dBm) (no IR)
    * 5785 MHz [157] (22.0 dBm) (no IR)
    * 5805 MHz [161] (22.0 dBm) (no IR)
    * 5825 MHz [165] (22.0 dBm) (no IR)
    """

    channel_to_frequency_mapping = {
        1: 2.412,
        2: 2.417,
        3: 2.422,
        4: 2.427,
        5: 2.432,
        6: 2.437,
        7: 2.442,
        8: 2.447,
        9: 2.452,
        10: 2.457,
        11: 2.462,
        12: 2.467,
        64: 5.320,
        100: 5.500
    }
