# Copyright 2001-2017 by Vinay Sajip. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Vinay Sajip
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# VINAY SAJIP DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# VINAY SAJIP BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

"""
Logging package for Python. Based on PEP 282 and comments thereto in
comp.lang.python.

Copyright (C) 2001-2017 Vinay Sajip. All Rights Reserved.

To use, simply 'import logging' and log away!
"""


class Logger(Filterer):
    """
    Instances of the Logger class represent a single logging channel. A
    "logging channel" indicates an area of an application. Exactly how an
    "area" is defined is up to the application developer. Since an
    application can have any number of areas, logging channels are identified
    by a unique string. Application areas can be nested (e.g. an area
    of "input processing" might include sub-areas "read CSV files", "read
    XLS files" and "read Gnumeric files"). To cater for this natural nesting,
    channel names are organized into a namespace hierarchy where levels are
    separated by periods, much like the Java or Python package namespace. So
    in the instance given above, channel names might be "input" for the upper
    level, and "input.csv", "input.xls" and "input.gnu" for the sub-levels.
    There is no arbitrary limit to the depth of nesting.
    """
    def __init__(self, name, level=NOTSET):

    def setLevel(self, level):

    def debug(self, msg, *args, **kwargs):

    def info(self, msg, *args, **kwargs):

    def warning(self, msg, *args, **kwargs):

    def warn(self, msg, *args, **kwargs):

    def error(self, msg, *args, **kwargs):

    def exception(self, msg, *args, exc_info=True, **kwargs):

    def critical(self, msg, *args, **kwargs):

    def log(self, level, msg, *args, **kwargs):

    def findCaller(self, stack_info=False, stacklevel=1):

    def makeRecord(self, name, level, fn, lno, msg, args, exc_info,
                   func=None, extra=None, sinfo=None):

    def _log(self, level, msg, args, exc_info=None, extra=None, stack_info=False,
             stacklevel=1):

    def handle(self, record):

    def addHandler(self, hdlr):

    def removeHandler(self, hdlr):

    def hasHandlers(self):

    def callHandlers(self, record):

    def getEffectiveLevel(self):

    def isEnabledFor(self, level):

    def getChild(self, suffix):

    def __repr__(self):

    def __reduce__(self):


def getLogger(name=None):
    return Logger()

def critical(msg, *args, **kwargs):

def error(msg, *args, **kwargs):

def exception(msg, *args, exc_info=True, **kwargs):

def warning(msg, *args, **kwargs):

def warn(msg, *args, **kwargs):

def info(msg, *args, **kwargs):

def debug(msg, *args, **kwargs):

def log(level, msg, *args, **kwargs):

def disable(level=CRITICAL):

def shutdown(handlerList=_handlerList):
