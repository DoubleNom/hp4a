import socket
import sys
import os
import time
from threading import Thread

server_address = '/data/user/0/a.humanitas.heaven_java/files/uds_socket'


def server():
    # Make sure the socket does not already exist
    try:
        os.unlink(server_address)
    except OSError:
        print('Server: error occurred')
        if os.path.exists(server_address):
            raise

    # Create a UDS socket
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    print('Server: binding to {}'.format(server_address))
    sock.bind(server_address)

    while 1:
        print('Server: listening...')
        data = sock.recv(4096)
        print('Server: received {}'.format(data))
        if not data:
            print('Server: the client is gone, exiting')
            break
    print('Server: closing socket')
    sock.close()


def client():
    # Create a UDS socket
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    print('Client: connecting to {}'.format(server_address))
    try:
        sock.connect(server_address)
    except socket.error, msg:
        print >> sys.stderr, msg
        sys.exit(1)

    for i in range(30):
        try:
            # Send data
            message = str(i)
            print('Client: sending {}'.format(message))
            sock.sendall(message)
            time.sleep(0.3)
        except:
            print('Client: error occurred')
    print('Client: closing socket')
    # sock.sendall('')  # signal this to the server
    sock.close()


if __name__ == '__main__':
    server_thread = Thread(target=server)
    client_thread = Thread(target=client)
    server_thread.start()
    time.sleep(2)
    client_thread.start()
    client_thread.join()
    server_thread.join()
