#!/usr/bin/env python
import time
import os
from datetime import datetime
from heaven_drones.network_tools import NetworkTools
from heaven_drones.config_reader import ConfigReader

'''
To launch this script at system boot, add:
------------------------
python [this file] &
------------------------
e.g. python /home/ubuntu/humanitas/heaven_drones/heave_drones/network_watchdog.py &

to /etc/rc.local, before the exit command
'''


def get_ts():
    return str(datetime.now().strftime('%H:%M:%S.%f')[:-2])


def log(message, nt, log_file):
    """
    Append a message to the log file. If it does not exist, it is created
    :param message:
    :return:
    """
    nt.run_os('echo {} {} >> {}'.format(get_ts(), message, log_file))


def configure_ad_hoc(config_reader, nt, log_file):

    config = config_reader.get_config_dict('ad_hoc_network')
    for tries in range(3):
        log('Ad-hoc try #{}...'.format(tries), nt, log_file)
        nt.configure_ad_hoc(
            config['interface'], int(config['channel']), config['essid'],
            config['ip_address'], config['network_address'])

        result = nt.check_ad_hoc_connection(config['interface'], config['essid'], int(config['channel']))

        if result:
            log('Ad-hoc successfully activated!', nt, log_file)
            return True
        else:
            time.sleep(2)
            log('Ad-hoc setup failed!', nt, log_file)

    return False


if __name__ == '__main__':
    time.sleep(30)  # grace time to ssh into the system and disable the watchdog
    this_folder = os.path.dirname(os.path.abspath(__file__))
    config_reader = ConfigReader('{}/heaven_config.ini'.format(this_folder))
    nt = NetworkTools(config_reader)
    log_file = '{}/watchdog_log.txt'.format(this_folder)
    log('Device booted', nt, log_file)
    networks = config_reader.get_config_dict('main')['networks'].split(',')

    # ------------------------------------ AD-HOC INTERFACE CONFIGURATION ---------------------------------------- #

    if 'ad_hoc_network' in networks:
        log('Starting configuration of ad-hoc interface', nt, log_file)
        result = configure_ad_hoc(config_reader, nt, log_file)
        if not result:
            nt.run_os('reboot')

        log('Finished configuration of ad-hoc interface', nt, log_file)

    time.sleep(5)

    # ------------------------------------ APPS -------------------------------------------------------------------- #
    log('Checking if apps need to start...', nt, log_file)
    apps = config_reader.get_config_dict('main')['apps'].split(',')

    if not apps:
        exit(0)

    if 'heaven' in apps:
        log('Starting Heaven...', nt, log_file)
        nt.run_bg('python {}/heaven.py'.format(this_folder))
        time.sleep(30)

    humanitas_root = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), os.pardir))
    for app in apps:

        if app == 'heaven':
            continue

        nt.run('python {}/{}.py &'.format(humanitas_root, app))
        log('Starting app {}'.format(app), nt, log_file)
        time.sleep(5)
    #
    # # ------------------------------------ WATCHDOG -------------------------------------------------------------- #
    # check_interval = 30
    # log('Checking connectivity every {} seconds'.format(check_interval), nt)
    # time.sleep(check_interval)
    #
    # while True:
    #     start_time = time.time()
    #     if 'ad_hoc_network' in networks:
    #         log('Checking ad-hoc connectivity...', nt)
    #         config = config_reader.get_config_dict('ad_hoc_network')
    #         result = nt.check_ad_hoc_connection(config['interface'], config['essid'], int(config['channel']))
    #         if not result:
    #             log('Ad-hoc lost connectivity, rebooting', nt)
    #             nt.run_os('reboot')
    #         log('...ad-hoc is still up!', nt)
    #
    #     if 'hotspot_network' in networks:
    #         log('Checking hotspot connectivity...', nt)
    #         result = nt.check_hotspot()
    #         if not result:
    #             log('Hotspot lost connectivity, rebooting', nt)
    #             nt.run_os('reboot')
    #         log('...hotspot is still up!', nt)
    #
    #     time.sleep(max(1.0, check_interval - (time.time() - start_time)))
