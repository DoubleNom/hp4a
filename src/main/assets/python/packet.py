class Packet:
    """
    RAW
    ----------------------------------------------------------------------------------------------------
    |      l3_header     |     l4_header   | tcp/udp_header | tcp_syn_header |      payload            |
    ----------------------------------------------------------------------------------------------------
    0                    l3_index          l4_index      tcp_index       tcp_syn_index
    l2_index                                             udp_index
    """

    def __init__(self, raw='', previous_hop=None, do_stop_queue=False):
        self.raw = raw  # string as read from the network socket
        self.previous_hop = previous_hop  # heaven address of the node that relayed the packet
        self.do_stop_queue = do_stop_queue
        self.payload_index = 0
        self.headers = []  # used in in downstream, list of STRINGS

        self.l3_header = None
        self.l4_header = None
        self.tcp_header = None
        self.tcp_syn_header = None

        self.hop_counter = None
        self.routing_protocol = None
        self.source = None

        self.destination = None
        self.routing_method = None

        self.udp_header = None

        # self.upstream_timestamps = {}
        # self.downstream_timestamps = {}

    def is_stop_packet(self):
        return self.do_stop_queue

    def add_header_str(self, header_str):
        self.headers.insert(0, header_str)

    # def add_timestamp(self, layer, direction):

    def modify_header_str(self, header_str, insertion_order):
        self.headers[-insertion_order-1] = header_str

    def packet_to_str(self):
        return ''.join(self.headers) + self.raw[self.payload_index:]

    def __str__(self):
        packet_str = '<{} = '.format(self.__class__)
        try:
            for field in self.__dict__.keys():
                value = str(self.__dict__[field])
                packet_str += '{}:{}, '.format(field, value)
        except RuntimeError:
            return ''
        return packet_str[:-2] + '>'

    def __copy__(self):
        new_packet = Packet(raw=self.raw)
        new_packet.payload_index = self.payload_index
        new_packet.headers = self.headers[:]
        return new_packet



