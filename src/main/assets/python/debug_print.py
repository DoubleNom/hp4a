from datetime import datetime

DO_PRINT_LOG = True
DO_PRINT_ERRORS = True


def print_log(*arg):
    if not DO_PRINT_LOG:
        return
    my_str = ''.join([str(i) for i in arg])
    print('Heaven Log - {} - {}'.format(datetime.now().strftime('%H:%M:%S.%f')[:-2], my_str))


def print_err(*arg):
    if not DO_PRINT_ERRORS:
        return
    my_str = ''.join([str(i) for i in arg])
    print('Heaven Err - {} - {}'.format(datetime.now().strftime('%H:%M:%S.%f')[:-2], my_str))