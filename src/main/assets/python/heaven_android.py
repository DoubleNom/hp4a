import sys
print(sys.version)

import os
import time
import logging
import ConfigParser
import threading
from socket import error as socket_error
from heaven_modules.common.config_reader import ConfigReader
from heaven_modules.api.heaven_api_server import HeavenAPIServer
from heaven_modules.common.utilities import get_socket_from_strings

if __name__ == '__main__':

    # read heaven configuration file
    # os.path.dirname(os.path.abspath(__file__)) equals to the folder of this file
    heaven_folder = os.path.dirname(os.path.abspath(__file__))

    # ------------------------------- Check config file -------------------------------- #
    heaven_config_path = '{}/heaven_config.ini'.format(heaven_folder)
    if not os.path.isfile(heaven_config_path):
        exit('Configuration file {} not found, cannot start Heaven'.format(heaven_config_path))

    try:
        config_reader = ConfigReader(heaven_config_path)
    except IOError:
        exit('Configuration file {} not valid, cannot start Heaven'.format(heaven_config_path))

    # ------------------------------- Check api sockets -------------------------------- #
    try:
        heaven_address = config_reader.get('main', 'heaven_address')
        socket_type_str = config_reader.get('api_server', 'api_server_socket_type')
        socket_family_str = config_reader.get('api_server', 'api_server_socket_family')
        socket_address_str = config_reader.get('api_server', 'api_server_socket_address')  # ip:port
    except (ConfigParser.NoOptionError, ConfigParser.NoSectionError) as e:
        exit('Configuration file incomplete, cannot start Heaven: {}'.format(e))

    socket_type, socket_family, socket_address, error_str = get_socket_from_strings(
        socket_type_str, socket_family_str, socket_address_str)

    if error_str:
        exit(error_str)

    # ------------------------------- Check heaven address -------------------------------- #
    if len(heaven_address) != 4:
        exit('{} is not a valid heaven address. Heaven Address must be a unique 4 char string'.format(heaven_address))

    # ------------------------------- Start heaven ---------------------------------------- #
    try:
        heaven_server = HeavenAPIServer(socket_address, config_reader, 0, socket_family, socket_type)
    except (ValueError, KeyError) as e:
        exit('Error in configuration file in section api_server: {}'.format(e))
    except socket_error as e:
        exit('Error in creating the API Server, is another instance of Heaven running? {}'.format(e))
    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError) as e:
        exit('Error in configuration file, missing sections or options: {}'.format(e))

    # made with http://www.network-science.de/ascii/ , font: slant
    print('''
            __  ___________ _    _________   __
           / / / / ____/   | |  / / ____/ | / /
          / /_/ / __/ / /| | | / / __/ /  |/ /
         / __  / /___/ ___ | |/ / /___/ /|  /
        /_/ /_/_____/_/  |_|___/_____/_/ |_/

Heterogeneous Embedded Ad-hoc Virtual Emergency Network
              Humanitas Solutions Inc

    ''')
    try:
        while not heaven_server.is_stopping:
            time.sleep(10)
            print(threading.enumerate())
    except (KeyboardInterrupt, SystemExit, SystemError):
        heaven_server.stop()
        heaven_server.join()
        print(threading.enumerate())

